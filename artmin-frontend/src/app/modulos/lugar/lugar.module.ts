import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LugarRoutingModule } from './lugar-routing.module';
import { RutaLugaresComponent } from './rutas/ruta-lugares/ruta-lugares.component';
import {TableModule, TreeTableModule} from 'primeng';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {ModalCrearEditarLugarModule} from './modales/modal-crear-editar-lugar/modal-crear-editar-lugar.module';


@NgModule({
  declarations: [RutaLugaresComponent],
  imports: [
    CommonModule,
    LugarRoutingModule
  ],
  exports: [
    RutaLugaresComponent,
    TreeTableModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    ModalCrearEditarLugarModule
  ]
})
export class LugarModule { }
