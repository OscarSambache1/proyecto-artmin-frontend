import {Component, OnInit} from '@angular/core';
import {LugarRestService} from '../../../../servicios/rest/lugar-rest.service';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {ToasterService} from 'angular2-toaster';
import {TreeNode} from 'primeng';
import {generarArrayNodos} from '../../../../funciones/funciones-tree-table/generar-array-nodos';
import {CargandoService} from '../../../../servicios/cargando-service';
import {LugarInterface} from '../../../../interfaces/lugar.interface';
import {llenarArbolConRegistro} from '../../../../funciones/funciones-tree-table/llenar-arbol-con-registros';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {ActivatedRoute, Router} from '@angular/router';
import {RUTAS_LUGARES} from '../definicion-rutas/rutas-lugares';
import {escucharBusquedaArbol} from '../../../../funciones/funciones-tree-table/escuchar-busqueda-arbol';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {ModalCrearEditarLugarComponent} from '../../modales/modal-crear-editar-lugar/modal-crear-editar-lugar.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-ruta-lugares',
  templateUrl: './ruta-lugares.component.html',
  styleUrls: ['./ruta-lugares.component.css']
})
export class RutaLugaresComponent implements OnInit {

  busqueda = '';
  tipo = '';
  lugaresTreeTable: TreeNode[] = [];
  columnas = [
    {
      field: 'nombre',
      header: 'Nombre',
    },
    {
      field: 'tipo',
      header: 'Tipo',
    },
    {
      field: 'acciones',
      header: 'Acciones',
    },
  ];
  rutaImagen = 'assets/imagenes/lugar.svg';
  queryParams: QueryParamsInterface = {};
  ruta = [];
  arregloRutas: any[];

  constructor(
    private readonly _lugarRestService: LugarRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    public dialog: MatDialog,

  ) {
  }

  ngOnInit(): void {
    this._activatedRoute
      .queryParams
      .subscribe(
        queryParams => {
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.tipo) {
              this.tipo = this.queryParams.consulta.tipo;
            }
          }
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          let consultaLugaresSubcriber;
          if ((this.busqueda && this.busqueda !== '') || (this.tipo && this.tipo !== '')) {
            const datosConsulta = {
              nombre: this.busqueda,
              tipo: this.tipo
            };
            consultaLugaresSubcriber = this._lugarRestService.obtenerLugares(datosConsulta);
          } else {
            const consulta = {
              where: {
                tipo: 'global'
              },
              relations: [
                'imagenesLugar'
              ]
            };
            consultaLugaresSubcriber = this._lugarRestService.findAll(JSON.stringify(consulta));
          }
          consultaLugaresSubcriber
            .subscribe(
              respuestaLugar => {
                this.lugaresTreeTable = generarArrayNodos(respuestaLugar[0]);
                if ((this.busqueda && this.busqueda !== '') || (this.tipo && this.tipo !== '')) {
                  this.lugaresTreeTable
                    .forEach(lugar => {
                        llenarArbolConRegistro(lugar, 'lugaresHijos');
                      }
                    );
                  escucharBusquedaArbol(
                    this.lugaresTreeTable,
                    this.busqueda,
                    ['nombre'],
                  );
                }
                this.lugaresTreeTable = [...this.lugaresTreeTable];
              }
              , error => {
                this._toasterService.pop(ToastErrorCargandoDatos);
                console.error(error);
              }
            );
        },
      );
  }

  expandirNodo(eventoNodo) {
    const nodoPadre = eventoNodo.node;
    nodoPadre.children = [];
    const consulta = {
      where: {
        lugarPadre: nodoPadre.data.id,
      },
      order: {
        id: 'DESC',
      },
      relations: [
        'imagenesLugar'
      ]
    };
    this._cargandoService.habilitarCargando();
    this._lugarRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe(
        (respuesta: [LugarInterface[], number]) => {
          this._cargandoService.deshabiltarCargando();
          nodoPadre.data.lugaresHijos = respuesta[0];
          llenarArbolConRegistro(nodoPadre, 'lugaresHijos');
          this.lugaresTreeTable = [...this.lugaresTreeTable];
        },
        error => {
          console.error(
            {
              error,
              mensaje: 'Error al buscar lugares',
              data: consulta
            }
          );
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        },
      );
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarCharts();
  }

  buscarCharts() {
    const consulta = {
      busqueda: this.busqueda,
      tipo: this.tipo
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    this.arregloRutas = [
      RUTAS_LUGARES.rutaGestionLugars(false, true)
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_LUGARES
      .rutaGestionLugars(
        false,
        true,
        [])
      .ruta;
  }

  abrirModalCrearEditarLugar(
    nodoLugar,
    lugar?: LugarInterface
  ) {
    let imagenObligatoria: boolean;
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarLugarComponent,
        {
          width: '800px',
          data: {
            lugar,
            tipo: lugar ? lugar.tipo : nodoLugar?.node?.data?.tipo,
            imagenObligatoria,
            idLugarPadre: lugar ? lugar.lugarPadre : nodoLugar?.node?.data?.id,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (lugarCreadoEditado: LugarInterface) => {
          if (lugarCreadoEditado) {
            if (lugar) {
              nodoLugar.node.data = lugarCreadoEditado;
            }  else {
              nodoLugar.node.children.push({
                data: lugarCreadoEditado
              });
            }
            this.lugaresTreeTable = [...this.lugaresTreeTable];
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  obtenerUrlPrincipal(
    objeto: any,
    campo: string) {
    if (objeto[campo] && objeto[campo].length) {
      return obtenerUrlImagenPrincipal(objeto, campo);
    }
  }
}
