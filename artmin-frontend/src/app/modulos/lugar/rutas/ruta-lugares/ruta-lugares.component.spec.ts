import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaLugaresComponent } from './ruta-lugares.component';

describe('RutaLugaresComponent', () => {
  let component: RutaLugaresComponent;
  let fixture: ComponentFixture<RutaLugaresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaLugaresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaLugaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
