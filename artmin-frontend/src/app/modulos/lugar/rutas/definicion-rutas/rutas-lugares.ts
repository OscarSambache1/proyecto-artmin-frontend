import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';

export const RUTAS_LUGARES = {
  _rutaInicioLugar: {
    ruta: 'lugar-modulo',
    nombre: 'Modulo lugar',
    generarRuta: (...argumentos) => {
      return `lugar-modulo`;
    }
  },

  _rutaGestionLugars: {
    ruta: 'gestion-lugars',
    nombre: 'Gestión de lugares',
    generarRuta: (...argumentos) => {
      return `gestion-lugares`;
    }
  },

  rutaGestionLugars: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    rutaArreglo = [
      this._rutaInicioLugar,
      this._rutaGestionLugars
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
