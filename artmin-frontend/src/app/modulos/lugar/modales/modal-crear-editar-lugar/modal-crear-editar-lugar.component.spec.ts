import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarLugarComponent } from './modal-crear-editar-lugar.component';

describe('ModalCrearEditarLugarComponent', () => {
  let component: ModalCrearEditarLugarComponent;
  let fixture: ComponentFixture<ModalCrearEditarLugarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarLugarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarLugarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
