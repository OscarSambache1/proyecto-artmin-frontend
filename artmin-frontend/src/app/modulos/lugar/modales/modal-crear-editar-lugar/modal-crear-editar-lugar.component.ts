import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {LugarRestService} from '../../../../servicios/rest/lugar-rest.service';
import {LugarInterface} from '../../../../interfaces/lugar.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {ToastErrorCargandoDatos, toastExitoCrear, toastExitoEditar} from '../../../../constantes/mensajes-toaster';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';

@Component({
  selector: 'app-modal-crear-editar-lugar',
  templateUrl: './modal-crear-editar-lugar.component.html',
  styleUrls: ['./modal-crear-editar-lugar.component.css']
})
export class ModalCrearEditarLugarComponent implements OnInit {
  formularioValido: boolean;
  lugarCrearEditar: LugarInterface;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      lugar: LugarInterface,
      tipo: string,
      idLugarPadre: number,
      imagenObligatoria: boolean
    },
    public dialogo: MatDialogRef<ModalCrearEditarLugarComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _lugarRestService: LugarRestService,
  ) { }

  ngOnInit(): void {
    if (this.data.lugar.tipo === 'global' || this.data.lugar.tipo === 'pais') {
      this.data.imagenObligatoria = true;
    } else {
      this.data.imagenObligatoria = false;
    }
  }

  validarFormularioLugar(lugar: LugarInterface | boolean) {
    if (lugar) {
      this.formularioValido = true;
      this.lugarCrearEditar = lugar as LugarInterface;
    } else {
      this.formularioValido = false;
    }
  }

  cerrarModal() {
  }

  crearEditarLugar() {
    this._cargandoService.habilitarCargando();
    if (!this.data.lugar) {
      if (this.data.tipo === 'continente') {
        this.lugarCrearEditar.tipo = 'pais';
      }
      if (this.data.tipo === 'pais') {
        this.lugarCrearEditar.tipo = 'ciudad';
      }
      this.lugarCrearEditar.lugarPadre = this.data.idLugarPadre;
      this._lugarRestService
        .crearLugarImagen(
          this.lugarCrearEditar,
          this.lugarCrearEditar.imagen,
        )
        .subscribe((respuestaLugarCreado: LugarInterface) => {
          this.dialogo.close(respuestaLugarCreado);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    } else {
      this.lugarCrearEditar.id = this.data.lugar.id;
      this._lugarRestService
        .crearLugarImagen(
          this.lugarCrearEditar,
          this.lugarCrearEditar.imagen,
        )
        .subscribe((respuestaLugarCreado: LugarInterface) => {
            this.dialogo.close(respuestaLugarCreado);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }

  }
}
