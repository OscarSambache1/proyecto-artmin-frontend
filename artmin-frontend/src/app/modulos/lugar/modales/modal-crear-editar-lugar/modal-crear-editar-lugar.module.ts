import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarLugarComponent } from './modal-crear-editar-lugar.component';
import {FormularioLugarModule} from '../../componentes/formulario-lugar/formulario-lugar.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarLugarComponent],
  imports: [
    CommonModule,
    FormularioLugarModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule
  ]
})
export class ModalCrearEditarLugarModule { }
