import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaLugaresComponent} from './rutas/ruta-lugares/ruta-lugares.component';


const routes: Routes = [
  {
    path: 'gestion-lugares',
    children: [
      {
        path: '',
        component: RutaLugaresComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: 'gestion-lugares',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LugarRoutingModule { }
