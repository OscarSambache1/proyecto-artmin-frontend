import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {LugarInterface} from '../../../../interfaces/lugar.interface';
import {InputImagenComponent} from '../../../../componentes/input-imagen/input-imagen.component';
import {ToasterService} from 'angular2-toaster';
import {MatDialog} from '@angular/material/dialog';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {PlataformaInterface} from '../../../../interfaces/plataforma.interface';
import {
  MENSAJES_VALIDACION_NOMBRE_LUGAR, MENSAJES_VALIDACION_TIPO_LUGAR,
  VALIDACION_IMAGEN_LUGAR,
  VALIDACION_NOMBRE_LUGAR,
  VALIDACION_TIPO_LUGAR
} from '../../../../constantes/validaciones-formulario/lugar';

@Component({
  selector: 'app-formulario-lugar',
  templateUrl: './formulario-lugar.component.html',
  styleUrls: ['./formulario-lugar.component.css']
})
export class FormularioLugarComponent implements OnInit {

  formularioLugar: FormGroup;
  mensajesError = {
    nombre: [],
    imagen: []
  };
  subscribers = [];
  imagenSeleccionada;

  @Input()
  width: number;

  @Input()
  lugar: LugarInterface;

  @Input()
  imagenObligatoria: boolean;

  @Output()
  lugarValidoEnviar: EventEmitter<LugarInterface | boolean> = new EventEmitter();

  @ViewChild(InputImagenComponent)
  componenteInputImagen: InputImagenComponent;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioLugar = this._formBuilder.group({
      nombre: [this.lugar ? this.lugar.nombre : '', VALIDACION_NOMBRE_LUGAR],
      imagen: [this.lugar ? this.cargarImagenLugar() : '', this.imagenObligatoria ? VALIDACION_IMAGEN_LUGAR : []],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_LUGAR);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioLugar.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioLugar;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const lugarValido = formularioFormGroup.valid;
          if (lugarValido) {
            this.lugarValidoEnviar.emit(formulario);
          } else {
            this.lugarValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharEventoImagen(event: any) {
    this.imagenSeleccionada = event;
    this.formularioLugar.patchValue(
      {
        imagen: this.imagenSeleccionada,
      }
    );
  }

  cargarImagenLugar() {
    if (this.lugar && this.lugar.imagenesLugar?.length) {
      return obtenerUrlImagenPrincipal(this.lugar, 'imagenesLugar');
    }
  }

  obtenerUrlPrincipal(lugar: LugarInterface) {
    if (lugar.imagenesLugar && lugar.imagenesLugar.length) {
      return obtenerUrlImagenPrincipal(lugar, 'imagenesLugar');
    }
  }
}
