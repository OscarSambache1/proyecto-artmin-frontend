import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioLugarComponent } from './formulario-lugar.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {InputTextModule} from 'primeng';



@NgModule({
  declarations: [FormularioLugarComponent],
  exports: [
    FormularioLugarComponent,
  ],
  imports: [
    CommonModule,
    InputImagenModule,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule,
    InputTextModule,
  ]
})
export class FormularioLugarModule { }
