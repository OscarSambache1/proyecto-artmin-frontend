import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioCertificadoChartComponent } from './formulario-certificado-chart.component';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteVideoModule} from '../../../../componentes/autocomplete-video/autocomplete-video.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TablaCertificadosChartFechaModule} from '../tabla-certificados-chart-fecha/tabla-certificados-chart-fecha.module';



@NgModule({
  declarations: [FormularioCertificadoChartComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule,
    AutocompleteCancionModule,
    AutocompleteAlbumsModule,
    AutocompleteVideoModule,
    TablaCertificadosChartFechaModule
  ],
  exports: [
    FormularioCertificadoChartComponent
  ]
})
export class FormularioCertificadoChartModule { }
