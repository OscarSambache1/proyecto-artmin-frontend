import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCertificadoChartComponent } from './formulario-certificado-chart.component';

describe('FormularioCertificadoChartComponent', () => {
  let component: FormularioCertificadoChartComponent;
  let fixture: ComponentFixture<FormularioCertificadoChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioCertificadoChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCertificadoChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
