import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {CertificadoChartInterface} from '../../../../interfaces/certificado-chart.interface';
import {
  MENSAJES_VALIDACION_ALBUM_CERTIFICADOS_CHART,
  MENSAJES_VALIDACION_CANCION_CERTIFICADOS_CHART,
  MENSAJES_VALIDACION_CERTIFICADOS_CHART_FECHA, MENSAJES_VALIDACION_VIDEO_CERTIFICADOS_CHART
} from '../../../../constantes/validaciones-formulario/certificado-chart';
import {CertificadoChartFechaInterface} from '../../../../interfaces/certificado-chart-fecha.interface';

@Component({
  selector: 'app-formulario-certificado-chart',
  templateUrl: './formulario-certificado-chart.component.html',
  styleUrls: ['./formulario-certificado-chart.component.css']
})
export class FormularioCertificadoChartComponent implements OnInit {

  formularioCertificadoChart: FormGroup;
  mensajesError = {
    cancion: [],
    album: [],
    video: [],
    certificadosChartFecha: [],
  };
  subscribers = [];

  @Output()
    certificadoChartValidoEnviar: EventEmitter<CertificadoChartInterface | boolean> = new EventEmitter();

  @Input()
  certificadoChart: CertificadoChartInterface;

  @Input()
  tipo: string;

  @Input()
  idsCancionesAlbumesVideos = [];

  placeHolderTipo = '';

  @Input()
  idArtista: number;

  @Input()
  idChart: number;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.setearPlaceholderTipo();
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioCertificadoChart = this._formBuilder.group({
      certificadosChartFecha: [this.certificadoChart ? this.certificadoChart.certificadosChartFecha : null, [Validators.required]],
      cancion: [this.certificadoChart ? this.certificadoChart.cancion : null, this.tipo === 'cancion' ? [Validators.required] : []],
      album: [this.certificadoChart ? this.certificadoChart.album : null, this.tipo === 'album' ? [Validators.required] : []],
      video: [this.certificadoChart ? this.certificadoChart.video : null, this.tipo === 'video' ? [Validators.required] : []],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('certificadosChartFecha', MENSAJES_VALIDACION_CERTIFICADOS_CHART_FECHA);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCION_CERTIFICADOS_CHART);
    this.verificarCampoFormControl('album', MENSAJES_VALIDACION_ALBUM_CERTIFICADOS_CHART);
    this.verificarCampoFormControl('video', MENSAJES_VALIDACION_VIDEO_CERTIFICADOS_CHART);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioCertificadoChart.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioCertificadoChart;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.certificadoChartValidoEnviar.emit(formulario);
          } else {
            this.certificadoChartValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }


  setearCancion(cancionSeleccionada: CancionInterface) {
    this.formularioCertificadoChart.patchValue(
      {
        cancion: cancionSeleccionada
      }
    );
  }

  setearAlbum(albumSeleccionado: AlbumInterface) {
    this.formularioCertificadoChart.patchValue(
      {
        album: albumSeleccionado
      }
    );
  }

  setearVideo(videoSeleccionado: VideoInterface) {
    this.formularioCertificadoChart.patchValue(
      {
        video: videoSeleccionado
      }
    );
  }

  setearPlaceholderTipo() {
    if (this.tipo === 'video') {
      this.placeHolderTipo = ' del video';
    }
    if (this.tipo === 'album') {
      this.placeHolderTipo = ' del album';
    }
    if (this.tipo === 'cancion') {
      this.placeHolderTipo = ' de la cancion';
    }
  }

  setearCertificadosChartFecha(certificadosChartFecha: CertificadoChartFechaInterface[]) {
    this.formularioCertificadoChart.patchValue(
      {
        certificadosChartFecha: certificadosChartFecha
      }
    );
  }
}
