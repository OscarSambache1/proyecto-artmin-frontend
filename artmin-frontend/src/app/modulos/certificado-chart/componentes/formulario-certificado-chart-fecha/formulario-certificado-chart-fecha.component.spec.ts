import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCertificadoChartFechaComponent } from './formulario-certificado-chart-fecha.component';

describe('FormularioCertificadoChartFechaComponent', () => {
  let component: FormularioCertificadoChartFechaComponent;
  let fixture: ComponentFixture<FormularioCertificadoChartFechaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioCertificadoChartFechaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCertificadoChartFechaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
