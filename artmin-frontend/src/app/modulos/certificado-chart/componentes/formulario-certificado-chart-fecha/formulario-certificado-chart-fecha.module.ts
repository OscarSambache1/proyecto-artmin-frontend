import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioCertificadoChartFechaComponent } from './formulario-certificado-chart-fecha.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {CalendarModule, InputNumberModule} from 'primeng';
import {AutocompleteCertificadoModule} from '../../../../componentes/autocomplete-certificado/autocomplete-certificado.module';



@NgModule({
  declarations: [FormularioCertificadoChartFechaComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule,
    CalendarModule,
    InputNumberModule,
    AutocompleteCertificadoModule,
  ],
  exports: [
    FormularioCertificadoChartFechaComponent
  ]
})
export class FormularioCertificadoChartFechaModule { }
