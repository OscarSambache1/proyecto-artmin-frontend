import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CertificadoChartFechaInterface} from '../../../../interfaces/certificado-chart-fecha.interface';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ToasterService} from 'angular2-toaster';
import {
  MENSAJES_VALIDACION_CANTIDAD_CERTIFICADO_CHART_FECHA,
  MENSAJES_VALIDACION_CERTIFICADO_CHART_FECHA,
  MENSAJES_VALIDACION_FECHA_CERTIFICADO_CHART_FECHA,
  VALIDACION_CANTIDAD_CERTIFICADO_CHART_FECHA,
  VALIDACION_CERTIFICADO_CHART_FECHA, VALIDACION_FECHA_CERTIFICADO_CHART_FECHA,
} from '../../../../constantes/validaciones-formulario/certificado-chart-fecha';
import {CertificadoInterface} from '../../../../interfaces/certificado.interface';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';

@Component({
  selector: 'app-formulario-certificado-chart-fecha',
  templateUrl: './formulario-certificado-chart-fecha.component.html',
  styleUrls: ['./formulario-certificado-chart-fecha.component.css']
})
export class FormularioCertificadoChartFechaComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;

  formularioCertificadoFechaChart: FormGroup;
  mensajesError = {
    cantidad: [],
    fecha: [],
    certificado: [],
  };
  subscribers = [];

  @Output()
  certificadoChartFechaValidoEnviar: EventEmitter<CertificadoChartFechaInterface | boolean> = new EventEmitter();

  @Input()
  certificadoChartFecha: CertificadoChartFechaInterface;

  @Input()
  idChart: number;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioCertificadoFechaChart = this._formBuilder.group({
      cantidad: [this.certificadoChartFecha ? this.certificadoChartFecha.cantidad : null, VALIDACION_CANTIDAD_CERTIFICADO_CHART_FECHA],
      fecha: [this.certificadoChartFecha ? this.certificadoChartFecha.fecha : null, VALIDACION_FECHA_CERTIFICADO_CHART_FECHA],
      certificado: [this.certificadoChartFecha ? this.certificadoChartFecha.certificado : null, VALIDACION_CERTIFICADO_CHART_FECHA],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('cantidad', MENSAJES_VALIDACION_CANTIDAD_CERTIFICADO_CHART_FECHA);
    this.verificarCampoFormControl('fecha', MENSAJES_VALIDACION_FECHA_CERTIFICADO_CHART_FECHA);
    this.verificarCampoFormControl('certificado', MENSAJES_VALIDACION_CERTIFICADO_CHART_FECHA);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioCertificadoFechaChart.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioCertificadoFechaChart;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.certificadoChartFechaValidoEnviar.emit(formulario);
          } else {
            this.certificadoChartFechaValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }


  setearCertificado(certificadoSeleccionado: CertificadoInterface) {
    this.formularioCertificadoFechaChart.patchValue(
      {
        certificado: certificadoSeleccionado
      }
    );
  }
}
