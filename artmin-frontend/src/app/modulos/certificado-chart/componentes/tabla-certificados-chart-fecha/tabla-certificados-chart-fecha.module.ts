import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaCertificadosChartFechaComponent } from './tabla-certificados-chart-fecha.component';
import {TableModule} from 'primeng';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {ModalCrearEditarCertificadoChartFechaModule} from '../../modales/modal-crear-editar-certificado-chart-fecha/modal-crear-editar-certificado-chart-fecha.module';
import {ModalCrearEditarCertificadoChartFechaComponent} from '../../modales/modal-crear-editar-certificado-chart-fecha/modal-crear-editar-certificado-chart-fecha.component';



@NgModule({
  declarations: [TablaCertificadosChartFechaComponent],
  imports: [
    CommonModule,
    TableModule,
    MatSortModule,
    ModalCrearEditarCertificadoChartFechaModule
  ],
  exports: [
    MatTableModule,
    TablaCertificadosChartFechaComponent,
  ],
  entryComponents: [
    ModalCrearEditarCertificadoChartFechaComponent
  ]
})
export class TablaCertificadosChartFechaModule { }
