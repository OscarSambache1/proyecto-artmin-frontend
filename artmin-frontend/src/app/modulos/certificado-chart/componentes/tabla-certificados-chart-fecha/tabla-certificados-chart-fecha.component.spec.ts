import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaCertificadosChartFechaComponent } from './tabla-certificados-chart-fecha.component';

describe('TablaCertificadosChartFechaComponent', () => {
  let component: TablaCertificadosChartFechaComponent;
  let fixture: ComponentFixture<TablaCertificadosChartFechaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaCertificadosChartFechaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaCertificadosChartFechaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
