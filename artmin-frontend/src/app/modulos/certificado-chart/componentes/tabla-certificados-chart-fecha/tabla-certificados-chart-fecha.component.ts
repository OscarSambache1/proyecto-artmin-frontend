import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CertificadoChartFechaInterface} from '../../../../interfaces/certificado-chart-fecha.interface';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {ModalCrearEditarCertificadoChartFechaComponent} from '../../modales/modal-crear-editar-certificado-chart-fecha/modal-crear-editar-certificado-chart-fecha.component';

@Component({
  selector: 'app-tabla-certificados-chart-fecha',
  templateUrl: './tabla-certificados-chart-fecha.component.html',
  styleUrls: ['./tabla-certificados-chart-fecha.component.css']
})
export class TablaCertificadosChartFechaComponent implements OnInit {

  @Input()
  certificadosChartFecha: CertificadoChartFechaInterface[] = [];

  @Input()
  idChart: number;

  @Input()
  verBotones: boolean;

  @Output()
  certificadosChartFechaAEnviar: EventEmitter<CertificadoChartFechaInterface[]> = new EventEmitter();

  columnas = [
    'cantidad', 'certificado', 'fecha'
  ];

  dataSource: MatTableDataSource<CertificadoChartFechaInterface>;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public dialog: MatDialog,

  ) { }

  ngOnInit(): void {
    if (this.verBotones) {
      this.columnas.push('acciones');
    }
    this.dataSource = new MatTableDataSource([...this.certificadosChartFecha]);
    this.dataSource.sort = this.sort;
  }

  abrirModalCrearEditarCertificadoChartFecha(
    certificadoChartFecha?: CertificadoChartFechaInterface,
    indice?: number
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarCertificadoChartFechaComponent,
        {
          width: '500px',
          data: {
            certificadoChartFecha,
            idChart: this.idChart,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (certificadoChartFechaCreadoEditado: CertificadoChartFechaInterface) => {
          if (certificadoChartFechaCreadoEditado) {
            if (indice || indice === 0) {
              this.dataSource.data[indice] = certificadoChartFechaCreadoEditado;
            } else {
              this.dataSource.data.push(certificadoChartFechaCreadoEditado);
            }
            this.dataSource.data = [...this.dataSource.data];
            this.certificadosChartFechaAEnviar.emit(this.dataSource.data);
          }

        },
        error => {
          console.error(error);
        },
      );
  }

  quitarCertificadoChartFecha(
    indice: number
  ) {
    this.dataSource.data.splice(indice, 1);
    this.dataSource.data = [...this.dataSource.data];
    this.dataSource.sort = this.sort;
    this.certificadosChartFechaAEnviar.emit(this.dataSource.data);
  }
}
