import {RUTAS_ARTISTA} from '../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CHART} from '../../chart/rutas/definicion-rutas/rutas-charts';
import {generarRespuestaRuta} from '../../../funciones/generar-rutas';
import {RUTAS_ALBUM} from '../../album/rutas/definicion-rutas/rutas-album';
import {RUTAS_CANCION} from '../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_VIDEO} from '../../video/rutas/definicion-rutas/rutas-videos';

export const RUTAS_CERTIFICADOS_CHART = {
  _rutaInicioCertificadosChart: {
    ruta: ':idChart/certificado-chart-modulo',
    nombre: 'Modulo Certificado Chart',
    generarRuta: (...argumentos) => {
      return `${argumentos[3]}/certificado-chart-modulo`;
    }
  },

  _rutaGestionCertificadoCharts: {
    ruta: 'gestion-certificados-chart',
    nombre: 'Gestión Certificados Charts',
    generarRuta: (...argumentos) => {
      return `gestion-certificados-chart`;
    }
  },

  rutaGestionCertificadosChart: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    const tipo = argumentos?.find(arg => ['A', 'AL', 'C', 'V', 'CH'].includes(arg));
    let rutasArtista = [];
    let rutaObjetos = [];

    if (argumentos[0]) {
      rutasArtista  = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
      ];
    }
    if (argumentos[1]) {
      if (tipo === 'AL') {
        rutaObjetos = [
          RUTAS_ALBUM._rutaInicioAlbum,
          RUTAS_ALBUM._rutaGestionAlbumes,
          RUTAS_ALBUM._rutaEditarAlbum,
        ];
      }
      if (tipo === 'C') {
        rutaObjetos = [
          RUTAS_CANCION._rutaInicioCancion,
          RUTAS_CANCION._rutaGestionCanciones,
          RUTAS_CANCION._rutaEditarCancion,
        ];
      }
      if (tipo === 'V') {
        rutaObjetos = [
          RUTAS_VIDEO._rutaInicioVideo,
          RUTAS_VIDEO._rutaGestionVideos,
          RUTAS_VIDEO._rutaIdVideo,
        ];
      }
    }
    const rutaArreglo = [
      ...rutasArtista,
      ...rutaObjetos,
      RUTAS_CHART._rutaInicioChart,
      RUTAS_CHART._rutaGestionCharts,
      this._rutaInicioCertificadosChart,
      this._rutaGestionCertificadoCharts
    ];


    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
