import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificadoChartRoutingModule } from './certificado-chart-routing.module';
import { RutaGestionCertificadosChartComponent } from './ruta-gestion-certificados-chart/ruta-gestion-certificados-chart.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {DropdownModule, MultiSelectModule, TableModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TablaCertificadosChartFechaModule} from './componentes/tabla-certificados-chart-fecha/tabla-certificados-chart-fecha.module';
import {AutocompleteCertificadoModule} from '../../componentes/autocomplete-certificado/autocomplete-certificado.module';
import {ModalCrearEditarCertificadoChartComponent} from './modales/modal-crear-editar-certificado-chart/modal-crear-editar-certificado-chart.component';
import {ModalCrearEditarCertificadoChartModule} from './modales/modal-crear-editar-certificado-chart/modal-crear-editar-certificado-chart.module';


@NgModule({
  declarations: [RutaGestionCertificadosChartComponent],
  imports: [
    CommonModule,
    CertificadoChartRoutingModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    TableModule,
    MultiSelectModule,
    FormsModule,
    DropdownModule,
    TablaCertificadosChartFechaModule,
    AutocompleteCertificadoModule,
    ModalCrearEditarCertificadoChartModule
  ],
  exports: [
    RutaGestionCertificadosChartComponent
  ],
  entryComponents: [
    ModalCrearEditarCertificadoChartComponent
  ]
})
export class CertificadoChartModule { }
