import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionCertificadosChartComponent } from './ruta-gestion-certificados-chart.component';

describe('RutaGestionCertificadosChartComponent', () => {
  let component: RutaGestionCertificadosChartComponent;
  let fixture: ComponentFixture<RutaGestionCertificadosChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaGestionCertificadosChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionCertificadosChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
