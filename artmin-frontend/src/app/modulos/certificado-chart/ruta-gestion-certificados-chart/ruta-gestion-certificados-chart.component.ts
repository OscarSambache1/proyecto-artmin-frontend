import { Component, OnInit } from '@angular/core';
import {QueryParamsInterface} from '../../../interfaces/query-params.interface';
import {ArtistaInterface} from '../../../interfaces/artista.interface';
import {ChartInterface} from '../../../interfaces/chart.interface';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../servicios/cargando-service';
import {ArtistaRestService} from '../../../servicios/rest/artista-rest.service';
import {ChartRestService} from '../../../servicios/rest/chart-rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../constantes/mensajes-toaster';
import {RUTAS_ARTISTA} from '../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CHART} from '../../chart/rutas/definicion-rutas/rutas-charts';
import {obtenerUrlImagenPrincipal} from '../../../funciones/obtener-imagen-principal';
import {CertificadoChartInterface} from '../../../interfaces/certificado-chart.interface';
import {CertificadoChartRestService} from '../../../servicios/rest/certificado-chart-rest.service';
import {RUTAS_CERTIFICADOS_CHART} from '../definicion-rutas/rutas-certificados-chart';
import {CertificadoInterface} from '../../../interfaces/certificado.interface';
import {ModalCrearEditarCertificadoChartComponent} from '../modales/modal-crear-editar-certificado-chart/modal-crear-editar-certificado-chart.component';
import {CancionRestService} from '../../../servicios/rest/cancion-rest.service';
import {AlbumRestService} from '../../../servicios/rest/album-rest.service';
import {VideoRestService} from '../../../servicios/rest/video-rest.service';
import {RUTAS_ALBUM} from '../../album/rutas/definicion-rutas/rutas-album';
import {RUTAS_CANCION} from '../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_VIDEO} from '../../video/rutas/definicion-rutas/rutas-videos';
import {AlbumInterface} from '../../../interfaces/album.interface';
import {CancionInterface} from '../../../interfaces/cancion.interface';
import {VideoInterface} from '../../../interfaces/video.interface';

@Component({
  selector: 'app-ruta-gestion-certificados-chart',
  templateUrl: './ruta-gestion-certificados-chart.component.html',
  styleUrls: ['./ruta-gestion-certificados-chart.component.css'],

})
export class RutaGestionCertificadosChartComponent implements OnInit {

  certificadosChart: CertificadoChartInterface[];

  columnas = [
    {
      field: 'nombre',
      header: 'Nombre',
      width: '15%'
    },
    {
      field: 'fechaActualizacion',
      header: 'Fecha Actualización',
      width: '15%'
    },
    {
      field: 'unidades',
      header: 'Unidades',
      width: '15%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '15%'
    },
  ];

  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  ruta = [];
  chart: ChartInterface;
  rutaImagen = '';
  idChart: number;
  idCertificado: number;
  arregloRutas: any[];
  tipo = '';
  arregloidsCancionesAlbumesVideosSeleccionadas = [];
  columnasCertificadosChartFecha = [
    { field: 'cantidad', header: 'Cantidad' },
    { field: 'fecha', header: 'fecha' },
  ];
  idObjParam = 0;
  tipoObjeto = '';
  objeto: any;
  constructor(
    private readonly _certificadoChartRestService: CertificadoChartRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _chartRestService: ChartRestService,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cancionRestService: CancionRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _videoRestService: VideoRestService,
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtista = +params.idArtista;
          if (+params.idAlbum) {
            this.idObjParam = +params.idAlbum;
          }
          if (+params.idCancion) {
            this.idObjParam = +params.idCancion;
          }
          if (+params.idVideo) {
            this.idObjParam = +params.idVideo;
          }
          this.idChart = +params.idChart;
          this.tipoObjeto = params.tipo;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(async queryParams => {
          this.idCertificado = +queryParams.idCertificado;
          const promesaArtista = this._artistaRestService.findOne(this.idArtista)
              .toPromise();
          this.objeto = await promesaArtista;
          if (this.idObjParam) {
      if (this.tipoObjeto === 'AL') {
        this._albumRestService.findOne(this.idObjParam)
            .subscribe(
                (album: AlbumInterface) => {
                  this.objeto = album;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
            );
      }
      if (this.tipoObjeto === 'C') {
        this._cancionRestService.findOne(this.idObjParam)
            .subscribe(
                (cancion: CancionInterface) => {
                  this.objeto = cancion;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
            );
      }
      if (this.tipoObjeto === 'V') {
        this._videoRestService.findOne(this.idObjParam)
            .subscribe(
                (video: VideoInterface) => {
                  this.objeto = video;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
            );
      }
    }
          if (this.idChart) {
            const promesaChart = this._chartRestService.findOne(this.idChart)
              .toPromise();
            this.chart = await promesaChart;
            this.tipo = this.chart.tipo;
          }
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.idCertificado) {
              this.idCertificado = this.queryParams.consulta.idCertificado;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idChart: this.idChart,
              idArtista: this.idArtista,
              idCertificado: this.idCertificado,
              tipo: this.tipo,
            };
          }
          this.queryParams.consulta.tipoObjeto = this.tipoObjeto;
          this.queryParams.consulta.idObjParam = this.idObjParam;
          this._cargandoService.habilitarCargando();
          return this._certificadoChartRestService.obtenerCertificados(
            JSON.stringify(this.queryParams.consulta)
          ).toPromise();
        })
      )
      .subscribe(
        (respuestaCertificadosChart: [CertificadoChartInterface[], number]) => {
          this.certificadosChart = respuestaCertificadosChart[0];
          this.arregloidsCancionesAlbumesVideosSeleccionadas = this.certificadosChart
            .map(chartCancionAlbumVideo => {
              return chartCancionAlbumVideo[this.tipo].id;
            });
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarCharts();
  }

  buscarCertifcadosChartPorCertificado(certificadoSeleccionado: CertificadoInterface) {
    this.idCertificado = certificadoSeleccionado ? certificadoSeleccionado.id : undefined;
    this.buscarCharts();
  }

  buscarCharts() {
    const consulta = {
      busqueda: this.busqueda,
      idChart: this.idChart,
      idArtista: this.idArtista,
      idCertificado: this.idCertificado,
      tipo: this.tipo,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    let rutasArtista = [];
    if (this.idArtista) {
      rutasArtista = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista])];
    }
    let rutasObjetos = [];
    if (this.idObjParam) {
      if (this.tipoObjeto === 'AL') {
        rutasObjetos = [
          RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'C') {
        rutasObjetos = [
          RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'V') {
        rutasObjetos = [
          RUTAS_VIDEO.rutaGestionVideos(false, true, [this.idArtista]),
        ];
      }
    }

    this.arregloRutas = [
      ...rutasArtista,
      ...rutasObjetos,
      RUTAS_CHART.rutaGestionCharts(false, true, [this.idArtista, this.idObjParam, this.tipoObjeto]),
      RUTAS_CERTIFICADOS_CHART.rutaGestionCertificadosChart(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_CERTIFICADOS_CHART
      .rutaGestionCertificadosChart(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
      .ruta;
  }

  abrirModalCrearEditarCertificadoChart(
    certificadoChart?: CertificadoChartInterface,
    indice?: number
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarCertificadoChartComponent,
        {
          width: '800px',
          data: {
            certificadoChart,
            idArtista: this.idArtista,
            tipo: this.tipo,
            chart: this.idChart,
            idsCancionesAlbumesVideos: this.arregloidsCancionesAlbumesVideosSeleccionadas,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (certificadoChartCreadoEditado: CertificadoChartInterface) => {
          if (certificadoChartCreadoEditado) {
            if (indice || indice === 0) {
              this.certificadosChart[indice] = certificadoChartCreadoEditado;
            } else {
              this.certificadosChart.unshift(certificadoChartCreadoEditado);
            }
          }

        },
        error => {
          console.error(error);
        },
      );
  }

  obtenerUrlPrincipal(
    objeto: any) {
    let campo;
    if (this.tipo === 'cancion') {
      campo = 'imagenesCancion';
    }
    if (this.tipo === 'album') {
      campo = 'imagenesAlbum';
    }
    if (objeto[campo] && objeto[campo].length) {
      return obtenerUrlImagenPrincipal(objeto, campo);
    }
  }

  setearUnidades(
    certificadoChart: CertificadoChartInterface
  ) {
    const ultimaCertificacion = certificadoChart.certificadosChartFecha[certificadoChart.certificadosChartFecha.length - 1];
    return ultimaCertificacion;
  }
}
