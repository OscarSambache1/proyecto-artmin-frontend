import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarCertificadoChartFechaComponent } from './modal-crear-editar-certificado-chart-fecha.component';

describe('ModalCrearEditarCertificadoChartFechaComponent', () => {
  let component: ModalCrearEditarCertificadoChartFechaComponent;
  let fixture: ComponentFixture<ModalCrearEditarCertificadoChartFechaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarCertificadoChartFechaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarCertificadoChartFechaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
