import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarCertificadoChartFechaComponent } from './modal-crear-editar-certificado-chart-fecha.component';
import {FormularioCertificadoChartFechaModule} from '../../componentes/formulario-certificado-chart-fecha/formulario-certificado-chart-fecha.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {MatDialogModule} from '@angular/material/dialog';



@NgModule({
  declarations: [ModalCrearEditarCertificadoChartFechaComponent],
  imports: [
    CommonModule,
    FormularioCertificadoChartFechaModule,
    BotonGuardarModule,
    BotonCancelarModule,
    MatDialogModule,
  ],
  exports: [
    ModalCrearEditarCertificadoChartFechaComponent
  ]
})
export class ModalCrearEditarCertificadoChartFechaModule { }
