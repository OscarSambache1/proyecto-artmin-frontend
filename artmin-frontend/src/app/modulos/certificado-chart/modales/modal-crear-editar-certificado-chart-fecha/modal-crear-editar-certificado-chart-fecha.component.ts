import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {CertificadoChartFechaInterface} from '../../../../interfaces/certificado-chart-fecha.interface';
import {CertificadoChartFechaRestService} from '../../../../servicios/rest/certificado-chart-fecha-rest.service';

@Component({
  selector: 'app-modal-crear-editar-certificado-chart-fecha',
  templateUrl: './modal-crear-editar-certificado-chart-fecha.component.html',
  styleUrls: ['./modal-crear-editar-certificado-chart-fecha.component.css']
})
export class ModalCrearEditarCertificadoChartFechaComponent implements OnInit {

  formularioValido: boolean;
  certificadoChartFechaACrearEditar: CertificadoChartFechaInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      certificadoChartFecha: CertificadoChartFechaInterface,
      idChart: number,
    },
    public dialogo: MatDialogRef<ModalCrearEditarCertificadoChartFechaComponent>,
    private readonly _certificadoChartFechaRestService: CertificadoChartFechaRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  crearEditarCertificadoChart() {
    this.dialogo.close(this.certificadoChartFechaACrearEditar);
  }

  cerrarModal() {
    this.dialogo.close();
  }

  validarFormulario(certificadoChartFecha: CertificadoChartFechaInterface | boolean) {
    if (certificadoChartFecha) {
      this.formularioValido = true;
      this.certificadoChartFechaACrearEditar = certificadoChartFecha as CertificadoChartFechaInterface;
    } else {
      this.formularioValido = false;
    }
  }
}
