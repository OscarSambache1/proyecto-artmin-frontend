import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {CertificadoChartInterface} from '../../../../interfaces/certificado-chart.interface';
import {CertificadoChartRestService} from '../../../../servicios/rest/certificado-chart-rest.service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';

@Component({
  selector: 'app-modal-crear-editar-certificado-chart',
  templateUrl: './modal-crear-editar-certificado-chart.component.html',
  styleUrls: ['./modal-crear-editar-certificado-chart.component.css']
})
export class ModalCrearEditarCertificadoChartComponent implements OnInit {

  formularioValido: boolean;
  certificadoChartACrearEditar: CertificadoChartInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      certificadoChart: CertificadoChartInterface,
      idArtista: number,
      tipo: string,
      chart: number,
      idsCancionesAlbumesVideos: number[],
    },
    public dialogo: MatDialogRef<ModalCrearEditarCertificadoChartComponent>,
    private readonly _certificadoChartRestService: CertificadoChartRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  crearEditarCertificadoChart() {
    this._cargandoService.habilitarCargando();
    if (this.data.certificadoChart) {
      this._certificadoChartRestService
        .editarCertificadoChart(
          this.data.certificadoChart.id,
          this.certificadoChartACrearEditar,
          this.certificadoChartACrearEditar.certificadosChartFecha
        )
        .subscribe((respuestaCertificadoChartEditado: CertificadoChartInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaCertificadoChartEditado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
      this._cargandoService.deshabiltarCargando();
    } else {
      this.certificadoChartACrearEditar.chart = this.data.chart;
      this._certificadoChartRestService
        .crearCertificadoChart(
          this.certificadoChartACrearEditar,
          this.certificadoChartACrearEditar.certificadosChartFecha
        )
        .subscribe((respuestaCertificadoChartCreado: CertificadoChartInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaCertificadoChartCreado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }

  validarFormulario(certificadoChart: CertificadoChartInterface | boolean) {
    if (certificadoChart) {
      this.formularioValido = true;
      this.certificadoChartACrearEditar = certificadoChart as CertificadoChartInterface;
    } else {
      this.formularioValido = false;
    }
  }
}
