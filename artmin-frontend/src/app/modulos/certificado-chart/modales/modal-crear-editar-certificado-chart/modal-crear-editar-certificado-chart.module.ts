import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarCertificadoChartComponent } from './modal-crear-editar-certificado-chart.component';
import {FormularioCertificadoChartModule} from '../../componentes/formulario-certificado-chart/formulario-certificado-chart.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {TablaCertificadosChartFechaModule} from '../../componentes/tabla-certificados-chart-fecha/tabla-certificados-chart-fecha.module';



@NgModule({
  declarations: [ModalCrearEditarCertificadoChartComponent],
  imports: [
    CommonModule,
    FormularioCertificadoChartModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
    TablaCertificadosChartFechaModule
  ],
  exports: [
    ModalCrearEditarCertificadoChartComponent
  ]
})
export class ModalCrearEditarCertificadoChartModule { }
