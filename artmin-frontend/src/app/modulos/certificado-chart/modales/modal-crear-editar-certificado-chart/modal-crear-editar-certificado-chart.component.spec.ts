import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarCertificadoChartComponent } from './modal-crear-editar-certificado-chart.component';

describe('ModalCrearEditarCertificadoChartComponent', () => {
  let component: ModalCrearEditarCertificadoChartComponent;
  let fixture: ComponentFixture<ModalCrearEditarCertificadoChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarCertificadoChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarCertificadoChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
