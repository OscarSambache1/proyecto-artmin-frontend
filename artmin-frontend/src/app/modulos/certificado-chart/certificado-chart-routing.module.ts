import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionCertificadosChartComponent} from './ruta-gestion-certificados-chart/ruta-gestion-certificados-chart.component';


const routes: Routes = [
  {
    path: 'gestion-certificados-chart',
    children: [
      {
        path: '',
        component: RutaGestionCertificadosChartComponent
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-certificados-chart',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificadoChartRoutingModule { }
