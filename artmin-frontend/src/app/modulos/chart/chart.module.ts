import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartRoutingModule } from './chart-routing.module';
import { RutaGestionChartsComponent } from './rutas/ruta-gestion-charts/ruta-gestion-charts.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {TableModule} from 'primeng';
import {AutocompletePlataformaModule} from '../../componentes/autocomplete-plataforma/autocomplete-plataforma.module';
import {AutocompleteLugarModule} from '../../componentes/autocomplete-lugar/autocomplete-lugar.module';
import {SelectTipoChartModule} from '../../componentes/select-tipo-chart/select-tipo-chart.module';
import {ModalCrearEditarChartModule} from './modales/modal-crear-editar-chart/modal-crear-editar-chart.module';
import {ModalCrearEditarChartComponent} from './modales/modal-crear-editar-chart/modal-crear-editar-chart.component';


@NgModule({
  declarations: [RutaGestionChartsComponent],
  imports: [
    CommonModule,
    ChartRoutingModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    TableModule,
    AutocompletePlataformaModule,
    AutocompleteLugarModule,
    SelectTipoChartModule,
    ModalCrearEditarChartModule
  ],
  exports: [
    RutaGestionChartsComponent
  ],
  entryComponents: [
    ModalCrearEditarChartComponent
  ]
})
export class ChartModule { }
