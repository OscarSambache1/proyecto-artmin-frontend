import {Component, OnInit} from '@angular/core';
import {ChartInterface} from '../../../../interfaces/chart.interface';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ChartRestService} from '../../../../servicios/rest/chart-rest.service';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {LugarInterface} from '../../../../interfaces/lugar.interface';
import {PlataformaInterface} from '../../../../interfaces/plataforma.interface';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CHART} from '../definicion-rutas/rutas-charts';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {ModalCrearEditarChartComponent} from '../../modales/modal-crear-editar-chart/modal-crear-editar-chart.component';
import {RUTAS_CHART_CANCION_ALBUM_ARTISTA_VIDEO} from '../../../chart-cancion-album-artista-video/rutas/definicion-rutas/rutas-charts';
import {RUTAS_UNIDADES} from '../../../unidad-cancion-album-video/rutas/definicion-rutas/rutas-unidades';
import {RUTAS_CERTIFICADOS_CHART} from '../../../certificado-chart/definicion-rutas/rutas-certificados-chart';
import {RUTAS_RECORDS_CHART} from '../../../record-chart/rutas/definicion-rutas/rutas-records-chart';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {RUTAS_VIDEO} from '../../../video/rutas/definicion-rutas/rutas-videos';
import {RUTAS_CHART_POSICION} from '../../../chart-posicion/rutas/definicion-rutas/rutas-charts';

@Component({
  selector: 'app-ruta-gestion-charts',
  templateUrl: './ruta-gestion-charts.component.html',
  styleUrls: ['./ruta-gestion-charts.component.css']
})
export class RutaGestionChartsComponent implements OnInit {

  charts: ChartInterface[];

  columnasCharts = [
    {
      field: 'nombre',
      header: 'Nombre',
      width: '15%'
    },
    {
      field: 'tipo',
      header: 'Tipo',
      width: '15%'
    },
    {
      field: 'plataforma',
      header: 'Plataforma',
      width: '15%'
    },
    {
      field: 'lugar',
      header: 'Lugar',
      width: '10%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '10%'
    },
  ];

  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  ruta = [];
  // artista: ArtistaInterface;
  rutaImagen = 'assets/imagenes/chart.svg';
  idArtistaQuery: number;
  idArtistaParams: number;
  idPlataforma: number;
  idLugar: number;
  tipoChart: string;
  arregloRutas: any[];
  tipo: any;
  idObjParam = 0;
  objeto: any;
  tipoChartNombre;

  constructor(
    private readonly _chartRestService: ChartRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _cancionRestService: CancionRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _videoRestService: VideoRestService,
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtistaParams = params.idArtista ? +params.idArtista : undefined;
          if (+params.idAlbum) {
            this.idObjParam = +params.idAlbum;
          }
          if (+params.idCancion) {
            this.idObjParam = +params.idCancion;
          }
          if (+params.idVideo) {
            this.idObjParam = +params.idVideo;
          }
          this.tipo = params.tipo;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          this.idPlataforma = +queryParams.idPlataforma;
          if (this.idArtistaParams) {
            this.idArtista = this.idArtistaParams;
            this._artistaRestService.findOne(this.idArtista)
              .subscribe(
                (artista: ArtistaInterface) => {
                  this.objeto = artista;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (this.idObjParam) {
            if (this.tipo === 'AL') {
              this._albumRestService.findOne(this.idObjParam)
                .subscribe(
                  (album: ArtistaInterface) => {
                    this.objeto = album;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipo === 'C') {
              this._cancionRestService.findOne(this.idObjParam)
                .subscribe(
                  (cancion: CancionInterface) => {
                    this.objeto = cancion;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipo === 'V') {
              this._videoRestService.findOne(this.idObjParam)
                .subscribe(
                  (video: VideoInterface) => {
                    this.objeto = video;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
          }

          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.idLugar) {
              this.idLugar = this.queryParams.consulta.idLugar;
            }
            if (this.queryParams.consulta.idPlataforma) {
              this.idPlataforma = this.queryParams.consulta.idPlataforma;
            }
            if (this.queryParams.consulta.tipoChart) {
              this.tipoChart = this.queryParams.consulta.tipoChart;
            }
            if (this.queryParams.consulta.idArtista) {
              this.idArtistaQuery = this.queryParams.consulta.idArtista;
              this.idArtista = this.idArtistaQuery;
            } else {
              this.queryParams.consulta.idArtista = this.idArtista;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idLugar: this.idLugar,
              idArtista: this.idArtista,
              idPlataforma: this.idPlataforma,
              tipoChart: this.tipoChart,
            };
          }
          if (this.tipo === 'C' || this.tipo === 'AL' || this.tipo === 'V') {
             this.tipoChartNombre = '';
            if (this.tipo === 'C') {
              this.tipoChartNombre = 'cancion';
            }
            if (this.tipo === 'AL') {
              this.tipoChartNombre = 'album';
            }
            if (this.tipo === 'V') {
              this.tipoChartNombre = 'video';
            }
            this.queryParams.consulta.tipoChart = this.tipoChartNombre;
          }
          this._cargandoService.habilitarCargando();
          return this._chartRestService.obtenerCharts(
            JSON.stringify(this.queryParams.consulta)
          );
        })
      )
      .subscribe(
        (respuestaCharts: [ChartInterface[], number]) => {
          this.charts = respuestaCharts[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarCharts();
  }

  buscarChartsPorLugar(lugarSeleccionado: LugarInterface) {
    this.idLugar = lugarSeleccionado ? lugarSeleccionado.id : undefined;
    this.buscarCharts();
  }

  buscarChartsPorPlataforma(plataformaSeleccionada: PlataformaInterface) {
    this.idPlataforma = plataformaSeleccionada ? plataformaSeleccionada.id : undefined;
    this.buscarCharts();
  }

  buscarChartPorTipo(tipoChartSeleccionado: string) {
    this.tipoChart = tipoChartSeleccionado ? tipoChartSeleccionado : undefined;
    this.buscarCharts();
  }

  buscarCharts() {
    const consulta = {
      busqueda: this.busqueda,
      idLugar: this.idLugar,
      esImagenPrincipal: 1,
      idArtista: this.idArtista,
      idPlataforma: this.idPlataforma,
      tipoChart: this.tipoChart
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    let rutasArtista = [];
    if (this.idArtistaParams) {
      rutasArtista = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtistaParams])];
    }
    let rutasObjetos = [];
    if (this.idObjParam) {
      if (this.tipo === 'AL') {
        rutasObjetos = [
          RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipo === 'C') {
        rutasObjetos = [
          RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipo === 'V') {
        rutasObjetos = [
          RUTAS_VIDEO.rutaGestionVideos(false, true, [this.idArtista]),
        ];
      }
    }
    this.arregloRutas = [
      ...rutasArtista,
      ...rutasObjetos,
      RUTAS_CHART.rutaGestionCharts(false, true, [this.idArtistaParams, this.idObjParam, this.tipo])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_CHART
      .rutaGestionCharts(
        false,
        true,
        [this.idArtistaParams, this.idObjParam, this.tipo])
      .ruta;
  }

  abrirModalCrearEditarChart(
    chart?: ChartInterface,
    indice?: number
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarChartComponent,
        {
          width: '1000px',
          data: {
            chart,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (chartCreadoEditado: ChartInterface) => {
          if (chartCreadoEditado) {
            if (indice || indice === 0) {
              this.charts[indice] = chartCreadoEditado;
            } else {
              this.charts.unshift(chartCreadoEditado);
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  obtenerUrlPrincipal(
    objeto: any,
    campo: string) {
    if (objeto[campo] && objeto[campo].length) {
      return obtenerUrlImagenPrincipal(objeto, campo);
    }
  }

  irListaCharts(
    idChart: number
  ) {
    const ruta = RUTAS_CHART_CANCION_ALBUM_ARTISTA_VIDEO
      .rutaGestionChartCancionAlbumArtistaVideo(
        true,
        false,
        [
          this.idArtista,
          this.idObjParam,
          this.tipo,
          idChart
        ]
      );
    this._router.navigate(
      ruta
    );
  }

  verUnidades(idChart: number) {
    const ruta = RUTAS_UNIDADES
      .rutaGestionUnidades(
        true,
        false,
        [
          this.idArtista,
          this.idObjParam,
          this.tipo,
          idChart
        ]
      );
    this._router.navigate(
      ruta
    );
  }

  verCertificados(idChart: number) {
    const ruta = RUTAS_CERTIFICADOS_CHART
      .rutaGestionCertificadosChart(
        true,
        false,
        [
          this.idArtista,
          this.idObjParam,
          this.tipo,
          idChart
        ]
      );
    this._router.navigate(
      ruta
    );
  }

  verRecords(idChart: number) {
    const ruta = RUTAS_RECORDS_CHART
      .rutaGestionRecordsChart(
        true,
        false,
        [
          this.idArtista,
          this.idObjParam,
          this.tipo,
          idChart
        ]
      );
    this._router.navigate(
      ruta
    );
  }

  verPosocionesChart(idChart: number) {
    const ruta = RUTAS_CHART_POSICION
      .rutaGestionChartPosicion(
        true,
        false,
        [
          this.idArtista,
          this.idObjParam,
          this.tipo,
          idChart
        ]
      );
    this._router.navigate(
      ruta
    );
  }
}
