import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioChartComponent } from './formulario-chart.component';

describe('FormularioChartComponent', () => {
  let component: FormularioChartComponent;
  let fixture: ComponentFixture<FormularioChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
