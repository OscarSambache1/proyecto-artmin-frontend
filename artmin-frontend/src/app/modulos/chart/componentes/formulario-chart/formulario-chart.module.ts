import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioChartComponent } from './formulario-chart.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AutocompletePlataformaModule} from '../../../../componentes/autocomplete-plataforma/autocomplete-plataforma.module';
import {AutocompleteLugarModule} from '../../../../componentes/autocomplete-lugar/autocomplete-lugar.module';
import {SelectTipoChartModule} from '../../../../componentes/select-tipo-chart/select-tipo-chart.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {InputTextareaModule, InputTextModule} from 'primeng';



@NgModule({
  declarations: [FormularioChartComponent],
  exports: [
    FormularioChartComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AutocompletePlataformaModule,
    AutocompleteLugarModule,
    SelectTipoChartModule,
    AlertaValidacionCampoFormularioModule,
    InputTextModule,
    InputTextareaModule
  ]
})
export class FormularioChartModule { }
