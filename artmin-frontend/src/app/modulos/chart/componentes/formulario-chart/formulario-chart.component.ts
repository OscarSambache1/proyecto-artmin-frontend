import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {PlataformaInterface} from '../../../../interfaces/plataforma.interface';
import {ChartInterface} from '../../../../interfaces/chart.interface';
import {LugarInterface} from '../../../../interfaces/lugar.interface';
import {
  MENSAJES_VALIDACION_DESCRIPCION_CHART, MENSAJES_VALIDACION_LUGAR_CHART,
  MENSAJES_VALIDACION_NOMBRE_CHART, MENSAJES_VALIDACION_PLATAFORMA_CHART, MENSAJES_VALIDACION_TIPO_CHART,
  VALIDACION_DESCRIPCION_CHART, VALIDACION_LUGAR_CHART,
  VALIDACION_NOMBRE_CHART, VALIDACION_PLATAFORMA_CHART, VALIDACION_TIPO_CHART
} from '../../../../constantes/validaciones-formulario/chart';

@Component({
  selector: 'app-formulario-chart',
  templateUrl: './formulario-chart.component.html',
  styleUrls: ['./formulario-chart.component.css']
})
export class FormularioChartComponent implements OnInit {

  formularioChart: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    tipo: [],
    plataforma: [],
    lugar: []
  };
  subscribers = [];

  @Input()
  chart: ChartInterface;

  idsPlataforma = [];

  @Output()
  chartValidoEnviar: EventEmitter<ChartInterface | boolean> = new EventEmitter();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioChart = this._formBuilder.group({
      nombre: [this.chart ? this.chart.nombre : '', VALIDACION_NOMBRE_CHART],
      descripcion: [this.chart ? this.chart.descripcion : null, VALIDACION_DESCRIPCION_CHART],
      plataforma: [this.chart ? this.chart.plataforma : null, VALIDACION_PLATAFORMA_CHART],
      lugar: [this.chart ? this.chart.lugar : null, VALIDACION_LUGAR_CHART],
      tipo: [this.chart ? this.chart.tipo : null, VALIDACION_TIPO_CHART],
    });
    if (this.chart && this.chart.plataforma) {
      this.idsPlataforma.push((this.chart.plataforma as PlataformaInterface).id);
    }
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_CHART);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_CHART);
    this.verificarCampoFormControl('plataforma', MENSAJES_VALIDACION_PLATAFORMA_CHART);
    this.verificarCampoFormControl('lugar', MENSAJES_VALIDACION_LUGAR_CHART);
    this.verificarCampoFormControl('tipo', MENSAJES_VALIDACION_TIPO_CHART);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioChart.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioChart;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.chartValidoEnviar.emit(formulario);
          } else {
            this.chartValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharPlataformaSeleccionada(plataformaSeleccionada: PlataformaInterface) {
    this.formularioChart
      .patchValue(
        {
          plataforma: plataformaSeleccionada
        }
      );
  }

  escucharLugarSeleccionado(lugarSeleccioando: LugarInterface) {
    this.formularioChart
      .patchValue(
        {
          lugar: lugarSeleccioando
        }
      );
  }

  escucharTipoSeleccionada(tipoChart: string) {
    this.formularioChart
      .patchValue(
        {
          tipo: tipoChart
        }
      );
  }
}
