import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarChartComponent } from './modal-crear-editar-chart.component';

describe('ModalCrearEditarChartComponent', () => {
  let component: ModalCrearEditarChartComponent;
  let fixture: ComponentFixture<ModalCrearEditarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
