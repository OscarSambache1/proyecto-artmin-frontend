import {Component, Inject, OnInit} from '@angular/core';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../../../interfaces/enlace-album-cancion-artista-video.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EnlaceAlbumCancionArtistaVideoRestService} from '../../../../servicios/rest/enlace-album-cancion-artista-video-rest.service';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ChartInterface} from '../../../../interfaces/chart.interface';
import {ChartRestService} from '../../../../servicios/rest/chart-rest.service';
import {obtenerAnio} from '../../../../funciones/obtener-anio';
import {convertirMinutosMascaraASegundos} from '../../../../funciones/convertir-minutos-segundos';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';

@Component({
  selector: 'app-modal-crear-editar-chart',
  templateUrl: './modal-crear-editar-chart.component.html',
  styleUrls: ['./modal-crear-editar-chart.component.css']
})
export class ModalCrearEditarChartComponent implements OnInit {

  formularioValido: boolean;
  chartACrearEditar: ChartInterface;
  chart: ChartInterface;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      chart: ChartInterface
    },
    public dialogo: MatDialogRef<ModalCrearEditarChartComponent>,
    private readonly _chartService: ChartRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  validarFormularioChart(chart: ChartInterface | boolean) {
    if (chart) {
      this.formularioValido = true;
      this.chartACrearEditar = chart as ChartInterface;
    } else {
      this.formularioValido = false;
    }
  }


  crearEditarChart() {
    this._cargandoService.habilitarCargando();
    if (this.data.chart) {
      this._chartService
        .updateOne(
          this.data.chart.id,
          this.chartACrearEditar
        )
        .subscribe((respuestaChartEditado: ChartInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            respuestaChartEditado.plataforma = this.chartACrearEditar.plataforma;
            respuestaChartEditado.lugar = this.chartACrearEditar.lugar;
            this.dialogo.close(respuestaChartEditado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
      this._cargandoService.deshabiltarCargando();
    } else {
      this._chartService
        .create(
          this.chartACrearEditar,
        )
        .subscribe((respuestaChartCreado: ChartInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaChartCreado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }  }

  cerrarModal() {
    this.dialogo.close();
  }

}
