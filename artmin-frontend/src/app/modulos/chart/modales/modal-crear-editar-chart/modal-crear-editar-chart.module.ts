import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarChartComponent } from './modal-crear-editar-chart.component';
import {FormularioChartModule} from '../../componentes/formulario-chart/formulario-chart.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarChartComponent],
  imports: [
    CommonModule,
    FormularioChartModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule
  ],
  exports: [
    ModalCrearEditarChartComponent
  ]
})
export class ModalCrearEditarChartModule { }
