import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionChartsComponent} from './rutas/ruta-gestion-charts/ruta-gestion-charts.component';


const routes: Routes = [
  {
    path: 'gestion-charts',
    children: [
      {
        path: '',
        component: RutaGestionChartsComponent
      },
      {
        path: ':idChart/chart-cancion-album-artista-video-modulo',
        loadChildren: 'src/app/modulos/chart-cancion-album-artista-video/chart-cancion-album-artista-video.module#ChartCancionAlbumArtistaVideoModule',
      },
      {
        path: ':idChart/unidad-modulo',
        loadChildren: 'src/app/modulos/unidad-cancion-album-video/unidad-cancion-album-video.module#UnidadCancionAlbumVideoModule',
      },
      {
        path: ':idChart/certificado-chart-modulo',
        loadChildren: 'src/app/modulos/certificado-chart/certificado-chart.module#CertificadoChartModule',
      },
      {
        path: ':idChart/record-chart-modulo',
        loadChildren: 'src/app/modulos/record-chart/record-chart.module#RecordChartModule',
      },
      {
        path: ':idChart/chart-posicion-modulo',
        loadChildren: 'src/app/modulos/chart-posicion/chart-posicion.module#ChartPosicionModule',
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-charts',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartRoutingModule { }
