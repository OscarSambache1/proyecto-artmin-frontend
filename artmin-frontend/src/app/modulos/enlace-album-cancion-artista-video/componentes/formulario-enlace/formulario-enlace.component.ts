import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ToasterService} from 'angular2-toaster';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../../../interfaces/enlace-album-cancion-artista-video.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {
  MENSAJES_VALIDACION_PLATAFORMA_ENLACE,
  MENSAJES_VALIDACION_SEGUIDORES_ENLACE,
  MENSAJES_VALIDACION_URL_ENLACE, VALIDACION_PLATAFORMA_ENLACE,
  VALIDACION_SEGUIDORES_ENLACE,
  VALIDACION_URL_ENLACE
} from '../../../../constantes/validaciones-formulario/enlace';
import {PlataformaInterface} from '../../../../interfaces/plataforma.interface';

@Component({
  selector: 'app-formulario-enlace',
  templateUrl: './formulario-enlace.component.html',
  styleUrls: ['./formulario-enlace.component.css']
})
export class FormularioEnlaceComponent implements OnInit {
  formularioEnlace: FormGroup;
  mensajesError = {
    plataforma: [],
    url: [],
    seguidores: [],
  };
  subscribers = [];

  @Input()
  enlace: EnlaceAlbumCancionArtistaVideoInterface;

  @Input()
  idsPlataformas: number[] = [];

  @Output()
  enlaceValidoEnviar: EventEmitter<EnlaceAlbumCancionArtistaVideoInterface | boolean> = new EventEmitter();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    if (this.enlace) {
      // this.deshabilitarFormulario();
    }
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioEnlace = this._formBuilder.group({
      url: [this.enlace ? this.enlace.url : '', VALIDACION_URL_ENLACE],
      plataforma: [this.enlace ? this.enlace.plataforma : null, VALIDACION_PLATAFORMA_ENLACE],
      seguidores: [this.enlace ? this.enlace.seguidores : null, VALIDACION_SEGUIDORES_ENLACE],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('url', MENSAJES_VALIDACION_URL_ENLACE);
    this.verificarCampoFormControl('seguidores', MENSAJES_VALIDACION_SEGUIDORES_ENLACE);
    this.verificarCampoFormControl('plataforma', MENSAJES_VALIDACION_PLATAFORMA_ENLACE);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioEnlace.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioEnlace;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.enlaceValidoEnviar.emit(formulario);
          } else {
            this.enlaceValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharPlataformaSeleccionada(plataformaSeleccionada: PlataformaInterface) {
    this.formularioEnlace
      .patchValue(
        {
          plataforma: plataformaSeleccionada
        }
      );
  }
}
