import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioEnlaceComponent } from './formulario-enlace.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {InputNumberModule, InputTextModule} from 'primeng';
import {AutocompletePlataformaModule} from '../../../../componentes/autocomplete-plataforma/autocomplete-plataforma.module';



@NgModule({
  declarations: [FormularioEnlaceComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule,
    InputNumberModule,
    InputTextModule,
    AutocompletePlataformaModule
  ],
  exports: [
    FormularioEnlaceComponent
  ]
})
export class FormularioEnlaceModule { }
