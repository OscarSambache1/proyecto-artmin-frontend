import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioEnlaceComponent } from './formulario-enlace.component';

describe('FormularioEnlaceComponent', () => {
  let component: FormularioEnlaceComponent;
  let fixture: ComponentFixture<FormularioEnlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioEnlaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioEnlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
