import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrearEditarEnlaceComponent } from './crear-editar-enlace.component';
import {FormularioEnlaceModule} from '../../componentes/formulario-enlace/formulario-enlace.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [CrearEditarEnlaceComponent],
  imports: [
    CommonModule,
    FormularioEnlaceModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule
  ],
  exports: [
    CrearEditarEnlaceComponent
  ]
})
export class CrearEditarEnlaceModule { }
