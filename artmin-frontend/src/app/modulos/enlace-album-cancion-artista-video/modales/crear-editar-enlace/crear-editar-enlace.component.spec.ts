import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEditarEnlaceComponent } from './crear-editar-enlace.component';

describe('CrearEditarEnlaceComponent', () => {
  let component: CrearEditarEnlaceComponent;
  let fixture: ComponentFixture<CrearEditarEnlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEditarEnlaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEditarEnlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
