import {Component, Inject, OnInit} from '@angular/core';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../../../interfaces/enlace-album-cancion-artista-video.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {EnlaceAlbumCancionArtistaVideoRestService} from '../../../../servicios/rest/enlace-album-cancion-artista-video-rest.service';

@Component({
  selector: 'app-crear-editar-enlace',
  templateUrl: './crear-editar-enlace.component.html',
  styleUrls: ['./crear-editar-enlace.component.css']
})
export class CrearEditarEnlaceComponent implements OnInit {
  formularioValido: boolean;
  private enlaceACrearEditar: EnlaceAlbumCancionArtistaVideoInterface;
  enlace: EnlaceAlbumCancionArtistaVideoInterface;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      enlace: EnlaceAlbumCancionArtistaVideoInterface
      idsPlataformas: number[]
    },
    public dialogo: MatDialogRef<CrearEditarEnlaceComponent>,
    private readonly _enlaceAlbumCancionArtistaVideoService: EnlaceAlbumCancionArtistaVideoRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  validarFormularioEnlace(enlace: EnlaceAlbumCancionArtistaVideoInterface | boolean) {
    if (enlace) {
      this.formularioValido = true;
      this.enlaceACrearEditar = enlace as EnlaceAlbumCancionArtistaVideoInterface;
    } else {
      this.formularioValido = false;
    }
  }


  crearEditarEnlace() {
    this.dialogo.close(this.enlaceACrearEditar);
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
