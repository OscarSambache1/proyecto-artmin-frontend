import {Component, Input, OnInit} from '@angular/core';
import {ChartInterface} from '../../../../interfaces/chart.interface';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ChartRestService} from '../../../../servicios/rest/chart-rest.service';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {ChartCancionAlbumArtistaVideoRestService} from '../../../../servicios/rest/chart-cancion-album-artista-video-rest.service';
import {ChartCancionAlbumArtistaVideoInterface} from '../../../../interfaces/chart-cancion-album-artista-video.interface';
import {RUTAS_CHART_CANCION_ALBUM_ARTISTA_VIDEO} from '../definicion-rutas/rutas-charts';
import {ModalCrearEditarChartVideoAlbumArtistaVideoComponent} from '../../modales/modal-crear-editar-chart-video-album-artista-video/modal-crear-editar-chart-video-album-artista-video.component';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_VIDEO} from '../../../video/rutas/definicion-rutas/rutas-videos';
import {VideoInterface} from '../../../../interfaces/video.interface';

@Component({
  selector: 'app-ruta-gestion-chart-cancion-album-artista-video',
  templateUrl: './ruta-gestion-chart-cancion-album-artista-video.component.html',
  styleUrls: ['./ruta-gestion-chart-cancion-album-artista-video.component.css']
})
export class RutaGestionChartCancionAlbumArtistaVideoComponent implements OnInit {

  chartsCancionesAlbumesArtistaVideos: ChartCancionAlbumArtistaVideoInterface[];

  columnasChartsCancionAlbumArtistaVideo = [
    {
      field: 'nombre',
      header: 'Nombre',
      width: '15%'
    },
    {
      field: 'peak',
      header: 'Peak',
      width: '4%'
    },
    {
      field: 'fechaPeak',
      header: 'Fecha peak',
      width: '7%'
    },
    {
      field: 'posicionDebut',
      header: 'Debut',
      width: '4%'
    },
    {
      field: 'fechaDebut',
      header: 'Fecha debut',
      width: '9%'
    },
    {
      field: 'numeroSemanas',
      header: '# semanas',
      width: '5%'
    },
    {
      field: 'numeroDias',
      header: '# dias',
      width: '5%'
    },
    {
      field: 'numeroSemanasPeak',
      header: '# sema peak',
      width: '5%'
    },
    {
      field: 'numeroDiasPeak',
      header: '# dias peak',
      width: '5%'
    },
    {
      field: 'numeroSemanasTop5',
      header: '# sem top 5',
      width: '5%'
    },
    {
      field: 'numeroDiasTop5',
      header: '# dias top 5',
      width: '5%'
    },
    {
      field: 'numeroSemanasTop10',
      header: '# sem top 10',
      width: '5%'
    },
    {
      field: 'numeroDiasTop10',
      header: '# dias top 10',
      width: '5%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '15%'
    },
  ];

  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  ruta = [];
  // artista: ArtistaInterface;
  objeto: any;
  chart: ChartInterface;
  rutaImagen = '';
  idChart: number;
  top: number;
  arregloRutas: any[];
  tipo = '';
  columnasSeleccionadas: any[];
  arregloTops = [
    {
      label: 5,
      value: 5
    },
    {
      label: 10,
      value: 10
    },
    {
      label: 20,
      value: 20
    },
    {
      label: 50,
      value: 50
    }
  ];
  arregloidsCancionesAlbumesVideosSeleccionadas = [];
  idObjParam = 0;
  tipoObjeto = '';

  constructor(
    private readonly _chartCancionAlbumVideoArtistaRestService: ChartCancionAlbumArtistaVideoRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _chartRestService: ChartRestService,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cancionRestService: CancionRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _videoRestService: VideoRestService,
  ) {
  }

  ngOnInit() {
    this.columnasSeleccionadas = this.columnasChartsCancionAlbumArtistaVideo;
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtista = +params.idArtista;
          if (+params.idAlbum) {
            this.idObjParam = +params.idAlbum;
          }
          if (+params.idCancion) {
            this.idObjParam = +params.idCancion;
          }
          if (+params.idVideo) {
            this.idObjParam = +params.idVideo;
          }
          this.idChart = +params.idChart;
          this.tipoObjeto = params.tipo;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(async queryParams => {
          if (this.idArtista) {
            const promesaArtista = this._artistaRestService.findOne(this.idArtista)
              .toPromise();
            this.objeto = await promesaArtista;
          }
          if (this.idObjParam) {
            if (this.tipoObjeto === 'AL') {
              this._albumRestService.findOne(this.idObjParam)
                .subscribe(
                  (album: AlbumInterface) => {
                    this.objeto = album;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipoObjeto === 'C') {
              this._cancionRestService.findOne(this.idObjParam)
                .subscribe(
                  (cancion: CancionInterface) => {
                    this.objeto = cancion;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipoObjeto === 'V') {
              this._videoRestService.findOne(this.idObjParam)
                .subscribe(
                  (video: VideoInterface) => {
                    this.objeto = video;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
          }
          if (this.idChart) {
            const promesaChart = this._chartRestService.findOne(this.idChart)
              .toPromise();
            this.chart = await promesaChart;
            this.tipo = this.chart.tipo;
          }
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.top) {
              this.top = this.queryParams.consulta.top;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idChart: this.idChart,
              idArtista: this.idArtista,
              top: this.top,
              tipo: this.tipo,
            };
          }
          this.queryParams.consulta.tipoObjeto = this.tipoObjeto;
          this.queryParams.consulta.idObjParam = this.idObjParam;
          this._cargandoService.habilitarCargando();
          return this._chartCancionAlbumVideoArtistaRestService.obtenerCharts(
            JSON.stringify(this.queryParams.consulta)
          ).toPromise();
        })
      )
      .subscribe(
        (respuestaCharts: [ChartCancionAlbumArtistaVideoInterface[], number]) => {
          this.chartsCancionesAlbumesArtistaVideos = respuestaCharts[0];
          this.arregloidsCancionesAlbumesVideosSeleccionadas = this.chartsCancionesAlbumesArtistaVideos
            .map(chartCancionAlbumVideo => {
              return chartCancionAlbumVideo[this.tipo].id;
            });
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarCharts();
  }

  buscarChartPorTop(top: number) {
    this.top = top ? top : undefined;
    this.buscarCharts();
  }

  buscarCharts() {
    const consulta = {
      busqueda: this.busqueda,
      idChart: this.idChart,
      idArtista: this.idArtista,
      top: this.top,
      tipo: this.tipo,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    let rutasArtista = [];
    if (this.idArtista) {
      rutasArtista = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista])];
    }
    let rutasObjetos = [];
    if (this.idObjParam) {
      if (this.tipoObjeto === 'AL') {
        rutasObjetos = [
          RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'C') {
        rutasObjetos = [
          RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'V') {
        rutasObjetos = [
          RUTAS_VIDEO.rutaGestionVideos(false, true, [this.idArtista]),
        ];
      }
    }
    this.arregloRutas = [
      ...rutasArtista,
      ...rutasObjetos,
      RUTAS_CHART.rutaGestionCharts(false, true, [this.idArtista, this.idObjParam, this.tipoObjeto]),
      RUTAS_CHART_CANCION_ALBUM_ARTISTA_VIDEO.rutaGestionChartCancionAlbumArtistaVideo(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_CHART_CANCION_ALBUM_ARTISTA_VIDEO
      .rutaGestionChartCancionAlbumArtistaVideo(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
      .ruta;
  }

  abrirModalCrearEditarChart(
    chartCancionAlbumVideoArtista?: ChartCancionAlbumArtistaVideoInterface,
    indice?: number
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarChartVideoAlbumArtistaVideoComponent,
        {
          width: '1000px',
          data: {
            chartCancionAlbumVideoArtista,
            idArtista: this.idArtista,
            tipo: this.tipo,
            chart: this.idChart,
            idsCancionesAlbumesVideos: this.arregloidsCancionesAlbumesVideosSeleccionadas,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (chartCreadoEditado: ChartCancionAlbumArtistaVideoInterface) => {
          if (chartCreadoEditado) {
            const consulta = {
              where: {
                id: chartCreadoEditado.id
              },
              relations: [
                'chart',
                'cancion',
                'cancion.imagenesCancion',
                'album',
                'album.imagenesAlbum',
                'video',
                'video.cancion'
              ]
            };
            this._chartCancionAlbumVideoArtistaRestService
              .findAll(
                JSON.stringify(consulta)
              )
              .subscribe(
                (respuestaCharts: [ChartCancionAlbumArtistaVideoInterface[], number]) => {
                  if (indice || indice === 0) {
                    this.chartsCancionesAlbumesArtistaVideos[indice] = respuestaCharts[0][0];
                  } else {
                    this.chartsCancionesAlbumesArtistaVideos.push(respuestaCharts[0][0]);
                  }
                  this.chartsCancionesAlbumesArtistaVideos = [...this.chartsCancionesAlbumesArtistaVideos];
                  this.arregloidsCancionesAlbumesVideosSeleccionadas = this.chartsCancionesAlbumesArtistaVideos
                    .map(chartCancionAlbumVideo => {
                      return chartCancionAlbumVideo[this.tipo].id;
                    });
                  this._cargandoService.deshabiltarCargando();
                }
                , error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                  this._cargandoService.deshabiltarCargando();
                }
              );

          }
        },
        error => {
          console.error(error);
        },
      );
  }

  obtenerUrlPrincipal(
    objeto: any) {
    let campo;
    if (this.tipo === 'cancion') {
      campo = 'imagenesCancion';
    }
    if (this.tipo === 'album') {
      campo = 'imagenesAlbum';
    }
    if (objeto[campo] && objeto[campo].length) {
      return obtenerUrlImagenPrincipal(objeto, campo);
    }
  }
}
