import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartCancionAlbumArtistaVideoRoutingModule } from './chart-cancion-album-artista-video-routing.module';
import { RutaGestionChartCancionAlbumArtistaVideoComponent } from './rutas/ruta-gestion-chart-cancion-album-artista-video/ruta-gestion-chart-cancion-album-artista-video.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {DropdownModule, MultiSelectModule, TableModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {ModalCrearEditarChartVideoAlbumArtistaVideoModule} from './modales/modal-crear-editar-chart-video-album-artista-video/modal-crear-editar-chart-video-album-artista-video.module';
import {ModalCrearEditarChartVideoAlbumArtistaVideoComponent} from './modales/modal-crear-editar-chart-video-album-artista-video/modal-crear-editar-chart-video-album-artista-video.component';


@NgModule({
  declarations: [RutaGestionChartCancionAlbumArtistaVideoComponent],
  imports: [
    CommonModule,
    ChartCancionAlbumArtistaVideoRoutingModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    TableModule,
    MultiSelectModule,
    FormsModule,
    DropdownModule,
    ModalCrearEditarChartVideoAlbumArtistaVideoModule
  ],
  exports: [
    RutaGestionChartCancionAlbumArtistaVideoComponent
  ],
  entryComponents: [
    ModalCrearEditarChartVideoAlbumArtistaVideoComponent
  ]
})
export class ChartCancionAlbumArtistaVideoModule { }
