import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioChartVideoAlbumArtistaVideoComponent } from './formulario-chart-video-album-artista-video.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {CalendarModule, InputNumberModule, InputTextareaModule, InputTextModule} from 'primeng';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteVideoModule} from '../../../../componentes/autocomplete-video/autocomplete-video.module';



@NgModule({
  declarations: [FormularioChartVideoAlbumArtistaVideoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule,
    InputTextModule,
    InputNumberModule,
    InputTextareaModule,
    CalendarModule,
    AutocompleteCancionModule,
    AutocompleteAlbumsModule,
    AutocompleteVideoModule
  ],
  exports: [
    FormularioChartVideoAlbumArtistaVideoComponent
  ]
})
export class FormularioChartVideoAlbumArtistaVideoModule { }
