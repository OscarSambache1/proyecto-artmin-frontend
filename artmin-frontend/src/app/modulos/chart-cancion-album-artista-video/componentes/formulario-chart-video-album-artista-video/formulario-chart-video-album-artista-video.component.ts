import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {ChartCancionAlbumArtistaVideoInterface} from '../../../../interfaces/chart-cancion-album-artista-video.interface';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {
  MENSAJES_VALIDACION_ALBUM_CHART_CANCION_ALBUM_VIDEO,
  MENSAJES_VALIDACION_CANCION_CHART_CANCION_ALBUM_VIDEO,
  MENSAJES_VALIDACION_DEBUT_CHART_CANCION_ALBUM_VIDEO,
  MENSAJES_VALIDACION_FECHA_DEBUT_CHART_CANCION_ALBUM_VIDEO,
  MENSAJES_VALIDACION_PEAK_CHART_CANCION_ALBUM_VIDEO, MENSAJES_VALIDACION_VIDEO_CHART_CANCION_ALBUM_VIDEO
} from '../../../../constantes/validaciones-formulario/chart-cancion-album-video-artista';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';

@Component({
  selector: 'app-formulario-chart-video-album-artista-video',
  templateUrl: './formulario-chart-video-album-artista-video.component.html',
  styleUrls: ['./formulario-chart-video-album-artista-video.component.css']
})
export class FormularioChartVideoAlbumArtistaVideoComponent implements OnInit {
  configuracionCalendario = CONFIGURACIONES_CALENDARIO;

  formularioChartCancionAlbumVideoArtista: FormGroup;
  mensajesError = {
    peak: [],
    fechaDebut: [],
    posicionDebut: [],
    fechaPeak: [],
    unidadesDebut: [],
    unidadesReales: [],
    numeroDias: [],
    numeroSemanasPeak: [],
    numeroDiasPeak: [],
    numeroSemanasTop5: [],
    numeroDiasTop5: [],
    numeroSemanasTop10: [],
    numeroDiasTop10: [],
    descripcion: [],
    cancion: [],
    album: [],
    video: [],
    numeroSemanas: [],
  };
  subscribers = [];

  @Input()
  tipo: string;

  @Output()
  chartCancionAlbumVideoArtistaValidoEnviar: EventEmitter<ChartCancionAlbumArtistaVideoInterface | boolean> = new EventEmitter();

  @Input()
  chartCancionAlbumVideoArtista: ChartCancionAlbumArtistaVideoInterface;

  placeHolderTipo = '';

  @Input()
  idArtista: number;

  @Input()
  idsCancionesAlbumesVideos: number[];
  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.setearPlaceholderTipo();
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioChartCancionAlbumVideoArtista = this._formBuilder.group({
      peak: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.peak : null, [Validators.required]],
      fechaDebut: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.fechaDebut : null, [Validators.required]],
      posicionDebut: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.posicionDebut : null, [Validators.required]],
      fechaPeak: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.fechaPeak : null, []],
      numeroSemanas: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroSemanas : null, []],
      numeroDias: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroDias : null, []],
      numeroSemanasPeak: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroSemanasPeak : null, []],
      numeroDiasPeak: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroDiasPeak : null, []],
      numeroSemanasTop5: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroSemanasTop5 : null, []],
      numeroDiasTop5: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroDiasTop5 : null, []],
      numeroSemanasTop10: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroSemanasTop10 : null, []],
      numeroDiasTop10: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.numeroDiasTop10 : null, []],
      descripcion: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.descripcion : null, []],
      unidadesDebut: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.unidadesDebut : null, []],
      unidadesReales: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.unidadesReales : null, []],
      cancion: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.cancion : null, this.tipo === 'cancion' ? [Validators.required] : []],
      album: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.album : null, this.tipo === 'album' ? [Validators.required] : []],
      video: [this.chartCancionAlbumVideoArtista ? this.chartCancionAlbumVideoArtista.video : null, this.tipo === 'video' ? [Validators.required] : []],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('peak', MENSAJES_VALIDACION_PEAK_CHART_CANCION_ALBUM_VIDEO);
    this.verificarCampoFormControl('fechaDebut', MENSAJES_VALIDACION_FECHA_DEBUT_CHART_CANCION_ALBUM_VIDEO);
    this.verificarCampoFormControl('posicionDebut', MENSAJES_VALIDACION_DEBUT_CHART_CANCION_ALBUM_VIDEO);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCION_CHART_CANCION_ALBUM_VIDEO);
    this.verificarCampoFormControl('album', MENSAJES_VALIDACION_ALBUM_CHART_CANCION_ALBUM_VIDEO);
    this.verificarCampoFormControl('video', MENSAJES_VALIDACION_VIDEO_CHART_CANCION_ALBUM_VIDEO);
    this.verificarCampoFormControl('fechaPeak', []);
    this.verificarCampoFormControl('numeroSemanas', []);
    this.verificarCampoFormControl('numeroDias', []);
    this.verificarCampoFormControl('numeroSemanasPeak', []);
    this.verificarCampoFormControl('numeroDiasPeak', []);
    this.verificarCampoFormControl('numeroSemanasTop5', []);
    this.verificarCampoFormControl('numeroDiasTop5', []);
    this.verificarCampoFormControl('numeroSemanasTop10', []);
    this.verificarCampoFormControl('numeroDiasTop10', []);
    this.verificarCampoFormControl('unidadesDebut', []);
    this.verificarCampoFormControl('unidadesReales', []);
    this.verificarCampoFormControl('descripcion', []);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioChartCancionAlbumVideoArtista.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioChartCancionAlbumVideoArtista;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.chartCancionAlbumVideoArtistaValidoEnviar.emit(formulario);
          } else {
            this.chartCancionAlbumVideoArtistaValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  setearCancion(cancionSeleccionada: CancionInterface) {
    this.formularioChartCancionAlbumVideoArtista.patchValue(
      {
        cancion: cancionSeleccionada
      }
    );
  }

  setearAlbum(albumSeleccionado: AlbumInterface) {
    this.formularioChartCancionAlbumVideoArtista.patchValue(
      {
        album: albumSeleccionado
      }
    );
  }

  setearVideo(videoSeleccionado: VideoInterface) {
    this.formularioChartCancionAlbumVideoArtista.patchValue(
      {
        video: videoSeleccionado
      }
    );
  }

  setearPlaceholderTipo() {
    if (this.tipo === 'video') {
      this.placeHolderTipo = ' del video';
    }
    if (this.tipo === 'album') {
      this.placeHolderTipo = ' del album';
    }
    if (this.tipo === 'cancion') {
      this.placeHolderTipo = ' de la cancion';
    }
  }
}
