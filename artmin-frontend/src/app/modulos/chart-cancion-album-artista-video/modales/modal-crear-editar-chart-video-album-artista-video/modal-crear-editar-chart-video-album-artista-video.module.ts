import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarChartVideoAlbumArtistaVideoComponent } from './modal-crear-editar-chart-video-album-artista-video.component';
import {FormularioChartVideoAlbumArtistaVideoModule} from '../../componentes/formulario-chart-video-album-artista-video/formulario-chart-video-album-artista-video.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarChartVideoAlbumArtistaVideoComponent],
  imports: [
    CommonModule,
    FormularioChartVideoAlbumArtistaVideoModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule
  ],
  exports: [
    ModalCrearEditarChartVideoAlbumArtistaVideoComponent
  ]
})
export class ModalCrearEditarChartVideoAlbumArtistaVideoModule { }
