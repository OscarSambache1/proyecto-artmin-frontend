import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ChartInterface} from '../../../../interfaces/chart.interface';
import {ChartRestService} from '../../../../servicios/rest/chart-rest.service';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ChartCancionAlbumArtistaVideoInterface} from '../../../../interfaces/chart-cancion-album-artista-video.interface';
import {ToastErrorCargandoDatos, toastExitoCrear, toastExitoEditar} from '../../../../constantes/mensajes-toaster';
import {ChartCancionAlbumArtistaVideoRestService} from '../../../../servicios/rest/chart-cancion-album-artista-video-rest.service';
import {obtenerAnio} from '../../../../funciones/obtener-anio';

@Component({
  selector: 'app-modal-crear-editar-chart-video-album-artista-video',
  templateUrl: './modal-crear-editar-chart-video-album-artista-video.component.html',
  styleUrls: ['./modal-crear-editar-chart-video-album-artista-video.component.css']
})
export class ModalCrearEditarChartVideoAlbumArtistaVideoComponent implements OnInit {
  formularioValido: boolean;
  chartCancionAlbumVideoArtistaACrearEditar: ChartCancionAlbumArtistaVideoInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      chartCancionAlbumVideoArtista: ChartCancionAlbumArtistaVideoInterface,
      idArtista: number,
      tipo: string,
      chart: number,
      idsCancionesAlbumesVideos: number[],
    },
    public dialogo: MatDialogRef<ModalCrearEditarChartVideoAlbumArtistaVideoComponent>,
    private readonly _chartCancionAlbumArtistaVideoRestService: ChartCancionAlbumArtistaVideoRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  crearEditarCahrtCancionAlbumCancionArtista() {
    this.chartCancionAlbumVideoArtistaACrearEditar.anio = obtenerAnio(this.chartCancionAlbumVideoArtistaACrearEditar.fechaDebut);
    this._cargandoService.habilitarCargando();
    if (this.data.chartCancionAlbumVideoArtista) {
      this._chartCancionAlbumArtistaVideoRestService
        .updateOne(
          this.data.chartCancionAlbumVideoArtista.id,
          this.chartCancionAlbumVideoArtistaACrearEditar
        )
        .subscribe((respuestaChartEditado: ChartCancionAlbumArtistaVideoInterface) => {
            this._toasterService.pop(toastExitoEditar);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaChartEditado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
      this._cargandoService.deshabiltarCargando();
    } else {
      this.chartCancionAlbumVideoArtistaACrearEditar.chart = this.data.chart;
      this._chartCancionAlbumArtistaVideoRestService
        .create(
          this.chartCancionAlbumVideoArtistaACrearEditar,
        )
        .subscribe((respuestaChartCreado: ChartCancionAlbumArtistaVideoInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaChartCreado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }

  validarFormulario(chartCancionAlbumVideoArtista: ChartCancionAlbumArtistaVideoInterface | boolean) {
    if (chartCancionAlbumVideoArtista) {
      this.formularioValido = true;
      this.chartCancionAlbumVideoArtistaACrearEditar = chartCancionAlbumVideoArtista as ChartCancionAlbumArtistaVideoInterface;
    } else {
      this.formularioValido = false;
    }
  }
}
