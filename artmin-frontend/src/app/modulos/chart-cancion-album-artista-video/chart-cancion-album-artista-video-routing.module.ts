import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionChartCancionAlbumArtistaVideoComponent} from './rutas/ruta-gestion-chart-cancion-album-artista-video/ruta-gestion-chart-cancion-album-artista-video.component';


const routes: Routes = [
  {
    path: 'gestion-chart-cancion-album-artista-video',
    children: [
      {
        path: '',
        component: RutaGestionChartCancionAlbumArtistaVideoComponent
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-chart-cancion-album-artista-video',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartCancionAlbumArtistaVideoRoutingModule { }
