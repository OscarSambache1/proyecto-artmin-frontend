import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartPosicionRoutingModule } from './chart-posicion-routing.module';
import { RutaChartPosicionComponent } from './rutas/ruta-chart-posicion/ruta-chart-posicion.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {TableModule} from 'primeng';
import {ModalChartPosicionModule} from './modales/modal-chart-posicion/modal-chart-posicion.module';
import {ModalChartPosicionComponent} from './modales/modal-chart-posicion/modal-chart-posicion.component';


@NgModule({
  declarations: [RutaChartPosicionComponent],
  imports: [
    CommonModule,
    ChartPosicionRoutingModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    TableModule,
    ModalChartPosicionModule,
  ],
  entryComponents: [ModalChartPosicionComponent]
})
export class ChartPosicionModule { }
