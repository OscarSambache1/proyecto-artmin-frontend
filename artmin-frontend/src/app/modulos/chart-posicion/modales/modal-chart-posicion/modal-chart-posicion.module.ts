import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalChartPosicionComponent } from './modal-chart-posicion.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioChartPosicionModule} from '../../componentes/formulario-chart-posicion/formulario-chart-posicion.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalChartPosicionComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioChartPosicionModule,
    BotonGuardarModule,
    BotonCancelarModule
  ]
})
export class ModalChartPosicionModule { }
