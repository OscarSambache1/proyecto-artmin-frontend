import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear, toastExitoEditar} from '../../../../constantes/mensajes-toaster';
import {ChartPosicionInterface} from '../../../../interfaces/chart-posicion.interface';
import {ChartPosicionRestService} from '../../../../servicios/rest/chart-posicion-rest.service';

@Component({
  selector: 'app-modal-chart-posicion',
  templateUrl: './modal-chart-posicion.component.html',
  styleUrls: ['./modal-chart-posicion.component.css']
})
export class ModalChartPosicionComponent implements OnInit {

  formularioValido: boolean;
  chartPosicionACrearEditar: ChartPosicionInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      chartPosicion: ChartPosicionInterface,
      idArtista: number,
      tipo: string,
      chart: number,
      idObjeto: number,
      tipoObjeto: any,
      idsCancionesAlbumesVideos: number[],
    },
    public dialogo: MatDialogRef<ModalChartPosicionComponent>,
    private readonly _chartPosicionRestService: ChartPosicionRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  crearEditarChartPosicion() {
    this._cargandoService.habilitarCargando();
    if (this.data.chartPosicion) {
      this._chartPosicionRestService
        .updateOne(
          this.data.chartPosicion.id,
          this.chartPosicionACrearEditar
        )
        .subscribe((respuestaChartEditado: ChartPosicionInterface) => {
            this._toasterService.pop(toastExitoEditar);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaChartEditado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
      this._cargandoService.deshabiltarCargando();
    } else {
      this.chartPosicionACrearEditar.chart = this.data.chart as any;
      this._chartPosicionRestService
        .create(
          this.chartPosicionACrearEditar,
        )
        .subscribe((respuestaChartCreado: ChartPosicionInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaChartCreado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }

  validarFormulario(chartPosicion: ChartPosicionInterface | boolean) {
    if (chartPosicion) {
      this.formularioValido = true;
      this.chartPosicionACrearEditar = chartPosicion as ChartPosicionInterface;
    } else {
      this.formularioValido = false;
    }
  }

}
