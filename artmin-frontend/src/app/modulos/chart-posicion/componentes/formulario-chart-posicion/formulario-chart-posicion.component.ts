import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {ChartPosicionInterface} from '../../../../interfaces/chart-posicion.interface';
import {
  MENSAJES_VALIDACION_ALBUM_CHART_POSICION,
  MENSAJES_VALIDACION_CANCION_CHART_POSICION,
  MENSAJES_VALIDACION_FECHA_CHART_POSICION,
  MENSAJES_VALIDACION_POSICION_CHART_POSICION,
  MENSAJES_VALIDACION_VIDEO_CHART_POSICION
} from '../../../../constantes/validaciones-formulario/chart-posicion';


@Component({
  selector: 'app-formulario-chart-posicion',
  templateUrl: './formulario-chart-posicion.component.html',
  styleUrls: ['./formulario-chart-posicion.component.css']
})
export class FormularioChartPosicionComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;

  formularioChartPosicion: FormGroup;
  mensajesError = {
    posicion: [],
    fecha: [],
    competencia: [],
    observaciones: [],
    cancion: [],
    album: [],
    video: [],
  };
  subscribers = [];

  @Input()
  tipo: string;

  @Output()
  chartPosicionValidoEnviar: EventEmitter<ChartPosicionInterface | boolean> = new EventEmitter();

  @Input()
  chartPosicion: ChartPosicionInterface;

  placeHolderTipo = '';

  @Input()
  idArtista: number;

  @Input()
  idObjeto: number;

  @Input()
  tipoObjeto: number;

  @Input()
  idsCancionesAlbumesVideos: number[];

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.setearPlaceholderTipo();
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioChartPosicion = this._formBuilder.group({
      posicion: [this.chartPosicion ? this.chartPosicion.posicion : null, [Validators.required]],
      fecha: [this.chartPosicion ? this.chartPosicion.fecha : null, [Validators.required]],
      observaciones: [this.chartPosicion ? this.chartPosicion.observaciones : null, []],
      competencia: [this.chartPosicion ? this.chartPosicion.competencia : null, []],
      cancion: [this.chartPosicion ? this.chartPosicion.cancion : null, this.tipo === 'cancion' ? [Validators.required] : []],
      album: [this.chartPosicion ? this.chartPosicion.album : null, this.tipo === 'album' ? [Validators.required] : []],
      video: [this.chartPosicion ? this.chartPosicion.video : null, this.tipo === 'video' ? [Validators.required] : []],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('posicion', MENSAJES_VALIDACION_POSICION_CHART_POSICION);
    this.verificarCampoFormControl('fecha', MENSAJES_VALIDACION_FECHA_CHART_POSICION);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCION_CHART_POSICION);
    this.verificarCampoFormControl('album', MENSAJES_VALIDACION_ALBUM_CHART_POSICION);
    this.verificarCampoFormControl('video', MENSAJES_VALIDACION_VIDEO_CHART_POSICION);
    this.verificarCampoFormControl('observaciones', []);
    this.verificarCampoFormControl('competencia', []);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioChartPosicion.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioChartPosicion;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.chartPosicionValidoEnviar.emit(formulario);
          } else {
            this.chartPosicionValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  setearCancion(cancionSeleccionada: CancionInterface) {
    this.formularioChartPosicion.patchValue(
      {
        cancion: cancionSeleccionada
      }
    );
  }

  setearAlbum(albumSeleccionado: AlbumInterface) {
    this.formularioChartPosicion.patchValue(
      {
        album: albumSeleccionado
      }
    );
  }

  setearVideo(videoSeleccionado: VideoInterface) {
    this.formularioChartPosicion.patchValue(
      {
        video: videoSeleccionado
      }
    );
  }

  setearPlaceholderTipo() {
    if (this.tipo === 'video') {
      this.placeHolderTipo = ' del video';
    }
    if (this.tipo === 'album') {
      this.placeHolderTipo = ' del album';
    }
    if (this.tipo === 'cancion') {
      this.placeHolderTipo = ' de la cancion';
    }
  }
}
