import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioChartPosicionComponent } from './formulario-chart-posicion.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteVideoModule} from '../../../../componentes/autocomplete-video/autocomplete-video.module';
import {CalendarModule, InputNumberModule, InputTextareaModule} from 'primeng';



@NgModule({
  declarations: [FormularioChartPosicionComponent],
  exports: [
    FormularioChartPosicionComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AutocompleteCancionModule,
    AlertaValidacionCampoFormularioModule,
    AutocompleteAlbumsModule,
    AutocompleteVideoModule,
    InputNumberModule,
    CalendarModule,
    InputTextareaModule
  ]
})
export class FormularioChartPosicionModule { }
