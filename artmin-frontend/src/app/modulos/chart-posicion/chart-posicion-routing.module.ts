import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaChartPosicionComponent} from './rutas/ruta-chart-posicion/ruta-chart-posicion.component';


const routes: Routes = [
  {
    path: 'gestion-chart-posicion',
    children: [
      {
        path: '',
        component: RutaChartPosicionComponent
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-chart-posicion',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChartPosicionRoutingModule { }
