import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FestivalRoutingModule } from './festival-routing.module';
import { RutaFestivalComponent } from './rutas/ruta-festival/ruta-festival.component';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {TableModule} from 'primeng';
import {ModalCrearEditarFestivalModule} from './modales/modal-crear-editar-festival/modal-crear-editar-festival.module';
import {ModalCrearEditarFestivalComponent} from './modales/modal-crear-editar-festival/modal-crear-editar-festival.component';
import { RutaFestivalLugarComponent } from './rutas/ruta-festival-lugar/ruta-festival-lugar.component';
import {ModalFestivalLugarComponent} from './modales/modal-festival-lugar/modal-festival-lugar.component';
import {ModalFestivalLugarModule} from './modales/modal-festival-lugar/modal-festival-lugar.module';


@NgModule({
  declarations: [RutaFestivalComponent, RutaFestivalLugarComponent],
  imports: [
    CommonModule,
    FestivalRoutingModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    MigasPanModule,
    TableModule,
    ModalCrearEditarFestivalModule,
    ModalFestivalLugarModule,
  ],
  entryComponents: [ModalCrearEditarFestivalComponent, ModalFestivalLugarComponent]
})
export class FestivalModule { }
