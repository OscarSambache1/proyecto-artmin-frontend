import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioFestivalLugarComponent } from './formulario-festival-lugar.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {InputTextModule} from 'primeng';
import {AutocompleteLugarModule} from '../../../../componentes/autocomplete-lugar/autocomplete-lugar.module';



@NgModule({
  declarations: [FormularioFestivalLugarComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule,
    InputTextModule,
    AutocompleteLugarModule
  ],
  exports: [FormularioFestivalLugarComponent]
})
export class FormularioFestivalLugarModule { }
