import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {FestivalLugarInterface} from '../../../../interfaces/festival-lugar.interface';
import {MENSAJES_VALIDACION_LUGAR_FT, VALIDACION_LUGAR_FT} from '../../../../constantes/validaciones-formulario/festival-lugar';
import {LugarInterface} from '../../../../interfaces/lugar.interface';

@Component({
  selector: 'app-formulario-festival-lugar',
  templateUrl: './formulario-festival-lugar.component.html',
  styleUrls: ['./formulario-festival-lugar.component.css']
})
export class FormularioFestivalLugarComponent implements OnInit {

  formularioFestivalLugar: FormGroup;

  mensajesError = {
    lugar: [],
  };

  subscribers = [];

  @Input()
  festivalLugar: FestivalLugarInterface;

  @Input()
  width: number;

  @Input()
  idLugar: number;

  @Output()
  festivalLugarValidoEnviar: EventEmitter<FestivalLugarInterface | boolean> = new EventEmitter();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioFestivalLugar = this._formBuilder.group({
      lugar: [this.festivalLugar ? this.festivalLugar.lugar : '', VALIDACION_LUGAR_FT],
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('lugar', MENSAJES_VALIDACION_LUGAR_FT);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioFestivalLugar.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioFestivalLugar;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const formularioValido = formularioFormGroup.valid;
          if (formularioValido) {
            this.festivalLugarValidoEnviar.emit(formulario);
          } else {
            this.festivalLugarValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharLugarPadreSeleccionado(lugarSeleccinado: LugarInterface) {
    this.idLugar = lugarSeleccinado.id;
    this.formularioFestivalLugar.patchValue(
      {
        lugar: lugarSeleccinado,
      }
    );
  }
}
