import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {InputImagenComponent} from '../../../../componentes/input-imagen/input-imagen.component';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {
  MENSAJES_VALIDACION_DESCRIPCION_FESTIVAL, MENSAJES_VALIDACION_IMAGEN_FESTIVAL,
  MENSAJES_VALIDACION_NOMBRE_FESTIVAL,
  VALIDACION_DESCRIPCION_FESTIVAL,
  VALIDACION_IMAGEN_FESTIVAL,
  VALIDACION_NOMBRE_FESTIVAL
} from '../../../../constantes/validaciones-formulario/festival';
import {FestivalInterface} from '../../../../interfaces/festival.interface';

@Component({
  selector: 'app-formulario-festival',
  templateUrl: './formulario-festival.component.html',
  styleUrls: ['./formulario-festival.component.css']
})
export class FormularioFestivalComponent implements OnInit {


  formularioPremio: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    imagen: [],
  };
  subscribers = [];
  imagenSeleccionada;

  @Input()
  festival: FestivalInterface;

  @Input()
  width: number;

  @Output()
  festivalValidoEnviar: EventEmitter<FestivalInterface | boolean> = new EventEmitter();

  @ViewChild(InputImagenComponent)
  componenteInputImagen: InputImagenComponent;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioPremio = this._formBuilder.group({
      nombre: [this.festival ? this.festival.nombre : '', VALIDACION_NOMBRE_FESTIVAL],
      descripcion: [this.festival ? this.festival.descripcion : '', VALIDACION_DESCRIPCION_FESTIVAL],
      imagen: [this.festival ? this.cargarImagenTour() : '', VALIDACION_IMAGEN_FESTIVAL],
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_FESTIVAL);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_FESTIVAL);
    this.verificarCampoFormControl('imagen', MENSAJES_VALIDACION_IMAGEN_FESTIVAL);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioPremio.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioPremio;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.festivalValidoEnviar.emit(formulario);
          } else {
            this.festivalValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharEventoImagen(event: any) {
    this.imagenSeleccionada = event;
    this.formularioPremio.patchValue(
      {
        imagen: this.imagenSeleccionada,
      }
    );
  }

  cargarImagenTour() {
    if (this.festival) {
      return obtenerUrlImagenPrincipal(this.festival, 'imagenesFestival');
    }
  }
}
