import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioFestivalComponent } from './formulario-festival.component';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {InputTextareaModule, InputTextModule} from 'primeng';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [FormularioFestivalComponent],
  imports: [
    CommonModule,
    AlertaValidacionCampoFormularioModule,
    InputTextareaModule,
    InputImagenModule,
    ReactiveFormsModule,
    InputTextModule
  ],
  exports: [FormularioFestivalComponent]
})
export class FormularioFestivalModule { }
