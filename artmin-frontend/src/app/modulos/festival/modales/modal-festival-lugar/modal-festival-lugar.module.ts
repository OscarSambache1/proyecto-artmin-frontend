import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalFestivalLugarComponent } from './modal-festival-lugar.component';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioFestivalLugarModule} from '../../componentes/formulario-festival-lugar/formulario-festival-lugar.module';



@NgModule({
  declarations: [ModalFestivalLugarComponent],
  imports: [
    CommonModule,
    BotonGuardarModule,
    BotonCancelarModule,
    MatDialogModule,
    FormularioFestivalLugarModule
  ],
  exports: [
    ModalFestivalLugarComponent,
  ]
})
export class ModalFestivalLugarModule { }
