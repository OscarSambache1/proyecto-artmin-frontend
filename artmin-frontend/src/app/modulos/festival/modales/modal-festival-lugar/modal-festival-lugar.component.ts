import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {FestivalLugarInterface} from '../../../../interfaces/festival-lugar.interface';
import {FestivalLugarRestService} from '../../../../servicios/rest/festival-lugar-rest.service';

@Component({
  selector: 'app-modal-festival-lugar',
  templateUrl: './modal-festival-lugar.component.html',
  styleUrls: ['./modal-festival-lugar.component.css']
})
export class ModalFestivalLugarComponent implements OnInit {

  festivalLugarACrearEditar: FestivalLugarInterface;
  formularioFestivalLugarValido = false;
  idLugar: number;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      festivalLugar: FestivalLugarInterface,
      idFestival: number,
    },
    public dialogo: MatDialogRef<ModalFestivalLugarComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _festivalLugarRestService: FestivalLugarRestService,
  ) { }

  ngOnInit(): void {
    if (this.data.festivalLugar) {
      this.idLugar = this.data.festivalLugar.lugar?.id;
      this.festivalLugarACrearEditar = this.data.festivalLugar;
    }
  }

  validarFormulario(festivalLugar: FestivalLugarInterface | boolean) {
    if (festivalLugar) {
      this.formularioFestivalLugarValido = true;
      this.festivalLugarACrearEditar = festivalLugar as FestivalLugarInterface;
    } else {
      this.formularioFestivalLugarValido = false;
    }
  }

  crearEditarPremioAnio(): void {
    this._cargandoService.habilitarCargando();
    let festivalLugar$;
    this.festivalLugarACrearEditar.festival = this.data.idFestival as any;
    if (!this.data.festivalLugar) {
      festivalLugar$ = this._festivalLugarRestService.create(this.festivalLugarACrearEditar);
    } else {
      festivalLugar$ = this._festivalLugarRestService.updateOne(
        this.data.festivalLugar?.id,
        this.festivalLugarACrearEditar
      );
    }

    festivalLugar$.subscribe({
      next: (respuestaPremioCreado) => {
        this._toasterService.pop(toastExitoCrear);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close(respuestaPremioCreado);
      },
      error: (error) => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close();
      },
    });
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
