import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModalCrearEditarFestivalComponent} from './modal-crear-editar-festival.component';
import {FormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {FormularioFestivalModule} from '../../componentes/formulario-festival/formulario-festival.module';
import {FormularioPremioModule} from '../../../premio/componentes/formulario-premio/formulario-premio.module';


@NgModule({
  declarations: [ModalCrearEditarFestivalComponent],
  imports: [
    CommonModule,
    FormularioFestivalModule,
    FormsModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
    FormularioPremioModule,
  ],
  exports: [ModalCrearEditarFestivalComponent],
})
export class ModalCrearEditarFestivalModule {
}
