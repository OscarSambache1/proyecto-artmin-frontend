import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {CategoriaInterface} from '../../../../interfaces/categoria.interface';
import {FormularioFestivalComponent} from '../../componentes/formulario-festival/formulario-festival.component';
import {FestivalInterface} from '../../../../interfaces/festival.interface';
import {FestivalRestService} from '../../../../servicios/rest/festival-rest.service';

@Component({
  selector: 'app-modal-crear-editar-festival',
  templateUrl: './modal-crear-editar-festival.component.html',
  styleUrls: ['./modal-crear-editar-festival.component.css']
})
export class ModalCrearEditarFestivalComponent implements OnInit {
  @ViewChild(FormularioFestivalComponent)
  componenteFormularioFestival: FormularioFestivalComponent;

  festivalACrearEditar: FestivalInterface;
  formularioFestivalValido = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      festival: FestivalInterface,
    },
    public dialogo: MatDialogRef<ModalCrearEditarFestivalComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _festivalRestService: FestivalRestService,
  ) { }

  ngOnInit(): void {
    if (this.data.festival) {
      this.festivalACrearEditar = this.data.festival;
    }
  }

  validarFormulario(festival: FestivalInterface | boolean) {
    if (festival) {
      this.formularioFestivalValido = true;
      this.festivalACrearEditar = festival as FestivalInterface;
    } else {
      this.formularioFestivalValido = false;
    }
  }

  crearEditarFestival(): void {
    this._cargandoService.habilitarCargando();
    const formData: FormData = new FormData();
    formData.append('imagen', this.festivalACrearEditar.imagen);
    if (this.data.festival) {
      this.festivalACrearEditar.id = this.data.festival.id;
    }
    formData.append('datos', JSON.stringify(this.festivalACrearEditar));
    const festival$ = this._festivalRestService.crearEditarFestival(formData);

    festival$.subscribe({
      next: (respuestaFestivalCreado) => {
        this._toasterService.pop(toastExitoCrear);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close(respuestaFestivalCreado);
      },
      error: (error) => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close();
      },
    });
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
