import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaFestivalComponent} from './rutas/ruta-festival/ruta-festival.component';
import {RutaFestivalLugarComponent} from './rutas/ruta-festival-lugar/ruta-festival-lugar.component';


const routes: Routes = [
  {
    path: 'gestion-festivales',
    children: [
      {
        path: '',
        component: RutaFestivalComponent,
      },
      {
        path: 'festivales-lugar/:idFestival',
        component: RutaFestivalLugarComponent,
      }
    ]
  },
  {
    path: '',
    redirectTo: 'gestion-festivales',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FestivalRoutingModule { }
