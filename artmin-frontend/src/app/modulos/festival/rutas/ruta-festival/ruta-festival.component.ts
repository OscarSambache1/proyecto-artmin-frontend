import { Component, OnInit } from '@angular/core';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {FestivalInterface} from '../../../../interfaces/festival.interface';
import {FestivalRestService} from '../../../../servicios/rest/festival-rest.service';
import {RUTAS_FESTIVAL} from '../definicion-rutas/rutas-festival';
import {ModalCrearEditarFestivalModule} from '../../modales/modal-crear-editar-festival/modal-crear-editar-festival.module';
import {ModalCrearEditarFestivalComponent} from '../../modales/modal-crear-editar-festival/modal-crear-editar-festival.component';

@Component({
  selector: 'app-ruta-festival',
  templateUrl: './ruta-festival.component.html',
  styleUrls: ['./ruta-festival.component.css']
})
export class RutaFestivalComponent implements OnInit {
  festivales: FestivalInterface[] = [];
  busqueda = '';
  queryParams: QueryParamsInterface = {};
  ruta;
  rutaImagen = 'assets/imagenes/festival.png';
  tipo: string;
  arregloRutas: any[];
  columnasFestivales = [
    {
      field: 'imagen',
      header: 'Imagen',
      width: '20%'
    },
    {
      field: 'nombre',
      header: 'Nombre',
      width: '50%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '30%'
    },
  ];

  constructor(
    private readonly _festivalRestService: FestivalRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              esImagenPrincipal: 1,
            };
          }
          const consulta = {
            camposOr: [
              {
                nombreCampo: 'nombre',
                like: true,
                valor: this.queryParams?.consulta?.busqueda || '',
              }
            ],
            relations: [
              'imagenesFestival',
            ]
          };
          this._cargandoService.habilitarCargando();
          return this._festivalRestService.findAll(
            JSON.stringify(consulta)
          );
        })
      )
      .subscribe(
        (respuestaFestival: [FestivalInterface[], number]) => {
          this.festivales = respuestaFestival[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerUrlPrincipal(festival: FestivalInterface): string {
    return obtenerUrlImagenPrincipal(festival, 'imagenesFestival');
  }


  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarFestivales();
  }

  buscarFestivales() {
    const consulta = {
      busqueda: this.busqueda,
      esImagenPrincipal: 1,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    this.arregloRutas = [
      RUTAS_FESTIVAL.rutaGestionFestival(false, true)
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_FESTIVAL
      .rutaGestionFestival(
        false,
        true,
        []).ruta;
  }

  abrirModalCrearEditarFestival(festival?: FestivalInterface) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarFestivalComponent,
        {
          width: '1000px',
          data: {
            festival,
          },
        }
      );
    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (festivalCreadoEditado: FestivalInterface) => {
          if (festivalCreadoEditado) {
            if (!festival?.id) {
              this.festivales.unshift(festivalCreadoEditado);
            } else {
              const indice = this.festivales.findIndex((festivalArreglo: FestivalInterface) => festivalArreglo.id === festival?.id);
              this.festivales[indice] = festivalCreadoEditado;
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  irFestivalesPorLugar(rowData: any) {
    this._router
      .navigate(
        RUTAS_FESTIVAL
          .rutaFestivalLugar(
            true,
            false,
            [rowData.id]
          ),
      );
  }
}
