import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';

export const RUTAS_FESTIVAL = {
  _rutaInicioFestival: {
    ruta: 'festival-modulo',
    nombre: 'Modulo Festival',
    generarRuta: (...argumentos) => {
      return `festival-modulo`;
    }
  },

  _rutaGestionFestival: {
    ruta: 'gestion-festivales',
    nombre: 'Gestión de festivales',
    generarRuta: (...argumentos) => {
      return `gestion-festivales`;
    }
  },

  rutaGestionFestival: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioFestival,
        this._rutaGestionFestival
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioFestival,
        this._rutaGestionFestival
      ];
    }


    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaFestivalLugar: {
    ruta: 'festivales-lugar/:idFestival',
    nombre: 'Lugares',
    generarRuta: (...argumentos) => {
      return `festivales-lugar/${argumentos[0]}`;
    }
  },

  rutaFestivalLugar: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    rutaArreglo = [
      this._rutaInicioFestival,
      this._rutaGestionFestival,
      this._rutaFestivalLugar
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  // _rutaNominacionPresentacion: {
  //   ruta: 'nominacion-presentacion/:idFestivalAnio',
  //   nombre: 'Nominaciones y Presentaciones',
  //   generarRuta: (...argumentos) => {
  //     return `nominacion-presentacion/${argumentos[1]}`;
  //   }
  // },
  //
  // rutaNominacionPresentacion: function(
  //   arreglo = false,
  //   migasDePan = false,
  //   argumentos?: any[],
  // ): any {
  //   let rutaArreglo = [];
  //   rutaArreglo = [
  //     this._rutaInicioFestival,
  //     this._rutaGestionFestival,
  //     this._rutaFestivalAnio,
  //     this._rutaNominacionPresentacion,
  //   ];
  //   return generarRespuestaRuta(
  //     arreglo,
  //     migasDePan,
  //     rutaArreglo,
  //     argumentos
  //   );
  // },
};
