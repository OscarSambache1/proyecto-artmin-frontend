import { Component, OnInit } from '@angular/core';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {FestivalInterface} from '../../../../interfaces/festival.interface';
import {FestivalLugarInterface} from '../../../../interfaces/festival-lugar.interface';
import {FestivalRestService} from '../../../../servicios/rest/festival-rest.service';
import {FestivalLugarRestService} from '../../../../servicios/rest/festival-lugar-rest.service';
import {RUTAS_FESTIVAL} from '../definicion-rutas/rutas-festival';
import {ModalFestivalLugarComponent} from '../../modales/modal-festival-lugar/modal-festival-lugar.component';

@Component({
  selector: 'app-ruta-festival-lugar',
  templateUrl: './ruta-festival-lugar.component.html',
  styleUrls: ['./ruta-festival-lugar.component.css']
})
export class RutaFestivalLugarComponent implements OnInit {

  festival: FestivalInterface;
  festivalesLugar: FestivalLugarInterface[] = [];
  idFestival: number;
  busqueda = '';
  queryParams: QueryParamsInterface = {};
  ruta;
  arregloRutas: any[];
  columnasFestivales = [
    {
      field: 'lugar',
      header: 'Lugar',
      width: '50%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '30%'
    },
  ];

  constructor(
    private readonly _festivalRestService: FestivalRestService,
    private readonly _festivalLugarRestService: FestivalLugarRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idFestival = +params['idFestival'];
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          const consulta = {
            where: {
              id: +this.idFestival,
            },
            relations: [
              'imagenesFestival',
              'festivalLugares',
              'festivalLugares.lugar',
            ]
          };
          this._cargandoService.habilitarCargando();
          return this._festivalRestService.findAll(
            JSON.stringify(consulta)
          );
        })
      )
      .subscribe(
        (respuestaFestival: [FestivalInterface[], number]) => {
          this.festival = respuestaFestival[0][0];
          this.festivalesLugar = this.festival?.festivalLugares;
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  consultarFestivalLugar() {
    const consulta = {
      where: {
        id: +this.idFestival,
      },
      relations: [
        'imagenesFestival',
        'festivalLugares',
        'festivalLugares.lugar',
      ]
    };
    this._cargandoService.habilitarCargando();
    this._festivalRestService.findAll(
      JSON.stringify(consulta)
    ).subscribe(
        (respuestaFestival: [FestivalInterface[], number]) => {
          this.festival = respuestaFestival[0][0];
          this.festivalesLugar = this.festival?.festivalLugares;
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerUrlPrincipal(): string {
    return obtenerUrlImagenPrincipal(this.festival, 'imagenesFestival');
  }


  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarFestivales();
  }

  buscarFestivales() {
    const consulta = {
      busqueda: this.busqueda,
      esImagenPrincipal: 1,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    this.arregloRutas = [
      RUTAS_FESTIVAL.rutaGestionFestival(false, true, ),
      RUTAS_FESTIVAL.rutaFestivalLugar(false, true, [this.idFestival])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_FESTIVAL
      .rutaFestivalLugar(
        false,
        true,
        [this.idFestival]).ruta;
  }

  abrirModalCrearEditarFestival(festivalLugar?: FestivalLugarInterface) {
    const dialogRef = this.dialog
      .open(
        ModalFestivalLugarComponent,
        {
          width: '1000px',
          data: {
            festivalLugar,
            idFestival: this.idFestival,
          },
        }
      );
    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (festivalLugarCreadoEditado: FestivalLugarInterface) => {
          if (festivalLugarCreadoEditado) {
            this.consultarFestivalLugar();
          }
        },
        error => {
          console.error(error);
        },
      );
  }
}
