import { Component, OnInit } from '@angular/core';
import {mergeMap} from 'rxjs/operators';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {PremioAnioRestService} from '../../../../servicios/rest/premio-anio-rest.service';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import {PremioAnioInterface} from '../../../../interfaces/premio-anio.interface';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {RUTAS_PREMIO} from '../definicion-rutas/rutas-premio';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';

@Component({
  selector: 'app-ruta-nominaciones-presentaciones',
  templateUrl: './ruta-nominaciones-presentaciones.component.html',
  styleUrls: ['./ruta-nominaciones-presentaciones.component.css']
})
export class RutaNominacionesPresentacionesComponent implements OnInit {
  arregloRutas: any[];
  premio: PremioInterface;
  premioAnio: PremioAnioInterface;
  idPremio: number;
  idPremioAnio: number;
  queryParams: QueryParamsInterface = {};
  ruta;
  esPremio: 0 | 1;
  constructor(
    private readonly _premioAnioRestService: PremioAnioRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _toasterService: ToasterService
  ) { }

  ngOnInit(): void {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idPremio = +params['idPremio'];
          this.idPremioAnio = +params['idPremioAnio'];
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          const consulta = {
            where: {
              id: this.idPremioAnio,
            },
            relations: [
              'premio',
              'premio.imagenesPremio',
              'premio.premiosAnioPremio'
            ]
          };
          this._cargandoService.habilitarCargando();
          return this._premioAnioRestService.findAll(
            JSON.stringify(consulta)
          );
        })
      )
      .subscribe(
        (respuestaPremios: [PremioAnioInterface[], number]) => {
          this.premioAnio = respuestaPremios[0][0];
          this.premio = this.premioAnio.premio as PremioInterface;
          this.esPremio = this.premio.esPremio;
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  setearArregloRutasMigasPan() {
    this.arregloRutas = [
      RUTAS_PREMIO.rutaGestionPremio(false, true, ),
      RUTAS_PREMIO.rutaPremioAnio(false, true, [this.idPremio]),
      RUTAS_PREMIO.rutaNominacionPresentacion(false, true, [this.idPremio, this.idPremioAnio])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_PREMIO
      .rutaNominacionPresentacion(
        false,
        true,
        [this.idPremio, this.idPremioAnio]).ruta;
  }

  obtenerUrlPrincipal(): string {
    return obtenerUrlImagenPrincipal(this.premio, 'imagenesPremio');
  }
}
