import { Component, OnInit } from '@angular/core';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {PremioRestService} from '../../../../servicios/rest/premio-rest.service';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {RUTAS_PREMIO} from '../definicion-rutas/rutas-premio';
import {ModalCrearEditarPremioComponent} from '../../modales/modal-crear-editar-premio/modal-crear-editar-premio.component';
import {PremioAnioInterface} from '../../../../interfaces/premio-anio.interface';
import {PremioAnioRestService} from '../../../../servicios/rest/premio-anio-rest.service';
import {ModalPremioAnioComponent} from '../../modales/modal-premio-anio/modal-premio-anio.component';

@Component({
  selector: 'app-ruta-premio-anio',
  templateUrl: './ruta-premio-anio.component.html',
  styleUrls: ['./ruta-premio-anio.component.css']
})
export class RutaPremioAnioComponent implements OnInit {

  premio: PremioInterface;
  premiosAnio: PremioAnioInterface[] = [];
  idPremio: number;
  busqueda = '';
  queryParams: QueryParamsInterface = {};
  ruta;
  tipo: string;
  arregloRutas: any[];
  columnasPremios = [
    {
      field: 'fecha',
      header: 'Fecha',
      width: '50%'
    },
    {
      field: 'lugar',
      header: 'Lugar',
      width: '50%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '30%'
    },
  ];

  constructor(
    private readonly _premioRestService: PremioRestService,
    private readonly _premioAnioRestService: PremioAnioRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idPremio = +params['idPremio'];
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          const consulta = {
            where: {
            id: this.idPremio,
          },
            relations: [
              'imagenesPremio',
              'premiosAnioPremio'
            ]
          };
          this._cargandoService.habilitarCargando();
          return this._premioRestService.findAll(
            JSON.stringify(consulta)
          );
        })
      )
      .subscribe(
        (respuestaPremios: [PremioInterface[], number]) => {
          this.premio = respuestaPremios[0][0];
          this.premiosAnio = this.premio.premiosAnioPremio;
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerUrlPrincipal(): string {
    return obtenerUrlImagenPrincipal(this.premio, 'imagenesPremio');
  }


  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarPremios();
  }

  buscarPremios() {
    const consulta = {
      busqueda: this.busqueda,
      esImagenPrincipal: 1,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    this.arregloRutas = [
      RUTAS_PREMIO.rutaGestionPremio(false, true, ),
      RUTAS_PREMIO.rutaPremioAnio(false, true, [this.idPremio])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_PREMIO
      .rutaPremioAnio(
        false,
        true,
        [this.idPremio]).ruta;
  }

  abrirModalCrearEditarPremio(premioAnio?: PremioAnioInterface) {
    const dialogRef = this.dialog
      .open(
        ModalPremioAnioComponent,
        {
          width: '1000px',
          data: {
            premioAnio,
            idPremio: this.idPremio,
          },
        }
      );
    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (premioCreadoEditado: PremioAnioInterface) => {
          if (premioCreadoEditado) {
            if (!premioAnio?.id) {
              this.premiosAnio.unshift(premioCreadoEditado);
            } else {
              const indice = this.premiosAnio.findIndex((premioAnioArreglo: PremioAnioInterface) => premioAnioArreglo.id === premioAnio?.id);
              this.premiosAnio[indice] = premioCreadoEditado;
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  irNominacionesPresentaciones(rowData: any) {
    this._router
      .navigate(
        RUTAS_PREMIO
          .rutaNominacionPresentacion(
            true,
            false,
            [this.idPremio, rowData.id]
          ),
      );
  }
}
