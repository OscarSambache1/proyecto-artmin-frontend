import {Component, OnInit} from '@angular/core';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {PremioRestService} from '../../../../servicios/rest/premio-rest.service';
import {RUTAS_PREMIO} from '../definicion-rutas/rutas-premio';
import {ModalCrearEditarPremioComponent} from '../../modales/modal-crear-editar-premio/modal-crear-editar-premio.component';

@Component({
  selector: 'app-rutas-premios',
  templateUrl: './rutas-premios.component.html',
  styleUrls: ['./rutas-premios.component.css']
})
export class RutasPremiosComponent implements OnInit {
  premios: PremioInterface[] = [];
  busqueda = '';
  queryParams: QueryParamsInterface = {};
  ruta;
  rutaImagen = 'assets/imagenes/premio.svg';
  tipo: string;
  arregloRutas: any[];
  columnasPremios = [
    {
      field: 'imagen',
      header: 'Imagen',
      width: '20%'
    },
    {
      field: 'nombre',
      header: 'Nombre',
      width: '50%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '30%'
    },
  ];

  constructor(
    private readonly _premioRestService: PremioRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              esImagenPrincipal: 1,
            };
          }
          const consulta = {
            camposOr: [
              {
                nombreCampo: 'nombre',
                like: true,
                valor: this.queryParams?.consulta?.busqueda || '',
              }
            ],
            relations: [
              'imagenesPremio',
              'categoriasPremio'
            ]
          };
          this._cargandoService.habilitarCargando();
          return this._premioRestService.findAll(
            JSON.stringify(consulta)
          );
        })
      )
      .subscribe(
        (respuestaPremios: [PremioInterface[], number]) => {
          this.premios = respuestaPremios[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerUrlPrincipal(premio: PremioInterface): string {
    return obtenerUrlImagenPrincipal(premio, 'imagenesPremio');
  }


  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarPremios();
  }

  buscarPremios() {
    const consulta = {
      busqueda: this.busqueda,
      esImagenPrincipal: 1,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    this.arregloRutas = [
      RUTAS_PREMIO.rutaGestionPremio(false, true)
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_PREMIO
      .rutaGestionPremio(
        false,
        true,
        []).ruta;
  }

  abrirModalCrearEditarPremio(premio?: PremioInterface) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarPremioComponent,
        {
          width: '1000px',
          data: {
            premio,
          },
        }
      );
    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (premioCreadoEditado: PremioInterface) => {
          if (premioCreadoEditado) {
            if (!premio?.id) {
              this.premios.unshift(premioCreadoEditado);
            } else {
              const indice = this.premios.findIndex((premioArreglo: PremioInterface) => premioArreglo.id === premio?.id);
              this.premios[indice] = premioCreadoEditado;
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  irPremiosPorAnio(rowData: any) {
    this._router
      .navigate(
        RUTAS_PREMIO
          .rutaPremioAnio(
            true,
            false,
            [rowData.id]
          ),
      );
  }
}
