import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';

export const RUTAS_PREMIO = {
  _rutaInicioPremio: {
    ruta: 'premio-modulo',
    nombre: 'Modulo Premio',
    generarRuta: (...argumentos) => {
      return `premio-modulo`;
    }
  },

  _rutaGestionPremio: {
    ruta: 'gestion-premios',
    nombre: 'Gestión de premios y eventos',
    generarRuta: (...argumentos) => {
      return `gestion-premios`;
    }
  },

  rutaGestionPremio: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioPremio,
        this._rutaGestionPremio
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioPremio,
        this._rutaGestionPremio
      ];
    }


    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaPremioAnio: {
    ruta: 'premios-anio/:idPremio',
    nombre: 'Premios y Eventos por año',
    generarRuta: (...argumentos) => {
      return `premios-anio/${argumentos[0]}`;
    }
  },

  rutaPremioAnio: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    rutaArreglo = [
      this._rutaInicioPremio,
      this._rutaGestionPremio,
      this._rutaPremioAnio
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaNominacionPresentacion: {
    ruta: 'nominacion-presentacion/:idPremioAnio',
    nombre: 'Nominaciones y Presentaciones',
    generarRuta: (...argumentos) => {
      return `nominacion-presentacion/${argumentos[1]}`;
    }
  },

  rutaNominacionPresentacion: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    rutaArreglo = [
      this._rutaInicioPremio,
      this._rutaGestionPremio,
      this._rutaPremioAnio,
      this._rutaNominacionPresentacion,
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
