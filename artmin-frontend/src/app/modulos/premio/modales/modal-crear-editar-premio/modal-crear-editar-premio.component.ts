import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {FormularioPremioComponent} from '../../componentes/formulario-premio/formulario-premio.component';
import {PremioRestService} from '../../../../servicios/rest/premio-rest.service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {CategoriaInterface} from '../../../../interfaces/categoria.interface';

@Component({
  selector: 'app-modal-crear-editar-premio',
  templateUrl: './modal-crear-editar-premio.component.html',
  styleUrls: ['./modal-crear-editar-premio.component.css']
})
export class ModalCrearEditarPremioComponent implements OnInit {
  @ViewChild(FormularioPremioComponent)
  componenteFormularioPremio: FormularioPremioComponent;

  premioACrearEditar: PremioInterface;
  formularioPremioValido = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      premio: PremioInterface,
    },
    public dialogo: MatDialogRef<ModalCrearEditarPremioComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _premioRestService: PremioRestService,
  ) { }

  ngOnInit(): void {
    if (this.data.premio) {
      this.premioACrearEditar = this.data.premio;
    }
  }

  validarFormulario(premio: PremioInterface | boolean) {
    if (premio) {
      this.formularioPremioValido = true;
      this.premioACrearEditar = premio as PremioInterface;
    } else {
      this.formularioPremioValido = false;
    }
  }

  crearEditarPremio(): void {
    this._cargandoService.habilitarCargando();
    const formData: FormData = new FormData();
    formData.append('imagen', this.premioACrearEditar.imagen);
    formData.append('datos', JSON.stringify(this.premioACrearEditar));
    const premio$ = this._premioRestService.crearEditarPremio(formData);

    premio$.subscribe({
      next: (respuestaPremioCreado) => {
        this._toasterService.pop(toastExitoCrear);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close(respuestaPremioCreado);
      },
      error: (error) => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close();
      },
    });
  }


  cerrarModal() {
    this.dialogo.close();
  }

  setearCategorias(categorias: CategoriaInterface[]) {
    this.premioACrearEditar.categoriasPremio = categorias;
    this.validarFormulario(this.premioACrearEditar);
  }
}
