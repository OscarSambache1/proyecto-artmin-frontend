import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarPremioComponent } from './modal-crear-editar-premio.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {FormularioPremioModule} from '../../componentes/formulario-premio/formulario-premio.module';
import {FormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {TablaCategoriasModule} from '../../componentes/tabla-categorias/tabla-categorias.module';



@NgModule({
  declarations: [ModalCrearEditarPremioComponent],
    imports: [
        CommonModule,
        MatStepperModule,
        MatIconModule,
        FormularioPremioModule,
        FormsModule,
        MatDialogModule,
        BotonGuardarModule,
        BotonCancelarModule,
        TablaCategoriasModule,
    ],
  exports: [ModalCrearEditarPremioComponent],
})
export class ModalCrearEditarPremioModule { }
