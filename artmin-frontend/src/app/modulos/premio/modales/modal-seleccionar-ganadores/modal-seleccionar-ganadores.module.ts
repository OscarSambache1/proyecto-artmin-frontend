import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSeleccionarGanadoresComponent } from './modal-seleccionar-ganadores.component';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {PickListModule} from 'primeng';



@NgModule({
  declarations: [ModalSeleccionarGanadoresComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
    PickListModule
  ]
})
export class ModalSeleccionarGanadoresModule { }
