import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {NominacionInterface} from '../../../../interfaces/nominacion.interface';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {NominacionArtistaInterface} from '../../../../interfaces/nominacion-artista.interface';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {NominacionRestService} from '../../../../servicios/rest/nominacion-rest.service';

@Component({
  selector: 'app-modal-seleccionar-ganadores',
  templateUrl: './modal-seleccionar-ganadores.component.html',
  styleUrls: ['./modal-seleccionar-ganadores.component.css']
})
export class ModalSeleccionarGanadoresComponent implements OnInit {
  nominacionesNoGana: NominacionArtistaInterface[] = [];
  nominacionesGana: NominacionArtistaInterface[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      nominacion: NominacionInterface,
    },
    public dialogo: MatDialogRef<ModalSeleccionarGanadoresComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _nominacionRestService: NominacionRestService,
  ) { }

  ngOnInit(): void {
    this.nominacionesGana = this.data.nominacion.nominacionesCategoria.filter(nomArt => nomArt.siGano);
    this.nominacionesNoGana = this.data.nominacion.nominacionesCategoria.filter(nomArt => !nomArt.siGano);
  }

  editarGanadores() {
    const datos = {
      idNominacionesGana: this.nominacionesGana.map(nom => nom.id),
      idNominacionesNoGana: this.nominacionesNoGana.map(nom => nom.id),
    };
    this._nominacionRestService
      .seleccionarGanadores(
        datos
      )
      .subscribe((respuesta: any) => {
          this._toasterService.pop(toastExitoCrear);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close();
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close();
        });
  }


  cerrarModal() {
    this.dialogo.close();
  }
}
