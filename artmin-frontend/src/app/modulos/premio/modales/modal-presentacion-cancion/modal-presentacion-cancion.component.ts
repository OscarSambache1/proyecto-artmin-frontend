import {Component, Inject, OnInit} from '@angular/core';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PresentacionInterface} from '../../../../interfaces/presentacion.interface';

@Component({
  selector: 'app-modal-presentacion-cancion',
  templateUrl: './modal-presentacion-cancion.component.html',
  styleUrls: ['./modal-presentacion-cancion.component.css']
})
export class ModalPresentacionCancionComponent implements OnInit {
  idsCanciones: number[] = [];
  cancionSeleccionada: CancionInterface;
  nombreCancion: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
    },
    public dialogo: MatDialogRef<ModalPresentacionCancionComponent>,
  ) { }

  ngOnInit(): void {
  }

  agregarCancion() {
    this.dialogo.close(
      {
        cancion: this.cancionSeleccionada,
        nombreCancion: this.nombreCancion,
      }
    );
  }

  cerrarModal() {
    this.dialogo.close();
  }

  setearCanciones(cancionSeleccionada: CancionInterface) {
    this.cancionSeleccionada = cancionSeleccionada;
    this.nombreCancion = this.cancionSeleccionada.nombre;
  }
}
