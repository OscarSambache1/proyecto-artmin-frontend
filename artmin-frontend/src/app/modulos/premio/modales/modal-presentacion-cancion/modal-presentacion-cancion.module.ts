import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalPresentacionCancionComponent } from './modal-presentacion-cancion.component';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {InputTextModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [ModalPresentacionCancionComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
    AutocompleteCancionModule,
    InputTextModule,
    FormsModule
  ],
  exports: [ModalPresentacionCancionComponent]
})
export class ModalPresentacionCancionModule { }
