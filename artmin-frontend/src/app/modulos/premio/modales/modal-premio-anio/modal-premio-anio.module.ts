import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalPremioAnioComponent } from './modal-premio-anio.component';
import {FormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {FormularioPremioAnioModule} from '../../componentes/formulario-premio-anio/formulario-premio-anio.module';



@NgModule({
  declarations: [ModalPremioAnioComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
    FormularioPremioAnioModule,
  ],
  exports: [ModalPremioAnioComponent]
})
export class ModalPremioAnioModule { }
