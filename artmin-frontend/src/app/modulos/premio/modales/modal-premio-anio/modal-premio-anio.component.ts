import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {FormularioPremioAnioComponent} from '../../componentes/formulario-premio-anio/formulario-premio-anio.component';
import {PremioAnioInterface} from '../../../../interfaces/premio-anio.interface';
import {PremioAnioRestService} from '../../../../servicios/rest/premio-anio-rest.service';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-premio-anio',
  templateUrl: './modal-premio-anio.component.html',
  styleUrls: ['./modal-premio-anio.component.css']
})
export class ModalPremioAnioComponent implements OnInit {

  @ViewChild(FormularioPremioAnioComponent)
  componenteFormularioPremioAnio: FormularioPremioAnioComponent;

  premioAnioACrearEditar: PremioAnioInterface;
  formularioPremioAnioValido = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      premioAnio: PremioAnioInterface,
      idPremio: number,
    },
    public dialogo: MatDialogRef<ModalPremioAnioComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _premioAnioRestService: PremioAnioRestService,
  ) { }

  ngOnInit(): void {
    if (this.data.premioAnio) {
      this.premioAnioACrearEditar = this.data.premioAnio;
    }
  }

  validarFormulario(premioAnio: PremioAnioInterface | boolean) {
    if (premioAnio) {
      this.formularioPremioAnioValido = true;
      this.premioAnioACrearEditar = premioAnio as PremioAnioInterface;
    } else {
      this.formularioPremioAnioValido = false;
    }
  }

  crearEditarPremioAnio(): void {
    this._cargandoService.habilitarCargando();
    let premioAnio$;
    this.premioAnioACrearEditar.premio = this.data.idPremio;
    const anio = moment(this.premioAnioACrearEditar.fecha).year();
    this.premioAnioACrearEditar.anio = anio;
    if (!this.data.premioAnio) {
      premioAnio$ = this._premioAnioRestService.create(this.premioAnioACrearEditar);
    } else {
      premioAnio$ = this._premioAnioRestService.updateOne(
        this.data.premioAnio?.id,
        this.premioAnioACrearEditar
      );
    }

    premioAnio$.subscribe({
      next: (respuestaPremioCreado) => {
        this._toasterService.pop(toastExitoCrear);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close(respuestaPremioCreado);
      },
      error: (error) => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
        this._cargandoService.deshabiltarCargando();
        this.dialogo.close();
      },
    });
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
