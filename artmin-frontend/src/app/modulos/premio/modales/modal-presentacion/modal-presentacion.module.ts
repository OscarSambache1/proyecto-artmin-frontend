import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalPresentacionComponent } from './modal-presentacion.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioPresentacionModule} from '../../componentes/formulario-presentacion/formulario-presentacion.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalPresentacionComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioPresentacionModule,
    BotonGuardarModule,
    BotonCancelarModule
  ],
  exports: [ModalPresentacionComponent]
})
export class ModalPresentacionModule { }
