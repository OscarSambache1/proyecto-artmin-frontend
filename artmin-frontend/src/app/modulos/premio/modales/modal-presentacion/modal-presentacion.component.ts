import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {PresentacionInterface} from '../../../../interfaces/presentacion.interface';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {PresentacionRestService} from '../../../../servicios/rest/presentacion-rest.service';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';

@Component({
  selector: 'app-modal-presentacion',
  templateUrl: './modal-presentacion.component.html',
  styleUrls: ['./modal-presentacion.component.css']
})
export class ModalPresentacionComponent implements OnInit {

  presentacionACrearEditar: PresentacionInterface;
  formularioPresentacionValido = false;
  idsArtistas: number[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      presentacion: PresentacionInterface,
      idPremio: number,
      idPremioAnio: number,
      mostrarPremio: boolean,
    },
    public dialogo: MatDialogRef<ModalPresentacionComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _presentacionRestService: PresentacionRestService,
  ) {
  }

  ngOnInit(): void {
    if (this.data.presentacion) {
      this.data.presentacion.presentacionesArtista.map(preArt => {
        const idArtista = (preArt.artista as ArtistaInterface).id;
        this.idsArtistas.push(idArtista);
      });
    }
  }


  validarFormulario(presentacion: PresentacionInterface | boolean) {
    if (presentacion) {
      this.formularioPresentacionValido = true;
      this.presentacionACrearEditar = presentacion as PresentacionInterface;
    } else {
      this.formularioPresentacionValido = false;
    }
  }

  crearEditarNominacion(): void {
    const idPremioAnio = this.data.mostrarPremio ? this.presentacionACrearEditar.premioAnio?.id : this.data.idPremioAnio;

    const datos = {
      idPresentacion: this.data.presentacion?.id,
      ...this.presentacionACrearEditar,
      idPremioAnio,
    };
    this._presentacionRestService
      .crearEditarPresentaacion(
        datos
      )
      .subscribe((respuesta: any) => {
          this._toasterService.pop(toastExitoCrear);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close();
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close();
        });
  }


  cerrarModal() {
    this.dialogo.close();
  }
}
