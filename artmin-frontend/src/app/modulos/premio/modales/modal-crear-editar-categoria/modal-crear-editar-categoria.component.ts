import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {CategoriaInterface} from '../../../../interfaces/categoria.interface';

@Component({
  selector: 'app-modal-crear-editar-categoria',
  templateUrl: './modal-crear-editar-categoria.component.html',
  styleUrls: ['./modal-crear-editar-categoria.component.css']
})
export class ModalCrearEditarCategoriaComponent implements OnInit {

  categoriaACrearEditar: CategoriaInterface;
  formularioCategoriaValido = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      categoria: CategoriaInterface,
    },
    public dialogo: MatDialogRef<ModalCrearEditarCategoriaComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  validarFormulario(categoria: CategoriaInterface | boolean) {
    if (categoria) {
      this.formularioCategoriaValido = true;
      this.categoriaACrearEditar = categoria as CategoriaInterface;
    } else {
      this.formularioCategoriaValido = false;
    }
  }

  crearEditarCategoria(): void {
    if (this.data?.categoria) {
      this.categoriaACrearEditar.id = this.data.categoria.id;
    }
    this.dialogo.close(this.categoriaACrearEditar);
  }


  cerrarModal() {
    this.dialogo.close();
  }

}
