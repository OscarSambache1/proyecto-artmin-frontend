import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarCategoriaComponent } from './modal-crear-editar-categoria.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioCategoriaModule} from '../../componentes/formulario-categoria/formulario-categoria.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarCategoriaComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioCategoriaModule,
    BotonGuardarModule,
    BotonCancelarModule
  ]
})
export class ModalCrearEditarCategoriaModule { }
