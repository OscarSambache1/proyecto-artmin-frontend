import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {NominacionInterface} from '../../../../interfaces/nominacion.interface';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {NominacionRestService} from '../../../../servicios/rest/nominacion-rest.service';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {ArtistaTourInterface} from '../../../../interfaces/artista-tour.interface';

@Component({
  selector: 'app-modal-nominacion',
  templateUrl: './modal-nominacion.component.html',
  styleUrls: ['./modal-nominacion.component.css']
})
export class ModalNominacionComponent implements OnInit {
  nominacionACrearEditar: NominacionInterface;
  formularioNominacionValido = false;
  idsArtistas: number[] = [];
  idsCanciones = [];
  idsAlbumes = [];
  idsTours = [];
  idsVideos = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      nominacion: NominacionInterface,
      idPremio: number,
      idPremioAnio: number,
      mostrarPremio: boolean,
      tipos: string[],
    },
    public dialogo: MatDialogRef<ModalNominacionComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _nominacionRestService: NominacionRestService,
  ) {
  }

  ngOnInit(): void {
    if (this.data.nominacion) {
      const tipo = this.data.nominacion.categoria.tipo.toLowerCase();
      this.data.nominacion[tipo] = [];
      this.data.nominacion.nominacionesCategoria.map(nomArt => {
        this.data.nominacion[tipo].push(nomArt[tipo]);
        if (tipo === 'artista') {
          this.idsArtistas.push((nomArt.artista as ArtistaInterface).id);
        }
        if (tipo === 'cancion') {
          const cancion = nomArt.cancion as CancionInterface;
          const artistas = cancion.artistasCancion;
          this.idsCanciones.push(cancion.id);
          artistas.map(artistaCancion => {
            this.idsArtistas.push((artistaCancion.artista as ArtistaInterface).id);
          });
        }
        if (tipo === 'album') {
          const album = nomArt.album as AlbumInterface;
          const artistas = album.artistasAlbum;
          this.idsAlbumes.push(album.id);
          artistas.map(artistaCancion => {
            this.idsArtistas.push((artistaCancion.artista as ArtistaInterface).id);
          });
        }
        if (tipo === 'video') {
          const video = nomArt.video as VideoInterface;
          const cancion = video.cancion as CancionInterface;
          const artistas = cancion.artistasCancion;
          this.idsVideos.push(video.id);
          artistas.map(artistaCancion => {
            this.idsArtistas.push((artistaCancion.artista as ArtistaInterface).id);
          });
        }
        if (tipo === 'tour') {
          const tour = nomArt.tour as TourInterface;
          const artistas = tour.artistasTour as ArtistaTourInterface[];
          this.idsTours.push(tour.id);
          artistas.map(artistaCancion => {
            this.idsArtistas.push((artistaCancion.artista as ArtistaInterface).id);
          });
        }
      });
    }
  }

  validarFormulario(nominacion: NominacionInterface | boolean) {
    if (nominacion) {
      this.formularioNominacionValido = true;
      this.nominacionACrearEditar = nominacion as NominacionInterface;
    } else {
      this.formularioNominacionValido = false;
    }
  }

  crearEditarNominacion(): void {
    const idCategoria = this.nominacionACrearEditar.categoria?.id;
    const tipoCategoria = this.nominacionACrearEditar.categoria?.tipo;
    const idPremioAnio = this.data.mostrarPremio ? this.nominacionACrearEditar.premioAnio?.id : this.data.idPremioAnio;
    const idPremio = this.data.mostrarPremio ? (this.nominacionACrearEditar.premioAnio?.premio as any)?.id : this.data.idPremio;
    const nominacionesCategoria = this.nominacionACrearEditar[tipoCategoria.toLowerCase()].map(artista => {
      return {
        [tipoCategoria.toLowerCase()]: artista.id,
      };
    });
    const datos = {
      idPremioAnio,
      idPremio,
      idCategoria,
      idNominacion: this.data.nominacion?.id,
      tipoCategoria,
      nominacionesCategoria,
      nominacionACrear: {
        id: this.data.nominacion?.id,
        premioAnio: idPremioAnio,
        categoria: idCategoria,
      },
      artistas: this.nominacionACrearEditar.artista,
    };
    this._nominacionRestService
      .crearEditarNominacion(
        datos
      )
      .subscribe((respuesta: any) => {
          this._toasterService.pop(toastExitoCrear);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close();
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close();
        });
  }


  cerrarModal() {
    this.dialogo.close();
  }

}

export interface CreaNominacionesInterface {
  idPremioAnio: number;
  idPremio: number;
  idCategoria: number;
  idNominacion: null;
  tipoCategoria: string;
  nominacionesCategoria: any[];
  nominacionACrear: NominacionInterface;
}
