import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalNominacionComponent } from './modal-nominacion.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioNominacionModule} from '../../componentes/formulario-nominacion/formulario-nominacion.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalNominacionComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioNominacionModule,
    BotonGuardarModule,
    BotonCancelarModule,
  ],
  exports: [ModalNominacionComponent]
})
export class ModalNominacionModule { }
