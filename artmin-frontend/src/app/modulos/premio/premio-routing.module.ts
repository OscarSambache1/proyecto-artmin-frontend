import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutasPremiosComponent} from './rutas/rutas-premios/rutas-premios.component';
import {RutaPremioAnioComponent} from './rutas/ruta-premio-anio/ruta-premio-anio.component';
import {RutaNominacionesPresentacionesComponent} from './rutas/ruta-nominaciones-presentaciones/ruta-nominaciones-presentaciones.component';


const routes: Routes = [
  {
    path: 'gestion-premios',
    children: [
      {
        path: '',
        component: RutasPremiosComponent
      },
      {
        path: 'premios-anio/:idPremio',
        children: [
          {
            path: '',
            component: RutaPremioAnioComponent,
          },
        ]
      },
      {
        path: 'premios-anio/:idPremio/nominacion-presentacion/:idPremioAnio',
        children: [
          {
            path: '',
            component: RutaNominacionesPresentacionesComponent,
          },
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: 'gestion-premios',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PremioRoutingModule { }
