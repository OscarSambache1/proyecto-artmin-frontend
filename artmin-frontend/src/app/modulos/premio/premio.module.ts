import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PremioRoutingModule } from './premio-routing.module';
import { RutasPremiosComponent } from './rutas/rutas-premios/rutas-premios.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {TableModule} from 'primeng';
import {ModalCrearEditarPremioComponent} from './modales/modal-crear-editar-premio/modal-crear-editar-premio.component';
import {ModalCrearEditarPremioModule} from './modales/modal-crear-editar-premio/modal-crear-editar-premio.module';
import { RutaPremioAnioComponent } from './rutas/ruta-premio-anio/ruta-premio-anio.component';
import {ModalPremioAnioComponent} from './modales/modal-premio-anio/modal-premio-anio.component';
import {ModalPremioAnioModule} from './modales/modal-premio-anio/modal-premio-anio.module';
import { RutaNominacionesPresentacionesComponent } from './rutas/ruta-nominaciones-presentaciones/ruta-nominaciones-presentaciones.component';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {SeccionNominacionesModule} from './componentes/seccion-nominaciones/seccion-nominaciones.module';
import {SeccionPresentacionesModule} from './componentes/seccion-presentaciones/seccion-presentaciones.module';


@NgModule({
  declarations: [RutasPremiosComponent, RutaPremioAnioComponent, RutaNominacionesPresentacionesComponent],
  imports: [
    CommonModule,
    PremioRoutingModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    TableModule,
    ModalCrearEditarPremioModule,
    ModalPremioAnioModule,
    MatStepperModule,
    MatIconModule,
    SeccionNominacionesModule,
    SeccionPresentacionesModule,
  ],
  entryComponents: [ModalCrearEditarPremioComponent, ModalPremioAnioComponent]
})
export class PremioModule { }
