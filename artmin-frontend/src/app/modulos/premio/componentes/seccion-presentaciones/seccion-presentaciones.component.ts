import {Component, Input, OnInit} from '@angular/core';
import {ModalPresentacionComponent} from '../../modales/modal-presentacion/modal-presentacion.component';
import {MatDialog} from '@angular/material/dialog';
import {PresentacionInterface} from '../../../../interfaces/presentacion.interface';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {ToasterService} from 'angular2-toaster';
import {PresentacionRestService} from '../../../../servicios/rest/presentacion-rest.service';

@Component({
  selector: 'app-seccion-presentaciones',
  templateUrl: './seccion-presentaciones.component.html',
  styleUrls: ['./seccion-presentaciones.component.css']
})
export class SeccionPresentacionesComponent implements OnInit {
  presentaciones: PresentacionInterface[] = [];
  columnas = [
    {
      field: 'artistas',
      header: 'Artistas',
      width: '40%'
    },
    {
      field: 'canciones',
      header: 'Canciones',
      width: '40%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '20%'
    },
  ];

  @Input()
  idPremioAnio: number;

  constructor(
    public dialog: MatDialog,
    private readonly _presentacionRestService: PresentacionRestService,
    private readonly _toasterService: ToasterService
  ) { }

  ngOnInit(): void {
    this.cargarPresentaciones();
  }

  abrirModalCrearEditarPresentacion(presentacion?: PresentacionInterface) {
    const dialogRef = this.dialog
      .open(
        ModalPresentacionComponent,
        {
          width: '1000px',
          data: {
            presentacion,
            idPremioAnio: this.idPremioAnio,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (res: any) => {
          this.cargarPresentaciones();
        },
        error => {
          console.error(error);
        },
      );
  }

  private cargarPresentaciones() {
    const consulta = {
      where: {
        premioAnio: this.idPremioAnio,
      },
      relations: [
        'premioAnio',
        'presentacionesArtista',
        'presentacionesArtista.artista',
        'presentacionesCancion',
        'presentacionesCancion.cancion',
      ]
    };
    this._presentacionRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuesta: [PresentacionInterface[], number]) => {
          this.presentaciones = respuesta[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

}
