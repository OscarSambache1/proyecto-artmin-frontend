import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SeccionPresentacionesComponent} from './seccion-presentaciones.component';
import {BotonNuevoModule} from '../../../../componentes/boton-nuevo/boton-nuevo.module';
import {ModalPresentacionModule} from '../../modales/modal-presentacion/modal-presentacion.module';
import {ModalPresentacionComponent} from '../../modales/modal-presentacion/modal-presentacion.component';
import {TableModule} from 'primeng';


@NgModule({
  declarations: [SeccionPresentacionesComponent],
    imports: [
        CommonModule,
        BotonNuevoModule,
        ModalPresentacionModule,
        TableModule,
    ],
  exports: [SeccionPresentacionesComponent],
  entryComponents: [ModalPresentacionComponent]
})
export class SeccionPresentacionesModule {
}
