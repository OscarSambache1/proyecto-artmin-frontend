import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioPresentacionComponent } from './formulario-presentacion.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AutocompleteArtistaModule} from '../../../../componentes/autocomplete-artista/autocomplete-artista.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {TableModule} from 'primeng';
import {ModalPresentacionCancionModule} from '../../modales/modal-presentacion-cancion/modal-presentacion-cancion.module';
import {ModalPresentacionCancionComponent} from '../../modales/modal-presentacion-cancion/modal-presentacion-cancion.component';
import {AutocompletePremioModule} from '../../../../componentes/autocomplete-premio/autocomplete-premio.module';
import {AutocompletePremioAnioModule} from '../../../../componentes/autocomplete-premio-anio/autocomplete-premio-anio.module';



@NgModule({
  declarations: [FormularioPresentacionComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AutocompleteArtistaModule,
    AlertaValidacionCampoFormularioModule,
    TableModule,
    ModalPresentacionCancionModule,
    AutocompletePremioModule,
    AutocompletePremioAnioModule,
  ],
  exports: [FormularioPresentacionComponent],
  entryComponents: [
    ModalPresentacionCancionComponent,
  ]
})
export class FormularioPresentacionModule { }
