import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ToasterService} from 'angular2-toaster';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {TourRestService} from '../../../../servicios/rest/tour-rest.service';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {PresentacionInterface} from '../../../../interfaces/presentacion.interface';
import {
  MENSAJES_VALIDACION_ARTISTAS_PRES, MENSAJES_VALIDACION_PMEMIO_ANIO_PRES,
  MENSAJES_VALIDACION_PREMIO_PRES
} from '../../../../constantes/validaciones-formulario/presentacion-premio';
import {PresentacionCancionInterface} from '../../../../interfaces/presentacion-cancion.interface';
import {ModalPresentacionCancionComponent} from '../../modales/modal-presentacion-cancion/modal-presentacion-cancion.component';
import {MatDialog} from '@angular/material/dialog';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {
  MENSAJES_VALIDACION_PMEMIO_ANIO_NOM,
  MENSAJES_VALIDACION_PREMIO_NOM
} from '../../../../constantes/validaciones-formulario/nominacion-premio';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {PremioAnioInterface} from '../../../../interfaces/premio-anio.interface';

@Component({
  selector: 'app-formulario-presentacion',
  templateUrl: './formulario-presentacion.component.html',
  styleUrls: ['./formulario-presentacion.component.css']
})
export class FormularioPresentacionComponent implements OnInit {


  formularioNominacion: FormGroup;
  mensajesError = {
    premio: [],
    premioAnio: [],
    artista: [],
  };
  subscribers = [];

  presentacionCanciones: PresentacionCancionInterface[] = [];

  columnas = [
    {
      field: 'cancion',
      header: 'Canción',
      width: '30%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '20%'
    },
  ];

  idCategoria: number;

  @Input()
  presentacion: PresentacionInterface;

  @Input()
  width: number;

  @Input()
  idsArtista: number[] = [];

  @Input()
  idPremio = -1;

  @Input()
  idPremioAnio = 0;

  @Input()
  mostrarPremio = false;

  @Output()
  presentacionValidoEnviar: EventEmitter<PresentacionInterface | boolean> = new EventEmitter();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _artistaService: ArtistaRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _cancionRestService: CancionRestService,
    private readonly _videoRestService: VideoRestService,
    private readonly _tourRestService: TourRestService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    if (this.presentacion) {
      this.presentacionCanciones = this.presentacion.presentacionesCancion;
      if (this.mostrarPremio) {
        this.idPremio = (this.presentacion?.premioAnio?.premio as any)?.id;
        this.idPremioAnio = this.presentacion?.premioAnio?.id;
      }
    }
    this.formularioNominacion = this._formBuilder.group({
      premio: [this.presentacion ? this.presentacion.premioAnio?.premio : '', this.mostrarPremio ? [Validators.required] : []],
      premioAnio: [this.presentacion ? this.presentacion.premioAnio : '', this.mostrarPremio ? [Validators.required] : []],
      artista: [this.presentacion ? this.presentacion.artista : [], [Validators.required]],
      canciones: [this.presentacion ? this.presentacion.canciones : [], [Validators.required]],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('artista', MENSAJES_VALIDACION_ARTISTAS_PRES);
    this.verificarCampoFormControl('premio', MENSAJES_VALIDACION_PREMIO_PRES);
    this.verificarCampoFormControl('premioAnio', MENSAJES_VALIDACION_PMEMIO_ANIO_PRES);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioNominacion.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioNominacion;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.presentacionValidoEnviar.emit(formulario);
          } else {
            this.presentacionValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  buscarCancionesPorArtista(respuestaArtista: ArtistaInterface[]) {
    this.formularioNominacion.patchValue(
      {
        artista: [
          ...respuestaArtista]
      }
    );
  }

  obtenerIdsArtistasSeleccionados() {
    const existeCampoArtistas = this.formularioNominacion.get('artista').value;
    if (existeCampoArtistas) {
      return this.formularioNominacion.get('artista').value.map(artista => artista.id);
    }
  }

  eliminarCancion(rowData: any, index: any) {
    this.presentacionCanciones.splice(index, 1);
    this.formularioNominacion.patchValue(
      {
        canciones: this.presentacionCanciones,
      }
    );
  }

  agregarCancion(rowData?: any) {
    const dialogRef = this.dialog
      .open(
        ModalPresentacionCancionComponent,
        {
          width: '1000px',
          data: {},
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (res: any) => {
          if (res) {
            const estaCancion = this.presentacionCanciones.find(presCancion => (presCancion?.cancion as CancionInterface)?.id === res?.cancion?.id);
            if (!estaCancion) {
              this.presentacionCanciones.push(res);
            }
            this.formularioNominacion.patchValue(
              {
                canciones: this.presentacionCanciones,
              }
            );
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  setearPremio(premio: PremioInterface) {
    this.idPremio = premio.id;
    this.formularioNominacion.patchValue(
      {
        premio,
        premioAnio: null,
      }
    );
  }

  setearPremioAnio(premioAnio: PremioAnioInterface) {
    this.idPremioAnio = premioAnio?.id;
    this.formularioNominacion.patchValue(
      {
        premioAnio,
      }
    );
  }
}
