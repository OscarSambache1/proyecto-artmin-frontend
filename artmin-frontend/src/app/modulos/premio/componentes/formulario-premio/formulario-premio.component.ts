import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {InputImagenComponent} from '../../../../componentes/input-imagen/input-imagen.component';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {
  MENSAJES_VALIDACION_DESCRIPCION_PREMIO, MENSAJES_VALIDACION_IMAGEN_PREMIO,
  MENSAJES_VALIDACION_NOMBRE_PREMIO,
  VALIDACION_DESCRIPCION_PREMIO,
  VALIDACION_IMAGEN_PREMIO,
  VALIDACION_NOMBRE_PREMIO
} from '../../../../constantes/validaciones-formulario/premio';

@Component({
  selector: 'app-formulario-premio',
  templateUrl: './formulario-premio.component.html',
  styleUrls: ['./formulario-premio.component.css']
})
export class FormularioPremioComponent implements OnInit {

  formularioPremio: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    imagen: [],
    esPremio: [],
  };
  subscribers = [];
  imagenSeleccionada;

  @Input()
  premio: PremioInterface;

  @Input()
  width: number;

  @Output()
  premioValidoEnviar: EventEmitter<PremioInterface | boolean> = new EventEmitter();

  @ViewChild(InputImagenComponent)
  componenteInputImagen: InputImagenComponent;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioPremio = this._formBuilder.group({
      nombre: [this.premio ? this.premio.nombre : '', VALIDACION_NOMBRE_PREMIO],
      descripcion: [this.premio ? this.premio.descripcion : '', VALIDACION_DESCRIPCION_PREMIO],
      imagen: [this.premio ? this.cargarImagenTour() : '', VALIDACION_IMAGEN_PREMIO],
      esPremio: [this.premio ? this.premio.esPremio : 0, []]
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_PREMIO);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_PREMIO);
    this.verificarCampoFormControl('imagen', MENSAJES_VALIDACION_IMAGEN_PREMIO);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioPremio.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioPremio;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.premioValidoEnviar.emit(formulario);
          } else {
            this.premioValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharEventoImagen(event: any) {
    this.imagenSeleccionada = event;
    this.formularioPremio.patchValue(
      {
        imagen: this.imagenSeleccionada,
      }
    );
  }

  cargarImagenTour() {
    if (this.premio) {
      return obtenerUrlImagenPrincipal(this.premio, 'imagenesPremio');
    }
  }
}
