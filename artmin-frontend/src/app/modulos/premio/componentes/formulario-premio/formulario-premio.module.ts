import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioPremioComponent } from './formulario-premio.component';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InputSwitchModule, InputTextareaModule, InputTextModule} from 'primeng';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';



@NgModule({
  declarations: [FormularioPremioComponent],
  exports: [
    FormularioPremioComponent
  ],
    imports: [
        CommonModule,
        InputImagenModule,
        ReactiveFormsModule,
        InputTextModule,
        AlertaValidacionCampoFormularioModule,
        InputTextareaModule,
        FormsModule,
        InputSwitchModule,
    ]
})
export class FormularioPremioModule { }
