import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SeccionNominacionesComponent} from './seccion-nominaciones.component';
import {BotonNuevoModule} from '../../../../componentes/boton-nuevo/boton-nuevo.module';
import {ModalNominacionModule} from '../../modales/modal-nominacion/modal-nominacion.module';
import {ModalNominacionComponent} from '../../modales/modal-nominacion/modal-nominacion.component';
import {TableModule} from 'primeng';
import {ModalSeleccionarGanadoresModule} from '../../modales/modal-seleccionar-ganadores/modal-seleccionar-ganadores.module';
import {ModalSeleccionarGanadoresComponent} from '../../modales/modal-seleccionar-ganadores/modal-seleccionar-ganadores.component';


@NgModule({
  declarations: [SeccionNominacionesComponent],
  imports: [
    CommonModule,
    BotonNuevoModule,
    ModalNominacionModule,
    ModalSeleccionarGanadoresModule,
    TableModule,
  ],
  exports: [SeccionNominacionesComponent],
  entryComponents: [ModalNominacionComponent, ModalSeleccionarGanadoresComponent]
})
export class SeccionNominacionesModule {
}
