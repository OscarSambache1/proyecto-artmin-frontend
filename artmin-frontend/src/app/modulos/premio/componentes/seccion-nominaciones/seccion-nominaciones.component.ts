import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ModalNominacionComponent} from '../../modales/modal-nominacion/modal-nominacion.component';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {NominacionRestService} from '../../../../servicios/rest/nominacion-rest.service';
import {NominacionInterface} from '../../../../interfaces/nominacion.interface';
import {ToasterService} from 'angular2-toaster';
import {NominacionArtistaInterface} from '../../../../interfaces/nominacion-artista.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {ModalSeleccionarGanadoresComponent} from '../../modales/modal-seleccionar-ganadores/modal-seleccionar-ganadores.component';

@Component({
  selector: 'app-seccion-nominaciones',
  templateUrl: './seccion-nominaciones.component.html',
  styleUrls: ['./seccion-nominaciones.component.css']
})
export class SeccionNominacionesComponent implements OnInit {
  nominaciones: NominacionInterface[] = [];
  columnas = [
    {
      field: 'categoria',
      header: 'Categoría',
      width: '30%'
    },
    {
      field: 'nominaciones',
      header: 'Nominaciones',
      width: '50%'
    },
    {
      field: 'ganadores',
      header: 'Ganadores',
      width: '20%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '20%'
    },
  ];

  @Input()
  idPremio: number;

  @Input()
  idPremioAnio: number;

  constructor(
    public dialog: MatDialog,
    private readonly _nominacionRestService: NominacionRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    this.cargarNominaciones();
  }

  abrirModalCrearEditarNominacion(nominacion?: NominacionInterface) {
    const dialogRef = this.dialog
      .open(
        ModalNominacionComponent,
        {
          width: '1000px',
          data: {
            idPremio: this.idPremio,
            idPremioAnio: this.idPremioAnio,
            nominacion,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (res: any) => {
          this.cargarNominaciones();
        },
        error => {
          console.error(error);
        },
      );
  }

  private cargarNominaciones() {
    const consulta = {
      where: {
        premioAnio: this.idPremioAnio,
      },
      relations: [
        'categoria',
        'premioAnio',
        'nominacionesCategoria',
        'nominacionesCategoria.artista',
        'nominacionesCategoria.album',
        'nominacionesCategoria.album.artistasAlbum',
        'nominacionesCategoria.album.artistasAlbum.artista',
        'nominacionesCategoria.cancion',
        'nominacionesCategoria.cancion.artistasCancion',
        'nominacionesCategoria.cancion.artistasCancion.artista',
        'nominacionesCategoria.tour',
        'nominacionesCategoria.tour.artistasTour',
        'nominacionesCategoria.tour.artistasTour.artista',
        'nominacionesCategoria.video',
        'nominacionesCategoria.video.cancion',
        'nominacionesCategoria.video.cancion.artistasCancion',
        'nominacionesCategoria.video.cancion.artistasCancion.artista',
      ]
    };
    this._nominacionRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuesta: [NominacionInterface[], number]) => {
          this.nominaciones = respuesta[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  setearNominacion(nominacion: NominacionArtistaInterface) {
    if (nominacion.artista) {
      return (nominacion.artista as ArtistaInterface)?.nombre;
    }
    if (nominacion.album) {
      return (nominacion.album as AlbumInterface)?.nombre;
    }
    if (nominacion.cancion) {
      return (nominacion.cancion as CancionInterface)?.nombre;
    }
    if (nominacion.video) {
      return (nominacion.video as VideoInterface)?.nombre;
    }
    if (nominacion.tour) {
      return (nominacion.tour as TourInterface)?.nombre;
    }
  }

  abrirModalSeleccionarGanador(rowData: any) {
    const dialogRef = this.dialog
      .open(
        ModalSeleccionarGanadoresComponent,
        {
          width: '1000px',
          data: {
            nominacion: rowData,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (res: any) => {
          this.cargarNominaciones();
        },
        error => {
          console.error(error);
        },
      );
  }
}
