import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {CategoriaInterface} from '../../../../interfaces/categoria.interface';
import {
  MENSAJES_VALIDACION_DESCRIPCION_CATEGORIA,
  MENSAJES_VALIDACION_NOMBRE_CATEGORIA, MENSAJES_VALIDACION_TIPO_CATEGORIA,
  VALIDACION_DESCRIPCION_CATEGORIA,
  VALIDACION_NOMBRE_CATEGORIA, VALIDACION_TIPO_CATEGORIA
} from '../../../../constantes/validaciones-formulario/categoria';
import {SelectItem} from 'primeng';

@Component({
  selector: 'app-formulario-categoria',
  templateUrl: './formulario-categoria.component.html',
  styleUrls: ['./formulario-categoria.component.css']
})
export class FormularioCategoriaComponent implements OnInit {

  formularioCategoria: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    tipo: [],
  };
  subscribers = [];
  tipos: SelectItem[] = [
    {
      label: 'Cancion',
      value: 'Cancion',
    },
    {
      label: 'Artista',
      value: 'Artista',
    },
    {
      label: 'Video',
      value: 'Video',
    },    {
      label: 'Album',
      value: 'Album',
    },
    {
      label: 'Tour',
      value: 'Tour',
    },
  ];
  @Input()
  categoria: CategoriaInterface;

  @Input()
  width: number;

  @Output()
  categoriaValidoEnviar: EventEmitter<CategoriaInterface | boolean> = new EventEmitter();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioCategoria = this._formBuilder.group({
      nombre: [this.categoria ? this.categoria.nombre : '', VALIDACION_NOMBRE_CATEGORIA],
      descripcion: [this.categoria ? this.categoria.descripcion : '', VALIDACION_DESCRIPCION_CATEGORIA],
      tipo: [this.categoria ? this.categoria.tipo : '', VALIDACION_TIPO_CATEGORIA],
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_CATEGORIA);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_CATEGORIA);
    this.verificarCampoFormControl('tipo', MENSAJES_VALIDACION_TIPO_CATEGORIA);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioCategoria.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioCategoria;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const formularioValido = formularioFormGroup.valid;
          if (formularioValido) {
            this.categoriaValidoEnviar.emit(formulario);
          } else {
            this.categoriaValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }
}
