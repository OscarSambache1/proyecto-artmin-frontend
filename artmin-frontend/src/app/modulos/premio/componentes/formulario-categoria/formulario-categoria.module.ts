import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioCategoriaComponent } from './formulario-categoria.component';
import {ReactiveFormsModule} from '@angular/forms';
import {DropdownModule, InputTextareaModule, InputTextModule} from 'primeng';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';



@NgModule({
  declarations: [FormularioCategoriaComponent],
  exports: [
    FormularioCategoriaComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        InputTextModule,
        AlertaValidacionCampoFormularioModule,
        InputTextareaModule,
        DropdownModule
    ]
})
export class FormularioCategoriaModule { }
