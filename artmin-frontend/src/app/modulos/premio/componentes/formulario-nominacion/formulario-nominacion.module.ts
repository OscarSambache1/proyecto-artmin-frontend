import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioNominacionComponent } from './formulario-nominacion.component';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {AutocompleteVideoModule} from '../../../../componentes/autocomplete-video/autocomplete-video.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {ReactiveFormsModule} from '@angular/forms';
import {AutocompleteArtistaModule} from '../../../../componentes/autocomplete-artista/autocomplete-artista.module';
import {AutocompleteCategoriaModule} from '../../../../componentes/autocomplete-categoria/autocomplete-categoria.module';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteTourModule} from '../../../../componentes/autocomplete-tour/autocomplete-tour.module';
import {AutocompletePremioModule} from '../../../../componentes/autocomplete-premio/autocomplete-premio.module';
import {AutocompletePremioAnioModule} from '../../../../componentes/autocomplete-premio-anio/autocomplete-premio-anio.module';



@NgModule({
  declarations: [FormularioNominacionComponent],
  exports: [
    FormularioNominacionComponent
  ],
  imports: [
    CommonModule,
    AutocompleteCancionModule,
    AutocompleteVideoModule,
    AlertaValidacionCampoFormularioModule,
    ReactiveFormsModule,
    AutocompleteArtistaModule,
    AutocompleteCategoriaModule,
    AutocompleteAlbumsModule,
    AutocompleteTourModule,
    AutocompletePremioModule,
    AutocompletePremioAnioModule
  ]
})
export class FormularioNominacionModule { }
