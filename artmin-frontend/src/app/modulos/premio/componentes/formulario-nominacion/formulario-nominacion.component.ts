import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {ToasterService} from 'angular2-toaster';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {NominacionInterface} from '../../../../interfaces/nominacion.interface';
import {
  MENSAJES_VALIDACION_ARTISTAS_NOM,
  MENSAJES_VALIDACION_CANCIONES_NOM,
  MENSAJES_VALIDACION_CATEGORIA_NOM,
  MENSAJES_VALIDACION_PMEMIO_ANIO_NOM,
  MENSAJES_VALIDACION_PREMIO_NOM,
  MENSAJES_VALIDACION_TOURS_NOM,
  MENSAJES_VALIDACION_VIDEOS_NOM,
  VALIDACION_ALBUMS_NOM,
  VALIDACION_ARTISTAS_NOM,
  VALIDACION_CANCIONES_NOM,
  VALIDACION_CATEGORIA_NOM,
  VALIDACION_TOURS_NOM,
  VALIDACION_VIDEOS_NOM
} from '../../../../constantes/validaciones-formulario/nominacion-premio';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {TourRestService} from '../../../../servicios/rest/tour-rest.service';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {CategoriaInterface} from '../../../../interfaces/categoria.interface';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {PremioAnioInterface} from '../../../../interfaces/premio-anio.interface';

@Component({
  selector: 'app-formulario-nominacion',
  templateUrl: './formulario-nominacion.component.html',
  styleUrls: ['./formulario-nominacion.component.css']
})
export class FormularioNominacionComponent implements OnInit {

  formularioNominacion: FormGroup;
  mensajesError = {
    premio: [],
    premioAnio: [],
    categoria: [],
    artista: [],
    album: [],
    cancion: [],
    video: [],
    tour: [],
  };
  subscribers = [];

  idCategoria: number;

  @Input()
  nominacion: NominacionInterface;

  @Input()
  deshabilitarAlbums: boolean;

  @Input()
  width: number;

  @Input()
  idsArtista: number[] = [];

  @Input()
  idsAlbums: number[] = [];

  @Input()
  idsCanciones: number[] = [];

  @Input()
  idsVideos: number[] = [];

  @Input()
  idsTours: number[] = [];

  @Input()
  idPremio = -1;

  @Input()
  idPremioAnio = 0;

  @Output()
  nominacionValidoEnviar: EventEmitter<NominacionInterface | boolean> = new EventEmitter();

  @Input()
  mostrarPremio = false;

  @Input()
  tipos: string[];

  tipoCategoria: string;
  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _artistaService: ArtistaRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _cancionRestService: CancionRestService,
    private readonly _videoRestService: VideoRestService,
    private readonly _tourRestService: TourRestService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    if (this.nominacion) {
      this.idCategoria = this.nominacion.categoria.id;
      if (this.mostrarPremio) {
        this.idPremio = (this.nominacion?.premioAnio?.premio as any)?.id;
        this.idPremioAnio = this.nominacion?.premioAnio?.id;
      }
    }

    this.formularioNominacion = this._formBuilder.group({
      premio: [this.nominacion ? this.nominacion.premioAnio?.premio : '', this.mostrarPremio ? [Validators.required] : []],
      premioAnio: [this.nominacion ? this.nominacion.premioAnio : '', this.mostrarPremio ? [Validators.required] : []],
      categoria: [this.nominacion ? this.nominacion.categoria : '', []],
      artista: [this.nominacion ? this.nominacion.artista : [], []],
      album: [this.nominacion ? this.nominacion.album : [], []],
      video: [this.nominacion ? this.nominacion.video : [], []],
      cancion: [this.nominacion ? this.nominacion.cancion : [], []],
      tour: [this.nominacion ? this.nominacion.tour : [], []],
    });
    if (this.nominacion) {
      this.setearCategoria(this.nominacion.categoria, false);
    }
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('categoria', MENSAJES_VALIDACION_CATEGORIA_NOM);
    this.verificarCampoFormControl('artista', MENSAJES_VALIDACION_ARTISTAS_NOM);
    this.verificarCampoFormControl('album', MENSAJES_VALIDACION_ARTISTAS_NOM);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCIONES_NOM);
    this.verificarCampoFormControl('video', MENSAJES_VALIDACION_VIDEOS_NOM);
    this.verificarCampoFormControl('tour', MENSAJES_VALIDACION_TOURS_NOM);
    this.verificarCampoFormControl('premio', MENSAJES_VALIDACION_PREMIO_NOM);
    this.verificarCampoFormControl('premioAnio', MENSAJES_VALIDACION_PMEMIO_ANIO_NOM);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioNominacion.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioNominacion;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.nominacionValidoEnviar.emit(formulario);
          } else {
            this.nominacionValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  buscarCancionesPorArtista(respuestaArtista: ArtistaInterface[]) {
    this.formularioNominacion.patchValue(
      {
        artista: [
          ...respuestaArtista]
      }
    );

  }


  setearAlbum(albumsSeleccionado: AlbumInterface[]) {
    this.formularioNominacion.patchValue(
      {
        album: [
          ...albumsSeleccionado]
      }
    );
  }

  setearCanciones(cancionSeleccionadas: CancionInterface[]) {
    this.formularioNominacion.patchValue(
      {
        cancion: [
          ...cancionSeleccionadas]
      }
    );
  }

  setearVideos(videoSeleccionados: VideoInterface[]) {
    this.formularioNominacion.patchValue(
      {
        video: [
          ...videoSeleccionados]
      }
    );
  }

  setearTours(tourSeleccionados: TourInterface[]) {
    this.formularioNominacion.patchValue(
      {
        tour: [
          ...tourSeleccionados]
      }
    );
  }

  obtenerIdsArtistasSeleccionados() {
    const existeCampoArtistas = this.formularioNominacion.get('artista').value;
    if (existeCampoArtistas) {
      return this.formularioNominacion.get('artista').value.map(artista => artista.id);
    }
  }

  setearCategoria(categoria: CategoriaInterface, limpiarForm: boolean) {
    if (categoria) {
      this.formularioNominacion.patchValue(
        {
          categoria,
        }
      );
      this.tipoCategoria = categoria.tipo;
      if (limpiarForm) {
        this.formularioNominacion.patchValue(
          {
            album: null,
            cancion: null,
            video: null,
            tour: null,
          }
        );
      }

      this.formularioNominacion.get('album').clearValidators();
      this.formularioNominacion.get('video').clearValidators();
      this.formularioNominacion.get('cancion').clearValidators();
      this.formularioNominacion.get('tour').clearValidators();
      if (categoria.tipo === 'Artista') {
        this.formularioNominacion.get('artista').setValidators(VALIDACION_ARTISTAS_NOM);
      }
      if (categoria.tipo === 'Cancion') {
        this.formularioNominacion.get('artista').setValidators(VALIDACION_ARTISTAS_NOM);
        this.formularioNominacion.get('cancion').setValidators(VALIDACION_CANCIONES_NOM);
      }
      if (categoria.tipo === 'Album') {
        this.formularioNominacion.get('artista').setValidators(VALIDACION_ARTISTAS_NOM);
        this.formularioNominacion.get('album').setValidators(VALIDACION_ALBUMS_NOM);
      }
      if (categoria.tipo === 'Video') {
        this.formularioNominacion.get('artista').setValidators(VALIDACION_ARTISTAS_NOM);
        this.formularioNominacion.get('video').setValidators(VALIDACION_VIDEOS_NOM);
      }
      if (categoria.tipo === 'Tour') {
        this.formularioNominacion.get('artista').setValidators(VALIDACION_ARTISTAS_NOM);
        this.formularioNominacion.get('tour').setValidators(VALIDACION_TOURS_NOM);
      }
      // this.formularioNominacion.patchValue(
      //   {
      //     artista: this.nominacion?.artista,
      //     album: this.nominacion?.album,
      //     cancion: this.nominacion?.cancion,
      //     tour: this.nominacion?.tour,
      //     video: this.nominacion?.video,
      //   }
      // );
    }
  }

  setearPremio(premio: PremioInterface) {
    this.idPremio = premio.id;
    this.formularioNominacion.patchValue(
      {
        premio,
        premioAnio: null,
        categoria: null,
      }
    );
  }

  setearPremioAnio(premioAnio: PremioAnioInterface) {
    this.idPremioAnio = premioAnio?.id;
    this.formularioNominacion.patchValue(
      {
        premioAnio,
      }
    );
  }
}
