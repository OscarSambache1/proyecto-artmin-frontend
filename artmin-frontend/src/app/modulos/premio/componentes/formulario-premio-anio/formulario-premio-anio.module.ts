import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioPremioAnioComponent } from './formulario-premio-anio.component';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CalendarModule, InputTextareaModule, InputTextModule} from 'primeng';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';



@NgModule({
  declarations: [FormularioPremioAnioComponent],
  imports: [
    CommonModule,
    InputImagenModule,
    ReactiveFormsModule,
    InputTextModule,
    AlertaValidacionCampoFormularioModule,
    InputTextareaModule,
    FormsModule,
    CalendarModule,
  ],
  exports: [FormularioPremioAnioComponent]
})
export class FormularioPremioAnioModule { }
