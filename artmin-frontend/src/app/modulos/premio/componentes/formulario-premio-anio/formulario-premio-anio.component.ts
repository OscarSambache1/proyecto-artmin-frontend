import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {
  MENSAJES_VALIDACION_FECHA_PREMIO_ANIO, MENSAJES_VALIDACION_LUGAR_PREMIO_ANIO,
  VALIDACION_FECHA_PREMIO_ANIO,
  VALIDACION_LUGAR_PREMIO_ANIO
} from '../../../../constantes/validaciones-formulario/premio-anio';
import {PremioAnioInterface} from '../../../../interfaces/premio-anio.interface';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';

@Component({
  selector: 'app-formulario-premio-anio',
  templateUrl: './formulario-premio-anio.component.html',
  styleUrls: ['./formulario-premio-anio.component.css']
})
export class FormularioPremioAnioComponent implements OnInit {

  formularioPremioAnio: FormGroup;

  mensajesError = {
    fecha: [],
    lugar: [],
  };

  subscribers = [];

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;

  @Input()
  premioAnio: PremioAnioInterface;

  @Input()
  width: number;

  @Output()
  premioAnioValidoEnviar: EventEmitter<PremioAnioInterface | boolean> = new EventEmitter();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioPremioAnio = this._formBuilder.group({
      fecha: [this.premioAnio ? this.premioAnio.fecha : '', VALIDACION_FECHA_PREMIO_ANIO],
      lugar: [this.premioAnio ? this.premioAnio.lugar : '', VALIDACION_LUGAR_PREMIO_ANIO],
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('fecha', MENSAJES_VALIDACION_FECHA_PREMIO_ANIO);
    this.verificarCampoFormControl('lugar', MENSAJES_VALIDACION_LUGAR_PREMIO_ANIO);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioPremioAnio.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioPremioAnio;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const formularioValido = formularioFormGroup.valid;
          if (formularioValido) {
            this.premioAnioValidoEnviar.emit(formulario);
          } else {
            this.premioAnioValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }
}
