import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CategoriaInterface} from '../../../../interfaces/categoria.interface';
import {ModalCrearEditarPremioComponent} from '../../modales/modal-crear-editar-premio/modal-crear-editar-premio.component';
import {PremioInterface} from '../../../../interfaces/premio.interface';
import {MatDialog} from '@angular/material/dialog';
import {ModalCrearEditarCategoriaComponent} from '../../modales/modal-crear-editar-categoria/modal-crear-editar-categoria.component';

@Component({
  selector: 'app-tabla-categorias',
  templateUrl: './tabla-categorias.component.html',
  styleUrls: ['./tabla-categorias.component.css']
})
export class TablaCategoriasComponent implements OnInit {
  columnasCategorias = [
    {
      field: 'nombre',
      header: 'Nombre',
      width: '50%'
    },
    {
      field: 'tipo',
      header: 'Tipo',
      width: '30%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '20%'
    },
  ];

  @Input()
  categorias: CategoriaInterface[] = [];

  @Output()
  cargarCategorias: EventEmitter<CategoriaInterface[]> = new EventEmitter<CategoriaInterface[]>();

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }

  abrirModalCrearEditarCategoria(categoria?: CategoriaInterface) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarCategoriaComponent,
        {
          width: '600px',
          data: {
            categoria,
          },
        }
      );
    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (categoriaCreadoEditado: CategoriaInterface) => {
          if (categoriaCreadoEditado) {
            if (!categoria?.id) {
              this.categorias.unshift(categoriaCreadoEditado);
            } else {
              const indice = this.categorias.findIndex((categoriaArreglo: CategoriaInterface) => categoriaArreglo.id === categoria?.id);
              this.categorias[indice] = categoriaCreadoEditado;
            }
            this.cargarCategorias.emit(this.categorias);
          }
        },
        error => {
          console.error(error);
        },
      );
  }
}
