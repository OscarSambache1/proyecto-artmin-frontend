import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TablaCategoriasComponent} from './tabla-categorias.component';
import {BotonNuevoModule} from '../../../../componentes/boton-nuevo/boton-nuevo.module';
import {TableModule} from 'primeng';
import {ModalCrearEditarCategoriaComponent} from '../../modales/modal-crear-editar-categoria/modal-crear-editar-categoria.component';
import {ModalCrearEditarCategoriaModule} from '../../modales/modal-crear-editar-categoria/modal-crear-editar-categoria.module';


@NgModule({
  declarations: [TablaCategoriasComponent],
  exports: [
    TablaCategoriasComponent
  ],
  imports: [
    CommonModule,
    BotonNuevoModule,
    TableModule,
    ModalCrearEditarCategoriaModule,
  ],
  entryComponents: [ModalCrearEditarCategoriaComponent]
})
export class TablaCategoriasModule {
}
