import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TourRoutingModule} from './tour-routing.module';
import {RutaGestionTourComponent} from './rutas/ruta-gestion-tour/ruta-gestion-tour.component';
import {MenuItemCuadradoModule} from '../../componentes/menu-item-cuadrado/menu-item-cuadrado.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {AutocompleteArtistaModule} from '../../componentes/autocomplete-artista/autocomplete-artista.module';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {SegundosAMinutosHorasPipeModule} from '../../pipes/segundos-a-minutos-horas-pipe/segundos-a-minutos-horas-pipe.module';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {SafePipeModule} from '../../pipes/safe-pipe/safe-pipe.module';
import {InputTextModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {RutaCrearEditarTourComponent} from './rutas/ruta-crear-editar-tour/ruta-crear-editar-tour.component';
import {FormularioTourModule} from './componentes/formulario-tour/formulario-tour.module';
import {TablaTourLugarModule} from '../../componentes/tabla-tour-lugar/tabla-tour-lugar.module';
import {ModalTourLugarModule} from './modales/modal-tour-lugar/modal-tour-lugar.module';
import {ModalTourLugarComponent} from './modales/modal-tour-lugar/modal-tour-lugar.component';
import {TablaSetlitsTourModule} from './componentes/tabla-setlits-tour/tabla-setlits-tour.module';
import {CarouselOpcionesModule} from '../../componentes/carousel-opciones/carousel-opciones.module';
import {RutaNominacionesModule} from '../../componentes/ruta-nominaciones/ruta-nominaciones.module';

@NgModule({
  declarations: [RutaGestionTourComponent, RutaCrearEditarTourComponent],
  imports: [
    CommonModule,
    TourRoutingModule,
    CommonModule,
    MenuItemCuadradoModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    AutocompleteArtistaModule,
    MatStepperModule,
    MatIconModule,
    SegundosAMinutosHorasPipeModule,
    MigasPanModule,
    SafePipeModule,
    InputTextModule,
    FormsModule,
    DropdownModule,
    FormularioTourModule,
    TablaTourLugarModule,
    ModalTourLugarModule,
    TablaSetlitsTourModule,
    CarouselOpcionesModule,
    RutaNominacionesModule,
  ],
  entryComponents: [
    ModalTourLugarComponent,
  ]
})
export class TourModule {
}
