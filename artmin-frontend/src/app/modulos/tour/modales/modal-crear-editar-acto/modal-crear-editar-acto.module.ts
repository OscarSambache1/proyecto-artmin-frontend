import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarActoComponent } from './modal-crear-editar-acto.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioActoModule} from '../../componentes/formulario-acto/formulario-acto.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarActoComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioActoModule,
    BotonGuardarModule,
    BotonCancelarModule
  ],
  exports: [
    ModalCrearEditarActoComponent,
  ]
})
export class ModalCrearEditarActoModule { }
