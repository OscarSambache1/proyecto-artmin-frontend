import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ActoTourRestService} from '../../../../servicios/rest/acto-tour-rest.service';
import {ActoTourInterface} from '../../../../interfaces/acto-tour.interface';
import {TourInterface} from '../../../../interfaces/tour.interface';

@Component({
  selector: 'app-modal-crear-editar-acto',
  templateUrl: './modal-crear-editar-acto.component.html',
  styleUrls: ['./modal-crear-editar-acto.component.css']
})
export class ModalCrearEditarActoComponent implements OnInit {
  formularioValido: boolean;
  actoTourCrearEditar: ActoTourInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      actoTour: ActoTourInterface,
      tour: TourInterface,
    },
    public dialogo: MatDialogRef<ModalCrearEditarActoComponent>,
    private readonly _cargandoService: CargandoService,
  ) {
  }

  ngOnInit(): void {}

  validarFormulario(actoTour: ActoTourInterface | boolean) {
    if (actoTour) {
      this.formularioValido = true;
      this.actoTourCrearEditar = actoTour as ActoTourInterface;
    } else {
      this.formularioValido = false;
    }
  }

  crearEditar() {
    this._cargandoService.habilitarCargando();
    if (!this.data.actoTour) {
      this.actoTourCrearEditar.tour = this.data.tour;
      this.dialogo.close(this.actoTourCrearEditar);
    } else {
      this.data.actoTour.nombre = this.actoTourCrearEditar.nombre;
      this.dialogo.close(this.data.actoTour);
    }
    this._cargandoService.deshabiltarCargando();
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
