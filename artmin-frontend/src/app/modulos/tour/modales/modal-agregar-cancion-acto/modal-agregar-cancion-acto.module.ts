import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalAgregarCancionActoComponent } from './modal-agregar-cancion-acto.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioAgregarCancionActoModule} from '../../componentes/formulario-agregar-cancion-acto/formulario-agregar-cancion-acto.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalAgregarCancionActoComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioAgregarCancionActoModule,
    BotonGuardarModule,
    BotonCancelarModule
  ],
  exports: [
    ModalAgregarCancionActoComponent
  ]
})
export class ModalAgregarCancionActoModule { }
