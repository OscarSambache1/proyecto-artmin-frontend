import {Component, Inject, OnInit} from '@angular/core';
import {CancionSetlistInterface} from '../../../../interfaces/cancion-setlist.interface';
import {ActoTourInterface} from '../../../../interfaces/acto-tour.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CargandoService} from '../../../../servicios/cargando-service';

@Component({
  selector: 'app-modal-agregar-cancion-acto',
  templateUrl: './modal-agregar-cancion-acto.component.html',
  styleUrls: ['./modal-agregar-cancion-acto.component.css']
})
export class ModalAgregarCancionActoComponent implements OnInit {
  formularioValido: boolean;
  cancionAsignar: CancionSetlistInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      actoTour: ActoTourInterface,
      idsArtistas: number[],
    },
    public dialogo: MatDialogRef<ModalAgregarCancionActoComponent>,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {}

  validarFormulario(cancionActo: CancionSetlistInterface | boolean) {
    if (cancionActo) {
      this.formularioValido = true;
      this.cancionAsignar = cancionActo as CancionSetlistInterface;
    } else {
      this.formularioValido = false;
    }
  }

  crearEditar() {
    this._cargandoService.habilitarCargando();
    this.dialogo.close(this.cancionAsignar);
    this._cargandoService.deshabiltarCargando();
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
