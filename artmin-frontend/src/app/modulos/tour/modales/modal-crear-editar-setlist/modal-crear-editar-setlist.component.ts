import {Component, Inject, OnInit} from '@angular/core';
import {SetlistInterface} from '../../../../interfaces/setlist.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {SetlistRestService} from '../../../../servicios/rest/setlist-rest.service';

@Component({
  selector: 'app-modal-crear-editar-setlist',
  templateUrl: './modal-crear-editar-setlist.component.html',
  styleUrls: ['./modal-crear-editar-setlist.component.css']
})
export class ModalCrearEditarSetlistComponent implements OnInit {
  formularioValido: boolean;
  setlistCrearEditar: SetlistInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      setlist: SetlistInterface,
      idTour: number,
    },
    public dialogo: MatDialogRef<ModalCrearEditarSetlistComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _setlistRestService: SetlistRestService,
  ) {
  }

  ngOnInit(): void {
  }

  validarFormulario(setlist: SetlistInterface | boolean) {
    if (setlist) {
      this.formularioValido = true;
      this.setlistCrearEditar = setlist as SetlistInterface;
    } else {
      this.formularioValido = false;
    }
  }

  crearEditar() {
    this._cargandoService.habilitarCargando();
    if (!this.data.setlist) {
      this.setlistCrearEditar.tour = this.data.idTour;
      this._setlistRestService
        .create(
          this.setlistCrearEditar,
        )
        .subscribe((registroCreado: SetlistInterface) => {
            this.dialogo.close(registroCreado);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    } else {
      this._setlistRestService
        .updateOne(
          this.data.setlist.id,
          this.setlistCrearEditar,
        )
        .subscribe((registroEditado: SetlistInterface) => {
            this.dialogo.close(registroEditado);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
