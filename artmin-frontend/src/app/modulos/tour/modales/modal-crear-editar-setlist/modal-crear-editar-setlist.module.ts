import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarSetlistComponent } from './modal-crear-editar-setlist.component';
import {FormularioSetlistModule} from "../../componentes/formulario-setlist/formulario-setlist.module";
import {MatDialogModule} from "@angular/material/dialog";
import {BotonGuardarModule} from "../../../../componentes/boton-guardar/boton-guardar.module";
import {BotonCancelarModule} from "../../../../componentes/boton-cancelar/boton-cancelar.module";



@NgModule({
  declarations: [ModalCrearEditarSetlistComponent],
  imports: [
    CommonModule,
    FormularioSetlistModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
  ]
})
export class ModalCrearEditarSetlistModule { }
