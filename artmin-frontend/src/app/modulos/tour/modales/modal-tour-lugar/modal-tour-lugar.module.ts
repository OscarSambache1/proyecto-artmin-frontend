import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalTourLugarComponent } from './modal-tour-lugar.component';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {FormularioTourLugarModule} from '../../componentes/formulario-tour-lugar/formulario-tour-lugar.module';



@NgModule({
  declarations: [ModalTourLugarComponent],
  imports: [
    CommonModule,
    FormularioTourLugarModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
  ]
})
export class ModalTourLugarModule { }
