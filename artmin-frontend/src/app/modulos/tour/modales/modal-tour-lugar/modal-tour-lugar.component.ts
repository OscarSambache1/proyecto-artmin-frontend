import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {TourLugarInterface} from '../../../../interfaces/tour-lugar.interface';
import {TourLugarRestService} from '../../../../servicios/rest/tour-lugar-rest.service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {obtenerAnio} from '../../../../funciones/obtener-anio';
import {TourInterface} from '../../../../interfaces/tour.interface';

@Component({
  selector: 'app-modal-tour-lugar',
  templateUrl: './modal-tour-lugar.component.html',
  styleUrls: ['./modal-tour-lugar.component.css']
})
export class ModalTourLugarComponent implements OnInit {
  formularioValido: boolean;
  tourLugarCrearEditar: TourLugarInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      tourLugar: TourLugarInterface,
      tour: TourInterface,
      idTour: number,
    },
    public dialogo: MatDialogRef<ModalTourLugarComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _toourLugarRestService: TourLugarRestService,
  ) {
  }

  ngOnInit(): void {
  }

  validarFormularioLugar(tourLugar: TourLugarInterface | boolean) {
    if (tourLugar) {
      this.formularioValido = true;
      this.tourLugarCrearEditar = tourLugar as TourLugarInterface;
    } else {
      this.formularioValido = false;
    }
  }

  crearEditarTourLugar() {
    this._cargandoService.habilitarCargando();
    this.tourLugarCrearEditar.anio = obtenerAnio(this.tourLugarCrearEditar.fecha);
    this.tourLugarCrearEditar.seCancelo = this.tourLugarCrearEditar.seCancelo ? 1 : 0;
    this.tourLugarCrearEditar.porcentaje = (((this.tourLugarCrearEditar.ticketsVendidos || 0) * 100) / (this.tourLugarCrearEditar.ticketsDisponibles || 0)) || 0;
    if (!this.data.tourLugar) {
      this.tourLugarCrearEditar.tour = this.data.idTour;
      this._toourLugarRestService
        .create(
          this.tourLugarCrearEditar,
        )
        .subscribe((respuestaTourLugarCreado: TourLugarInterface) => {
            respuestaTourLugarCreado.lugar = this.tourLugarCrearEditar.lugar;
            respuestaTourLugarCreado.recinto = this.tourLugarCrearEditar.recinto;
            this.dialogo.close(respuestaTourLugarCreado);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    } else {
      delete this.tourLugarCrearEditar.pais;
      this._toourLugarRestService
        .updateOne(
          this.data.tourLugar.id,
          this.tourLugarCrearEditar,
        )
        .subscribe((respuestaTourLugarEditado: TourLugarInterface) => {
            respuestaTourLugarEditado.lugar = this.tourLugarCrearEditar.lugar;
            respuestaTourLugarEditado.recinto = this.tourLugarCrearEditar.recinto;
            this.dialogo.close(respuestaTourLugarEditado);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }

  }

  cerrarModal() {
    this.dialogo.close();
  }
}
