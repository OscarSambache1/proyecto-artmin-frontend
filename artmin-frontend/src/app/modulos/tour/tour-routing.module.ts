import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionTourComponent} from './rutas/ruta-gestion-tour/ruta-gestion-tour.component';
import {RutaCrearEditarTourComponent} from './rutas/ruta-crear-editar-tour/ruta-crear-editar-tour.component';
import {RutaNominacionesComponent} from '../../componentes/ruta-nominaciones/ruta-nominaciones.component';

const routes: Routes = [
  {
    path: 'gestion-tour',
    children: [
      {
        path: '',
        component: RutaGestionTourComponent
      },
      {
        path: 'crear-tour',
        component: RutaCrearEditarTourComponent
      },
      {
        path: 'editar-tour/:idTour',
        children: [
          {
            path: '',
            component: RutaCrearEditarTourComponent,
          },
          {
            path: 'nominaciones/:tipo',
            component: RutaNominacionesComponent,
          },
        ]
      },

    ]
  },

  {
    path: '',
    redirectTo: 'gestion-tour',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TourRoutingModule { }
