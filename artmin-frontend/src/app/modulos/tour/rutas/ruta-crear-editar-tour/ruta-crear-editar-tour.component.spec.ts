import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaCrearEditarTourComponent } from './ruta-crear-editar-tour.component';

describe('RutaCrearEditarTourComponent', () => {
  let component: RutaCrearEditarTourComponent;
  let fixture: ComponentFixture<RutaCrearEditarTourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaCrearEditarTourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaCrearEditarTourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
