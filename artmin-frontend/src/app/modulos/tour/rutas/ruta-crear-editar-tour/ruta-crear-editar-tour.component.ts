import {Component, OnInit, ViewChild} from '@angular/core';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {TourRestService} from '../../../../servicios/rest/tour-rest.service';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {
  ToastErrorCargandoDatos, toastErrorCrear,
  toastErrorEditar,
  toastExitoCrear,
  toastExitoEditar
} from '../../../../constantes/mensajes-toaster';
import {MatStepper} from '@angular/material/stepper';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {RUTAS_TOUR} from '../definicion-rutas/rutas-tour';
import {FormularioTourComponent} from '../../componentes/formulario-tour/formulario-tour.component';
import {obtenerAnio} from '../../../../funciones/obtener-anio';
import {ArtistaTourInterface} from '../../../../interfaces/artista-tour.interface';
import {OPCIONES_TOUR} from '../../../../constantes/opciones-gestion-cancion';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';

@Component({
  selector: 'app-ruta-crear-editar-tour',
  templateUrl: './ruta-crear-editar-tour.component.html',
  styleUrls: ['./ruta-crear-editar-tour.component.css']
})
export class RutaCrearEditarTourComponent implements OnInit {

  idArtista: number;
  idsArtista: number[] = [];
  idTour: number;
  tourCrearEditar: TourInterface;
  formularioValido: boolean;
  @ViewChild(FormularioTourComponent)
  componenteFormularioTour: FormularioTourComponent;

  inputEnlace: string;
  tour: TourInterface;
  ruta = [];
  arregloRutas: any[];
  mostrarInputEnlace: boolean;
  opcionesTour = OPCIONES_TOUR;

  constructor(
    private readonly _tourRestService: TourRestService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idTour = +params.idTour;
          this.idArtista = +params.idArtista;
          this.setearArregloRutasMigaPan();
          this.ruta = this.setearRuta();
          if (this.idTour) {
            const consulta = {
              where: {
                id: this.idTour
              },
              relations: [
                'imagenesTour',
                'artistasTour',
                'artistasTour.artista',
                'lugaresTour',
                'lugaresTour.recinto',
                'lugaresTour.lugar',
                'lugaresTour.lugar.lugarPadre',
                'festival',
              ]
            };
            this._cargandoService.habilitarCargando();
            return this._tourRestService.findAll(
              JSON.stringify(consulta)
            );
          } else {
            this.idsArtista.push(this.idArtista);
            return of([], 0);
          }
        })
      )
      .subscribe(
        (respuestaTour: [TourInterface[], number]) => {
          if (respuestaTour && respuestaTour[0] && respuestaTour[0][0]) {
            this.tour = respuestaTour[0][0];
            this.idsArtista = (this.tour.artistasTour as ArtistaTourInterface[])
              .map(artistaTour => (artistaTour.artista as ArtistaInterface).id);
          }
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  validarFormulario(tour: TourInterface | boolean) {
    if (tour) {
      this.formularioValido = true;
      this.tourCrearEditar = tour as TourInterface;
    } else {
      this.formularioValido = false;
    }
  }

  cancelarCreacionEdicion() {
    this.componenteFormularioTour.volverFormularioInicial();
  }

  crearEditarArtista(stepper?) {
    this._cargandoService.habilitarCargando();
    this.tourCrearEditar.anio = obtenerAnio(this.tourCrearEditar.fechaInicio);
    if (this.tour) {
      this.tourCrearEditar.id = this.tour.id;
    }
    this.tourCrearEditar.artistasTour = [...this.tourCrearEditar.artistas].map(artista => (artista as ArtistaInterface).id);
    this.tourCrearEditar.tipo = (this.tourCrearEditar.tipo as any).value;
    if (this.tour) {
      this._tourRestService
        .crearEditarTour(
          this.tourCrearEditar,
          this.tourCrearEditar.imagen,
        )
        .subscribe((respuestaTourEditado: TourInterface) => {
            this.tour = respuestaTourEditado;
            this.idsArtista = (this.tour.artistasTour as ArtistaTourInterface[]).map(artistaTour => (artistaTour.artista as ArtistaInterface).id);
            this.componenteFormularioTour.tour = respuestaTourEditado;
            this.componenteFormularioTour.volverFormularioInicial();
            this._toasterService.pop(toastExitoEditar);
            this._cargandoService.deshabiltarCargando();
            RUTAS_TOUR
              .rutaEditarTour(
                true,
                false,
                [this.idArtista, this.tour.id]
              );
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    } else {
      this._tourRestService
        .crearEditarTour(
          this.tourCrearEditar,
          this.tourCrearEditar.imagen,
        )
        .subscribe((respuestaTourCreado: TourInterface) => {
            this.tour = respuestaTourCreado;
            this.idsArtista = (this.tour.artistasTour as ArtistaTourInterface[]).map(artistaTour => (artistaTour.artista as ArtistaInterface).id);
            this.componenteFormularioTour.tour = respuestaTourCreado;
            this.componenteFormularioTour.volverFormularioInicial();
            this.cambiarSiguienteStep(stepper);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  habilitarFormularioEditarTour() {
    this.componenteFormularioTour.formularioTour.enable();
  }

  cambiarSiguienteStep(steper: MatStepper) {
    setTimeout(() => {
      steper.next();
    }, 800);
  }

  setearArregloRutasMigaPan() {
    if (this.idArtista) {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
        RUTAS_TOUR.rutaGestionTour(false, true, [this.idArtista]),
      ];
    } else {
      this.arregloRutas = [
        RUTAS_TOUR.rutaGestionTour(false, true),
      ];
    }
    if (this.idTour) {
      this.arregloRutas.push(
        RUTAS_TOUR.rutaEditarTour(false, true)
      );
    } else {
      this.arregloRutas.push(
        RUTAS_TOUR.rutaCrearTour(false, true)
      );
    }
  }

  setearRuta(): any[] {
    if (this.idTour) {
      return RUTAS_TOUR.rutaEditarTour(false, true, [this.idTour, this.idArtista]).ruta;
    } else {
      return RUTAS_TOUR.rutaCrearTour(false, true, [this.idArtista]).ruta;
    }
  }

  cambiarValorMostrarInputEnlace() {
    this.mostrarInputEnlace = !this.mostrarInputEnlace;
  }

  irAGestion(modulo: string) {
    if (modulo === 'PREMIOS Y NOMINACIONES') {
      return this._router.navigate(
        RUTAS_TOUR
          .rutaNominaciones(
            true,
            false,
            [this.idArtista, this.tour.id, 'T']
          )
      );
    }
  }


}
