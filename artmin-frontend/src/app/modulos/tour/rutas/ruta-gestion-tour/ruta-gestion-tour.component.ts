import { Component, OnInit } from '@angular/core';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {TourRestService} from '../../../../servicios/rest/tour-rest.service';
import {RUTAS_TOUR} from '../definicion-rutas/rutas-tour';
import {TIPOS_TOUR, TIPOS_TOUR_ENUM} from '../../../../constantes/tipos-tour';

@Component({
  selector: 'app-ruta-gestion-tour',
  templateUrl: './ruta-gestion-tour.component.html',
  styleUrls: ['./ruta-gestion-tour.component.css']
})
export class RutaGestionTourComponent implements OnInit {

  tours: TourInterface[] = [];
  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  ruta;
  artista: ArtistaInterface;
  rutaImagen = 'assets/imagenes/tour.svg';
  idArtistaQuery: number;
  idArtistaParams: number;
  tipo: string;
  arregloRutas: any[];
  opcionesTipoTour = TIPOS_TOUR;

  tipoTourSeleccionado: any;
  constructor(
    private readonly _tourRestService: TourRestService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtistaParams = params.idArtista ? +params.idArtista : undefined;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          if (this.idArtistaParams) {
            this.idArtista = this.idArtistaParams;
            this._artistaRestService.findOne(this.idArtista)
              .subscribe(
                (artista: ArtistaInterface) => {
                  this.artista = artista;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.tipo) {
              this.tipo = this.queryParams.consulta.tipo;
              this.tipoTourSeleccionado = {
                value: this.queryParams.consulta.tipo,
                key: TIPOS_TOUR_ENUM[this.queryParams.consulta.tipo]
              };
            }
            if (this.queryParams.consulta.idArtista) {
              this.idArtistaQuery = this.queryParams.consulta.idArtista;
              this.idArtista = this.idArtistaQuery;
            } else {
              this.queryParams.consulta.idArtista = this.idArtista;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              esImagenPrincipal: 1,
              idArtista: this.idArtista,
            };
          }
          this._cargandoService.habilitarCargando();
          return this._tourRestService.obtenerTourPorArtistasTipo(
            JSON.stringify(this.queryParams.consulta)
          );
        })
      )
      .subscribe(
        (respuestaTours: [TourInterface[], number]) => {
          this.tours = respuestaTours[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerUrlPrincipal(tour: TourInterface): string {
    return obtenerUrlImagenPrincipal(tour, 'imagenesTour');
  }


  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarTours();
  }

  buscarToursPorArtista(artistaSeleccionado: ArtistaInterface) {
    this.idArtista = artistaSeleccionado ? artistaSeleccionado.id : undefined;
    this.buscarTours();
  }

  irRutaCrearTour() {
    this._router
      .navigate(
        RUTAS_TOUR
          .rutaCrearTour(
            true,
            false,
            [this.idArtistaParams]
          ),
      );
  }

  obtenerRuta(idTour) {
    return RUTAS_TOUR
      .rutaEditarTour(
        true,
        false,
        [this.idArtistaParams, idTour]
      );
  }

  buscarTourPorTipo(tipoTourSeleccionado) {
    this.tipo = tipoTourSeleccionado ? tipoTourSeleccionado : undefined;
    this.buscarTours();
  }

  buscarTours() {
    const consulta = {
      busqueda: this.busqueda,
      esImagenPrincipal: 1,
      idArtista: this.idArtista,
      tipo: this.tipo,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    if (this.idArtistaParams) {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtistaParams]),
        RUTAS_TOUR.rutaGestionTour(false, true, [this.idArtistaParams])
      ];
    } else {
      this.arregloRutas = [
        RUTAS_TOUR.rutaGestionTour(false, true)
      ];
    }
  }

  seteoRutas() {
    this.ruta = RUTAS_TOUR
      .rutaGestionTour(
        false,
        true,
        [this.idArtistaParams]).ruta;
  }

}
