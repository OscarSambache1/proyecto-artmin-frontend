import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';

export const RUTAS_TOUR = {
  _rutaInicioTour: {
    ruta: 'tour-modulo',
    nombre: 'Modulo Tour',
    generarRuta: (...argumentos) => {
      return `tour-modulo`;
    }
  },

  _rutaGestionTour: {
    ruta: 'gestion-tour',
    nombre: 'Gestión de tours y festivales',
    generarRuta: (...argumentos) => {
      return `gestion-tour`;
    }
  },

  rutaGestionTour: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioTour,
        this._rutaGestionTour
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioTour,
        this._rutaGestionTour
      ];
    }


    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaEditarTour: {
    ruta: 'editar-tour/:idTour',
    nombre: 'Editar tour',
    generarRuta: (...argumentos) => {
      return `editar-tour/${argumentos[1]}`;
    }
  },

  rutaEditarTour: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioTour,
        this._rutaGestionTour,
        this._rutaEditarTour
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioTour,
        this._rutaGestionTour,
        this._rutaEditarTour
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaCrearTour: {
    ruta: 'crear-tour',
    nombre: 'Registrar tour',
    generarRuta: (...argumentos) => {
      return `crear-tour`;
    }
  },

  rutaCrearTour: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioTour,
        this._rutaGestionTour,
        this._rutaCrearTour
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioTour,
        this._rutaGestionTour,
        this._rutaCrearTour
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaNominaciones: {
    ruta: 'nominaciones/:tipo',
    nombre: 'Nominaciones',
    generarRuta: (...argumentos) => {
      return `nominaciones/${argumentos[2]}`;
    }
  },

  rutaNominaciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioTour,
        this._rutaGestionTour,
        this._rutaEditarTour,
        this._rutaNominaciones,
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioTour,
        this._rutaGestionTour,
        this._rutaEditarTour,
        this._rutaNominaciones,
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
