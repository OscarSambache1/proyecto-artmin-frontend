import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeccionSetlistComponent } from './seccion-setlist.component';
import {TablaActosSetlistModule} from '../tabla-actos-setlist/tabla-actos-setlist.module';
import {TablaEditarPosicionActosModule} from '../tabla-editar-posicion-actos/tabla-editar-posicion-actos.module';
import {TablaActosSinEditarModule} from '../tabla-actos-sin-editar/tabla-actos-sin-editar.module';
import {ModalCrearEditarActoModule} from '../../modales/modal-crear-editar-acto/modal-crear-editar-acto.module';
import {ModalCrearEditarActoComponent} from '../../modales/modal-crear-editar-acto/modal-crear-editar-acto.component';
import {ModalAgregarCancionActoComponent} from '../../modales/modal-agregar-cancion-acto/modal-agregar-cancion-acto.component';
import {ModalAgregarCancionActoModule} from '../../modales/modal-agregar-cancion-acto/modal-agregar-cancion-acto.module';



@NgModule({
  declarations: [SeccionSetlistComponent],
  imports: [
    CommonModule,
    TablaActosSetlistModule,
    TablaEditarPosicionActosModule,
    TablaActosSinEditarModule,
    ModalCrearEditarActoModule,
    ModalAgregarCancionActoModule,
  ],
  exports: [SeccionSetlistComponent],
  entryComponents: [ModalCrearEditarActoComponent, ModalAgregarCancionActoComponent]
})
export class SeccionSetlistModule { }
