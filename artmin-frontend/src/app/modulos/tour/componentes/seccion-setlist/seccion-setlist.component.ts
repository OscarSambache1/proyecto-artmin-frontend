import {Component, Input, OnInit} from '@angular/core';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {SetlistInterface} from '../../../../interfaces/setlist.interface';
import {CancionSetlistInterface} from '../../../../interfaces/cancion-setlist.interface';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {ActoTourInterface} from '../../../../interfaces/acto-tour.interface';
import {CancioSetlistRestService} from '../../../../servicios/rest/cancio-setlist-rest.service';
import {ActoTourRestService} from '../../../../servicios/rest/acto-tour-rest.service';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ModalCrearEditarActoComponent} from '../../modales/modal-crear-editar-acto/modal-crear-editar-acto.component';
import {MatDialog} from '@angular/material/dialog';
import {ModalAgregarCancionActoComponent} from '../../modales/modal-agregar-cancion-acto/modal-agregar-cancion-acto.component';
import {ArtistaTourInterface} from '../../../../interfaces/artista-tour.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';

@Component({
  selector: 'app-seccion-setlist',
  templateUrl: './seccion-setlist.component.html',
  styleUrls: ['./seccion-setlist.component.css']
})
export class SeccionSetlistComponent implements OnInit {

  @Input()
  tour: TourInterface;

  @Input()
  setlistTour: SetlistInterface;

  actosSetlistTour: ActoTourInterface[] = [];
  cancionesSetlist: CancionSetlistInterface[] = [];
  cancionesSetlistOriginal: CancionSetlistInterface[] = [];
  actosSetlistTourOriginal: ActoTourInterface[] = [];
  editar = false;
  mostrarTablas = true;
  editarPosicionActos = false;
  idsArtistas = [];

  constructor(
    private readonly _cancioSetlistRestService: CancioSetlistRestService,
    private readonly _actoTourRestService: ActoTourRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    const tour = this.setlistTour?.tour as TourInterface;
    this.idsArtistas = (tour.artistasTour as ArtistaTourInterface[])
      .map((artistaTour: ArtistaTourInterface) => {
        return (artistaTour.artista as ArtistaInterface)?.id;
      });
    this.cargarActosSetlist();
  }

  cargarCancionesSetlist() {
    this._cargandoService.habilitarCargando();
    const consulta = {
      where: {
        setlistTour: this.setlistTour.id,
      },
      relations: [
        'actoTour', 'cancion'
      ]
    };
    this._cancioSetlistRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe(
        (respuesta: [CancionSetlistInterface[], number]) => {
          this.cancionesSetlist = respuesta[0];
          this.cancionesSetlistOriginal = [...this.cancionesSetlist];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  cargarActosSetlist() {
    this._cargandoService.habilitarCargando();
    const consulta = {
      where: {
        setlist: this.setlistTour.id,
      },
      relations: [
        'cancionesSetlistTour',
        'cancionesSetlistTour.cancion',
      ]
    };
    this._actoTourRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe(
        (respuesta: [ActoTourInterface[], number]) => {
          this.actosSetlistTour = respuesta[0];
          this.actosSetlistTourOriginal = [...this.actosSetlistTour];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  cancelar() {
    this.editar = false;
    this.cargarActosSetlist();
  }

  abrirModalCrearEditarActo(
    actoTour?: ActoTourInterface,
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarActoComponent,
        {
          width: '800px',
          data: {
            actoTour,
            tour: this.tour,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (registro: ActoTourInterface) => {
          if (registro) {
            if (actoTour) {
              const indice = this.actosSetlistTour.findIndex(actoTourS => actoTourS.id === actoTour?.id);
              this.actosSetlistTour[indice] = registro;
            } else {
              this.actosSetlistTour.push(registro);
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  eliminarActo(indice: number) {
    this.actosSetlistTour.splice(indice, 1);
  }

  abrirModaAgregarCancionActo(
    actoTour: ActoTourInterface,
  ) {
    const dialogRef = this.dialog
      .open(
        ModalAgregarCancionActoComponent,
        {
          width: '800px',
          data: {
            actoTour,
            tour: this.tour,
            idsArtistas: this.idsArtistas,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (registro: any) => {
          if (registro) {
            actoTour.cancionesSetlistTour.push(registro);
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  guardarActosTour() {
    this._cargandoService.habilitarCargando();
    const datos = {
      idSetlist: this.setlistTour.id,
      actosTour: this.actosSetlistTour,
    };
    this._actoTourRestService
      .crearActoTpur(
        datos
      )
      .subscribe((respuesta: any) => {
          this.editar = false;
          this._toasterService.pop(toastExitoCrear);
          this._cargandoService.deshabiltarCargando();
          this.cargarActosSetlist();
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        });
  }

  eliminarCancion(evento: any) {
    const actoTour = this.actosSetlistTour[evento.indiceActo];
    const cancionesActo = actoTour.cancionesSetlistTour;
    cancionesActo.splice(evento.indiceCancion);
  }
}
