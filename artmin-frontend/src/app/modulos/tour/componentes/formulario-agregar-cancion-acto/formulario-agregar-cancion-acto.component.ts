import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {CancionSetlistInterface} from '../../../../interfaces/cancion-setlist.interface';
import {
  MENSAJES_VALIDACION_ARTISTAS_CANCION_ACTO, MENSAJES_VALIDACION_CANCION_ACTO, MENSAJES_VALIDACION_CANCION_CANCION_ACTO,
  VALIDACION_ARTISTAS_CANCION_ACTO, VALIDACION_CANCION_CANCION_ACTO,
  VALIDACION_NOMBRE_CANCION_ACTO
} from '../../../../constantes/validaciones-formulario/cancion-acto';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';

@Component({
  selector: 'app-formulario-agregar-cancion-acto',
  templateUrl: './formulario-agregar-cancion-acto.component.html',
  styleUrls: ['./formulario-agregar-cancion-acto.component.css']
})
export class FormularioAgregarCancionActoComponent implements OnInit {
  formularioCancionActo: FormGroup;
  mensajesError = {
    nombre: [],
    artista: [],
    cancion: [],
  };
  subscribers = [];

  @Output()
  cancionActoValidoEnviar: EventEmitter<CancionSetlistInterface | boolean> = new EventEmitter();

  @Input()
  idsArtistas: number[] = [];

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    console.log(this.idsArtistas);
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioCancionActo = this._formBuilder.group({
      nombre: [null, VALIDACION_NOMBRE_CANCION_ACTO],
      artista: [null, VALIDACION_ARTISTAS_CANCION_ACTO],
      cancion: [null, VALIDACION_CANCION_CANCION_ACTO],
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_CANCION_ACTO);
    this.verificarCampoFormControl('artista', MENSAJES_VALIDACION_ARTISTAS_CANCION_ACTO);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCION_CANCION_ACTO);

  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioCancionActo.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioCancionActo;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.cancionActoValidoEnviar.emit(formulario);
          } else {
            this.cancionActoValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  buscarAlbumesPorArtista(respuestaArtista: ArtistaInterface[]) {
    this.idsArtistas = respuestaArtista.map(artista => artista.id);
    this.formularioCancionActo.patchValue(
      {
        artista: [
          ...respuestaArtista]
      }
    );
  }

  setearCancion(cancionSeleccionada: CancionInterface) {
    this.formularioCancionActo.patchValue(
      {
        cancion: cancionSeleccionada,
        nombre: cancionSeleccionada.nombre,
      }
    );
  }

}
