import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioAgregarCancionActoComponent } from './formulario-agregar-cancion-acto.component';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {InputTextModule} from 'primeng';
import {ReactiveFormsModule} from '@angular/forms';
import {AutocompleteArtistaModule} from '../../../../componentes/autocomplete-artista/autocomplete-artista.module';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';



@NgModule({
  declarations: [FormularioAgregarCancionActoComponent],
  imports: [
    CommonModule,
    AlertaValidacionCampoFormularioModule,
    InputTextModule,
    ReactiveFormsModule,
    AutocompleteArtistaModule,
    AutocompleteCancionModule
  ],
  exports: [FormularioAgregarCancionActoComponent]
})
export class FormularioAgregarCancionActoModule { }
