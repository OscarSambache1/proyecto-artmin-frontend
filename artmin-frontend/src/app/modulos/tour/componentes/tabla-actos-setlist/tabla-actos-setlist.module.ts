import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaActosSetlistComponent } from './tabla-actos-setlist.component';
import {TableModule} from 'primeng';
import {DragDropModule} from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [TablaActosSetlistComponent],
  imports: [
    CommonModule,
    TableModule,
    DragDropModule
  ],
  exports: [TablaActosSetlistComponent]
})
export class TablaActosSetlistModule { }
