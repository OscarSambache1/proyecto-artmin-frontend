import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CancionSetlistInterface} from '../../../../interfaces/cancion-setlist.interface';
import {MatDialog} from '@angular/material/dialog';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {SetlistInterface} from '../../../../interfaces/setlist.interface';
import {ActoTourInterface} from '../../../../interfaces/acto-tour.interface';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {ArtistaTourInterface} from '../../../../interfaces/artista-tour.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';

@Component({
  selector: 'app-tabla-actos-setlist',
  templateUrl: './tabla-actos-setlist.component.html',
  styleUrls: ['./tabla-actos-setlist.component.css']
})
export class TablaActosSetlistComponent implements OnInit {
  @Input()
  cancionesSetlist: CancionSetlistInterface[] = [];

  @Input()
  actosSetlistTour: ActoTourInterface[] = [];

  @Input()
  tour: TourInterface;

  @Input()
  setlistTour: SetlistInterface;

  @Output()
  eventoEditarActo: EventEmitter<ActoTourInterface> = new EventEmitter<ActoTourInterface>();

  @Output()
  eventoEliminarActo: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  eventoAgregarCancion: EventEmitter<ActoTourInterface> = new EventEmitter<ActoTourInterface>();

  @Output()
  eventoEliminarCancion: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
  }

  editar(actoTour: ActoTourInterface) {
    this.eventoEditarActo.emit(actoTour);
  }

  eliminar(indice: number) {
    this.eventoEliminarActo.emit(indice);
  }

  agregarCancion(actoTour: ActoTourInterface) {
    this.eventoAgregarCancion.emit(
      actoTour
    );
  }

  eliminarCancion(indiceActo: number, indiceCancion: number) {
    this.eventoEliminarCancion.emit(
      {
        indiceActo,
        indiceCancion,
      }
    );
  }
}
