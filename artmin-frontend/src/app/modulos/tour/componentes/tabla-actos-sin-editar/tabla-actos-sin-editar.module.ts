import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaActosSinEditarComponent } from './tabla-actos-sin-editar.component';



@NgModule({
  declarations: [TablaActosSinEditarComponent],
  imports: [
    CommonModule
  ],
  exports: [TablaActosSinEditarComponent]
})
export class TablaActosSinEditarModule { }
