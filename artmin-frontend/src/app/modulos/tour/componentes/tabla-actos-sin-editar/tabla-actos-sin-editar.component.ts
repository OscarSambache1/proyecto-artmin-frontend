import {Component, Input, OnInit} from '@angular/core';
import {CancionSetlistInterface} from '../../../../interfaces/cancion-setlist.interface';
import {ActoTourInterface} from '../../../../interfaces/acto-tour.interface';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {SetlistInterface} from '../../../../interfaces/setlist.interface';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-tabla-actos-sin-editar',
  templateUrl: './tabla-actos-sin-editar.component.html',
  styleUrls: ['./tabla-actos-sin-editar.component.css']
})
export class TablaActosSinEditarComponent implements OnInit {
  @Input()
  actosSetlistTour: ActoTourInterface[] = [];

  @Input()
  cancionesSetlist: CancionSetlistInterface[] = [];

  @Input()
  tour: TourInterface;

  @Input()
  setlistTour: SetlistInterface;


  constructor(
    public dialog: MatDialog,

  ) { }

  ngOnInit() {
  }
}
