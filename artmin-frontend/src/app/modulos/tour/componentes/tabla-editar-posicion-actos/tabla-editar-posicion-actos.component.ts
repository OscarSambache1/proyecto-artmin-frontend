import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActoTourInterface} from '../../../../interfaces/acto-tour.interface';
import {CancionSetlistInterface} from '../../../../interfaces/cancion-setlist.interface';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {SetlistInterface} from '../../../../interfaces/setlist.interface';
import {MatDialog} from '@angular/material/dialog';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-tabla-editar-posicion-actos',
  templateUrl: './tabla-editar-posicion-actos.component.html',
  styleUrls: ['./tabla-editar-posicion-actos.component.css']
})
export class TablaEditarPosicionActosComponent implements OnInit {
  @Input()
  actosSetlistTour: ActoTourInterface[] = [];

  @Input()
  cancionesSetlist: CancionSetlistInterface[] = [];

  @Input()
  tour: TourInterface;

  @Input()
  setlistTour: SetlistInterface;

  @Output()
  eventoEditar: EventEmitter<ActoTourInterface> = new EventEmitter<ActoTourInterface>();

  @Output()
  eventoEliminar: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  eventoEliminarCancion: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }

  drop(event: any) {
    moveItemInArray(this.actosSetlistTour, event.previousIndex, event.currentIndex);
  }

  editar(actoTour: ActoTourInterface) {
    this.eventoEditar.emit(actoTour);
  }

  eliminar(indice: number) {
    this.eventoEliminar.emit(indice);
  }

  eliminarCancion(indiceActo: number, indiceCancion: number) {
    this.eventoEliminarCancion.emit(
      {
        indiceActo,
        indiceCancion,
      }
    );
  }
}
