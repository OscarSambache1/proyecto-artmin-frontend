import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaEditarPosicionActosComponent } from './tabla-editar-posicion-actos.component';
import {DragDropModule} from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [TablaEditarPosicionActosComponent],
  imports: [
    CommonModule,
    DragDropModule
  ],
  exports: [TablaEditarPosicionActosComponent]
})
export class TablaEditarPosicionActosModule { }
