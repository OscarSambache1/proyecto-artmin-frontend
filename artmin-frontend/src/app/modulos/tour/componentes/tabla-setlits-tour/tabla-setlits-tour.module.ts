import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaSetlitsTourComponent } from './tabla-setlits-tour.component';
import {ModalCrearEditarSetlistModule} from '../../modales/modal-crear-editar-setlist/modal-crear-editar-setlist.module';
import {ModalCrearEditarSetlistComponent} from '../../modales/modal-crear-editar-setlist/modal-crear-editar-setlist.component';
import {ButtonModule, TableModule} from 'primeng';
import {TablaActosSetlistModule} from '../tabla-actos-setlist/tabla-actos-setlist.module';
import {SeccionSetlistModule} from '../seccion-setlist/seccion-setlist.module';


@NgModule({
    declarations: [TablaSetlitsTourComponent],
    exports: [
        TablaSetlitsTourComponent
    ],
  imports: [
    CommonModule,
    ModalCrearEditarSetlistModule,
    TableModule,
    ButtonModule,
    TablaActosSetlistModule,
    SeccionSetlistModule,
  ],
  entryComponents: [
    ModalCrearEditarSetlistComponent
  ]
})
export class TablaSetlitsTourModule { }
