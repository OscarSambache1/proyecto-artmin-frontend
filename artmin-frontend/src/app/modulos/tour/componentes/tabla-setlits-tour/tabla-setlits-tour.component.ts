import {Component, Input, OnInit} from '@angular/core';
import {SetlistInterface} from '../../../../interfaces/setlist.interface';
import {MatDialog} from '@angular/material/dialog';
import {ModalCrearEditarSetlistComponent} from '../../modales/modal-crear-editar-setlist/modal-crear-editar-setlist.component';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {SetlistRestService} from '../../../../servicios/rest/setlist-rest.service';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';

@Component({
  selector: 'app-tabla-setlits-tour',
  templateUrl: './tabla-setlits-tour.component.html',
  styleUrls: ['./tabla-setlits-tour.component.css']
})
export class TablaSetlitsTourComponent implements OnInit {
  @Input()
  tour: TourInterface;

  setlists: SetlistInterface[] = [];

  columnasSetlits = [
    {
      field: 'numero',
      header: '#',
      width: '5%'
    },
    {
      field: 'nombre',
      header: 'Nombre',
      width: '30%'
    },
    {
      field: 'fechaInicio',
      header: 'Fecha de Inicio',
      width: '15%'
    },
    {
      field: 'fechaFin',
      header: 'Fecha de Fin',
      width: '15%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '15%'
    },
  ];
  constructor(
    public dialog: MatDialog,
    private readonly _setlistRestService: SetlistRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
    this.cargarSetlist();
  }

  abrirModalCrearEditarSetlist(
    setlist?: SetlistInterface,
    index?: number
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarSetlistComponent,
        {
          width: '800px',
          data: {
            setlist,
            idTour: this.tour?.id,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (registro: SetlistInterface) => {
          if (registro) {
            if (setlist) {
              this.setlists[index] = registro;
            } else {
              this.setlists.push(registro);
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  cargarSetlist() {
    this._cargandoService.habilitarCargando();
    const consulta = {
      where: {
        tour: this.tour.id,
      },
      relations: [
        'tour',
        'tour.artistasTour',
        'tour.artistasTour.artista'
      ]
    };
    this._setlistRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe(
        (respuesta: [SetlistInterface[], number]) => {
          this.setlists = respuesta[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

}
