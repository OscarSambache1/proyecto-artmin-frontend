import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ActoTourInterface} from '../../../../interfaces/acto-tour.interface';
import {MENSAJES_VALIDACION_NOMBRE_ACTO, VALIDACION_NOMBRE_ACTO} from '../../../../constantes/validaciones-formulario/acto-tour';

@Component({
  selector: 'app-formulario-acto',
  templateUrl: './formulario-acto.component.html',
  styleUrls: ['./formulario-acto.component.css']
})
export class FormularioActoComponent implements OnInit {
  formularioTour: FormGroup;
  mensajesError = {
    nombre: [],
  };
  subscribers = [];

  @Input()
  actoTour: ActoTourInterface;

  @Output()
  actoTourValidoEnviar: EventEmitter<ActoTourInterface | boolean> = new EventEmitter();

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioTour = this._formBuilder.group({
      nombre: [this.actoTour ? this.actoTour.nombre : '', VALIDACION_NOMBRE_ACTO],
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_ACTO);

  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioTour.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioTour;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.actoTourValidoEnviar.emit(formulario);
          } else {
            this.actoTourValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }
}
