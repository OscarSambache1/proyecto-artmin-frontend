import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioActoComponent } from './formulario-acto.component';
import {InputTextModule} from 'primeng';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';



@NgModule({
  declarations: [FormularioActoComponent],
  imports: [
    CommonModule,
    InputTextModule,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule
  ],
  exports: [FormularioActoComponent],
})
export class FormularioActoModule { }
