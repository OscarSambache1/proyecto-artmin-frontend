import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TourInterface} from '../../../../interfaces/tour.interface';
import {SelectItem} from 'primeng';
import {InputImagenComponent} from '../../../../componentes/input-imagen/input-imagen.component';
import {ToasterService} from 'angular2-toaster';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {
  MENSAJES_VALIDACION_ARTISTAS_TOUR,
  MENSAJES_VALIDACION_CANTIDAD_SHOWS_TOUR,
  MENSAJES_VALIDACION_CANTIDAD_TICKETS_TOUR,
  MENSAJES_VALIDACION_DESCRIPCION_TOUR,
  MENSAJES_VALIDACION_FECHA_FIN_TOUR,
  MENSAJES_VALIDACION_FECHA_INICIO_TOUR,
  MENSAJES_VALIDACION_IMAGEN_TOUR,
  MENSAJES_VALIDACION_LUGAR_FIN_TOUR,
  MENSAJES_VALIDACION_LUGAR_INICIO_TOUR,
  MENSAJES_VALIDACION_NOMBRE_TOUR, MENSAJES_VALIDACION_OBSERVACIONES_TOUR,
  MENSAJES_VALIDACION_RECAUDACION_TOUR,
  MENSAJES_VALIDACION_SINOPSIS_TOUR,
  MENSAJES_VALIDACION_TIPO_TOUR,
  VALIDACION_ARTISTAS_TOUR,
  VALIDACION_CANTIDAD_SHOWS_TOUR,
  VALIDACION_CANTIDAD_TICKETS_TOUR, VALIDACION_DESCRIPCION_TOUR,
  VALIDACION_FECHA_FIN_TOUR,
  VALIDACION_FECHA_INICIO_TOUR,
  VALIDACION_IMAGEN_TOUR,
  VALIDACION_LUGAR_FIN_TOUR,
  VALIDACION_LUGAR_INICIO_TOUR, VALIDACION_NOMBRE_TOUR, VALIDACION_OBSERVACIONES_TOUR,
  VALIDACION_RECAAUDACION_TOUR, VALIDACION_SINOPSIS_TOUR,
  VALIDACION_TIPO_TOUR
} from '../../../../constantes/validaciones-formulario/tour';
import {TIPOS_ALBUMES} from '../../../../constantes/tipos-album';
import {TIPOS_TOUR, TIPOS_TOUR_ENUM} from '../../../../constantes/tipos-tour';
import {ArtistaTourInterface} from '../../../../interfaces/artista-tour.interface';
import {FestivalInterface} from '../../../../interfaces/festival.interface';

@Component({
  selector: 'app-formulario-tour',
  templateUrl: './formulario-tour.component.html',
  styleUrls: ['./formulario-tour.component.css']
})
export class FormularioTourComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;
  formularioTour: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    sinopsis: [],
    observaciones: [],
    calificacion: [],
    fechaInicio: [],
    fechaFin: [],
    lugarInicio: [],
    lugarFin: [],
    cantidadShows: [],
    cantidadTickets: [],
    recaudacion: [],
    imagen: [],
    artistas: [],
    tipo: [],
    festival: [],
  };
  subscribers = [];
  imagenSeleccionada;

  @Input()
  tour: TourInterface;

  tipos: SelectItem[] = TIPOS_TOUR;

  @Input()
  width: number;

  @Input()
  idArtista: number;

  @Input()
  idsArtista: number[];

  @Output()
  tourValidoEnviar: EventEmitter<TourInterface | boolean> = new EventEmitter();

  @ViewChild(InputImagenComponent)
  componenteInputImagen: InputImagenComponent;

  festival: FestivalInterface;

  idTipoTour: number;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _artistaService: ArtistaRestService
  ) {
  }

  get esFestival(): boolean {
    const tipo = this.formularioTour?.get('tipo')?.value?.value;
    return tipo === 'festival';
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    if (this.tour) {
      this.deshabilitarFormulario();
    } else {
      if (this.idsArtista && this.idsArtista.length) {
        this.cargarMultiselectArtista();
      }
    }
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioTour = this._formBuilder.group({
      nombre: [this.tour ? this.tour.nombre : '', VALIDACION_NOMBRE_TOUR],
      descripcion: [this.tour ? this.tour.descripcion : '', VALIDACION_DESCRIPCION_TOUR],
      sinopsis: [this.tour ? this.tour.sinopsis : '', VALIDACION_SINOPSIS_TOUR],
      observaciones: [this.tour ? this.tour.observaciones : '', VALIDACION_OBSERVACIONES_TOUR],
      fechaInicio: [this.tour ? this.tour.fechaInicio : '', VALIDACION_FECHA_INICIO_TOUR],
      fechaFin: [this.tour ? this.tour.fechaFin : null, VALIDACION_FECHA_FIN_TOUR],
      lugarInicio: [this.tour ? this.tour.lugarInicio : '', VALIDACION_LUGAR_INICIO_TOUR],
      lugarFin: [this.tour ? this.tour.lugarFin : null, VALIDACION_LUGAR_FIN_TOUR],
      cantidadShows: [this.tour ? this.tour.cantidadShows : 0, VALIDACION_CANTIDAD_SHOWS_TOUR],
      cantidadTickets: [this.tour ? this.tour.cantidadTickets : 0, VALIDACION_CANTIDAD_TICKETS_TOUR],
      recaudacion: [this.tour ? this.tour.recaudacion : 0, VALIDACION_RECAAUDACION_TOUR],
      artistas: [this.tour ? this.cargarArtistasTour() : [], VALIDACION_ARTISTAS_TOUR],
      imagen: [this.tour ? this.cargarImagenTour() : '', VALIDACION_IMAGEN_TOUR],
      tipo: [this.tour ? this.retornarTipo(this.tour.tipo) : null, VALIDACION_TIPO_TOUR],
      festival: [this.tour ? this.tour.festival : null, []],
    });
    if (this.tour && this.tour.festival) {
      this.festival = this.tour.festival;
    }
  }

  retornarTipo(tipo: string) {
    return {
      value: tipo,
      key: TIPOS_TOUR_ENUM[tipo]
    };
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_TOUR);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_TOUR);
    this.verificarCampoFormControl('sinopsis', MENSAJES_VALIDACION_SINOPSIS_TOUR);
    this.verificarCampoFormControl('observaciones', MENSAJES_VALIDACION_OBSERVACIONES_TOUR);
    this.verificarCampoFormControl('fechaInicio', MENSAJES_VALIDACION_FECHA_INICIO_TOUR);
    this.verificarCampoFormControl('fechaFin', MENSAJES_VALIDACION_FECHA_FIN_TOUR);
    this.verificarCampoFormControl('lugarInicio', MENSAJES_VALIDACION_LUGAR_INICIO_TOUR);
    this.verificarCampoFormControl('lugarFin', MENSAJES_VALIDACION_LUGAR_FIN_TOUR);
    this.verificarCampoFormControl('cantidadShows', MENSAJES_VALIDACION_CANTIDAD_SHOWS_TOUR);
    this.verificarCampoFormControl('cantidadTickets', MENSAJES_VALIDACION_CANTIDAD_TICKETS_TOUR);
    this.verificarCampoFormControl('recaudacion', MENSAJES_VALIDACION_RECAUDACION_TOUR);
    this.verificarCampoFormControl('artistas', MENSAJES_VALIDACION_ARTISTAS_TOUR);
    this.verificarCampoFormControl('imagen', MENSAJES_VALIDACION_IMAGEN_TOUR);
    this.verificarCampoFormControl('tipo', MENSAJES_VALIDACION_TIPO_TOUR);
    this.verificarCampoFormControl('festival', '');
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioTour.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioTour;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.tourValidoEnviar.emit(formulario);
          } else {
            this.tourValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharEventoImagen(event: any) {
    this.imagenSeleccionada = event;
    this.formularioTour.patchValue(
      {
        imagen: this.imagenSeleccionada,
      }
    );
  }

  cargarImagenTour() {
    if (this.tour) {
      return obtenerUrlImagenPrincipal(this.tour, 'imagenesTour');
    }
  }

  deshabilitarFormulario() {
    this.formularioTour.disable();
  }

  buscarTouresPorArtista(respuestaArtista: ArtistaInterface[]) {
    this.formularioTour.patchValue(
      {
        artistas: [
          ...respuestaArtista]
      }
    );
  }

  cargarMultiselectArtista() {
    const consulta = {
      camposIn: [
        {
          nombreCampo: 'id',
          valor: this.idsArtista
        }
      ],
      relations: [
        'imagenesArtista'
      ]
    };
    this._artistaService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe( (respuestaArtista: [ArtistaInterface[], number]) => {
        this.formularioTour.patchValue(
          {
            artistas: [...respuestaArtista[0]]
          }
        );
      }, error => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
      });
  }

  cargarArtistasTour() {
    return (this.tour.artistasTour as ArtistaTourInterface[]).map(artistaTour => artistaTour.artista as ArtistaInterface);
  }

  volverFormularioInicial() {
    this.formularioTour.patchValue(
      {
        nombre: this.tour.nombre,
        descripcion: this.tour.descripcion,
        sinopsis: this.tour.sinopsis,
        observaciones: this.tour.observaciones,
        fechaFin: this.tour.fechaFin,
        fechaInicio: this.tour.fechaInicio,
        lugarInicio: this.tour.lugarInicio,
        lugarFin: this.tour.lugarFin,
        cantidadTickets: this.tour.cantidadTickets,
        cantidadShows: this.tour.cantidadShows,
        recaudacion: this.tour.recaudacion,
        tipo: this.tour.tipo,
        artistas: this.cargarArtistasTour(),
        imagen: this.cargarImagenTour(),
      }
    );
    this.componenteInputImagen.pathImagen = this.cargarImagenTour();
    this.deshabilitarFormulario();
  }

  escucharTipoTourSeleccionado(
    tipoTourSeleccionado,
  ) {
    this.formularioTour.patchValue(
      {
        tipo: tipoTourSeleccionado,
      }
    );
  }

  setearFestival(festival: FestivalInterface) {
    this.formularioTour.patchValue(
      {
        festival,
      }
    );
  }
}
