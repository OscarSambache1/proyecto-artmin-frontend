import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  AutoCompleteModule,
  CalendarModule, DropdownModule,
  InputNumberModule,
  InputTextareaModule,
  InputTextModule,
  MultiSelectModule
} from 'primeng';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteArtistaModule} from '../../../../componentes/autocomplete-artista/autocomplete-artista.module';
import {FormularioTourComponent} from './formulario-tour.component';
import {AutocompleteFestivalModule} from '../../../../componentes/autocomplete-festival/autocomplete-festival.module';



@NgModule({
  declarations: [
    FormularioTourComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        InputTextModule,
        InputTextareaModule,
        FormsModule,
        CalendarModule,
        InputNumberModule,
        MultiSelectModule,
        InputImagenModule,
        AlertaValidacionCampoFormularioModule,
        AutocompleteArtistaModule,
        DropdownModule,
        AutocompleteFestivalModule,
    ],
  exports: [
    FormularioTourComponent
  ]
})
export class FormularioTourModule { }
