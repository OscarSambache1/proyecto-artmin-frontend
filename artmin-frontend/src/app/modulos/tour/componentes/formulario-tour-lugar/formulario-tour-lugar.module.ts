import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioTourLugarComponent } from './formulario-tour-lugar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {
  CalendarModule,
  InputMaskModule,
  InputNumberModule,
  InputSwitchModule,
  InputTextareaModule,
  InputTextModule
} from 'primeng';
import {SafePipeModule} from '../../../../pipes/safe-pipe/safe-pipe.module';
import {MaskedInputDirective} from 'angular2-text-mask';
import {MaskinputModuleModule} from '../../../../componentes/maskinput-module/maskinput-module.module';
import {AutocompleteLugarModule} from '../../../../componentes/autocomplete-lugar/autocomplete-lugar.module';
import {AutocompleteRecintoModule} from '../../../../componentes/autocomplete-recinto/autocomplete-recinto.module';
import {AutocompleteFestivalModule} from '../../../../componentes/autocomplete-festival/autocomplete-festival.module';



@NgModule({
  declarations: [FormularioTourLugarComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AlertaValidacionCampoFormularioModule,
        InputNumberModule,
        CalendarModule,
        InputTextModule,
        InputTextareaModule,
        InputMaskModule,
        SafePipeModule,
        FormsModule,
        MaskinputModuleModule,
        InputSwitchModule,
        AutocompleteLugarModule,
        AutocompleteRecintoModule,
        AutocompleteFestivalModule,
    ],
  exports: [
    FormularioTourLugarComponent,
  ]
})
export class FormularioTourLugarModule { }
