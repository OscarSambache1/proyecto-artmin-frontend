import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {TourLugarInterface} from '../../../../interfaces/tour-lugar.interface';
import {
  MENSAJES_VALIDACION_FECHA_TOUR_LUGAR,
  MENSAJES_VALIDACION_HORA_TOUR_LUGAR,
  MENSAJES_VALIDACION_LOCACION_TOUR_LUGAR,
  MENSAJES_VALIDACION_LUGAR_TOUR_LUGAR,
  MENSAJES_VALIDACION_OBSERVACION_TOUR_LUGAR, MENSAJES_VALIDACION_PAIS_TOUR_LUGAR,
  MENSAJES_VALIDACION_RAZON_CANCELACION_TOUR_LUGAR,
  MENSAJES_VALIDACION_RECAUDACION_TOUR_LUGAR,
  MENSAJES_VALIDACION_RECINTO_TOUR_LUGAR,
  MENSAJES_VALIDACION_SE_CANCELO_TOUR_LUGAR,
  MENSAJES_VALIDACION_TICKETS_DISPONIBLES_TOUR_LUGAR,
  MENSAJES_VALIDACION_TICKETS_VENDIDOS_TOUR_LUGAR,
  VALIDACION_FECHA_TOUR_LUGAR,
  VALIDACION_HORA_TOUR_LUGAR,
  VALIDACION_LOCACION_TOUR_LUGAR, VALIDACION_LUGAR_TOUR_LUGAR,
  VALIDACION_OBSERVACION_TOUR_LUGAR, VALIDACION_PAIS_TOUR_LUGAR,
  VALIDACION_RAZON_CANCELACION_TOUR_LUGAR,
  VALIDACION_RECAUDACION_TOUR_LUGAR,
  VALIDACION_RECINTO_TOUR_LUGAR,
  VALIDACION_SE_CANCELO_TOUR_LUGAR,
  VALIDACION_TICKETS_DISPONIBLES_TOUR_LUGAR,
  VALIDACION_TICKETS_VENDIDOS_TOUR_LUGAR
} from '../../../../constantes/validaciones-formulario/tour-lugar';
import {LugarInterface} from '../../../../interfaces/lugar.interface';
import {RecintoInterface} from '../../../../interfaces/recinto-interface';
import {FestivalInterface} from '../../../../interfaces/festival.interface';
import {TourInterface} from '../../../../interfaces/tour.interface';

@Component({
  selector: 'app-formulario-tour-lugar',
  templateUrl: './formulario-tour-lugar.component.html',
  styleUrls: ['./formulario-tour-lugar.component.css']
})
export class FormularioTourLugarComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;
  formularioTour: FormGroup;
  mensajesError = {
    locacion: [],
    ticketsDisponibles: [],
    seCancelo: [],
    razonCancelacion: [],
    recaudacion: [],
    fecha: [],
    hora: [],
    observacion: [],
    ticketsVendidos: [],
    recinto: [],
    lugar: [],
    pais: [],
    esFestival: [],
    festival: [],
  };
  subscribers = [];
  public mask = [/[0-2]/, /[0-3]/, ':', /[0-5]/, /\d/, ':', /[0-5]/, /\d/];

  @Input()
  tourLugar: TourLugarInterface;

  @Input()
  tour: TourInterface;

  @Input()
  width: number;

  festival: FestivalInterface;

  @Output()
  tourLugarValidoEnviar: EventEmitter<TourLugarInterface | boolean> = new EventEmitter();

  idLugarPadre: number;
  idsLugarAsignado: number[] = [-1];
  lugar: LugarInterface;

  get esFestival(): boolean {
    const esFestival = this.formularioTour?.get('esFestival')?.value;
    return !!esFestival;
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _artistaService: ArtistaRestService
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    if (this.tourLugar) {
      this.lugar = this.tourLugar.lugar as LugarInterface;
      this.idsLugarAsignado = [this.lugar.id];
      this.idLugarPadre = (this.lugar.lugarPadre as LugarInterface).id;
    }
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioTour = this._formBuilder.group({
      locacion: [this.tourLugar ? this.tourLugar.locacion : '', VALIDACION_LOCACION_TOUR_LUGAR],
      ticketsDisponibles: [this.tourLugar ? this.tourLugar.ticketsDisponibles : 0, VALIDACION_TICKETS_DISPONIBLES_TOUR_LUGAR],
      ticketsVendidos: [this.tourLugar ? this.tourLugar.ticketsVendidos : 0, VALIDACION_TICKETS_VENDIDOS_TOUR_LUGAR],
      seCancelo: [this.tourLugar ? this.tourLugar.seCancelo : 0, VALIDACION_SE_CANCELO_TOUR_LUGAR],
      razonCancelacion: [this.tourLugar ? this.tourLugar.razonCancelacion : '', VALIDACION_RAZON_CANCELACION_TOUR_LUGAR],
      recaudacion: [this.tourLugar ? this.tourLugar.recaudacion : 0, VALIDACION_RECAUDACION_TOUR_LUGAR],
      fecha: [this.tourLugar ? this.tourLugar.fecha : null, VALIDACION_FECHA_TOUR_LUGAR],
      hora: [this.tourLugar ? this.tourLugar.hora : '', VALIDACION_HORA_TOUR_LUGAR],
      observacion: [this.tourLugar ? this.tourLugar.observacion : '', VALIDACION_OBSERVACION_TOUR_LUGAR],
      recinto: [this.tourLugar ? this.tourLugar.recinto : '', VALIDACION_RECINTO_TOUR_LUGAR],
      lugar: [this.tourLugar ? this.tourLugar.lugar : '', VALIDACION_LUGAR_TOUR_LUGAR],
      pais: [this.tourLugar ? (this.tourLugar?.lugar as LugarInterface)?.lugarPadre : '', VALIDACION_PAIS_TOUR_LUGAR],
      esFestival: [this.tourLugar ? this.tourLugar.esFestival : 0, []],
      festival: [this.tourLugar ? this.tourLugar.festival : null, []],
    });
    const tipoTourFestival = this.tour?.tipo === 'festival';
    if (tipoTourFestival) {
      this.festival = this.tour?.festival;
      this.formularioTour.patchValue(
        {
          esFestival: true,
          festival: this.festival,
        },
      );
    }
    if (this.tourLugar && this.tourLugar.festival) {
      this.festival = this.tourLugar.festival;
    }
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('locacion', MENSAJES_VALIDACION_LOCACION_TOUR_LUGAR);
    this.verificarCampoFormControl('ticketsDisponibles', MENSAJES_VALIDACION_TICKETS_DISPONIBLES_TOUR_LUGAR);
    this.verificarCampoFormControl('ticketsVendidos', MENSAJES_VALIDACION_TICKETS_VENDIDOS_TOUR_LUGAR);
    this.verificarCampoFormControl('seCancelo', MENSAJES_VALIDACION_SE_CANCELO_TOUR_LUGAR);
    this.verificarCampoFormControl('razonCancelacion', MENSAJES_VALIDACION_RAZON_CANCELACION_TOUR_LUGAR);
    this.verificarCampoFormControl('recaudacion', MENSAJES_VALIDACION_RECAUDACION_TOUR_LUGAR);
    this.verificarCampoFormControl('fecha', MENSAJES_VALIDACION_FECHA_TOUR_LUGAR);
    this.verificarCampoFormControl('hora', MENSAJES_VALIDACION_HORA_TOUR_LUGAR);
    this.verificarCampoFormControl('observacion', MENSAJES_VALIDACION_OBSERVACION_TOUR_LUGAR);
    this.verificarCampoFormControl('recinto', MENSAJES_VALIDACION_RECINTO_TOUR_LUGAR);
    this.verificarCampoFormControl('lugar', MENSAJES_VALIDACION_LUGAR_TOUR_LUGAR);
    this.verificarCampoFormControl('pais', MENSAJES_VALIDACION_PAIS_TOUR_LUGAR);
    this.verificarCampoFormControl('esFestival', '');
    this.verificarCampoFormControl('festival', '');
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioTour.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioTour;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.tourLugarValidoEnviar.emit(formulario);
          } else {
            this.tourLugarValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharLugarSeleccionado(lugarSeleccinado: LugarInterface) {
    this.formularioTour.patchValue(
      {
        lugar: lugarSeleccinado,
      }
    );
  }

  escucharLugarPadreSeleccionado(lugarSeleccinado: LugarInterface) {
    this.idLugarPadre = lugarSeleccinado.id;
    this.idsLugarAsignado = [-1];
    this.lugar = null;
    this.formularioTour.patchValue(
      {
        pais: lugarSeleccinado,
        lugar: null,
      }
    );
  }

  escucharRecintoSeleccionado(recintoSeleccionado: RecintoInterface) {
    this.formularioTour.patchValue(
      {
        recinto: recintoSeleccionado,
      }
    );
  }

  setearFestival(festival: FestivalInterface) {
    this.formularioTour.patchValue(
      {
        festival,
      }
    );
  }
}
