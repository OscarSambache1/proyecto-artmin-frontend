import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioSetlistComponent } from './formulario-setlist.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CalendarModule, DropdownModule, InputTextModule} from "primeng";
import {AlertaValidacionCampoFormularioModule} from "../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module";



@NgModule({
  declarations: [FormularioSetlistComponent],
  exports: [
    FormularioSetlistComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        AlertaValidacionCampoFormularioModule,
        DropdownModule,
    ]
})
export class FormularioSetlistModule { }
