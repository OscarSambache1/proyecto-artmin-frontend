import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SetlistInterface} from '../../../../interfaces/setlist.interface';
import {ToasterService} from 'angular2-toaster';
import {
  MENSAJES_VALIDACION_FECHA_FIN_SETLIST,
  MENSAJES_VALIDACION_FECHA_INICIO_SETLIST,
  MENSAJES_VALIDACION_NOMBRE_SETLIST, MENSAJES_VALIDACION_TIENE_ACTOS_SETLIST,
  VALIDACION_FECHA_FIN_SETLIST, VALIDACION_FECHA_INICIO_SETLIST,
  VALIDACION_NOMBRE_SETLIST, VALIDACION_TIENE_ACTOS_SETLIST
} from '../../../../constantes/validaciones-formulario/setlist';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {SelectItem} from "primeng";

@Component({
  selector: 'app-formulario-setlist',
  templateUrl: './formulario-setlist.component.html',
  styleUrls: ['./formulario-setlist.component.css']
})
export class FormularioSetlistComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;
  formularioSetlist: FormGroup;
  mensajesError = {
    nombre: [],
    fechaInicio: [],
    fechaFin: [],
    tieneActos: [],
  };
  subscribers = [];
  tieneActos: SelectItem[] = [
    {
      label: 'SI',
      value: true,
    },
    {
      label: 'NO',
      value: false,
    },
  ];
  @Input()
  setlist: SetlistInterface;

  @Output()
  setlistValidoEnviar: EventEmitter<SetlistInterface | boolean> = new EventEmitter();


  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioSetlist = this._formBuilder.group({
      nombre: [this.setlist ? this.setlist.nombre : '', VALIDACION_NOMBRE_SETLIST],
      fechaInicio: [this.setlist ? this.setlist.fechaInicio : '', VALIDACION_FECHA_INICIO_SETLIST],
      fechaFin: [this.setlist ? this.setlist.fechaFin : '', VALIDACION_FECHA_FIN_SETLIST],
      tieneActos: [this.setlist ? this.setlist.tieneActos : '', VALIDACION_TIENE_ACTOS_SETLIST],
    });
  }

  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_SETLIST);
    this.verificarCampoFormControl('fechaInicio', MENSAJES_VALIDACION_FECHA_INICIO_SETLIST);
    this.verificarCampoFormControl('fechaFin', MENSAJES_VALIDACION_FECHA_FIN_SETLIST);
    this.verificarCampoFormControl('tieneActos', MENSAJES_VALIDACION_TIENE_ACTOS_SETLIST);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioSetlist.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioSetlist;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const formularioValido = formularioFormGroup.valid;
          if (formularioValido) {
            this.setlistValidoEnviar.emit(formulario);
          } else {
            this.setlistValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }
}
