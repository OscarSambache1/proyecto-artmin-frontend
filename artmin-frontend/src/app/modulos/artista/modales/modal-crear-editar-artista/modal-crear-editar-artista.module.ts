import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarArtistaComponent } from './modal-crear-editar-artista.component';
import {MatDialogModule} from '@angular/material/dialog';
import {FormularioArtistaModule} from '../../componentes/formulario-artista/formulario-artista.module';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarArtistaComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioArtistaModule,
    BotonGuardarModule,
    BotonCancelarModule
  ],
  exports: [
    ModalCrearEditarArtistaComponent
  ]
})
export class ModalCrearEditarArtistaModule { }
