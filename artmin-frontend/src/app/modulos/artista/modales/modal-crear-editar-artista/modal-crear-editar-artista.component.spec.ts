import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarArtistaComponent } from './modal-crear-editar-artista.component';

describe('ModalCrearEditarArtistaComponent', () => {
  let component: ModalCrearEditarArtistaComponent;
  let fixture: ComponentFixture<ModalCrearEditarArtistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarArtistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarArtistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
