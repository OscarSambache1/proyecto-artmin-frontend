import {Component, Inject, OnInit} from '@angular/core';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {PlataformaInterface} from '../../../../interfaces/plataforma.interface';

@Component({
  selector: 'app-modal-crear-editar-artista',
  templateUrl: './modal-crear-editar-artista.component.html',
  styleUrls: ['./modal-crear-editar-artista.component.css']
})
export class ModalCrearEditarArtistaComponent implements OnInit {
  formularioValido: boolean;
  artistaACrearEditar: ArtistaInterface;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {},
    public dialogo: MatDialogRef<ModalCrearEditarArtistaComponent>,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) {
  }

  ngOnInit(): void {
  }

  validarFormularioArtista(artista: ArtistaInterface | boolean) {
    if (artista) {
      this.formularioValido = true;
      this.artistaACrearEditar = artista as ArtistaInterface;
    } else {
      this.formularioValido = false;
    }
  }

  crearEditarArtista() {
    this._cargandoService.habilitarCargando();
    const generos = [...this.artistaACrearEditar.generos]?.map(genero => genero.id);
    const enlaces = this.artistaACrearEditar.enlacesArtista?.map( enlaceArtista => {
      enlaceArtista.plataforma = (enlaceArtista.plataforma as PlataformaInterface).id;
      return enlaceArtista;
    });
    this._artistaRestService
      .crearArtistaImagen(
        this.artistaACrearEditar,
        generos,
        enlaces,
        this.artistaACrearEditar.imagen,
      )
      .subscribe( respuestaArtistaCreado => {
          this._toasterService.pop(toastExitoCrear);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close(respuestaArtistaCreado);
      },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
          this.dialogo.close();
        });
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
