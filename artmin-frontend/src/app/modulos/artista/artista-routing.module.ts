import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionArtistasComponent} from './rutas/ruta-gestion-artistas/ruta-gestion-artistas.component';
import {RutaEditarArtistaComponent} from './rutas/ruta-editar-artista/ruta-editar-artista.component';
import {RutaNominacionesComponent} from '../../componentes/ruta-nominaciones/ruta-nominaciones.component';
import {RutaPresentacionesComponent} from '../../componentes/ruta-presentaciones/ruta-presentaciones.component';


const routes: Routes = [
  {
    path: 'gestion-artistas',
    children: [
      {
        path: '',
        component: RutaGestionArtistasComponent
      },
      {
        path: 'editar-artista/:idArtista',
        children: [
          {
            path: '',
            component: RutaEditarArtistaComponent,
          },
          {
            path: 'nominaciones/:tipo',
            component: RutaNominacionesComponent,
          },
          {
            path: 'presentaciones/:tipo',
            component: RutaPresentacionesComponent,
          },
          {
            path: 'album-modulo',
            loadChildren: 'src/app/modulos/album/album.module#AlbumModule',
          },
          {
            path: 'cancion-modulo',
            loadChildren: 'src/app/modulos/cancion/cancion.module#CancionModule',
          },
          {
            path: 'video-modulo',
            loadChildren: 'src/app/modulos/video/video.module#VideoModule',
          },
          {
            path: 'chart-modulo/:tipo',
            loadChildren: 'src/app/modulos/chart/chart.module#ChartModule',
          },
          {
            path: 'tour-modulo',
            loadChildren: 'src/app/modulos/tour/tour.module#TourModule',
          }
        ]
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-artistas',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistaRoutingModule { }
