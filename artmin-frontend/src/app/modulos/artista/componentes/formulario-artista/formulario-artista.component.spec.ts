import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioArtistaComponent } from './formulario-artista.component';

describe('FormularioArtistaComponent', () => {
  let component: FormularioArtistaComponent;
  let fixture: ComponentFixture<FormularioArtistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioArtistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioArtistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
