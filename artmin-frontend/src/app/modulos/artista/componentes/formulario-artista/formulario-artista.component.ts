import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {GeneroInterface} from '../../../../interfaces/genero.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {
  MENSAJES_VALIDACION_ANIO_DEBUT_ARTISTA,
  MENSAJES_VALIDACION_DESCRIPCION_ARTISTA,
  MENSAJES_VALIDACION_FECHA_NACIMIENTO_ARTISTA,
  MENSAJES_VALIDACION_GENEROS_ARTISTA,
  MENSAJES_VALIDACION_NOMBRE_ARTISTA, VALIDACION_ANIO_DEBUT_ARTISTA,
  VALIDACION_DESCRIPCION_ARTISTA,
  VALIDACION_FECHA_NACIMIENTO_ARTISTA, VALIDACION_GENEROS_ARTISTA, VALIDACION_IMAGEN_ARTISTA,
  VALIDACION_NOMBRE_ARTISTA
} from '../../../../constantes/validaciones-formulario/artista';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ToastErrorCargandoDatos, ToastErrorEstado} from '../../../../constantes/mensajes-toaster';
import {environment} from '../../../../../environments/environment';
import {InputImagenComponent} from '../../../../componentes/input-imagen/input-imagen.component';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {PlataformaInterface} from '../../../../interfaces/plataforma.interface';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../../../interfaces/enlace-album-cancion-artista-video.interface';
import {MatDialog} from '@angular/material/dialog';
import {CrearEditarEnlaceComponent} from '../../../enlace-album-cancion-artista-video/modales/crear-editar-enlace/crear-editar-enlace.component';

@Component({
  selector: 'app-formulario-artista',
  templateUrl: './formulario-artista.component.html',
  styleUrls: ['./formulario-artista.component.css']
})
export class FormularioArtistaComponent implements OnInit {
  configuracionCalendario = CONFIGURACIONES_CALENDARIO;
  arregloGeneros: GeneroInterface[] = [];
  formularioArtista: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    anioDebut: [],
    fechaNacimiento: [],
    generos: [],
    imagen: []
  };
  subscribers = [];
  imagenSeleccionada;

  @Input()
  width: number;

  @Input()
  artista: ArtistaInterface;

  @Output()
  artistaValidoEnviar: EventEmitter<ArtistaInterface | boolean> = new EventEmitter();

  @ViewChild(InputImagenComponent)
  componenteInputImagen: InputImagenComponent;

  enlacesArtista: EnlaceAlbumCancionArtistaVideoInterface[] = [];
  idsPlataformas: number[] = [];

  constructor(
    private readonly _generoRestService: GeneroRestService,
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.cargarGeneros();
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    if (this.artista) {
      this.deshabilitarFormulario();
    }
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioArtista = this._formBuilder.group({
      nombre: [this.artista ? this.artista.nombre : '', VALIDACION_NOMBRE_ARTISTA],
      descripcion: [this.artista ? this.artista.descripcion : '', VALIDACION_DESCRIPCION_ARTISTA],
      fechaNacimiento: [this.artista ? this.artista.fechaNacimiento : '', VALIDACION_FECHA_NACIMIENTO_ARTISTA],
      anioDebut: [this.artista ? this.artista.anioDebut : null, VALIDACION_ANIO_DEBUT_ARTISTA],
      generos: [this.artista ? this.cargarGenerosArtista() : [], VALIDACION_GENEROS_ARTISTA],
      imagen: [this.artista ? this.cargarImagenArtista() : '', VALIDACION_IMAGEN_ARTISTA],
      enlacesArtista: [this.artista ? this.cargarEnlaces() : null, []]
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_ARTISTA);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_ARTISTA);
    this.verificarCampoFormControl('fechaNacimiento', MENSAJES_VALIDACION_FECHA_NACIMIENTO_ARTISTA);
    this.verificarCampoFormControl('anioDebut', MENSAJES_VALIDACION_ANIO_DEBUT_ARTISTA);
    this.verificarCampoFormControl('generos', MENSAJES_VALIDACION_GENEROS_ARTISTA);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioArtista.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioArtista;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.artistaValidoEnviar.emit(formulario);
          } else {
            this.artistaValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharEventoImagen(event: any) {
    this.imagenSeleccionada = event;
    this.formularioArtista.patchValue(
      {
        imagen: this.imagenSeleccionada,
      }
    );
  }

  cargarGeneros() {
    this._generoRestService
      .findAll(JSON.stringify({}))
      .subscribe((respuestaGeneros: [GeneroInterface[], number]) => {
        this.arregloGeneros = respuestaGeneros[0];
      }, error => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
      });
  }

  cargarGenerosArtista() {
    return this.artista.generosArtista.map(generoArtista => generoArtista.genero as GeneroInterface);
  }

  cargarImagenArtista() {
    if (this.artista) {
      return obtenerUrlImagenPrincipal(this.artista, 'imagenesArtista');
    }
  }

  deshabilitarFormulario() {
    this.formularioArtista.disable();
  }

  volverFormularioInicial() {
    this.formularioArtista.patchValue(
      {
        nombre: this.artista.nombre,
        descripcion: this.artista.descripcion,
        fechaNacimiento: this.artista.fechaNacimiento,
        anioDebut: this.artista.anioDebut,
        generos: this.cargarGenerosArtista(),
        imagen: this.cargarImagenArtista(),
      }
    );
    this.componenteInputImagen.pathImagen = this.cargarImagenArtista();
    this.enlacesArtista = [...this.artista.enlacesArtista];
    this.deshabilitarFormulario();
  }

  obtenerUrlPrincipal(plataforma: PlataformaInterface) {
    if (plataforma.imagenesPlataforma && plataforma.imagenesPlataforma.length) {
      return obtenerUrlImagenPrincipal(plataforma, 'imagenesPlataforma');
    }
  }

  abrirModalCrearEditarEnlace(
    enlace?: EnlaceAlbumCancionArtistaVideoInterface,
  ) {
    const indice = this.enlacesArtista.indexOf(enlace);
    if (this.enlacesArtista && this.enlacesArtista.length) {
      this.idsPlataformas = this.enlacesArtista.map(enlaceArtista => (enlaceArtista.plataforma as PlataformaInterface).id);
    }
    const dialogRef = this.dialog
      .open(
        CrearEditarEnlaceComponent,
        {
          width: '800px',
          data: {
            enlace,
            idsPlataformas: this.idsPlataformas
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (enlaceCreadoEditado: EnlaceAlbumCancionArtistaVideoInterface) => {
          if (enlaceCreadoEditado) {
            if (!enlace) {
              this.enlacesArtista.push(enlaceCreadoEditado);
            } else {
              this.enlacesArtista[indice] = enlaceCreadoEditado;
            }
            if (this.enlacesArtista && this.enlacesArtista.length) {
              this.idsPlataformas = this.enlacesArtista
                .map(enlaceArtista => (enlaceArtista.plataforma as PlataformaInterface).id);
            }
            this.formularioArtista.patchValue(
              {
                enlacesArtista: this.enlacesArtista
              }
            );
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  quitarEnlaceSeleccionado(enlace: EnlaceAlbumCancionArtistaVideoInterface) {
    const indice = this.enlacesArtista.indexOf(enlace);
    this.enlacesArtista.splice(indice, 1);
  }

  cargarEnlaces() {
    this.enlacesArtista = [...this.artista.enlacesArtista];
    return this.enlacesArtista;
  }
}
