import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormularioArtistaComponent} from './formulario-artista.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  CalendarModule, CarouselModule,
  InputNumberModule,
  InputTextareaModule,
  InputTextModule,
  MultiSelectModule,
} from 'primeng';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {MatExpansionModule} from '@angular/material/expansion';
import {TablaEnlacesPlataformaModule} from '../../../../componentes/tabla-enlaces-plataforma/tabla-enlaces-plataforma.module';
import {CrearEditarEnlaceModule} from '../../../enlace-album-cancion-artista-video/modales/crear-editar-enlace/crear-editar-enlace.module';
import {CrearEditarEnlaceComponent} from '../../../enlace-album-cancion-artista-video/modales/crear-editar-enlace/crear-editar-enlace.component';
import {AutocompletePlataformaModule} from '../../../../componentes/autocomplete-plataforma/autocomplete-plataforma.module';


@NgModule({
  declarations: [FormularioArtistaComponent],
  exports: [
    FormularioArtistaComponent,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    FormsModule,
    CalendarModule,
    InputNumberModule,
    MultiSelectModule,
    InputImagenModule,
    AlertaValidacionCampoFormularioModule,
    MatExpansionModule,
    TablaEnlacesPlataformaModule,
    CarouselModule,
  ],
  imports: [
    CommonModule,
    CrearEditarEnlaceModule,
    AutocompletePlataformaModule
  ],
  entryComponents: [
    CrearEditarEnlaceComponent
  ]
})
export class FormularioArtistaModule {
}
