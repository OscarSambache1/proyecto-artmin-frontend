import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';

export const RUTAS_ARTISTA = {
  _rutaInicioArtista: {
    ruta: 'artista-modulo',
    nombre: 'Gestión de artistas',
    generarRuta: (...argumentos) => {
      return `artista-modulo`;
    }
  },

  _rutaGestionArtista: {
    ruta: 'gestion-artistas',
    nombre: 'Gestión de artistas',
    generarRuta: (...argumentos) => {
      return `gestion-artistas`;
    }
  },

  rutaGestionArtistas: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    const rutaArreglo = [
      this._rutaInicioArtista,
      this._rutaGestionArtista
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaVerEditarArtistas: {
    ruta: 'editar-artista/:idArtista',
    nombre: 'Editar artista',
    generarRuta: (...argumentos) => {
      return `editar-artista/${argumentos[0]}`;
    }
  },

  rutaVerEditarArtistas: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    const rutaArreglo = [
      this._rutaInicioArtista,
      this._rutaGestionArtista,
      this._rutaVerEditarArtistas
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaNominaciones: {
    ruta: 'nominaciones/:tipo',
    nombre: 'Nominaciones',
    generarRuta: (...argumentos) => {
      return `nominaciones/${argumentos[1]}`;
    }
  },

  rutaNominaciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    const rutaArreglo = [
      this._rutaInicioArtista,
      this._rutaGestionArtista,
      this._rutaVerEditarArtistas,
      this._rutaNominaciones,
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaPresentaciones: {
    ruta: 'presentaciones/:tipo',
    nombre: 'Presentaciones',
    generarRuta: (...argumentos) => {
      return `presentaciones/${argumentos[1]}`;
    }
  },

  rutaPresentaciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    const rutaArreglo = [
      this._rutaInicioArtista,
      this._rutaGestionArtista,
      this._rutaVerEditarArtistas,
      this._rutaPresentaciones,
    ];
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
