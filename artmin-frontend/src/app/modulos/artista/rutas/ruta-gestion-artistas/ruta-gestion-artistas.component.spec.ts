import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionArtistasComponent } from './ruta-gestion-artistas.component';

describe('RutaGestionArtistasComponent', () => {
  let component: RutaGestionArtistasComponent;
  let fixture: ComponentFixture<RutaGestionArtistasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaGestionArtistasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionArtistasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
