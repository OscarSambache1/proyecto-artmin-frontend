import {Component, OnInit} from '@angular/core';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ModalCrearEditarArtistaComponent} from '../../modales/modal-crear-editar-artista/modal-crear-editar-artista.component';
import {ActivatedRoute, Router} from '@angular/router';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {mergeMap} from 'rxjs/operators';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {GeneroInterface} from '../../../../interfaces/genero.interface';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {RUTAS_ARTISTA} from '../definicion-rutas/rutas-artista';

@Component({
  selector: 'app-ruta-gestion-artistas',
  templateUrl: './ruta-gestion-artistas.component.html',
  styleUrls: ['./ruta-gestion-artistas.component.css']
})
export class RutaGestionArtistasComponent implements OnInit {

  artistas: ArtistaInterface[] = [];
  rutaImagen = 'assets/imagenes/artista.svg';
  busqueda = '';
  queryParams: QueryParamsInterface = {};
  idGenero: number;
  ruta = [];
  arregloRutas: any[];
  constructor(
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    this._activatedRoute
      .queryParams
      .pipe(
        mergeMap(queryParams => {
          this.arregloRutas = [RUTAS_ARTISTA.rutaGestionArtistas(false, true)];
          this.ruta = RUTAS_ARTISTA.rutaGestionArtistas(false, true).ruta;
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.idGenero) {
              this.idGenero = this.queryParams.consulta.idGenero;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idGenero: this.idGenero,
              esImagenPrincipal: 1
            };
          }
          this._cargandoService.habilitarCargando();
          return this._artistaRestService.obtenerArtistasPorGenero(
            JSON.stringify(this.queryParams.consulta)
          );
        })
      )
      .subscribe(
        (respuestaArtista: [ArtistaInterface[], number]) => {
          this.artistas = respuestaArtista[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerUrlPrincipal(artista: ArtistaInterface): string {
    return obtenerUrlImagenPrincipal(artista, 'imagenesArtista');
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    const consulta = {
      busqueda: this.busqueda,
      idGenero: this.idGenero,
      esImagenPrincipal: 1
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta, {
          queryParams: {...this.queryParams}
        });
  }

  abrirModalCrearArtista(artista?: ArtistaInterface) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarArtistaComponent,
        {
          width: '1200px',
          data: {},
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (artistaCreado: ArtistaInterface) => {
          if (artistaCreado) {
            this.artistas.unshift(artistaCreado);
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  buscarArtistasPorGenero(generoSeleccionado: GeneroInterface) {
    this.idGenero = generoSeleccionado ? generoSeleccionado.id : undefined;
    const consulta = {
      busqueda: this.busqueda,
      idGenero: this.idGenero,
      esImagenPrincipal: 1
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        });
  }

  obtenerRuta(idArtista) {
    return RUTAS_ARTISTA.
      rutaVerEditarArtistas(
        true,
        false,
        [idArtista]
      );
  }
}
