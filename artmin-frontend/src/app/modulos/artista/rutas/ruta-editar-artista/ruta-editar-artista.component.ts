import {Component, OnInit, ViewChild} from '@angular/core';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ToastErrorCargandoDatos, toastExitoCrear, toastExitoEditar} from '../../../../constantes/mensajes-toaster';
import {FormularioArtistaComponent} from '../../componentes/formulario-artista/formulario-artista.component';
import {OPCIONES_ARTISTA} from '../../../../constantes/opciones-gestion-artista';
import {RUTAS_ARTISTA} from '../definicion-rutas/rutas-artista';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {PlataformaInterface} from '../../../../interfaces/plataforma.interface';
import {RUTAS_VIDEO} from '../../../video/rutas/definicion-rutas/rutas-videos';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';
import {RUTAS_TOUR} from "../../../tour/rutas/definicion-rutas/rutas-tour";

@Component({
  selector: 'app-ruta-editar-artista',
  templateUrl: './ruta-editar-artista.component.html',
  styleUrls: ['./ruta-editar-artista.component.css']
})
export class RutaEditarArtistaComponent implements OnInit {

  artista: ArtistaInterface;

  formularioValido: boolean;

  artistaACrearEditar: ArtistaInterface;
  ruta;
  @ViewChild(FormularioArtistaComponent)
  componenteFormularioArtista: FormularioArtistaComponent;
  opcionesArtista = OPCIONES_ARTISTA;
  arregloRutas: any[];
  idArtista: number;

  constructor(
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtista = +params.idArtista;
          const consulta = {
            where: {
              id: this.idArtista
            },
            relations: [
              'imagenesArtista',
              'generosArtista',
              'generosArtista.genero',
              'enlacesArtista',
              'enlacesArtista.plataforma',
              'enlacesArtista.plataforma.imagenesPlataforma'
            ]
          };
          this.arregloRutas = [
            RUTAS_ARTISTA.rutaGestionArtistas(false, true),
            RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
          ];
          this.ruta = RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]).ruta;
          this._cargandoService.habilitarCargando();
          return this._artistaRestService.findAll(
            JSON.stringify(consulta)
          );
        })
      )
      .subscribe(
        (respuestaArtista: [ArtistaInterface[], number]) => {
          this.artista = respuestaArtista[0][0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  habilitarFormularioEditarArtista() {
    this.componenteFormularioArtista.formularioArtista.enable();
  }

  cancelarEdicion() {
    this.componenteFormularioArtista.volverFormularioInicial();
  }

  validarFormularioArtista(artista: ArtistaInterface | boolean) {
    if (artista) {
      this.formularioValido = true;
      this.artistaACrearEditar = artista as ArtistaInterface;
    } else {
      this.formularioValido = false;
    }
  }

  editarArtista() {
    this._cargandoService.habilitarCargando();
    const imagenPrincipal = this.artista.imagenesArtista.find(imagenArtista => imagenArtista.esPrincipal);
    const generos = [...this.artistaACrearEditar.generos].map(genero => genero.id);
    const enlaces = this.artistaACrearEditar.enlacesArtista.map( enlaceArtista => {
      enlaceArtista.plataforma = (enlaceArtista.plataforma as PlataformaInterface).id;
      return enlaceArtista;
    });
    const seCambioFoto = typeof {...this.artistaACrearEditar.imagen} === 'object';
    let imagen;
    if (seCambioFoto) {
      imagen = this.artistaACrearEditar.imagen;
    } else {
      imagen = {};
    }
    this._artistaRestService
      .editarArtistaImagen(
        this.artistaACrearEditar,
        generos,
        imagen,
        this.artista.id,
        imagenPrincipal.id,
        enlaces
      )
      .subscribe(respuestaArtistaCreado => {
          this.componenteFormularioArtista.artista = respuestaArtistaCreado;
          this.componenteFormularioArtista.volverFormularioInicial();
          this._toasterService.pop(toastExitoEditar);
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        });
  }

  irAGestion(modulo: string) {
    if (modulo === 'CANCIONES') {
      return this._router.navigate(
        RUTAS_CANCION
          .rutaGestionCanciones(
            true,
            false,
            [this.idArtista]
          )
      );
    }
    if (modulo === 'ALBUMS') {
      return this._router.navigate(
        RUTAS_ALBUM
          .rutaGestionAlbumes(
            true,
            false,
            [this.idArtista]
          )
      );
    }
    if (modulo === 'VIDEOS') {
      return this._router.navigate(
        RUTAS_VIDEO
          .rutaGestionVideos(
            true,
            false,
            [this.idArtista]
          )
      );
    }
    if (modulo === 'CHARTS') {
      return this._router.navigate(
        RUTAS_CHART
          .rutaGestionCharts(
            true,
            false,
            [this.idArtista, 0, 'A']
          )
      );
    }
    if (modulo === 'TOURS') {
      return this._router.navigate(
        RUTAS_TOUR
          .rutaGestionTour(
            true,
            false,
            [this.idArtista]
          )
      );
    }
    if (modulo === 'PREMIOS Y NOMINACIONES') {
      return this._router.navigate(
        RUTAS_ARTISTA
          .rutaNominaciones(
            true,
            false,
            [this.idArtista, 'A']
          )
      );
    }
    if (modulo === 'PRESENTACIONES') {
      return this._router.navigate(
        RUTAS_ARTISTA
          .rutaPresentaciones(
            true,
            false,
            [this.idArtista, 'A']
          )
      );
    }
  }
}
