import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RutaGestionArtistasComponent } from './rutas/ruta-gestion-artistas/ruta-gestion-artistas.component';
import {ArtistaRoutingModule} from './artista-routing.module';
import {MenuItemRedondoModule} from '../../componentes/menu-item-redondo/menu-item-redondo.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {FormularioArtistaModule} from './componentes/formulario-artista/formulario-artista.module';
import {ModalCrearEditarArtistaModule} from './modales/modal-crear-editar-artista/modal-crear-editar-artista.module';
import {ModalCrearEditarArtistaComponent} from './modales/modal-crear-editar-artista/modal-crear-editar-artista.component';
import {MatDialogModule} from '@angular/material/dialog';
import {AutoCompleteModule, CarouselModule, InputTextModule, PanelModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {DataViewModule} from 'primeng/dataview';
import {AutcompleteGenerosModule} from '../../componentes/autcomplete-generos/autcomplete-generos.module';
import { RutaEditarArtistaComponent } from './rutas/ruta-editar-artista/ruta-editar-artista.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {CarouselOpcionesModule} from '../../componentes/carousel-opciones/carousel-opciones.module';
import {RutaNominacionesModule} from '../../componentes/ruta-nominaciones/ruta-nominaciones.module';
import {RutaPresentacionesModule} from '../../componentes/ruta-presentaciones/ruta-presentaciones.module';


@NgModule({
  declarations: [RutaGestionArtistasComponent, RutaEditarArtistaComponent],
  imports: [
    CommonModule,
    ArtistaRoutingModule,
    MenuItemRedondoModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    FormularioArtistaModule,
    ModalCrearEditarArtistaModule,
    MatDialogModule,
    AutoCompleteModule,
    FormsModule,
    DataViewModule,
    InputTextModule,
    PanelModule,
    AutcompleteGenerosModule,
    MigasPanModule,
    CarouselOpcionesModule,
    RutaNominacionesModule,
    RutaPresentacionesModule,
  ],
  entryComponents: [
    ModalCrearEditarArtistaComponent
  ]
})
export class ArtistaModule { }
