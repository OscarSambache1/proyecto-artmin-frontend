import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionVideosComponent} from './rutas/ruta-gestion-videos/ruta-gestion-videos.component';
import {RutaNominacionesComponent} from '../../componentes/ruta-nominaciones/ruta-nominaciones.component';


const routes: Routes = [
  {
    path: 'gestion-videos',
    children: [
      {
        path: '',
        component: RutaGestionVideosComponent
      },
      {
        path: ':idVideo/nominaciones/:tipo',
        component: RutaNominacionesComponent,
      },
      {
        path: ':idVideo/chart-modulo/:tipo',
        loadChildren: 'src/app/modulos/chart/chart.module#ChartModule',
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-videos',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoRoutingModule { }
