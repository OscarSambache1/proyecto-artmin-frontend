import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';

export const RUTAS_VIDEO = {
  _rutaInicioVideo: {
    ruta: 'video-modulo',
    nombre: 'Modulo video',
    generarRuta: (...argumentos) => {
      return `video-modulo`;
    }
  },

  _rutaGestionVideos: {
    ruta: 'gestion-videos',
    nombre: 'Gestión de videos',
    generarRuta: (...argumentos) => {
      return `gestion-videos`;
    }
  },

  _rutaIdVideo: {
    ruta: ':idVideo',
    nombre: 'Id de videos',
    generarRuta: (...argumentos) => {
      return `${argumentos[1]}`;
    }
  },

  rutaGestionVideos: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0] && !argumentos[1]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioVideo,
        this._rutaGestionVideos
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioVideo,
        this._rutaGestionVideos
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaEditarVideo: {
    ruta: 'editar-video/:idVideo',
    nombre: 'Editar video',
    generarRuta: (...argumentos) => {
      return `editar-video/${argumentos[1]}`;
    }
  },

  rutaIdVideo: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioVideo,
        this._rutaGestionVideos,
        this._rutaIdVideo
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioVideo,
        this._rutaGestionVideos,
        this._rutaIdVideo
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaNominaciones: {
    ruta: ':idVideo/nominaciones/:tipo',
    nombre: 'Nominaciones',
    generarRuta: (...argumentos) => {
      return `${argumentos[0]}/nominaciones/${argumentos[1]}`;
    }
  },

  rutaNominaciones: function(
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];

    rutaArreglo = [
      this._rutaInicioVideo,
      this._rutaGestionVideos,
      this._rutaNominaciones,
    ];

    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
