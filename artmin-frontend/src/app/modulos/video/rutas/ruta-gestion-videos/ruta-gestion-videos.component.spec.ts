import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionVideosComponent } from './ruta-gestion-videos.component';

describe('RutaGestionVideosComponent', () => {
  let component: RutaGestionVideosComponent;
  let fixture: ComponentFixture<RutaGestionVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaGestionVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
