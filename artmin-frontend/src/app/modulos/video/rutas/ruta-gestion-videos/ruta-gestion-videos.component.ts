import {Component, OnInit} from '@angular/core';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {mergeMap} from 'rxjs/operators';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {of} from 'rxjs';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_VIDEO} from '../definicion-rutas/rutas-videos';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {CancionArtistaInterface} from '../../../../interfaces/cancion-artista.interface';
import {obtenerObjeto} from '../../../../funciones/obtener-objeto';
import {AlbumCancionInterface} from '../../../../interfaces/album-cancion.interface';
import {TipoVideoInterface} from '../../../../interfaces/tipo-video.interface';
import {CrearEditarVideoComponent} from '../../modales/crear-editar-video/crear-editar-video.component';
import {RUTAS_TOUR} from '../../../tour/rutas/definicion-rutas/rutas-tour';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';

@Component({
  selector: 'app-ruta-gestion-videos',
  templateUrl: './ruta-gestion-videos.component.html',
  styleUrls: ['./ruta-gestion-videos.component.css']
})
export class RutaGestionVideosComponent implements OnInit {
  idArtistaParams: number;
  idArtista: number;
  arregloRutas: any[];
  ruta: any;
  videos: VideoInterface[] = [];
  artista: ArtistaInterface;
  queryParams: QueryParamsInterface = {};
  busqueda = '';
  idArtistaQuery: number;
  idAlbum: number;
  idCancion: number;
  idTipoVideo: number;
  rutaImagen = 'assets/imagenes/video.svg';

  columnasVideos = [
    {
      field: 'nombre',
      header: 'Nombre',
      width: '15%'
    },
    {
      field: 'artistas',
      header: 'Artista(s)',
      width: '15%'
    },
    {
      field: 'albums',
      header: 'Album',
      width: '15%'
    },
    {
      field: 'duracionSegundos',
      header: 'Duracion',
      width: '10%'
    },
    {
      field: 'fechaLanzamiento',
      header: 'Fecha Lanzamiento',
      width: '10%'
    },
    {
      field: 'tipo',
      header: 'Tipo',
      width: '10%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '10%'
    },
  ];

  constructor(
    private readonly _videoRestService: VideoRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtistaParams = params.idArtista ? +params.idArtista : undefined;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
            if (this.idArtistaParams) {
              this.idArtista = this.idArtistaParams;
              this._artistaRestService.findOne(this.idArtista)
                .subscribe(
                  (artista: ArtistaInterface) => {
                    this.artista = artista;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (queryParams.consulta) {
              this.queryParams.consulta = JSON.parse(queryParams.consulta);
              if (this.queryParams.consulta.busqueda) {
                this.busqueda = this.queryParams.consulta.busqueda;
              }
              if (this.queryParams.consulta.idAlbum) {
                this.idAlbum = this.queryParams.consulta.idAlbum;
              }
              if (this.queryParams.consulta.idCancion) {
                this.idCancion = this.queryParams.consulta.idCancion;
              }
              if (this.queryParams.consulta.idTipoVideo) {
                this.idTipoVideo = this.queryParams.consulta.idTipoVideo;
              }
              if (this.queryParams.consulta.idArtista) {
                this.idArtistaQuery = this.queryParams.consulta.idArtista;
                this.idArtista = this.idArtistaQuery;
              } else {
                this.queryParams.consulta.idArtista = this.idArtista;
              }
            } else {
              this.queryParams.consulta = {
                busqueda: '',
                idArtista: this.idArtista,
                idAlbum: this.idAlbum,
                idCancion: this.idCancion,
                idTipoVideo: this.idTipoVideo,
              };
            }
            this._cargandoService.habilitarCargando();
            return this._videoRestService.obtenerVideosPorCancionArtistaAlbumTipo(
              JSON.stringify(this.queryParams.consulta)
            );
          }
        )
      )
      .subscribe(
        (respuestaVideos: [VideoInterface[], number]) => {
          this.videos = respuestaVideos[0];
          console.log(this.videos);
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  setearArregloRutasMigasPan() {
    if (this.idArtistaParams) {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtistaParams]),
        RUTAS_VIDEO.rutaGestionVideos(false, true, [this.idArtistaParams])
      ];
    } else {
      this.arregloRutas = [
        RUTAS_VIDEO.rutaGestionVideos(false, true)
      ];
    }
  }

  seteoRutas() {
    this.ruta = RUTAS_VIDEO
      .rutaGestionVideos(
        false,
        true,
        [this.idArtistaParams])
      .ruta;
  }

  buscarVideosPorArtista(artistaSeleccionado: ArtistaInterface) {
    this.idArtista = artistaSeleccionado ? artistaSeleccionado.id : undefined;
    this.buscarVideos();
  }

  buscarVideosPorAlbum(albumSeleccionado: AlbumInterface) {
    this.idAlbum = albumSeleccionado ? albumSeleccionado.id : undefined;
    this.buscarVideos();
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarVideos();
  }

  obtenerArtistasCancion(artistasCancion: CancionArtistaInterface[]): string {
    return obtenerObjeto(artistasCancion, 'artista', 'nombre');
  }

  obtenerAlbumesCancion(albumesCancion: AlbumCancionInterface[]): string {
    return obtenerObjeto(albumesCancion, 'album', 'nombre');
  }

  buscarVideosPorCancion(cancionSeleccionada: CancionInterface) {
    this.idCancion = cancionSeleccionada ? cancionSeleccionada.id : undefined;
    this.buscarVideos();
  }

  buscarVideosPorTipo(tipoVideo: TipoVideoInterface) {
    this.idTipoVideo = tipoVideo ? tipoVideo.id : undefined;
    this.buscarVideos();
  }

  buscarVideos() {
    const consulta = {
      busqueda: this.busqueda,
      idCancion: this.idCancion,
      idArtista: this.idArtista,
      idAlbum: this.idAlbum,
      idTipoVideo: this.idTipoVideo
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  abrirModalCrearEditarVideo(
    video?: VideoInterface,
    id?: number
  ) {
    const dialogRef = this.dialog
      .open(
        CrearEditarVideoComponent,
        {
          width: '1000px',
          data: {
            video,
            idArtista: this.idArtista
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (videoCreadaEditado: VideoInterface) => {
          if (videoCreadaEditado) {
            if (id === undefined) {
              this.videos.unshift(videoCreadaEditado);
            } else {
              this.videos[id] = videoCreadaEditado;
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  irAGestion(idVideo: number) {
    this._router.navigate(
      RUTAS_VIDEO
        .rutaNominaciones(
          true,
          false,
          [idVideo, 'V']
        )
    );
  }

  irAGestionCharts(idVideo: number) {
    this._router.navigate(
      RUTAS_CHART
        .rutaGestionCharts(
          true,
          false,
          [
            this.idArtista || 0,
            idVideo || 0,
            idVideo, 'V']
        ),
    );
  }
}
