import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrearEditarVideoComponent } from './crear-editar-video.component';
import {FormularioVideoModule} from '../../componentes/formulario-video/formulario-video.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [CrearEditarVideoComponent],
  imports: [
    CommonModule,
    FormularioVideoModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule
  ]
})
export class CrearEditarVideoModule { }
