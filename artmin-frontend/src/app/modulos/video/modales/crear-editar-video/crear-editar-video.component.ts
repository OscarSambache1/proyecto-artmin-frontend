import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {obtenerAnio} from '../../../../funciones/obtener-anio';
import {convertirMinutosMascaraASegundos} from '../../../../funciones/convertir-minutos-segundos';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';

@Component({
  selector: 'app-crear-editar-video',
  templateUrl: './crear-editar-video.component.html',
  styleUrls: ['./crear-editar-video.component.css']
})
export class CrearEditarVideoComponent implements OnInit {
  formularioValido: boolean;
  private videoCrearEditar: VideoInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      video: VideoInterface,
      idArtista: number
    },
    public dialogo: MatDialogRef<CrearEditarVideoComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _vidoRestService: VideoRestService,
  ) {
  }

  ngOnInit(): void {
  }

  crearEditarVideo() {
    this._cargandoService.habilitarCargando();
    this.videoCrearEditar.anio = obtenerAnio(this.videoCrearEditar.fechaLanzamiento);
    this.videoCrearEditar.duracionSegundos = +convertirMinutosMascaraASegundos(this.videoCrearEditar.duracionSegundos.toString());
    const enlaces = [
      {
        url: this.videoCrearEditar.enlace,
        plataforma: 2
      }
    ];
    if (this.data.video) {
      this._vidoRestService
        .editarVideo(
          this.videoCrearEditar,
          enlaces,
          this.data.video.id
        )
        .subscribe((respuestaVideoEditado: VideoInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaVideoEditado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    } else {
      this._vidoRestService
        .crearVideo(
          this.videoCrearEditar,
          enlaces,
        )
        .subscribe((respuestaVideoCreado: VideoInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaVideoCreado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }

  }


  validarFormularioVideo(video: VideoInterface | boolean) {
    if (video) {
      this.formularioValido = true;
      this.videoCrearEditar = video as VideoInterface;
    } else {
      this.formularioValido = false;
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }
}
