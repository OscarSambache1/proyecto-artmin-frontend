import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEditarVideoComponent } from './crear-editar-video.component';

describe('CrearEditarVideoComponent', () => {
  let component: CrearEditarVideoComponent;
  let fixture: ComponentFixture<CrearEditarVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEditarVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEditarVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
