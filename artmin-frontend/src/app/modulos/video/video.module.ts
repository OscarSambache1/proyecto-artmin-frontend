import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoRoutingModule } from './video-routing.module';
import { RutaGestionVideosComponent } from './rutas/ruta-gestion-videos/ruta-gestion-videos.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {TableModule} from 'primeng';
import {SegundosAMinutosHorasPipeModule} from '../../pipes/segundos-a-minutos-horas-pipe/segundos-a-minutos-horas-pipe.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {AutocompleteArtistaModule} from '../../componentes/autocomplete-artista/autocomplete-artista.module';
import {AutocompleteAlbumsModule} from '../../componentes/autocomplete-albums/autocomplete-albums.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {AutocompleteCancionModule} from '../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {AutocompleteTipoVideoModule} from '../../componentes/autocomplete-tipo-video/autocomplete-tipo-video.module';
import {CrearEditarVideoModule} from './modales/crear-editar-video/crear-editar-video.module';
import {CrearEditarVideoComponent} from './modales/crear-editar-video/crear-editar-video.component';
import {RutaNominacionesModule} from '../../componentes/ruta-nominaciones/ruta-nominaciones.module';


@NgModule({
  declarations: [RutaGestionVideosComponent],
  imports: [
    CommonModule,
    VideoRoutingModule,
    MigasPanModule,
    TableModule,
    SegundosAMinutosHorasPipeModule,
    InputBuscarBotonModule,
    AutocompleteArtistaModule,
    AutocompleteCancionModule,
    AutocompleteAlbumsModule,
    BotonNuevoModule,
    AutocompleteTipoVideoModule,
    CrearEditarVideoModule,
    RutaNominacionesModule,
  ],
  exports: [
    RutaGestionVideosComponent
  ],
  entryComponents: [
    CrearEditarVideoComponent
  ]
})
export class VideoModule { }
