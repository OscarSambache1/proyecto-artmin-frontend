import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {mostrarSegundosEnMinutos} from '../../../../funciones/mostrar-segundos-en-minutos';
import {TipoCancionInterface} from '../../../../interfaces/tipo-cancion.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {TipoVideoInterface} from '../../../../interfaces/tipo-video.interface';
import {
  MENSAJES_VALIDACION_CANCION_VIDEO,
  MENSAJES_VALIDACION_DESCRIPCION_VIDEO,
  MENSAJES_VALIDACION_DURACION_SEGUNDOS_VIDEO,
  MENSAJES_VALIDACION_ENLACE_VIDEO,
  MENSAJES_VALIDACION_FECHA_LANZAMIENTO_VIDEO,
  MENSAJES_VALIDACION_NOMBRE_VIDEO,
  MENSAJES_VALIDACION_TIPO_VIDEO, VALIDACION_CANCION_VIDEO,
  VALIDACION_DESCRIPCION_VIDEO,
  VALIDACION_DURACION_SEGUNDOS_VIDEO,
  VALIDACION_ENLACE_VIDEO,
  VALIDACION_FECHA_LANZAMIENTO_VIDEO,
  VALIDACION_NOMBRE_VIDEO,
  VALIDACION_TIPO_VIDEO
} from '../../../../constantes/validaciones-formulario/video';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';

@Component({
  selector: 'app-formulario-video',
  templateUrl: './formulario-video.component.html',
  styleUrls: ['./formulario-video.component.css']
})
export class FormularioVideoComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;
  arregloCanciones: CancionInterface[] = [];
  formularioVideo: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    duracionSegundos: [],
    fechaLanzamiento: [],
    cancion: [],
    tipoVideo: [],
    enlace: []
  };
  subscribers = [];

  @Input()
  video: VideoInterface;

  @Output()
  videoValidoEnviar: EventEmitter<VideoInterface | boolean> = new EventEmitter();

  idTipoVideo: number;

  @Input()
  idsArtistas: number[];

  public mask = [/[0-5]/, /\d/, ':', /[0-5]/, /\d/];

  constructor(
    private readonly _cancionRestService: CancionRestService,
    private readonly _videoRestService: VideoRestService,
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    if (this.video) {
      this.idTipoVideo = (this.video.tipoVideo as TipoVideoInterface).id;
    }
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioVideo = this._formBuilder.group({
      nombre: [this.video ? this.video.nombre : '', VALIDACION_NOMBRE_VIDEO],
      descripcion: [this.video ? this.video.descripcion : '', VALIDACION_DESCRIPCION_VIDEO],
      fechaLanzamiento: [this.video ? this.video.fechaLanzamiento : '', VALIDACION_FECHA_LANZAMIENTO_VIDEO],
      duracionSegundos: [this.video ? mostrarSegundosEnMinutos(this.video.duracionSegundos) : null, VALIDACION_DURACION_SEGUNDOS_VIDEO],
      cancion: [this.video ? this.video.cancion : null, VALIDACION_CANCION_VIDEO],
      tipoVideo: [this.video ? this.video.tipoVideo : null, VALIDACION_TIPO_VIDEO],
      enlace: [this.video ? this.obtenerEnlace() : null, VALIDACION_ENLACE_VIDEO]
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_VIDEO);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_VIDEO);
    this.verificarCampoFormControl('fechaLanzamiento', MENSAJES_VALIDACION_FECHA_LANZAMIENTO_VIDEO);
    this.verificarCampoFormControl('duracionSegundos', MENSAJES_VALIDACION_DURACION_SEGUNDOS_VIDEO);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCION_VIDEO);
    this.verificarCampoFormControl('tipoVideo', MENSAJES_VALIDACION_TIPO_VIDEO);
    this.verificarCampoFormControl('enlace', MENSAJES_VALIDACION_ENLACE_VIDEO);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioVideo.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioVideo;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const videoValido = formularioFormGroup.valid;
          if (videoValido) {
            this.videoValidoEnviar.emit(formulario);
          } else {
            this.videoValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  deshabilitarFormulario() {
    this.formularioVideo.disable();
  }

  buscarCancionesPorArtista(respuestaArtista: ArtistaInterface[]) {
    this.formularioVideo.patchValue(
      {
        artistas: [
          ...respuestaArtista]
      }
    );
  }

  escucharTipoVideoSeleccionado(
    tipoVideoSeleccionado: TipoVideoInterface
  ) {
    this.formularioVideo.patchValue(
      {
        tipoVideo: tipoVideoSeleccionado,
      }
    );
  }

  volverFormularioInicial() {
    this.formularioVideo.patchValue(
      {
        nombre: this.video.nombre,
        descripcion: this.video.descripcion,
        fechaLanzamiento: this.video.fechaLanzamiento,
        duracionSegundos: this.video.duracionSegundos,
        tipoVideo: this.video.tipoVideo,
        cancion: this.video.cancion,
        enlace: this.obtenerEnlace(),
      }
    );
    this.idTipoVideo = (this.video.tipoVideo as TipoVideoInterface).id;
    this.deshabilitarFormulario();
  }

  obtenerEnlace() {
    if (this.video && this.video.enlacesVideo.length) {
      return this.video.enlacesVideo[0].url;
    } else {
      return null;
    }
  }

  setearCancion(cancionSeleccionada: CancionInterface) {
    this.formularioVideo.patchValue(
      {
        cancion: cancionSeleccionada
      }
    );
  }
}
