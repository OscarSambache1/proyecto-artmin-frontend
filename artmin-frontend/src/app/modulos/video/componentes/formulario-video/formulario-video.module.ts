import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioVideoComponent } from './formulario-video.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  CalendarModule,
  DropdownModule, InputMaskModule,
  InputNumberModule,
  InputTextareaModule,
  InputTextModule,
  MultiSelectModule
} from 'primeng';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteArtistaModule} from '../../../../componentes/autocomplete-artista/autocomplete-artista.module';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteTipoCancionModule} from '../../../../componentes/autocomplete-tipo-cancion/autocomplete-tipo-cancion.module';
import {SafePipeModule} from '../../../../pipes/safe-pipe/safe-pipe.module';
import {AutocompleteTipoVideoModule} from '../../../../componentes/autocomplete-tipo-video/autocomplete-tipo-video.module';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {TextMaskModule} from 'angular2-text-mask';



@NgModule({
  declarations: [FormularioVideoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    FormsModule,
    CalendarModule,
    InputNumberModule,
    MultiSelectModule,
    InputImagenModule,
    AlertaValidacionCampoFormularioModule,
    DropdownModule,
    InputMaskModule,
    AutocompleteTipoVideoModule,
    AutocompleteCancionModule,
    SafePipeModule,
    TextMaskModule
  ],
  exports: [
    FormularioVideoComponent
  ]
})
export class FormularioVideoModule { }
