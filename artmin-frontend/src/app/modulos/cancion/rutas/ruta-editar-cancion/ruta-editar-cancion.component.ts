import {Component, OnInit, ViewChild} from '@angular/core';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {FormularioCancionComponent} from '../../componentes/formulario-cancion/formulario-cancion.component';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {mergeMap} from 'rxjs/operators';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {RUTAS_CANCION} from '../definicion-rutas/rutas-cancion';
import {obtenerAnio} from '../../../../funciones/obtener-anio';
import {convertirMinutosMascaraASegundos} from '../../../../funciones/convertir-minutos-segundos';
import {OPCIONES_CANCION} from '../../../../constantes/opciones-gestion-cancion';
import {RUTAS_VIDEO} from '../../../video/rutas/definicion-rutas/rutas-videos';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';

@Component({
  selector: 'app-ruta-editar-cancion',
  templateUrl: './ruta-editar-cancion.component.html',
  styleUrls: ['./ruta-editar-cancion.component.css']
})
export class RutaEditarCancionComponent implements OnInit {
  arregloRutas: any[];
  cancion: CancionInterface;
  formularioValido: boolean;
  idCancion: number;
  @ViewChild(FormularioCancionComponent)
  componenteFormularioCancion: FormularioCancionComponent;
  cancionCrearEditar: CancionInterface;
  idsArtistas: number[] = [];
  idsAlbum: number[] = [];
  idArtista: number;
  ruta: any[];
  opcionesCancion = OPCIONES_CANCION;

  constructor(
    private readonly _cancionRestService: CancionRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idCancion = +params.idCancion;
          this.idArtista = +params.idArtista;
          this.setearArregloRutasMigaPan();
          this.ruta = this.setearRuta();
          const consulta = {
            where: {
              id: this.idCancion
            },
            relations: [
              'tipoCancion',
              'imagenesCancion',
              'artistasCancion',
              'artistasCancion.artista',
              'generosCancion',
              'generosCancion.genero',
              'albumesCancion',
              'albumesCancion.album',
              'enlacesCancion',
            ]
          };

          this._cargandoService.habilitarCargando();
          return this._cancionRestService.findAll(
            JSON.stringify(consulta)
          );
        })
      )
      .subscribe(
        (respuestaCancion: [CancionInterface[], number]) => {
          this.cancion = respuestaCancion[0][0];
          this.idsArtistas = this.cancion.artistasCancion.map(artistaCancion => (artistaCancion.artista as ArtistaInterface).id);
          this.idsAlbum = this.cancion.albumesCancion.map(albumCancion => (albumCancion.album as AlbumInterface).id);
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  cancelarEdicion() {
    this.componenteFormularioCancion.volverFormularioInicial();
  }

  editarCancion() {
    this._cargandoService.habilitarCargando();
    this.cancionCrearEditar.anio = obtenerAnio(this.cancionCrearEditar.fechaLanzamiento);
    this.cancionCrearEditar.duracionSegundos = +convertirMinutosMascaraASegundos(this.cancionCrearEditar.duracionSegundos as string);
    const generos = [...this.cancionCrearEditar.generos].map(genero => genero.id);
    const artistas = this.obtenerArtistaPrincipal(this.cancionCrearEditar.artistas);
    const albumes = [...this.cancionCrearEditar.albums].map(genero => genero.id);
    const imagenPrincipal = this.cancion.imagenesCancion.find(imagenCancion => imagenCancion.esPrincipal);
    const enlaces = [];
    if (this.cancionCrearEditar.enlace) {
      if (this.cancion.enlacesCancion.length === 0) {
        enlaces.push(
          {
            url: this.cancionCrearEditar.enlace,
            plataforma: 1
          }
        );
      } else {
        this.cancion.enlacesCancion[0].url = this.cancionCrearEditar.enlace;
        enlaces.push(
          this.cancion.enlacesCancion[0]
        );
      }
    }
    this._cancionRestService
      .editarCancion(
        this.cancionCrearEditar,
        this.cancion.id,
        generos,
        artistas,
        this.cancionCrearEditar.imagen,
        null,
        albumes,
        enlaces,
        imagenPrincipal.id
      )
      .subscribe((respuestaCancionEditada: CancionInterface) => {
          this.cancion = respuestaCancionEditada;
          this.componenteFormularioCancion.cancion = respuestaCancionEditada;
          this.componenteFormularioCancion.volverFormularioInicial();
          this._toasterService.pop(toastExitoCrear);
          this._cargandoService.deshabiltarCargando();
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        });
    this._cargandoService.deshabiltarCargando();
  }

  habilitarFormularioEditarCancion() {
    this.componenteFormularioCancion.formularioCancion.enable();
  }

  validarFormularioCancion(cancion: CancionInterface | boolean) {
    if (cancion) {
      this.formularioValido = true;
      this.cancionCrearEditar = cancion as CancionInterface;
    } else {
      this.formularioValido = false;
    }
  }

  setearArregloRutasMigaPan() {
    if (this.idArtista) {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
        RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
      ];
    } else {
      this.arregloRutas = [
        RUTAS_CANCION.rutaGestionCanciones(false, true),
      ];
    }
    if (this.idCancion) {
      this.arregloRutas.push(
        RUTAS_CANCION.rutaEditarCancion(false, true)
      );
    } else {
      this.arregloRutas.push(
        RUTAS_CANCION.rutaEditarCancion(false, true)
      );
    }
  }

  setearRuta(): any[] {
    if (this.idCancion) {
      return RUTAS_CANCION.rutaEditarCancion(false, true, [this.idCancion, this.idArtista]).ruta;
    } else {
      return RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista]).ruta;
    }
  }

  obtenerArtistaPrincipal(artistas: ArtistaInterface[]): Array<{ id: number, esPrincipal: 0 | 1 }> {
    return artistas.map(
      (artistaCancion, indice) => {
        return {
          id: artistaCancion.id,
          esPrincipal: indice === 0 ? 1 : 0,
        };
      }
    );
  }

  irAGestion(modulo: string) {
    if (modulo === 'PREMIOS Y NOMINACIONES') {
      return this._router.navigate(
        RUTAS_CANCION
          .rutaNominaciones(
            true,
            false,
            [this.idArtista, this.cancion.id, 'C']
          )
      );
    }
    if (modulo === 'PRESENTACIONES') {
      return this._router.navigate(
        RUTAS_CANCION
          .rutaPresentaciones(
            true,
            false,
            [this.idArtista, this.cancion.id, 'C']
          )
      );
    }
    if (modulo === 'CHARTS') {
      return this._router.navigate(
        RUTAS_CHART
          .rutaGestionCharts(
            true,
            false,
            [
              this.idArtista || 0,
              this.idCancion || 0,
              this.idCancion, 'C']
          ),
      );
    }

  }
}
