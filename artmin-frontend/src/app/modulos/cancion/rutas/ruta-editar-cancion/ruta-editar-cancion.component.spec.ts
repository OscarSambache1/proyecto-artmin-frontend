import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaEditarCancionComponent } from './ruta-editar-cancion.component';

describe('RutaEditarCancionComponent', () => {
  let component: RutaEditarCancionComponent;
  let fixture: ComponentFixture<RutaEditarCancionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaEditarCancionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaEditarCancionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
