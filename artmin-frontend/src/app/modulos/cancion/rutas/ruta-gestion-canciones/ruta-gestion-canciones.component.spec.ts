import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionCancionesComponent } from './ruta-gestion-canciones.component';

describe('RutaGestionCancionesComponent', () => {
  let component: RutaGestionCancionesComponent;
  let fixture: ComponentFixture<RutaGestionCancionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaGestionCancionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionCancionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
