import {Component, OnInit} from '@angular/core';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {mergeMap} from 'rxjs/operators';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {CancionArtistaInterface} from '../../../../interfaces/cancion-artista.interface';
import {obtenerObjeto} from '../../../../funciones/obtener-objeto';
import {AlbumCancionInterface} from '../../../../interfaces/album-cancion.interface';
import {GeneroArtistaAlbumCancionInterface} from '../../../../interfaces/genero-artista-album-cancion.interface';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {GeneroInterface} from '../../../../interfaces/genero.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {CrearEditarCancionComponent} from '../../modales/crear-editar-cancion/crear-editar-cancion.component';
import {TipoCancionInterface} from '../../../../interfaces/tipo-cancion.interface';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';
import {RUTAS_CANCION} from '../definicion-rutas/rutas-cancion';

@Component({
  selector: 'app-ruta-gestion-canciones',
  templateUrl: './ruta-gestion-canciones.component.html',
  styleUrls: ['./ruta-gestion-canciones.component.css']
})
export class RutaGestionCancionesComponent implements OnInit {
  canciones: CancionInterface[];

  columnasCanciones = [
    {
      field: 'imagen',
      header: 'Imagen',
      width: '5%'
    },
    {
      field: 'nombre',
      header: 'Nombre',
      width: '15%'
    },
    {
      field: 'artistas',
      header: 'Artista(s)',
      width: '15%'
    },
    {
      field: 'albums',
      header: 'Album',
      width: '15%'
    },
    {
      field: 'duracionSegundos',
      header: 'Duracion',
      width: '10%'
    },
    {
      field: 'fechaLanzamiento',
      header: 'Fecha Lanzamiento',
      width: '10%'
    },
    {
      field: 'tipo',
      header: 'Tipo',
      width: '10%'
    },
    {
      field: 'generos',
      header: 'Generos',
      width: '10%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '10%'
    },
  ];

  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  idGenero: number;
  ruta = [];
  artista: ArtistaInterface;
  rutaImagen = 'assets/imagenes/cancion.svg';
  idArtistaQuery: number;
  idArtistaParams: number;
  idAlbum: number;
  idTipoCancion: number;
  arregloRutas: any[];

  constructor(
    private readonly _cancionRestService: CancionRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtistaParams = params.idArtista ? +params.idArtista : undefined;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          this.idAlbum = +queryParams.idAlbum;
          if (this.idArtistaParams) {
            this.idArtista = this.idArtistaParams;
            this._artistaRestService.findOne(this.idArtista)
              .subscribe(
                (artista: ArtistaInterface) => {
                  this.artista = artista;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          } else {
            this.ruta = [
              '/cancion-modulo',
              'gestion-canciones'
            ];
          }
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.idGenero) {
              this.idGenero = this.queryParams.consulta.idGenero;
            }
            if (this.queryParams.consulta.idAlbum) {
              this.idAlbum = this.queryParams.consulta.idAlbum;
            }
            if (this.queryParams.consulta.idTipoCancion) {
              this.idTipoCancion = this.queryParams.consulta.idTipoCancion;
            }
            if (this.queryParams.consulta.idArtista) {
              this.idArtistaQuery = this.queryParams.consulta.idArtista;
              this.idArtista = this.idArtistaQuery;
            } else {
              this.queryParams.consulta.idArtista = this.idArtista;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idGenero: this.idGenero,
              esImagenPrincipal: 1,
              idArtista: this.idArtista,
              idAlbum: this.idAlbum,
              idTipoCancion: this.idTipoCancion,
            };
          }
          this._cargandoService.habilitarCargando();
          return this._cancionRestService.obtenerCancionesPorGeneroArtista(
            JSON.stringify(this.queryParams.consulta)
          );
        })
      )
      .subscribe(
        (respuestaCanciones: [CancionInterface[], number]) => {
          this.canciones = respuestaCanciones[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerArtistasCancion(artistasCancion: CancionArtistaInterface[]): string {
    return obtenerObjeto(artistasCancion, 'artista', 'nombre');
  }

  obtenerAlbumesCancion(albumesCancion: AlbumCancionInterface[]): string {
    return obtenerObjeto(albumesCancion, 'album', 'nombre');
  }

  obtenerGenerosCancion(generosCancion: GeneroArtistaAlbumCancionInterface[]): string {
    return obtenerObjeto(generosCancion, 'genero', 'nombre');
  }

  obtenerUrlPrincipal(cancion: CancionInterface): string {
    if (cancion.imagenesCancion && cancion.imagenesCancion.length) {
      return obtenerUrlImagenPrincipal(cancion, 'imagenesCancion');
    }
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarCanciones();
  }

  buscarCancionesPorGenero(generoSeleccionado: GeneroInterface) {
    this.idGenero = generoSeleccionado ? generoSeleccionado.id : undefined;
    this.buscarCanciones();
  }

  buscarCancionPorArtista(artistaSeleccionado: ArtistaInterface) {
    this.idArtista = artistaSeleccionado ? artistaSeleccionado.id : undefined;
    this.buscarCanciones();
  }

  buscarCancionesPorAlbum(albumSeleccionado: AlbumInterface) {
    this.idAlbum = albumSeleccionado ? albumSeleccionado.id : undefined;
    this.buscarCanciones();
  }

  abrirModalCrearEditarCancion() {
    const dialogRef = this.dialog
      .open(
        CrearEditarCancionComponent,
        {
          width: '1000px',
          data: {
            idsArtista: this.idArtistaParams ? [this.idArtistaParams] : null,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (cancionCreadaEditada: CancionInterface) => {
          if (cancionCreadaEditada) {
            this.canciones.unshift(cancionCreadaEditada);
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  buscarCancionPorTipo(tipoCancionSeleccionado: TipoCancionInterface) {
    this.idTipoCancion = tipoCancionSeleccionado ? tipoCancionSeleccionado.id : undefined;
    this.buscarCanciones();
  }

  buscarCanciones() {
    const consulta = {
      busqueda: this.busqueda,
      idGenero: this.idGenero,
      esImagenPrincipal: 1,
      idArtista: this.idArtista,
      idAlbum: this.idAlbum,
      idTipoCancion: this.idTipoCancion
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    if (this.idArtistaParams) {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtistaParams]),
        RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtistaParams])
      ];
    } else {
      this.arregloRutas = [
        RUTAS_CANCION.rutaGestionCanciones(false, true)
      ];
    }
  }

  seteoRutas() {
    this.ruta = RUTAS_CANCION
      .rutaGestionCanciones(
        false,
        true,
        [this.idArtistaParams])
      .ruta;
  }

  irRutaEditarCancion(cancion: CancionInterface) {
    this._router
      .navigate(
        RUTAS_CANCION
          .rutaEditarCancion(
            true,
            false,
            [
              this.idArtista,
              cancion.id,
            ]
          )
      );
  }
}
