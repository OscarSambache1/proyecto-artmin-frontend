import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';

export const RUTAS_CANCION = {
  _rutaInicioCancion: {
    ruta: 'cancion-modulo',
    nombre: 'Modulo cancion',
    generarRuta: (...argumentos) => {
      return `cancion-modulo`;
    }
  },

  _rutaGestionCanciones: {
    ruta: 'gestion-canciones',
    nombre: 'Gestión de canciones',
    generarRuta: (...argumentos) => {
      return `gestion-canciones`;
    }
  },

  rutaGestionCanciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioCancion,
        this._rutaGestionCanciones
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioCancion,
        this._rutaGestionCanciones
      ];
    }


    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaEditarCancion: {
    ruta: 'editar-cancion/:idCancion',
    nombre: 'Editar canción',
    generarRuta: (...argumentos) => {
      return `editar-cancion/${argumentos[1]}`;
    }
  },

  rutaEditarCancion: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioCancion,
        this._rutaGestionCanciones,
        this._rutaEditarCancion
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioCancion,
        this._rutaGestionCanciones,
        this._rutaEditarCancion
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaNominaciones: {
    ruta: 'nominaciones/:tipo',
    nombre: 'Nominaciones',
    generarRuta: (...argumentos) => {
      return `nominaciones/${argumentos[2]}`;
    }
  },

  rutaNominaciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioCancion,
        this._rutaGestionCanciones,
        this._rutaEditarCancion,
        this._rutaNominaciones,
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioCancion,
        this._rutaGestionCanciones,
        this._rutaEditarCancion,
        this._rutaNominaciones,
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaPresentaciones: {
    ruta: 'presentaciones/:tipo',
    nombre: 'Presentaciones',
    generarRuta: (...argumentos) => {
      return `presentaciones/${argumentos[2]}`;
    }
  },

  rutaPresentaciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioCancion,
        this._rutaGestionCanciones,
        this._rutaEditarCancion,
        this._rutaPresentaciones,
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioCancion,
        this._rutaGestionCanciones,
        this._rutaEditarCancion,
        this._rutaPresentaciones,
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
