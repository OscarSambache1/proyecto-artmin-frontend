import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionCancionesComponent} from './rutas/ruta-gestion-canciones/ruta-gestion-canciones.component';
import {RutaEditarCancionComponent} from './rutas/ruta-editar-cancion/ruta-editar-cancion.component';
import {RutaEditarArtistaComponent} from '../artista/rutas/ruta-editar-artista/ruta-editar-artista.component';
import {RutaNominacionesComponent} from '../../componentes/ruta-nominaciones/ruta-nominaciones.component';
import {RutaPresentacionesComponent} from '../../componentes/ruta-presentaciones/ruta-presentaciones.component';


const routes: Routes = [
  {
    path: 'gestion-canciones',
    children: [
      {
        path: '',
        component: RutaGestionCancionesComponent
      },
      {
        path: 'editar-cancion/:idCancion',
        children: [
          {
            path: '',
            component: RutaEditarCancionComponent,
          },
          {
            path: 'nominaciones/:tipo',
            component: RutaNominacionesComponent,
          },
          {
            path: 'presentaciones/:tipo',
            component: RutaPresentacionesComponent,
          },
          {
            path: 'video-modulo',
            loadChildren: 'src/app/modulos/video/video.module#VideoModule',
          },
          {
            path: 'chart-modulo/:tipo',
            loadChildren: 'src/app/modulos/chart/chart.module#ChartModule',
          },
        ]
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-canciones',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CancionRoutingModule { }
