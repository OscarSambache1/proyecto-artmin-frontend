import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearEditarCancionComponent } from './crear-editar-cancion.component';

describe('CrearEditarCancionComponent', () => {
  let component: CrearEditarCancionComponent;
  let fixture: ComponentFixture<CrearEditarCancionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearEditarCancionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearEditarCancionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
