import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {FormularioCancionComponent} from '../../componentes/formulario-cancion/formulario-cancion.component';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {convertirMinutosMascaraASegundos} from '../../../../funciones/convertir-minutos-segundos';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {obtenerAnio} from '../../../../funciones/obtener-anio';
import {ToastErrorCargandoDatos, toastExitoCrear} from '../../../../constantes/mensajes-toaster';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';

@Component({
  selector: 'app-crear-editar-cancion',
  templateUrl: './crear-editar-cancion.component.html',
  styleUrls: ['./crear-editar-cancion.component.css']
})
export class CrearEditarCancionComponent implements OnInit {
  formularioValido: boolean;

  @ViewChild(FormularioCancionComponent)
  formularioCancionComponent: FormularioCancionComponent;

  cancionCrearEditar: CancionInterface;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      idsArtista: number[],
      idsAlbums: number[],
      album: AlbumInterface,
      cancion: CancionInterface,
      deshabilitarAlbums: boolean
    },
    public dialogo: MatDialogRef<CrearEditarCancionComponent>,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _cancionRestService: CancionRestService,
  ) {
  }

  ngOnInit(): void {
  }

  validarFormularioCancion(cancion: CancionInterface | boolean) {
    if (cancion) {
      this.formularioValido = true;
      this.cancionCrearEditar = cancion as CancionInterface;
    } else {
      this.formularioValido = false;
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }

  crearEditarCancion() {
    this._cargandoService.habilitarCargando();
    this.cancionCrearEditar.anio = obtenerAnio(this.cancionCrearEditar.fechaLanzamiento);
    this.cancionCrearEditar.duracionSegundos = +convertirMinutosMascaraASegundos(this.cancionCrearEditar.duracionSegundos as string);
    const generos = [...this.cancionCrearEditar.generos].map(genero => genero.id);
    const artistas = this.obtenerArtistaPrincipal(this.cancionCrearEditar.artistas);
    const albumes = [...this.cancionCrearEditar.albums].map(genero => genero.id);
    const pathAlterno = typeof  this.cancionCrearEditar.imagen === 'string' ? this.obtenerPathImagenAlbum() : null;
    const enlaces = [];
    if (this.cancionCrearEditar.enlace) {
      enlaces.push(
        {
          cancion: this.data.cancion ? this.data.cancion.id : null,
          url: this.cancionCrearEditar.enlacesCancion,
          plataforma: 1
        }
      );
    }
    if (this.data.cancion) {
      const imagenPrincipal = this.data.cancion.imagenesCancion.find(imagenCancion => imagenCancion.esPrincipal);
      this._cancionRestService
        .editarCancion(
          this.cancionCrearEditar,
          this.data.cancion.id,
          generos,
          artistas,
          this.cancionCrearEditar.imagen,
          pathAlterno,
          albumes,
          enlaces,
          imagenPrincipal.id
        )
        .subscribe((respuestaCancionEditada: CancionInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaCancionEditada);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
      this._cargandoService.deshabiltarCargando();
    } else {
      this._cancionRestService
        .crearCancion(
          this.cancionCrearEditar,
          generos,
          artistas,
          this.cancionCrearEditar.imagen,
          pathAlterno,
          enlaces,
          albumes
        )
        .subscribe((respuestaCancionCreada: CancionInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaCancionCreada);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  obtenerPathImagenAlbum(): string {
    const imagenPrincipal = this.data.album.imagenesAlbum.find(imagenAlbum => imagenAlbum.esPrincipal);
    if (imagenPrincipal) {
      return imagenPrincipal.url;
    }
  }

  obtenerArtistaPrincipal(artistas: ArtistaInterface[]): Array<{id: number, esPrincipal: 0| 1}> {
    return artistas.map(
      (artistaCancion, indice) => {
        return {
          id: artistaCancion.id,
          esPrincipal: indice === 0 ? 1 : 0,
        };
      }
    );
  }
}
