import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrearEditarCancionComponent } from './crear-editar-cancion.component';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';
import {FormularioCancionModule} from '../../componentes/formulario-cancion/formulario-cancion.module';
import {InputTextModule} from 'primeng';



@NgModule({
  declarations: [CrearEditarCancionComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    FormularioCancionModule,
    BotonGuardarModule,
    BotonCancelarModule,
    InputTextModule,
  ],
  exports: [
    CrearEditarCancionComponent
  ]
})
export class CrearEditarCancionModule { }
