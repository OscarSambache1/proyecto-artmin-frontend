import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {GeneroInterface} from '../../../../interfaces/genero.interface';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {InputImagenComponent} from '../../../../componentes/input-imagen/input-imagen.component';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {environment} from '../../../../../environments/environment';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {
  MENSAJES_VALIDACION_ALBUMS_CANCION,
  MENSAJES_VALIDACION_ARTISTAS_CANCION,
  MENSAJES_VALIDACION_DESCRIPCION_CANCION,
  MENSAJES_VALIDACION_DURACION_SEGUNDOS_CANCION,
  MENSAJES_VALIDACION_ENLACE_CANCION,
  MENSAJES_VALIDACION_FECHA_LANZAMIENTO_CANCION,
  MENSAJES_VALIDACION_GENEROS_CANCION,
  MENSAJES_VALIDACION_NOMBRE_CANCION,
  MENSAJES_VALIDACION_TIPO_CANCION,
  VALIDACION_ALBUMS_CANCION,
  VALIDACION_ARTISTAS_CANCION,
  VALIDACION_DESCRIPCION_CANCION,
  VALIDACION_DURACION_SEGUNDOS_CANCION,
  VALIDACION_ENLACE_CANCION,
  VALIDACION_FECHA_LANZAMIENTO_CANCION,
  VALIDACION_GENEROS_CANCION,
  VALIDACION_IMAGEN_CANCION,
  VALIDACION_NOMBRE_CANCION,
  VALIDACION_TIPO_CANCION
} from '../../../../constantes/validaciones-formulario/cancion';
import {TIPOS_CANCIONES} from '../../../../constantes/tipos-cancion';
import {SelectItem} from 'primeng';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {mostrarSegundosEnMinutos} from '../../../../funciones/mostrar-segundos-en-minutos';
import {convertirMinutosMascaraASegundos} from '../../../../funciones/convertir-minutos-segundos';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {TipoAlbumInterface} from '../../../../interfaces/tipo-album.interface';
import {TipoCancionInterface} from '../../../../interfaces/tipo-cancion.interface';

@Component({
  selector: 'app-formulario-cancion',
  templateUrl: './formulario-cancion.component.html',
  styleUrls: ['./formulario-cancion.component.css']
})
export class FormularioCancionComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;
  arregloGeneros: GeneroInterface[] = [];
  formularioCancion: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    duracionSegundos: [],
    fechaLanzamiento: [],
    generos: [],
    imagen: [],
    artistas: [],
    albums: [],
    tipoCancion: [],
    enlace: []
  };
  subscribers = [];
  imagenSeleccionada;

  @Input()
  cancion: CancionInterface;

  @Input()
  deshabilitarAlbums: boolean;

  @Input()
  width: number;

  @Input()
  idsArtista: number[] = [];

  @Input()
  idsAlbums: number[] = [];

  @Input()
  album: AlbumInterface;

  @Output()
  cancionValidoEnviar: EventEmitter<CancionInterface | boolean> = new EventEmitter();

  @ViewChild(InputImagenComponent)
  componenteInputImagen: InputImagenComponent;

  idTipoCancion: number;
  tipos: SelectItem[] = TIPOS_CANCIONES;

  public mask = [/[0-5]/, /\d/, ':', /[0-5]/, /\d/];

  constructor(
    private readonly _generoRestService: GeneroRestService,
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _artistaService: ArtistaRestService,
    private readonly _albumRestService: AlbumRestService
  ) {
  }

  ngOnInit(): void {
    this.cargarGeneros();
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    if (this.cancion) {
      this.idTipoCancion = this.cancion.tipoCancion.id;
      if (!this.album) {
        this.formularioCancion.disable();
      }
    } else {
      if (this.idsArtista && this.idsArtista.length) {
        this.cargarMultiselectArtista();
      }
      if (this.idsAlbums && this.idsAlbums.length) {
        this.cargarMultiselectAlbums();
      }
    }
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioCancion = this._formBuilder.group({
      nombre: [this.cancion ? this.cancion.nombre : '', VALIDACION_NOMBRE_CANCION],
      descripcion: [this.cancion ? this.cancion.descripcion : '', VALIDACION_DESCRIPCION_CANCION],
      fechaLanzamiento: [this.cargarFechaLanzamiento(), VALIDACION_FECHA_LANZAMIENTO_CANCION],
      duracionSegundos: [this.cancion ? mostrarSegundosEnMinutos(this.cancion.duracionSegundos) : null, VALIDACION_DURACION_SEGUNDOS_CANCION],
      generos: [this.cancion ? this.cargarGenerosCancion() : [], VALIDACION_GENEROS_CANCION],
      artistas: [this.cancion ? this.cargarMultiselectArtista() : [], VALIDACION_ARTISTAS_CANCION],
      albums: [this.cancion ? this.cargarMultiselectAlbums() : [], VALIDACION_ALBUMS_CANCION],
      imagen: [this.cargarImagenCancion(), VALIDACION_IMAGEN_CANCION],
      tipoCancion: [this.cancion ? this.cancion.tipoCancion : null, VALIDACION_TIPO_CANCION],
      enlace: [this.cancion ? this.obtenerEnlace() : '', VALIDACION_ENLACE_CANCION]
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_CANCION);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_CANCION);
    this.verificarCampoFormControl('fechaLanzamiento', MENSAJES_VALIDACION_FECHA_LANZAMIENTO_CANCION);
    this.verificarCampoFormControl('duracionSegundos', MENSAJES_VALIDACION_DURACION_SEGUNDOS_CANCION);
    this.verificarCampoFormControl('generos', MENSAJES_VALIDACION_GENEROS_CANCION);
    this.verificarCampoFormControl('artistas', MENSAJES_VALIDACION_ARTISTAS_CANCION);
    this.verificarCampoFormControl('tipoCancion', MENSAJES_VALIDACION_TIPO_CANCION);
    this.verificarCampoFormControl('albums', MENSAJES_VALIDACION_ALBUMS_CANCION);
    this.verificarCampoFormControl('enlace', MENSAJES_VALIDACION_ENLACE_CANCION);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioCancion.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioCancion;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.cancionValidoEnviar.emit(formulario);
          } else {
            this.cancionValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharEventoImagen(event: any) {
    this.imagenSeleccionada = event;
    this.formularioCancion.patchValue(
      {
        imagen: this.imagenSeleccionada,
      }
    );
  }

  cargarGeneros() {
    this._generoRestService
      .findAll(JSON.stringify({}))
      .subscribe((respuestaGeneros: [GeneroInterface[], number]) => {
        this.arregloGeneros = respuestaGeneros[0];
      }, error => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
      });
  }

  cargarGenerosCancion() {
    return this.cancion.generosCancion.map(generoCancion => generoCancion.genero as GeneroInterface);
  }

  cargarImagenCancion() {
    if (this.cancion) {
      return obtenerUrlImagenPrincipal(this.cancion, 'imagenesCancion');
    } else {
      if (this.album) {
        return obtenerUrlImagenPrincipal(this.album, 'imagenesAlbum');
      } else {
        return '';
      }
    }
  }

  deshabilitarFormulario() {
    this.formularioCancion.disable();
  }

  buscarCancionesPorArtista(respuestaArtista: ArtistaInterface[]) {
    this.formularioCancion.patchValue(
      {
        artistas: [
          ...respuestaArtista]
      }
    );
  }

  cargarMultiselectArtista() {
    const consulta = {
      camposIn: [
        {
          nombreCampo: 'id',
          valor: this.idsArtista
        }
      ],
      relations: [
        'imagenesArtista'
      ]
    };
    this._artistaService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaArtista: [ArtistaInterface[], number]) => {
        this.formularioCancion.patchValue(
          {
            artistas: [...respuestaArtista[0]]
          }
        );
      }, error => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
      });
  }

  cargarFechaLanzamiento() {
    if (this.cancion) {
      return this.cancion.fechaLanzamiento;
    } else {
      if (this.album) {
        return this.album.fechaLanzamiento;
      } else {
        return '';
      }
    }
  }

  buscarCancionesPorAlbum(albumsSeleccionado: AlbumInterface[]) {
    this.formularioCancion.patchValue(
      {
        albums: [
          ...albumsSeleccionado]
      }
    );
  }

  private cargarMultiselectAlbums() {
    if (this.idsAlbums && this.idsAlbums.length) {
      const consulta = {
        camposIn: [
          {
            nombreCampo: 'id',
            valor: this.idsAlbums
          }
        ],
        relations: [
          'imagenesAlbum'
        ]
      };
      this._albumRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaAlbum: [AlbumInterface[], number]) => {
          this.formularioCancion.patchValue(
            {
              albums: [...respuestaAlbum[0]]
            }
          );
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    } else {
      return [];
    }
  }

  escucharTipoCancionSeleccionado(
    tipoCancionSeleccionado: TipoCancionInterface
  ) {
    this.formularioCancion.patchValue(
      {
        tipoCancion: tipoCancionSeleccionado,
      }
    );
  }

  obtenerIdsArtistasSeleccionados() {
    const existeCampoArtistas = this.formularioCancion.get('artistas').value;
    if (existeCampoArtistas) {
      return this.formularioCancion.get('artistas').value.map(artista => artista.id);
    }
  }

  volverFormularioInicial() {
    this.formularioCancion.patchValue(
      {
        nombre: this.cancion.nombre,
        descripcion: this.cancion.descripcion,
        fechaLanzamiento: this.cancion.fechaLanzamiento,
        duracionSegundos: this.cancion.duracionSegundos,
        tipoCancion: this.cancion.tipoCancion,
        artistas: this.cargarMultiselectArtista(),
        albums: this.cargarMultiselectAlbums(),
        generos: this.cargarGeneros(),
        imagen: this.cargarImagenCancion(),
        enlace: this.obtenerEnlace(),
      }
    );
    this.idTipoCancion = this.cancion.tipoCancion.id;
    this.componenteInputImagen.pathImagen = this.cargarImagenCancion();
    this.deshabilitarFormulario();
  }

  obtenerEnlace() {
    if (this.cancion && this.cancion.enlacesCancion.length) {
      return this.cancion.enlacesCancion[0].url;
    }
  }
}
