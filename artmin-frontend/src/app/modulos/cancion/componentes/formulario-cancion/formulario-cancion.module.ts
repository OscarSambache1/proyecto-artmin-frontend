import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormularioCancionComponent} from './formulario-cancion.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  CalendarModule,
  DropdownModule, InputMaskModule,
  InputNumberModule,
  InputTextareaModule,
  InputTextModule,
  MultiSelectModule
} from 'primeng';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteArtistaModule} from '../../../../componentes/autocomplete-artista/autocomplete-artista.module';
import {MaskedInputDirective} from 'angular2-text-mask';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteTipoCancionModule} from '../../../../componentes/autocomplete-tipo-cancion/autocomplete-tipo-cancion.module';
import {SafePipeModule} from '../../../../pipes/safe-pipe/safe-pipe.module';
import {MaskinputModuleModule} from '../../../../componentes/maskinput-module/maskinput-module.module';


@NgModule({
  declarations: [FormularioCancionComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    FormsModule,
    CalendarModule,
    InputNumberModule,
    MultiSelectModule,
    InputImagenModule,
    AlertaValidacionCampoFormularioModule,
    AutocompleteArtistaModule,
    DropdownModule,
    InputMaskModule,
    AutocompleteAlbumsModule,
    AutocompleteTipoCancionModule,
    SafePipeModule,
    MaskinputModuleModule,
  ],
  exports: [
    FormularioCancionComponent
  ]
})
export class FormularioCancionModule {
}
