import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RutaGestionCancionesComponent} from './rutas/ruta-gestion-canciones/ruta-gestion-canciones.component';
import {CancionRoutingModule} from './cancion-routing.module';
import {TableModule} from 'primeng';
import {SegundosAMinutosHorasPipeModule} from '../../pipes/segundos-a-minutos-horas-pipe/segundos-a-minutos-horas-pipe.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {AutcompleteGenerosModule} from '../../componentes/autcomplete-generos/autcomplete-generos.module';
import {AutocompleteArtistaModule} from '../../componentes/autocomplete-artista/autocomplete-artista.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {AutocompleteAlbumsModule} from '../../componentes/autocomplete-albums/autocomplete-albums.module';
import {CrearEditarCancionComponent} from './modales/crear-editar-cancion/crear-editar-cancion.component';
import {CrearEditarCancionModule} from './modales/crear-editar-cancion/crear-editar-cancion.module';
import {AutocompleteTipoCancionModule} from '../../componentes/autocomplete-tipo-cancion/autocomplete-tipo-cancion.module';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {RutaEditarCancionComponent} from './rutas/ruta-editar-cancion/ruta-editar-cancion.component';
import {FormularioCancionModule} from './componentes/formulario-cancion/formulario-cancion.module';
import {CarouselOpcionesModule} from '../../componentes/carousel-opciones/carousel-opciones.module';
import {RutaNominacionesModule} from '../../componentes/ruta-nominaciones/ruta-nominaciones.module';
import {RutaPresentacionesModule} from '../../componentes/ruta-presentaciones/ruta-presentaciones.module';


@NgModule({
  declarations: [RutaGestionCancionesComponent, RutaEditarCancionComponent],
  imports: [
    CommonModule,
    CancionRoutingModule,
    TableModule,
    SegundosAMinutosHorasPipeModule,
    InputBuscarBotonModule,
    AutcompleteGenerosModule,
    AutocompleteArtistaModule,
    AutocompleteAlbumsModule,
    BotonNuevoModule,
    CrearEditarCancionModule,
    AutocompleteTipoCancionModule,
    MigasPanModule,
    FormularioCancionModule,
    CarouselOpcionesModule,
    RutaNominacionesModule,
    RutaPresentacionesModule,
  ],
  entryComponents: [
    CrearEditarCancionComponent
  ]
})
export class CancionModule {
}
