import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioUnidadComponent } from './formulario-unidad.component';

describe('FormularioUnidadComponent', () => {
  let component: FormularioUnidadComponent;
  let fixture: ComponentFixture<FormularioUnidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioUnidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioUnidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
