import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {UnidadesCancionAlbumVideoInterface} from '../../../../interfaces/unidades-cancion-album-video.interface';
import {
  MENSAJES_VALIDACION_ALBUM_UNIDAD, MENSAJES_VALIDACION_CANCION_UNIDAD,
  MENSAJES_VALIDACION_MEDIDA_UNIDAD,
  MENSAJES_VALIDACION_UNIDADES_DEBUT_UNIDAD, MENSAJES_VALIDACION_UNIDADES_REAL_UNIDAD, MENSAJES_VALIDACION_VIDEO_UNIDAD,
  VALIDACION_MEDIDA_UNIDAD,
  VALIDACION_UNIDADES_DEBUT_UNIDAD,
  VALIDACION_UNIDADES_REAL_UNIDAD
} from '../../../../constantes/validaciones-formulario/unidad';
import {MedidaInterface} from '../../../../interfaces/medida.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';

@Component({
  selector: 'app-formulario-unidad',
  templateUrl: './formulario-unidad.component.html',
  styleUrls: ['./formulario-unidad.component.css']
})
export class FormularioUnidadComponent implements OnInit {

  formularioUnidad: FormGroup;
  mensajesError = {
    unidadesDebut: [],
    unidadesReales: [],
    medida: [],
    cancion: [],
    album: [],
    video: [],
  };
  subscribers = [];

  @Output()
  unidadValidoEnviar: EventEmitter<UnidadesCancionAlbumVideoInterface | boolean> = new EventEmitter();

  @Input()
  unidad: UnidadesCancionAlbumVideoInterface;

  @Input()
  tipo: string;

  @Input()
  idsCancionesAlbumesVideos = [];

  placeHolderTipo = '';

  @Input()
  idArtista: number;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.setearPlaceholderTipo();
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioUnidad = this._formBuilder.group({
      unidadesDebut: [this.unidad ? this.unidad.unidadesDebut : null, VALIDACION_UNIDADES_DEBUT_UNIDAD],
      unidadesReales: [this.unidad ? this.unidad.unidadesReales : null, VALIDACION_UNIDADES_REAL_UNIDAD],
      medida: [this.unidad ? this.unidad.medida : null, VALIDACION_MEDIDA_UNIDAD],
      cancion: [this.unidad ? this.unidad.cancion : null, this.tipo === 'cancion' ? [Validators.required] : []],
      album: [this.unidad ? this.unidad.album : null, this.tipo === 'album' ? [Validators.required] : []],
      video: [this.unidad ? this.unidad.video : null, this.tipo === 'video' ? [Validators.required] : []],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('unidadesDebut', MENSAJES_VALIDACION_UNIDADES_DEBUT_UNIDAD);
    this.verificarCampoFormControl('unidadesReales', MENSAJES_VALIDACION_UNIDADES_REAL_UNIDAD);
    this.verificarCampoFormControl('medida', MENSAJES_VALIDACION_MEDIDA_UNIDAD);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCION_UNIDAD);
    this.verificarCampoFormControl('album', MENSAJES_VALIDACION_ALBUM_UNIDAD);
    this.verificarCampoFormControl('video', MENSAJES_VALIDACION_VIDEO_UNIDAD);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioUnidad.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioUnidad;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.unidadValidoEnviar.emit(formulario);
          } else {
            this.unidadValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  setearMedida(medidaSeleccioanda: MedidaInterface) {
    this.formularioUnidad.patchValue(
      {
        medida: medidaSeleccioanda
      }
    );
  }

  setearCancion(cancionSeleccionada: CancionInterface) {
    this.formularioUnidad.patchValue(
      {
        cancion: cancionSeleccionada
      }
    );
  }

  setearAlbum(albumSeleccionado: AlbumInterface) {
    this.formularioUnidad.patchValue(
      {
        album: albumSeleccionado
      }
    );
  }

  setearVideo(videoSeleccionado: VideoInterface) {
    this.formularioUnidad.patchValue(
      {
        video: videoSeleccionado
      }
    );
  }

  setearPlaceholderTipo() {
    if (this.tipo === 'video') {
      this.placeHolderTipo = ' del video';
    }
    if (this.tipo === 'album') {
      this.placeHolderTipo = ' del album';
    }
    if (this.tipo === 'cancion') {
      this.placeHolderTipo = ' de la cancion';
    }
  }
}
