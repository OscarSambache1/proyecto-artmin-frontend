import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioUnidadComponent } from './formulario-unidad.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputNumberModule} from 'primeng';
import {AutocompleteMedidaModule} from '../../../../componentes/autocomplete-medida/autocomplete-medida.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteVideoModule} from '../../../../componentes/autocomplete-video/autocomplete-video.module';



@NgModule({
  declarations: [FormularioUnidadComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InputNumberModule,
    AutocompleteMedidaModule,
    AlertaValidacionCampoFormularioModule,
    AutocompleteCancionModule,
    AutocompleteAlbumsModule,
    AutocompleteVideoModule
  ],
  exports: [
    FormularioUnidadComponent
  ]
})
export class FormularioUnidadModule { }
