import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GestionUnidadesComponent} from './rutas/gestion-unidades/gestion-unidades.component';


const routes: Routes = [
  {
    path: 'gestion-unidades',
    children: [
      {
        path: '',
        component: GestionUnidadesComponent
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-chart-cancion-album-artista-video',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnidadCancionAlbumVideoRoutingModule { }
