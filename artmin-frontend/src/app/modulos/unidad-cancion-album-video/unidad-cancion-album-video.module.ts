import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnidadCancionAlbumVideoRoutingModule } from './unidad-cancion-album-video-routing.module';
import { GestionUnidadesComponent } from './rutas/gestion-unidades/gestion-unidades.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {TableModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {ModalCrearEditarUnidadModule} from './modales/modal-crear-editar-unidad/modal-crear-editar-unidad.module';
import {ModalCrearEditarUnidadComponent} from './modales/modal-crear-editar-unidad/modal-crear-editar-unidad.component';


@NgModule({
  declarations: [GestionUnidadesComponent],
  imports: [
    CommonModule,
    UnidadCancionAlbumVideoRoutingModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    TableModule,
    FormsModule,
    ModalCrearEditarUnidadModule
  ],
  exports: [
    GestionUnidadesComponent
  ],
  entryComponents: [
    ModalCrearEditarUnidadComponent
  ]
})
export class UnidadCancionAlbumVideoModule { }
