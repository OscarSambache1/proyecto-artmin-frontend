import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear, toastExitoEditar} from '../../../../constantes/mensajes-toaster';
import {UnidadesCancionAlbumVideoInterface} from '../../../../interfaces/unidades-cancion-album-video.interface';
import {UnidadCancionAlbumVideoRestService} from '../../../../servicios/rest/unidad-cancion-album-video-rest.service';

@Component({
  selector: 'app-modal-crear-editar-unidad',
  templateUrl: './modal-crear-editar-unidad.component.html',
  styleUrls: ['./modal-crear-editar-unidad.component.css']
})
export class ModalCrearEditarUnidadComponent implements OnInit {

  formularioValido: boolean;
  unidadACrearEditar: UnidadesCancionAlbumVideoInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      unidad: UnidadesCancionAlbumVideoInterface,
      idArtista: number,
      tipo: string,
      chart: number,
      idsCancionesAlbumesVideos: number[],
    },
    public dialogo: MatDialogRef<ModalCrearEditarUnidadComponent>,
    private readonly _unidadRestService: UnidadCancionAlbumVideoRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) { }

  ngOnInit(): void {
  }

  crearEditarUnidad() {
    this._cargandoService.habilitarCargando();
    if (this.data.unidad) {
      this._unidadRestService
        .updateOne(
          this.data.unidad.id,
          this.unidadACrearEditar
        )
        .subscribe((respuestaChartEditado: UnidadesCancionAlbumVideoInterface) => {
            this._toasterService.pop(toastExitoEditar);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaChartEditado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
      this._cargandoService.deshabiltarCargando();
    } else {
      this.unidadACrearEditar.chart = this.data.chart;
      this._unidadRestService
        .create(
          this.unidadACrearEditar,
        )
        .subscribe((respuestaChartCreado: UnidadesCancionAlbumVideoInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaChartCreado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }

  validarFormulario(unidad: UnidadesCancionAlbumVideoInterface | boolean) {
    if (unidad) {
      this.formularioValido = true;
      this.unidadACrearEditar = unidad as UnidadesCancionAlbumVideoInterface;
    } else {
      this.formularioValido = false;
    }
  }

}
