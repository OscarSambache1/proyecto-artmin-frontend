import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarUnidadComponent } from './modal-crear-editar-unidad.component';

describe('ModalCrearEditarUnidadComponent', () => {
  let component: ModalCrearEditarUnidadComponent;
  let fixture: ComponentFixture<ModalCrearEditarUnidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarUnidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarUnidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
