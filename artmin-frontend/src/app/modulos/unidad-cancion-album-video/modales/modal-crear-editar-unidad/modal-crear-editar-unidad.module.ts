import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarUnidadComponent } from './modal-crear-editar-unidad.component';
import {FormularioUnidadModule} from '../../componentes/formulario-unidad/formulario-unidad.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarUnidadComponent],
  imports: [
    CommonModule,
    FormularioUnidadModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule
  ]
})
export class ModalCrearEditarUnidadModule { }
