import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_VIDEO} from '../../../video/rutas/definicion-rutas/rutas-videos';

export const RUTAS_UNIDADES = {
  _rutaInicioUnidades: {
    ruta: ':idChart/unidad-modulo',
    nombre: 'Modulo Unidad',
    generarRuta: (...argumentos) => {
      return `${argumentos[3]}/unidad-modulo`;
    }
  },

  _rutaGestionUnidades: {
    ruta: 'gestion-unidades',
    nombre: 'Gestión Unidades',
    generarRuta: (...argumentos) => {
      return `gestion-unidades`;
    }
  },

  rutaGestionUnidades: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    const tipo = argumentos?.find(arg => ['A', 'AL', 'C', 'V', 'CH'].includes(arg));
    let rutasArtista = [];
    let rutaObjetos = [];

    if (argumentos[0]) {
      rutasArtista  = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
      ];
    }
    if (argumentos[1]) {
      if (tipo === 'AL') {
        rutaObjetos = [
          RUTAS_ALBUM._rutaInicioAlbum,
          RUTAS_ALBUM._rutaGestionAlbumes,
          RUTAS_ALBUM._rutaEditarAlbum,
        ];
      }
      if (tipo === 'C') {
        rutaObjetos = [
          RUTAS_CANCION._rutaInicioCancion,
          RUTAS_CANCION._rutaGestionCanciones,
          RUTAS_CANCION._rutaEditarCancion,
        ];
      }
      if (tipo === 'V') {
        rutaObjetos = [
          RUTAS_VIDEO._rutaInicioVideo,
          RUTAS_VIDEO._rutaGestionVideos,
          RUTAS_VIDEO._rutaIdVideo,
        ];
      }
    }
    const rutaArreglo = [
      ...rutasArtista,
      ...rutaObjetos,
      RUTAS_CHART._rutaInicioChart,
      RUTAS_CHART._rutaGestionCharts,
      this._rutaInicioUnidades,
      this._rutaGestionUnidades
    ];

    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
