import { Component, OnInit } from '@angular/core';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ChartInterface} from '../../../../interfaces/chart.interface';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {ChartRestService} from '../../../../servicios/rest/chart-rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {UnidadesCancionAlbumVideoInterface} from '../../../../interfaces/unidades-cancion-album-video.interface';
import {UnidadCancionAlbumVideoRestService} from '../../../../servicios/rest/unidad-cancion-album-video-rest.service';
import {RUTAS_UNIDADES} from '../definicion-rutas/rutas-unidades';
import {ModalCrearEditarUnidadComponent} from '../../modales/modal-crear-editar-unidad/modal-crear-editar-unidad.component';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_VIDEO} from '../../../video/rutas/definicion-rutas/rutas-videos';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';

@Component({
  selector: 'app-gestion-unidades',
  templateUrl: './gestion-unidades.component.html',
  styleUrls: ['./gestion-unidades.component.css']
})
export class GestionUnidadesComponent implements OnInit {

  unidades: UnidadesCancionAlbumVideoInterface[];

  columnasUnidades = [
    {
      field: 'nombre',
      header: 'Nombre',
    },
    {
      field: 'unidadesDebut',
      header: 'Unidades Debut',
    },
    {
      field: 'unidadesReales',
      header: 'Unidades Reales',
    },
    {
      field: 'medida',
      header: 'Medida',
    },
    {
      field: 'acciones',
      header: 'Acciones',
    },
  ];

  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  ruta = [];
  // artista: ArtistaInterface;
  chart: ChartInterface;
  rutaImagen = '';
  idChart: number;
  arregloRutas: any[];
  tipo = '';
  arregloidsCancionesAlbumesVideosSeleccionadas = [];
  idObjParam = 0;
  tipoObjeto = '';
  objeto: any;

  constructor(
    private readonly _unidadRestService: UnidadCancionAlbumVideoRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _chartRestService: ChartRestService,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cancionRestService: CancionRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _videoRestService: VideoRestService,
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtista = +params.idArtista;
          if (+params.idAlbum) {
            this.idObjParam = +params.idAlbum;
          }
          if (+params.idCancion) {
            this.idObjParam = +params.idCancion;
          }
          if (+params.idVideo) {
            this.idObjParam = +params.idVideo;
          }
          this.idChart = +params.idChart;
          this.tipoObjeto = params.tipo;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(async queryParams => {
          if (this.idArtista) {
            const promesaArtista = this._artistaRestService.findOne(this.idArtista)
              .toPromise();
            this.objeto = await promesaArtista;
          }
          if (this.idObjParam) {
            if (this.tipoObjeto === 'AL') {
              this._albumRestService.findOne(this.idObjParam)
                .subscribe(
                  (album: AlbumInterface) => {
                    this.objeto = album;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipoObjeto === 'C') {
              this._cancionRestService.findOne(this.idObjParam)
                .subscribe(
                  (cancion: CancionInterface) => {
                    this.objeto = cancion;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipoObjeto === 'V') {
              this._videoRestService.findOne(this.idObjParam)
                .subscribe(
                  (video: VideoInterface) => {
                    this.objeto = video;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
          }

          if (this.idChart) {
            const promesaChart = this._chartRestService.findOne(this.idChart)
              .toPromise();
            this.chart = await promesaChart;
            this.tipo = this.chart.tipo;
          }
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idChart: this.idChart,
              idArtista: this.idArtista,
              tipo: this.tipo,
            };
          }
          this.queryParams.consulta.tipoObjeto = this.tipoObjeto;
          this.queryParams.consulta.idObjParam = this.idObjParam;
          this._cargandoService.habilitarCargando();
          return this._unidadRestService.obtenerUnidades(
            JSON.stringify(this.queryParams.consulta)
          ).toPromise();
        })
      )
      .subscribe(
        (respuestaUnidades: [UnidadesCancionAlbumVideoInterface[], number]) => {
          this.unidades = respuestaUnidades[0];
          this.arregloidsCancionesAlbumesVideosSeleccionadas = this.unidades
            .map(chartCancionAlbumVideo => {
              return chartCancionAlbumVideo[this.tipo].id;
            });
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarUnidades();
  }

  buscarUnidades() {
    const consulta = {
      busqueda: this.busqueda,
      idChart: this.idChart,
      idArtista: this.idArtista,
      tipo: this.tipo,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    let rutasArtista = [];
    if (this.idArtista) {
      rutasArtista = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista])];
    }
    let rutasObjetos = [];
    if (this.idObjParam) {
      if (this.tipoObjeto === 'AL') {
        rutasObjetos = [
          RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'C') {
        rutasObjetos = [
          RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'V') {
        rutasObjetos = [
          RUTAS_VIDEO.rutaGestionVideos(false, true, [this.idArtista]),
        ];
      }
    }
    this.arregloRutas = [
      ...rutasArtista,
      ...rutasObjetos,
      RUTAS_CHART.rutaGestionCharts(false, true, [this.idArtista, this.idObjParam, this.tipoObjeto]),
      RUTAS_UNIDADES.rutaGestionUnidades(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_UNIDADES
      .rutaGestionUnidades(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
      .ruta;
  }

  abrirModalCrearEditarUnidad(
    unidad?: UnidadesCancionAlbumVideoInterface,
    indice?: number
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarUnidadComponent,
        {
          width: '600px',
          data: {
            unidad,
            idArtista: this.idArtista,
            tipo: this.tipo,
            chart: this.idChart,
            idsCancionesAlbumesVideos: this.arregloidsCancionesAlbumesVideosSeleccionadas,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (unidadCreadaEditada: UnidadesCancionAlbumVideoInterface) => {
          if (unidadCreadaEditada) {
            const consulta = {
              where: {
                id: unidadCreadaEditada.id
              },
              relations: [
                'chart',
                'medida',
                'cancion',
                'cancion.imagenesCancion',
                'album',
                'album.imagenesAlbum',
                'video',
                'video.cancion'
              ]
            };
            this._unidadRestService
              .findAll(
                JSON.stringify(consulta)
              )
              .subscribe(
                (respuestaUnidad: [UnidadesCancionAlbumVideoInterface[], number]) => {
                  if (indice || indice === 0) {
                    this.unidades[indice] = respuestaUnidad[0][0];
                  } else {
                    this.unidades.push(respuestaUnidad[0][0]);
                  }
                  this.unidades = [...this.unidades];
                  this.arregloidsCancionesAlbumesVideosSeleccionadas = this.unidades
                    .map(chartCancionAlbumVideo => {
                      return chartCancionAlbumVideo[this.tipo].id;
                    });
                  this._cargandoService.deshabiltarCargando();
                }
                , error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                  this._cargandoService.deshabiltarCargando();
                }
              );

          }
        },
        error => {
          console.error(error);
        },
      );
  }

  obtenerUrlPrincipal(
    objeto: any) {
    let campo;
    if (this.tipo === 'cancion') {
      campo = 'imagenesCancion';
    }
    if (this.tipo === 'album') {
      campo = 'imagenesAlbum';
    }
    if (objeto[campo] && objeto[campo].length) {
      return obtenerUrlImagenPrincipal(objeto, campo);
    }
  }

}
