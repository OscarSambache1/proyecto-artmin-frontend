import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecordChartRoutingModule } from './record-chart-routing.module';
import { RutaGestionRecordsChartComponent } from './rutas/ruta-gestion-records-chart/ruta-gestion-records-chart.component';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {MultiSelectModule, TableModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {AutocompleteRecordModule} from '../../componentes/autocomplete-record/autocomplete-record.module';
import {ModalCrearEditarRecordChartModule} from './modales/modal-crear-editar-record-chart/modal-crear-editar-record-chart.module';
import {ModalCrearEditarRecordChartComponent} from './modales/modal-crear-editar-record-chart/modal-crear-editar-record-chart.component';


@NgModule({
  declarations: [RutaGestionRecordsChartComponent],
  imports: [
    CommonModule,
    RecordChartRoutingModule,
    MigasPanModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    TableModule,
    MultiSelectModule,
    FormsModule,
    AutocompleteRecordModule,
    ModalCrearEditarRecordChartModule
  ],
  exports: [
    RutaGestionRecordsChartComponent
  ],
  entryComponents: [
    ModalCrearEditarRecordChartComponent
  ]
})
export class RecordChartModule { }
