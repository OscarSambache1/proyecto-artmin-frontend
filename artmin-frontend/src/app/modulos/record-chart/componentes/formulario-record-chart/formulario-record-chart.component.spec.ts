import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioRecordChartComponent } from './formulario-record-chart.component';

describe('FormularioRecordChartComponent', () => {
  let component: FormularioRecordChartComponent;
  let fixture: ComponentFixture<FormularioRecordChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioRecordChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioRecordChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
