import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormularioRecordChartComponent} from './formulario-record-chart.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteCancionModule} from '../../../../componentes/autocomplete-cancion/autocomplete-cancion.module';
import {AutocompleteAlbumsModule} from '../../../../componentes/autocomplete-albums/autocomplete-albums.module';
import {AutocompleteVideoModule} from '../../../../componentes/autocomplete-video/autocomplete-video.module';
import {AutocompleteMedidaModule} from '../../../../componentes/autocomplete-medida/autocomplete-medida.module';
import {AutocompleteRecordModule} from '../../../../componentes/autocomplete-record/autocomplete-record.module';
import {CalendarModule, InputNumberModule} from 'primeng';


@NgModule({
  declarations: [FormularioRecordChartComponent],
  exports: [
    FormularioRecordChartComponent,
    ReactiveFormsModule,
    AlertaValidacionCampoFormularioModule,
    AutocompleteCancionModule,
    AutocompleteAlbumsModule,
    AutocompleteVideoModule,
    AutocompleteMedidaModule,
    AutocompleteRecordModule,
    CalendarModule,
    InputNumberModule
  ],
  imports: [
    CommonModule
  ]
})
export class FormularioRecordChartModule {
}
