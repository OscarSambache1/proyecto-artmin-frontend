import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RecordChartInterface} from '../../../../interfaces/record-chart.interface';
import {ToasterService} from 'angular2-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';
import {RecordInterface} from '../../../../interfaces/record.interface';
import {MedidaInterface} from '../../../../interfaces/medida.interface';
import {
  MENSAJES_VALIDACION_ALBUM_RECORDS_CHART,
  MENSAJES_VALIDACION_CANCION_RECORDS_CHART,
  MENSAJES_VALIDACION_RECORDS_CHART_CANTIDAD,
  MENSAJES_VALIDACION_RECORDS_CHART_FECHA, MENSAJES_VALIDACION_RECORDS_CHART_MEDIDA,
  MENSAJES_VALIDACION_RECORDS_CHART_RECORD,
  MENSAJES_VALIDACION_VIDEO_RECORDS_CHART
} from '../../../../constantes/validaciones-formulario/record-chart';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';

@Component({
  selector: 'app-formulario-record-chart',
  templateUrl: './formulario-record-chart.component.html',
  styleUrls: ['./formulario-record-chart.component.css']
})
export class FormularioRecordChartComponent implements OnInit {
  configuracionCalendario = CONFIGURACIONES_CALENDARIO;

  formularioRecordChart: FormGroup;
  mensajesError = {
    cancion: [],
    album: [],
    video: [],
    record: [],
    medida: [],
    fechaRecord: [],
    cantidadMedida: [],

  };
  subscribers = [];

  @Output()
  recordChartValidoEnviar: EventEmitter<RecordChartInterface | boolean> = new EventEmitter();

  @Input()
  recordChart: RecordChartInterface;

  @Input()
  tipo: string;

  @Input()
  idsCancionesAlbumesVideos = [];

  placeHolderTipo = '';

  @Input()
  idArtista: number;

  @Input()
  idChart: number;

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
  ) {
  }

  ngOnInit(): void {
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.setearPlaceholderTipo();
    this.crearFormulario();
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioRecordChart = this._formBuilder.group({
      cantidadMedida: [this.recordChart ? this.recordChart.cantidadMedida : null, [Validators.required]],
      fechaRecord: [this.recordChart ? this.recordChart.fechaRecord : null, [Validators.required]],
      record: [this.recordChart ? this.recordChart.record : [Validators.required]],
      medida: [this.recordChart ? this.recordChart.medida : [Validators.required]],
      cancion: [this.recordChart ? this.recordChart.cancion : null, this.tipo === 'cancion' ? [Validators.required] : []],
      album: [this.recordChart ? this.recordChart.album : null, this.tipo === 'album' ? [Validators.required] : []],
      video: [this.recordChart ? this.recordChart.video : null, this.tipo === 'video' ? [Validators.required] : []],
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('cantidadMedida', MENSAJES_VALIDACION_RECORDS_CHART_FECHA);
    this.verificarCampoFormControl('fechaRecord', MENSAJES_VALIDACION_RECORDS_CHART_CANTIDAD);
    this.verificarCampoFormControl('record', MENSAJES_VALIDACION_RECORDS_CHART_RECORD);
    this.verificarCampoFormControl('medida', MENSAJES_VALIDACION_RECORDS_CHART_MEDIDA);
    this.verificarCampoFormControl('cancion', MENSAJES_VALIDACION_CANCION_RECORDS_CHART);
    this.verificarCampoFormControl('album', MENSAJES_VALIDACION_ALBUM_RECORDS_CHART);
    this.verificarCampoFormControl('video', MENSAJES_VALIDACION_VIDEO_RECORDS_CHART);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioRecordChart.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
        valor => {
          this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioRecordChart;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const enlaceValido = formularioFormGroup.valid;
          if (enlaceValido) {
            this.recordChartValidoEnviar.emit(formulario);
          } else {
            this.recordChartValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }


  setearCancion(cancionSeleccionada: CancionInterface) {
    this.formularioRecordChart.patchValue(
      {
        cancion: cancionSeleccionada
      }
    );
  }

  setearAlbum(albumSeleccionado: AlbumInterface) {
    this.formularioRecordChart.patchValue(
      {
        album: albumSeleccionado
      }
    );
  }

  setearVideo(videoSeleccionado: VideoInterface) {
    this.formularioRecordChart.patchValue(
      {
        video: videoSeleccionado
      }
    );
  }

  setearRecord(recordSeleccionado: RecordInterface) {
    this.formularioRecordChart.patchValue(
      {
        record: recordSeleccionado
      }
    );
  }

  setearMedida(medidaSeleccionada: MedidaInterface) {
    this.formularioRecordChart.patchValue(
      {
        medida: medidaSeleccionada
      }
    );
  }

  setearPlaceholderTipo() {
    if (this.tipo === 'video') {
      this.placeHolderTipo = ' del video';
    }
    if (this.tipo === 'album') {
      this.placeHolderTipo = ' del album';
    }
    if (this.tipo === 'cancion') {
      this.placeHolderTipo = ' de la cancion';
    }
  }

}
