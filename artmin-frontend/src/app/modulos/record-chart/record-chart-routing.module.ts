import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionRecordsChartComponent} from './rutas/ruta-gestion-records-chart/ruta-gestion-records-chart.component';


const routes: Routes = [
  {
    path: 'gestion-records-chart',
    children: [
      {
        path: '',
        component: RutaGestionRecordsChartComponent
      },
    ]
  },

  {
    path: '',
    redirectTo: 'gestion-records-chart',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordChartRoutingModule { }
