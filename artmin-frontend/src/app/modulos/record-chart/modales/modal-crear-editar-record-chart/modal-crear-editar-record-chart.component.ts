import {Component, Inject, OnInit} from '@angular/core';
import {RecordChartInterface} from '../../../../interfaces/record-chart.interface';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {RecordChartRestService} from '../../../../servicios/rest/record-chart-rest.service';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ToastErrorCargandoDatos, toastExitoCrear, toastExitoEditar} from '../../../../constantes/mensajes-toaster';

@Component({
  selector: 'app-modal-crear-editar-record-chart',
  templateUrl: './modal-crear-editar-record-chart.component.html',
  styleUrls: ['./modal-crear-editar-record-chart.component.css']
})
export class ModalCrearEditarRecordChartComponent implements OnInit {

  formularioValido: boolean;
  recordChartACrearEditar: RecordChartInterface;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      recordChart: RecordChartInterface,
      idArtista: number,
      tipo: string,
      chart: number,
      idsCancionesAlbumesVideos: number[],
    },
    public dialogo: MatDialogRef<ModalCrearEditarRecordChartComponent>,
    private readonly _recordChartRestService: RecordChartRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) {
  }

  ngOnInit(): void {
  }

  crearEditarRecordChart() {
    this._cargandoService.habilitarCargando();
    if (this.data.recordChart) {
      this._recordChartRestService
        .updateOne(
          this.data.recordChart.id,
          this.recordChartACrearEditar
        )
        .subscribe((respuestaRecordEditado: RecordChartInterface) => {
            this._toasterService.pop(toastExitoEditar);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaRecordEditado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
      this._cargandoService.deshabiltarCargando();
    } else {
      this.recordChartACrearEditar.chart = this.data.chart;
      this._recordChartRestService
        .create(
          this.recordChartACrearEditar,
        )
        .subscribe((respuestaRecordCreado: RecordChartInterface) => {
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.dialogo.close(respuestaRecordCreado);
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  cerrarModal() {
    this.dialogo.close();
  }

  validarFormulario(recordChart: RecordChartInterface | boolean) {
    if (recordChart) {
      this.formularioValido = true;
      this.recordChartACrearEditar = recordChart as RecordChartInterface;
    } else {
      this.formularioValido = false;
    }
  }
}
