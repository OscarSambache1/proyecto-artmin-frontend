import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCrearEditarRecordChartComponent } from './modal-crear-editar-record-chart.component';

describe('ModalCrearEditarRecordChartComponent', () => {
  let component: ModalCrearEditarRecordChartComponent;
  let fixture: ComponentFixture<ModalCrearEditarRecordChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCrearEditarRecordChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCrearEditarRecordChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
