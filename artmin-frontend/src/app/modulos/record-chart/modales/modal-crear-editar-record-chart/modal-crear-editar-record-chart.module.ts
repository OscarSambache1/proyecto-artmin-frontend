import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalCrearEditarRecordChartComponent } from './modal-crear-editar-record-chart.component';
import {FormularioRecordChartModule} from '../../componentes/formulario-record-chart/formulario-record-chart.module';
import {MatDialogModule} from '@angular/material/dialog';
import {BotonGuardarModule} from '../../../../componentes/boton-guardar/boton-guardar.module';
import {BotonCancelarModule} from '../../../../componentes/boton-cancelar/boton-cancelar.module';



@NgModule({
  declarations: [ModalCrearEditarRecordChartComponent],
  imports: [
    CommonModule,
    FormularioRecordChartModule,
    MatDialogModule,
    BotonGuardarModule,
    BotonCancelarModule,
  ],
  exports: [
    ModalCrearEditarRecordChartComponent
  ]
})
export class ModalCrearEditarRecordChartModule { }
