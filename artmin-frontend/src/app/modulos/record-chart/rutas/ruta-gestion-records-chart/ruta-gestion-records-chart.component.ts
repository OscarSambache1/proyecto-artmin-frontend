import { Component, OnInit } from '@angular/core';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ChartInterface} from '../../../../interfaces/chart.interface';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../../../servicios/cargando-service';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {ChartRestService} from '../../../../servicios/rest/chart-rest.service';
import {ActivatedRoute, Router} from '@angular/router';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {RecordChartInterface} from '../../../../interfaces/record-chart.interface';
import {RecordChartRestService} from '../../../../servicios/rest/record-chart-rest.service';
import {RecordInterface} from '../../../../interfaces/record.interface';
import {RUTAS_RECORDS_CHART} from '../definicion-rutas/rutas-records-chart';
import {ModalCrearEditarRecordChartComponent} from '../../modales/modal-crear-editar-record-chart/modal-crear-editar-record-chart.component';
import {ChartCancionAlbumArtistaVideoInterface} from '../../../../interfaces/chart-cancion-album-artista-video.interface';
import {CancionRestService} from '../../../../servicios/rest/cancion-rest.service';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {VideoRestService} from '../../../../servicios/rest/video-rest.service';
import {RUTAS_ALBUM} from '../../../album/rutas/definicion-rutas/rutas-album';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_VIDEO} from '../../../video/rutas/definicion-rutas/rutas-videos';
import {RUTAS_UNIDADES} from '../../../unidad-cancion-album-video/rutas/definicion-rutas/rutas-unidades';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {VideoInterface} from '../../../../interfaces/video.interface';

@Component({
  selector: 'app-ruta-gestion-records-chart',
  templateUrl: './ruta-gestion-records-chart.component.html',
  styleUrls: ['./ruta-gestion-records-chart.component.css']
})
export class RutaGestionRecordsChartComponent implements OnInit {


  recordsChart: RecordChartInterface[];

  columnas = [
    {
      field: 'nombre',
      header: 'Nombre',
      width: '35%'
    },
    {
      field: 'record',
      header: 'Record',
      width: '35%'
    },
    {
      field: 'cantidadMedida',
      header: 'Cantidad',
      width: '10%'
    },
    {
      field: 'medida',
      header: 'Medida',
      width: '10%'
    },
    {
      field: 'fechaRecord',
      header: 'Fecha',
      width: '10%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '10%'
    },
  ];

  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  ruta = [];
  artista: ArtistaInterface;
  chart: ChartInterface;
  rutaImagen = '';
  idChart: number;
  idRecord: number;
  arregloRutas: any[];
  tipo = '';
  arregloidsCancionesAlbumesVideosSeleccionadas = [];
  idObjParam = 0;
  tipoObjeto = '';
  objeto: any;
  constructor(
    private readonly _recordChartRestService: RecordChartRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _chartRestService: ChartRestService,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cancionRestService: CancionRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _videoRestService: VideoRestService,
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtista = +params.idArtista;
          if (+params.idAlbum) {
            this.idObjParam = +params.idAlbum;
          }
          if (+params.idCancion) {
            this.idObjParam = +params.idCancion;
          }
          if (+params.idVideo) {
            this.idObjParam = +params.idVideo;
          }
          this.idChart = +params.idChart;
          this.tipoObjeto = params.tipo;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(async queryParams => {
          this.idRecord = +queryParams.idRecord;
          if (this.idArtista) {
            const promesaArtista = this._artistaRestService.findOne(this.idArtista)
              .toPromise();
            this.objeto = await promesaArtista;
          }
          if (this.idObjParam) {
            if (this.tipoObjeto === 'AL') {
              this._albumRestService.findOne(this.idObjParam)
                .subscribe(
                  (album: AlbumInterface) => {
                    this.objeto = album;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipoObjeto === 'C') {
              this._cancionRestService.findOne(this.idObjParam)
                .subscribe(
                  (cancion: CancionInterface) => {
                    this.objeto = cancion;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
            if (this.tipoObjeto === 'V') {
              this._videoRestService.findOne(this.idObjParam)
                .subscribe(
                  (video: VideoInterface) => {
                    this.objeto = video;
                  }, error => {
                    console.error(error);
                    this._toasterService.pop(ToastErrorCargandoDatos);
                  }
                );
            }
          }

          if (this.idChart) {
            const promesaChart = this._chartRestService.findOne(this.idChart)
              .toPromise();
            this.chart = await promesaChart;
            this.tipo = this.chart.tipo;
          }
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.idRecord) {
              this.idRecord = this.queryParams.consulta.idRecord;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idChart: this.idChart,
              idArtista: this.idArtista,
              idRecord: this.idRecord,
              tipo: this.tipo,
            };
          }
          this.queryParams.consulta.tipoObjeto = this.tipoObjeto;
          this.queryParams.consulta.idObjParam = this.idObjParam;
          this._cargandoService.habilitarCargando();
          return this._recordChartRestService.obtenerRecords(
            JSON.stringify(this.queryParams.consulta)
          ).toPromise();
        })
      )
      .subscribe(
        (respuestaRecordsChart: [RecordChartInterface[], number]) => {
          this.recordsChart = respuestaRecordsChart[0];
          this.arregloidsCancionesAlbumesVideosSeleccionadas = this.recordsChart
            .map(chartCancionAlbumVideo => {
              return chartCancionAlbumVideo[this.tipo].id;
            });
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarCharts();
  }

  buscarRecordsChartPorRecord(recordSeleccionado: RecordInterface) {
    this.idRecord = recordSeleccionado ? recordSeleccionado.id : undefined;
    this.buscarCharts();
  }

  buscarCharts() {
    const consulta = {
      busqueda: this.busqueda,
      idChart: this.idChart,
      idArtista: this.idArtista,
      idRecord: this.idRecord,
      tipo: this.tipo,
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    let rutasArtista = [];
    if (this.idArtista) {
      rutasArtista = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista])];
    }
    let rutasObjetos = [];
    if (this.idObjParam) {
      if (this.tipoObjeto === 'AL') {
        rutasObjetos = [
          RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'C') {
        rutasObjetos = [
          RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista, this.idObjParam]),
        ];
      }
      if (this.tipoObjeto === 'V') {
        rutasObjetos = [
          RUTAS_VIDEO.rutaGestionVideos(false, true, [this.idArtista]),
        ];
      }
    }
    this.arregloRutas = [
      ...rutasArtista,
      ...rutasObjetos,
      RUTAS_CHART.rutaGestionCharts(false, true, [this.idArtista, this.idObjParam, this.tipoObjeto]),
      RUTAS_RECORDS_CHART.rutaGestionRecordsChart(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
    ];
  }

  seteoRutas() {
    this.ruta = RUTAS_RECORDS_CHART
      .rutaGestionRecordsChart(
        false,
        true,
        [
          this.idArtista,
          this.idObjParam,
          this.tipoObjeto,
          this.idChart
        ])
      .ruta;
  }

  abrirModalCrearEditarRecordChart(
    recordChart?: RecordChartInterface,
    indice?: number
  ) {
    const dialogRef = this.dialog
      .open(
        ModalCrearEditarRecordChartComponent,
        {
          width: '800px',
          data: {
            recordChart,
            idArtista: this.idArtista,
            tipo: this.tipo,
            chart: this.idChart,
            idsCancionesAlbumesVideos: this.arregloidsCancionesAlbumesVideosSeleccionadas,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (recordChartCreadoEditado: RecordChartInterface) => {
          if (recordChartCreadoEditado) {
            const consulta = {
              where: {
                id: recordChartCreadoEditado.id
              },
              relations: [
                'chart',
                'medida',
                'record',
                'cancion',
                'cancion.imagenesCancion',
                'album',
                'album.imagenesAlbum',
                'video',
                'video.cancion'
              ]
            };
            this._recordChartRestService
              .findAll(
                JSON.stringify(consulta)
              )
              .subscribe(
                (respuestaCharts: [RecordChartInterface[], number]) => {
                  if (indice || indice === 0) {
                    this.recordsChart[indice] = respuestaCharts[0][0];
                  } else {
                    this.recordsChart.unshift(respuestaCharts[0][0]);
                  }
                  this.recordsChart = [...this.recordsChart];
                  this.arregloidsCancionesAlbumesVideosSeleccionadas = this.recordsChart
                    .map(chartCancionAlbumVideo => {
                      return chartCancionAlbumVideo[this.tipo].id;
                    });
                  this._cargandoService.deshabiltarCargando();
                }
                , error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                  this._cargandoService.deshabiltarCargando();
                }
              );
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  obtenerUrlPrincipal(
    objeto: any) {
    let campo;
    if (this.tipo === 'cancion') {
      campo = 'imagenesCancion';
    }
    if (this.tipo === 'album') {
      campo = 'imagenesAlbum';
    }
    if (objeto[campo] && objeto[campo].length) {
      return obtenerUrlImagenPrincipal(objeto, campo);
    }
  }
}
