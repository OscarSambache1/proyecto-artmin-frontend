import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionRecordsChartComponent } from './ruta-gestion-records-chart.component';

describe('RutaGestionRecordsChartComponent', () => {
  let component: RutaGestionRecordsChartComponent;
  let fixture: ComponentFixture<RutaGestionRecordsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaGestionRecordsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionRecordsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
