import {generarRespuestaRuta} from '../../../../funciones/generar-rutas';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';

export const RUTAS_ALBUM = {
  _rutaInicioAlbum: {
    ruta: 'album-modulo',
    nombre: 'Modulo Album',
    generarRuta: (...argumentos) => {
      return `album-modulo`;
    }
  },

  _rutaGestionAlbumes: {
    ruta: 'gestion-albumes',
    nombre: 'Gestión de albumes',
    generarRuta: (...argumentos) => {
      return `gestion-albumes`;
    }
  },

  rutaGestionAlbumes: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes
      ];
    }


    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaEditarAlbum: {
    ruta: 'editar-album/:idAlbum',
    nombre: 'Editar album',
    generarRuta: (...argumentos) => {
      return `editar-album/${argumentos[1]}`;
    }
  },

  rutaEditarAlbum: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaEditarAlbum
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaEditarAlbum
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaCrearAlbum: {
    ruta: 'crear-album',
    nombre: 'Registrar album',
    generarRuta: (...argumentos) => {
      return `crear-album`;
    }
  },

  rutaCrearTour: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaCrearAlbum
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaCrearAlbum
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },

  _rutaNominaciones: {
    ruta: 'nominaciones/:tipo',
    nombre: 'Nominaciones',
    generarRuta: (...argumentos) => {
      return `nominaciones/${argumentos[2]}`;
    }
  },

  rutaNominaciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaEditarAlbum,
        this._rutaNominaciones,
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaEditarAlbum,
        this._rutaNominaciones,
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },


  _rutaPresentaciones: {
    ruta: 'presentaciones/:tipo',
    nombre: 'Presentaciones',
    generarRuta: (...argumentos) => {
      return `presentaciones/${argumentos[2]}`;
    }
  },

  rutaPresentaciones: function (
    arreglo = false,
    migasDePan = false,
    argumentos?: any[],
  ): any {
    let rutaArreglo = [];
    if (argumentos && argumentos[0]) {
      rutaArreglo = [
        RUTAS_ARTISTA._rutaInicioArtista,
        RUTAS_ARTISTA._rutaGestionArtista,
        RUTAS_ARTISTA._rutaVerEditarArtistas,
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaEditarAlbum,
        this._rutaPresentaciones,
      ];
    } else {
      rutaArreglo = [
        this._rutaInicioAlbum,
        this._rutaGestionAlbumes,
        this._rutaEditarAlbum,
        this._rutaPresentaciones,
      ];
    }
    return generarRespuestaRuta(
      arreglo,
      migasDePan,
      rutaArreglo,
      argumentos
    );
  },
};
