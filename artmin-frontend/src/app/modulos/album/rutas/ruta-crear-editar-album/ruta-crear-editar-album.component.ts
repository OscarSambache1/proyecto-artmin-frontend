import {Component, OnInit, ViewChild} from '@angular/core';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {FormularioAlbumComponent} from '../../componentes/formulario-album/formulario-album.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatStepper} from '@angular/material/stepper';
import {obtenerAnio} from '../../../../funciones/obtener-anio';
import {
  ToastErrorCargandoDatos,
  toastErrorCrear, toastErrorEditar,
  toastExitoCrear,
  toastExitoEditar
} from '../../../../constantes/mensajes-toaster';
import {mergeMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {CancionInterface} from '../../../../interfaces/cancion.interface';
import {AlbumCancionInterface} from '../../../../interfaces/album-cancion.interface';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_ALBUM} from '../definicion-rutas/rutas-album';
import {EnlaceAlbumCancionArtistaVideoRestService} from '../../../../servicios/rest/enlace-album-cancion-artista-video-rest.service';
import {OPCIONES_ALBUM, OPCIONES_CANCION} from '../../../../constantes/opciones-gestion-cancion';
import {RUTAS_CANCION} from '../../../cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_CHART} from '../../../chart/rutas/definicion-rutas/rutas-charts';

@Component({
  selector: 'app-ruta-crear-editar-album',
  templateUrl: './ruta-crear-editar-album.component.html',
  styleUrls: ['./ruta-crear-editar-album.component.css']
})
export class RutaCrearEditarAlbumComponent implements OnInit {

  idArtista: number;
  idsArtista: number[] = [];
  idAlbum: number;
  albumCrearEditar: AlbumInterface;
  formularioValido: boolean;
  @ViewChild(FormularioAlbumComponent)
  componenteFormularioAlbum: FormularioAlbumComponent;

  inputEnlace: string;
  album: AlbumInterface;
  ruta = [];
  cancionesAlbum: AlbumCancionInterface[] = [];
  arregloRutas: any[];
  mostrarInputEnlace: boolean;
  opcionesAlbum = OPCIONES_ALBUM;

  constructor(
    private readonly _albumRestService: AlbumRestService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService,
    private readonly _enlaceAlbumCancionArtistaVideoRestService: EnlaceAlbumCancionArtistaVideoRestService
  ) {
  }

  ngOnInit(): void {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idAlbum = +params.idAlbum;
          this.idArtista = +params.idArtista;
          this.setearArregloRutasMigaPan();
          this.ruta = this.setearRuta();
          if (this.idAlbum) {
            const consulta = {
              where: {
                id: this.idAlbum
              },
              relations: [
                'imagenesAlbum',
                'generosAlbum',
                'generosAlbum.genero',
                'artistasAlbum',
                'artistasAlbum.artista',
                'cancionesAlbum',
                'cancionesAlbum.cancion',
                'cancionesAlbum.cancion.tipoCancion',
                'cancionesAlbum.cancion.imagenesCancion',
                'cancionesAlbum.cancion.artistasCancion',
                'cancionesAlbum.cancion.artistasCancion.artista',
                'cancionesAlbum.cancion.generosCancion',
                'cancionesAlbum.cancion.generosCancion.genero',
                'cancionesAlbum.cancion.enlacesCancion',
                'tipoAlbum',
                'enlacesAlbum'
              ]
            };
            this._cargandoService.habilitarCargando();
            return this._albumRestService.findAll(
              JSON.stringify(consulta)
            );
          } else {
            this.idsArtista.push(this.idArtista);
            return of([], 0);
          }
        })
      )
      .subscribe(
        (respuestaAlbum: [AlbumInterface[], number]) => {
          if (respuestaAlbum && respuestaAlbum[0] && respuestaAlbum[0][0]) {
            this.album = respuestaAlbum[0][0];
            this.idsArtista = this.album.artistasAlbum.map(artistaAlbum => artistaAlbum.artista.id);
            this.cancionesAlbum = this.album.cancionesAlbum;
            if (this.album.enlacesAlbum.length) {
              this.inputEnlace = this.album.enlacesAlbum[0].url;
            }
          }
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  validarFormulario(album: AlbumInterface | boolean) {
    if (album) {
      this.formularioValido = true;
      this.albumCrearEditar = album as AlbumInterface;
    } else {
      this.formularioValido = false;
    }
  }

  cancelarCreacionEdicion() {
    this.componenteFormularioAlbum.volverFormularioInicial();
  }

  crearEditarArtista(stepper?) {
    this._cargandoService.habilitarCargando();
    this.albumCrearEditar.anio = obtenerAnio(this.albumCrearEditar.fechaLanzamiento);
    const generos = [...this.albumCrearEditar.generos].map(genero => genero.id);
    const artistas = [...this.albumCrearEditar.artistas].map(genero => genero.id);
    if (this.album) {
      const imagenPrincipal = this.album.imagenesAlbum.find(imagenAlbum => imagenAlbum.esPrincipal);
      this._albumRestService
        .editarAlbumArtistaGenerosImagen(
          this.albumCrearEditar,
          generos,
          artistas,
          this.albumCrearEditar.imagen,
          this.album.id,
          imagenPrincipal.id
        )
        .subscribe((respuestaAlbumEditado: AlbumInterface) => {
            this.album = respuestaAlbumEditado;
            this.idsArtista = this.album.artistasAlbum.map(artistaAlbum => artistaAlbum.artista.id);
            this.componenteFormularioAlbum.album = respuestaAlbumEditado;
            this.componenteFormularioAlbum.volverFormularioInicial();
            this._toasterService.pop(toastExitoEditar);
            this._cargandoService.deshabiltarCargando();
            RUTAS_ALBUM
              .rutaEditarAlbum(
                true,
                false,
                [this.idArtista, this.album.id]
              );
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    } else {
      this._albumRestService
        .crearAlbumImagen(
          this.albumCrearEditar,
          generos,
          artistas,
          this.albumCrearEditar.imagen,
        )
        .subscribe((respuestaAlbumCreado: AlbumInterface) => {
            this.album = respuestaAlbumCreado;
            this.cancionesAlbum = this.album.cancionesAlbum;
            this.idsArtista = this.album.artistasAlbum.map(artistaAlbum => artistaAlbum.artista.id);
            this.componenteFormularioAlbum.album = respuestaAlbumCreado;
            this.componenteFormularioAlbum.volverFormularioInicial();
            this.cambiarSiguienteStep(stepper);
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
          },
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
            this._cargandoService.deshabiltarCargando();
          });
    }
  }

  habilitarFormularioEditarAlbum() {
    this.componenteFormularioAlbum.formularioAlbum.enable();
  }

  cambiarSiguienteStep(steper: MatStepper) {
    setTimeout(() => {
      steper.next();
    }, 800);
  }

  setearArregloRutasMigaPan() {
    if (this.idArtista) {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
        RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
      ];
    } else {
      this.arregloRutas = [
        RUTAS_ALBUM.rutaGestionAlbumes(false, true),
      ];
    }
    if (this.idAlbum) {
      this.arregloRutas.push(
        RUTAS_ALBUM.rutaEditarAlbum(false, true)
      );
    } else {
      this.arregloRutas.push(
        RUTAS_ALBUM.rutaCrearTour(false, true)
      );
    }
  }

  setearRuta(): any[] {
    if (this.idAlbum) {
      return RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idAlbum, this.idArtista]).ruta;
    } else {
      return RUTAS_ALBUM.rutaCrearTour(false, true, [this.idArtista]).ruta;
    }
  }

  obtenerEnlace() {
    if (this.album && this.album.enlacesAlbum.length) {
      return this.album.enlacesAlbum[0].url;
    }
  }

  cambiarValorMostrarInputEnlace() {
    this.mostrarInputEnlace = !this.mostrarInputEnlace;
  }

  guardarEnlace() {
    this._cargandoService.habilitarCargando();
    if (this.album && this.album.enlacesAlbum && this.album.enlacesAlbum.length) {
      const idEnlace = this.album.enlacesAlbum[0].id;
      this._enlaceAlbumCancionArtistaVideoRestService
        .updateOne(
          idEnlace,
          {
            url: this.inputEnlace
          }
        )
        .subscribe(
          enlaceEditado => {
            this.album.enlacesAlbum[0] = enlaceEditado;
            this._toasterService.pop(toastExitoEditar);
            this._cargandoService.deshabiltarCargando();
            this.mostrarInputEnlace = false;
          }
          , error => {
            console.error(error);
            this._toasterService.pop(toastErrorEditar);
            this._cargandoService.deshabiltarCargando();
          }
        );
    } else {
      const enlaceACrear = {
        url: this.inputEnlace,
        album: this.album.id
      };
      this._enlaceAlbumCancionArtistaVideoRestService
        .create(
          enlaceACrear
        )
        .subscribe(
          enlaceCreado => {
            this.album.enlacesAlbum[0] = enlaceCreado;
            this._toasterService.pop(toastExitoCrear);
            this._cargandoService.deshabiltarCargando();
            this.mostrarInputEnlace = false;
          },
          error => {
            console.error(error);
            this._toasterService.pop(toastErrorCrear);
            this._cargandoService.deshabiltarCargando();
          }
        );
    }
  }

  irAGestion(modulo: string) {
    if (modulo === 'PREMIOS Y NOMINACIONES') {
      return this._router.navigate(
        RUTAS_ALBUM
          .rutaNominaciones(
            true,
            false,
            [this.idArtista, this.album.id, 'AL']
          )
      );
    }
    if (modulo === 'PRESENTACIONES') {
      return this._router.navigate(
        RUTAS_ALBUM
          .rutaPresentaciones(
            true,
            false,
            [this.idArtista, this.album.id, 'AL']
          )
      );
    }
    if (modulo === 'CHARTS') {
      return this._router.navigate(
        RUTAS_CHART
          .rutaGestionCharts(
            true,
            false,
            [
              this.idArtista || 0,
              this.idAlbum || 0,
              this.idAlbum, 'AL']
          )
      );
    }
  }
}
