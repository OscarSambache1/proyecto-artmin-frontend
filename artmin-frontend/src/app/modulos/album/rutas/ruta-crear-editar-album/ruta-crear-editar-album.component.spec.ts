import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaCrearEditarAlbumComponent } from './ruta-crear-editar-album.component';

describe('RutaCrearEditarAlbumComponent', () => {
  let component: RutaCrearEditarAlbumComponent;
  let fixture: ComponentFixture<RutaCrearEditarAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaCrearEditarAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaCrearEditarAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
