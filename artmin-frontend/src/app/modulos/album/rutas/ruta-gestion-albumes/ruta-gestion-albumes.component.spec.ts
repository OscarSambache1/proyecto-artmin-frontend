import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RutaGestionAlbumesComponent } from './ruta-gestion-albumes.component';

describe('RutaGestionAlbumesComponent', () => {
  let component: RutaGestionAlbumesComponent;
  let fixture: ComponentFixture<RutaGestionAlbumesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RutaGestionAlbumesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RutaGestionAlbumesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
