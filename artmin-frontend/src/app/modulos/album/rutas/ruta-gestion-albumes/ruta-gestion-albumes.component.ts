import {Component, OnInit} from '@angular/core';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {CargandoService} from '../../../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {AlbumRestService} from '../../../../servicios/rest/album-rest.service';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {environment} from '../../../../../environments/environment';
import {mergeMap} from 'rxjs/operators';
import {QueryParamsInterface} from '../../../../interfaces/query-params.interface';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {GeneroInterface} from '../../../../interfaces/genero.interface';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {TipoAlbumInterface} from '../../../../interfaces/tipo-album.interface';
import {RUTAS_ARTISTA} from '../../../artista/rutas/definicion-rutas/rutas-artista';
import {RUTAS_ALBUM} from '../definicion-rutas/rutas-album';

@Component({
  selector: 'app-ruta-gestion-albumes',
  templateUrl: './ruta-gestion-albumes.component.html',
  styleUrls: ['./ruta-gestion-albumes.component.css']
})
export class RutaGestionAlbumesComponent implements OnInit {

  albumes: AlbumInterface[] = [];
  busqueda = '';
  idArtista: number;
  queryParams: QueryParamsInterface = {};
  idGenero: number;
  ruta;
  artista: ArtistaInterface;
  rutaImagen = 'assets/imagenes/album.svg';
  idArtistaQuery: number;
  idArtistaParams: number;
  idTipoAlbum: number;
  arregloRutas: any[];

  constructor(
    private readonly _albumRestService: AlbumRestService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly  _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtistaParams = params.idArtista ? +params.idArtista : undefined;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          if (this.idArtistaParams) {
            this.idArtista = this.idArtistaParams;
            this._artistaRestService.findOne(this.idArtista)
              .subscribe(
                (artista: ArtistaInterface) => {
                  this.artista = artista;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (queryParams.consulta) {
            this.queryParams.consulta = JSON.parse(queryParams.consulta);
            if (this.queryParams.consulta.busqueda) {
              this.busqueda = this.queryParams.consulta.busqueda;
            }
            if (this.queryParams.consulta.idGenero) {
              this.idGenero = this.queryParams.consulta.idGenero;
            }
            if (this.queryParams.consulta.idTipoAlbum) {
              this.idTipoAlbum = this.queryParams.consulta.idTipoAlbum;
            }
            if (this.queryParams.consulta.idArtista) {
              this.idArtistaQuery = this.queryParams.consulta.idArtista;
              this.idArtista = this.idArtistaQuery;
            } else {
              this.queryParams.consulta.idArtista = this.idArtista;
            }
          } else {
            this.queryParams.consulta = {
              busqueda: '',
              idGenero: this.idGenero,
              esImagenPrincipal: 1,
              idArtista: this.idArtista,
            };
          }
          this._cargandoService.habilitarCargando();
          return this._albumRestService.obtenerAlbumesPorGenero(
            JSON.stringify(this.queryParams.consulta)
          );
        })
      )
      .subscribe(
        (respuestaAlbumes: [AlbumInterface[], number]) => {
          this.albumes = respuestaAlbumes[0];
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  obtenerUrlPrincipal(album: AlbumInterface): string {
    return obtenerUrlImagenPrincipal(album, 'imagenesAlbum');
  }


  escucharBusqueda(busqueda: string) {
    this.busqueda = busqueda;
    this.buscarAlbums();
  }

  buscarAlbumesPorGenero(generoSeleccionado: GeneroInterface) {
    this.idGenero = generoSeleccionado ? generoSeleccionado.id : undefined;
    this.buscarAlbums();
  }

  buscarAlbumesPorArtista(artistaSeleccionado: ArtistaInterface) {
    this.idArtista = artistaSeleccionado ? artistaSeleccionado.id : undefined;
    this.buscarAlbums();
  }

  irRutaCrearAlbum() {
    this._router
      .navigate(
        RUTAS_ALBUM
          .rutaCrearTour(
            true,
            false,
            [this.idArtistaParams]
          ),
      );
  }

  obtenerRuta(idAlbum) {
    return RUTAS_ALBUM
      .rutaEditarAlbum(
        true,
        false,
        [this.idArtistaParams, idAlbum]
      );
  }

  buscarAlbumPorTipo(tipoAlbumSeleccionado: TipoAlbumInterface) {
    this.idTipoAlbum = tipoAlbumSeleccionado ? tipoAlbumSeleccionado.id : undefined;
    this.buscarAlbums();
  }

  buscarAlbums() {
    const consulta = {
      busqueda: this.busqueda,
      idGenero: this.idGenero,
      esImagenPrincipal: 1,
      idArtista: this.idArtista,
      idTipoAlbum: this.idTipoAlbum
    };
    this.queryParams.consulta = JSON.stringify(consulta);
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: {...this.queryParams}
        }
      );
  }

  setearArregloRutasMigasPan() {
    if (this.idArtistaParams) {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtistaParams]),
        RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtistaParams])
      ];
    } else {
      this.arregloRutas = [
        RUTAS_ALBUM.rutaGestionAlbumes(false, true)
      ];
    }
  }

  seteoRutas() {
    this.ruta = RUTAS_ALBUM
      .rutaGestionAlbumes(
        false,
        true,
        [this.idArtistaParams]).ruta;
  }
}
