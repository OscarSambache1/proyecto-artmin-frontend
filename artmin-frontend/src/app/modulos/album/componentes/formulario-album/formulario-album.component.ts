import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {CONFIGURACIONES_CALENDARIO} from '../../../../constantes/configuracion-calendario';
import {GeneroInterface} from '../../../../interfaces/genero.interface';
import {FormBuilder, FormGroup} from '@angular/forms';
import {GeneroRestService} from '../../../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../../../constantes/mensajes-toaster';
import {debounceTime} from 'rxjs/operators';
import {generarMensajesError} from '../../../../funciones/generar-mensajes-error';
import {InputImagenComponent} from '../../../../componentes/input-imagen/input-imagen.component';
import {AlbumInterface} from '../../../../interfaces/album.interface';
import {
  MENSAJES_VALIDACION_ARTISTAS_ALBUM,
  MENSAJES_VALIDACION_CALIFICACION_ALBUM,
  MENSAJES_VALIDACION_DESCRIPCION_ALBUM, MENSAJES_VALIDACION_FECHA_LANZAMIENTO_ALBUM, MENSAJES_VALIDACION_GENEROS_ALBUM,
  MENSAJES_VALIDACION_NOMBRE_ALBUM, MENSAJES_VALIDACION_TIPO_ALBUM, VALIDACION_ARTISTAS_ALBUM,
  VALIDACION_CALIFICACION_ALBUM,
  VALIDACION_DESCRIPCION_ALBUM, VALIDACION_FECHA_LANZAMIENTO_ALBUM, VALIDACION_GENEROS_ALBUM, VALIDACION_IMAGEN_ALBUM,
  VALIDACION_NOMBRE_ALBUM, VALIDACION_TIPO_ALBUM
} from '../../../../constantes/validaciones-formulario/album';
import {ArtistaInterface} from '../../../../interfaces/artista.interface';
import {ArtistaRestService} from '../../../../servicios/rest/artista-rest.service';
import {SelectItem} from 'primeng';
import {TIPOS_ALBUMES} from '../../../../constantes/tipos-album';
import {obtenerUrlImagenPrincipal} from '../../../../funciones/obtener-imagen-principal';
import {AutocompleteArtistaComponent} from '../../../../componentes/autocomplete-artista/autocomplete-artista.component';
import {TipoAlbumInterface} from '../../../../interfaces/tipo-album.interface';

@Component({
  selector: 'app-formulario-album',
  templateUrl: './formulario-album.component.html',
  styleUrls: ['./formulario-album.component.css']
})
export class FormularioAlbumComponent implements OnInit {

  configuracionCalendario = CONFIGURACIONES_CALENDARIO;
  arregloGeneros: GeneroInterface[] = [];
  formularioAlbum: FormGroup;
  mensajesError = {
    nombre: [],
    descripcion: [],
    calificacion: [],
    fechaLanzamiento: [],
    generos: [],
    imagen: [],
    artistas: [],
    tipoAlbum: []
  };
  subscribers = [];
  imagenSeleccionada;

  @Input()
  album: AlbumInterface;

  tipos: SelectItem[] = TIPOS_ALBUMES;

  @Input()
  width: number;

  @Input()
  idArtista: number;

  @Input()
  idsArtista: number[];

  @Output()
  artistaValidoEnviar: EventEmitter<AlbumInterface | boolean> = new EventEmitter();

  @ViewChild(InputImagenComponent)
  componenteInputImagen: InputImagenComponent;

  idTipoAlbum: number;
  constructor(
    private readonly _generoRestService: GeneroRestService,
    private readonly _formBuilder: FormBuilder,
    private readonly _toasterService: ToasterService,
    private readonly _artistaService: ArtistaRestService
  ) {
  }

  ngOnInit(): void {
    this.cargarGeneros();
    this.iniciarFormulario();
  }

  iniciarFormulario() {
    this.crearFormulario();
    if (this.album) {
      this.idTipoAlbum = this.album.tipoAlbum.id;
      this.deshabilitarFormulario();
    } else {
      if (this.idsArtista && this.idsArtista.length) {
        this.cargarMultiselectArtista();
      }
    }
    this.verificarCamposFormulario();
    this.verificarFormulario();
  }

  crearFormulario() {
    this.formularioAlbum = this._formBuilder.group({
      nombre: [this.album ? this.album.nombre : '', VALIDACION_NOMBRE_ALBUM],
      descripcion: [this.album ? this.album.descripcion : '', VALIDACION_DESCRIPCION_ALBUM],
      fechaLanzamiento: [this.album ? this.album.fechaLanzamiento : '', VALIDACION_FECHA_LANZAMIENTO_ALBUM],
      calificacion: [this.album ? this.album.calificacion : null, VALIDACION_CALIFICACION_ALBUM],
      generos: [this.album ? this.cargarGenerosAlbum() : [], VALIDACION_GENEROS_ALBUM],
      artistas: [this.album ? this.cargarArtistasAlbum() : [], VALIDACION_ARTISTAS_ALBUM],
      imagen: [this.album ? this.cargarImagenAlbum() : '', VALIDACION_IMAGEN_ALBUM],
      tipoAlbum: [this.album ? this.album.tipoAlbum : null, VALIDACION_TIPO_ALBUM]
    });
  }


  verificarCamposFormulario() {
    this.verificarCampoFormControl('nombre', MENSAJES_VALIDACION_NOMBRE_ALBUM);
    this.verificarCampoFormControl('descripcion', MENSAJES_VALIDACION_DESCRIPCION_ALBUM);
    this.verificarCampoFormControl('fechaLanzamiento', MENSAJES_VALIDACION_FECHA_LANZAMIENTO_ALBUM);
    this.verificarCampoFormControl('calificacion', MENSAJES_VALIDACION_CALIFICACION_ALBUM);
    this.verificarCampoFormControl('generos', MENSAJES_VALIDACION_GENEROS_ALBUM);
    this.verificarCampoFormControl('artistas', MENSAJES_VALIDACION_ARTISTAS_ALBUM);
    this.verificarCampoFormControl('tipoAlbum', MENSAJES_VALIDACION_TIPO_ALBUM);
  }

  verificarCampoFormControl(campo, mensajeValidacion) {
    const campoFormControl = this.formularioAlbum.get(campo);
    const subscriber = campoFormControl
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(
    valor => {
      this.mensajesError[campo] = generarMensajesError(campoFormControl, this.mensajesError[campo], mensajeValidacion);
        }
      );
    this.subscribers.push(subscriber);
  }

  verificarFormulario() {
    const formularioFormGroup = this.formularioAlbum;
    const subscriber = formularioFormGroup
      .valueChanges
      .subscribe(
        formulario => {
          const artistaValido = formularioFormGroup.valid;
          if (artistaValido) {
            this.artistaValidoEnviar.emit(formulario);
          } else {
            this.artistaValidoEnviar.emit(false);
          }
        }
      );
    this.subscribers.push(subscriber);
  }

  escucharEventoImagen(event: any) {
    this.imagenSeleccionada = event;
    this.formularioAlbum.patchValue(
      {
        imagen: this.imagenSeleccionada,
      }
    );
  }

  cargarGeneros() {
    this._generoRestService
      .findAll(JSON.stringify({}))
      .subscribe((respuestaGeneros: [GeneroInterface[], number]) => {
        this.arregloGeneros = respuestaGeneros[0];
      }, error => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
      });
  }

  cargarGenerosAlbum() {
    return this.album.generosAlbum.map(generoAlbum => generoAlbum.genero as GeneroInterface);
  }

  cargarImagenAlbum() {
    if (this.album) {
      return obtenerUrlImagenPrincipal(this.album, 'imagenesAlbum');
    }
  }

  deshabilitarFormulario() {
    this.formularioAlbum.disable();
  }

  buscarAlbumesPorArtista(respuestaArtista: ArtistaInterface[]) {
    this.formularioAlbum.patchValue(
      {
        artistas: [
          ...respuestaArtista]
      }
    );
  }

  cargarMultiselectArtista() {
    const consulta = {
      camposIn: [
        {
          nombreCampo: 'id',
          valor: this.idsArtista
        }
      ],
      relations: [
        'imagenesArtista'
      ]
    };
    this._artistaService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe( (respuestaArtista: [ArtistaInterface[], number]) => {
        this.formularioAlbum.patchValue(
          {
            artistas: [...respuestaArtista[0]]
          }
        );
      }, error => {
        console.error(error);
        this._toasterService.pop(ToastErrorCargandoDatos);
      });
  }

  cargarArtistasAlbum() {
    return this.album.artistasAlbum.map(artistaAlbum => artistaAlbum.artista as ArtistaInterface);
  }

    volverFormularioInicial() {
      this.formularioAlbum.patchValue(
        {
          nombre: this.album.nombre,
          descripcion: this.album.descripcion,
          fechaLanzamiento: this.album.fechaLanzamiento,
          calificacion: this.album.calificacion,
          tipoAlbum: this.album.tipoAlbum,
          artistas: this.cargarArtistasAlbum(),
          generos: this.cargarGenerosAlbum(),
          imagen: this.cargarImagenAlbum(),
        }
      );
      this.idTipoAlbum = this.album.tipoAlbum.id;
      this.componenteInputImagen.pathImagen = this.cargarImagenAlbum();
      this.deshabilitarFormulario();
  }

  escucharTipoAlbumSeleccionado(
    tipoAlbumSeleccionado: TipoAlbumInterface
  ) {
    this.formularioAlbum.patchValue(
      {
        tipoAlbum: tipoAlbumSeleccionado,
      }
    );
  }
}
