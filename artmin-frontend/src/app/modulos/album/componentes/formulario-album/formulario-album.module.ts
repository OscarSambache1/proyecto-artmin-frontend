import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormularioAlbumComponent} from './formulario-album.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  AutoCompleteModule,
  CalendarModule, DropdownModule,
  InputNumberModule,
  InputTextareaModule,
  InputTextModule,
  MultiSelectModule
} from 'primeng';
import {InputImagenModule} from '../../../../componentes/input-imagen/input-imagen.module';
import {AlertaValidacionCampoFormularioModule} from '../../../../componentes/alerta-validacion-campo-formulario/alerta-validacion-campo-formulario.module';
import {AutocompleteArtistaModule} from '../../../../componentes/autocomplete-artista/autocomplete-artista.module';
import {AutocompleteTipoAlbumModule} from '../../../../componentes/autocomplete-tipo-album/autocomplete-tipo-album.module';



@NgModule({
  declarations: [
    FormularioAlbumComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    FormsModule,
    CalendarModule,
    InputNumberModule,
    MultiSelectModule,
    InputImagenModule,
    AlertaValidacionCampoFormularioModule,
    AutocompleteArtistaModule,
    DropdownModule,
    AutocompleteTipoAlbumModule
  ],
  exports: [
    FormularioAlbumComponent
  ]
})
export class FormularioAlbumModule { }
