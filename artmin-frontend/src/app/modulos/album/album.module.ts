import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AlbumRoutingModule} from './album-routing.module';
import {RutaGestionAlbumesComponent} from './rutas/ruta-gestion-albumes/ruta-gestion-albumes.component';
import {MenuItemCuadradoModule} from '../../componentes/menu-item-cuadrado/menu-item-cuadrado.module';
import {InputBuscarBotonModule} from '../../componentes/input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../../componentes/boton-nuevo/boton-nuevo.module';
import {AutcompleteGenerosModule} from '../../componentes/autcomplete-generos/autcomplete-generos.module';
import {AutocompleteArtistaModule} from '../../componentes/autocomplete-artista/autocomplete-artista.module';
import {RutaCrearEditarAlbumComponent} from './rutas/ruta-crear-editar-album/ruta-crear-editar-album.component';
import {FormularioAlbumModule} from './componentes/formulario-album/formulario-album.module';
import {MatStepperModule} from '@angular/material/stepper';
import {MatIconModule} from '@angular/material/icon';
import {DragDropCancionesModule} from '../../componentes/drag-drop-canciones/drag-drop-canciones.module';
import {SegundosAMinutosHorasPipeModule} from '../../pipes/segundos-a-minutos-horas-pipe/segundos-a-minutos-horas-pipe.module';
import {AutocompleteTipoAlbumModule} from '../../componentes/autocomplete-tipo-album/autocomplete-tipo-album.module';
import {MigasPanModule} from '../../componentes/migas-pan/migas-pan.module';
import {SafePipeModule} from '../../pipes/safe-pipe/safe-pipe.module';
import {InputTextModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {CarouselOpcionesModule} from '../../componentes/carousel-opciones/carousel-opciones.module';
import {RutaNominacionesModule} from '../../componentes/ruta-nominaciones/ruta-nominaciones.module';
import {RutaPresentacionesModule} from '../../componentes/ruta-presentaciones/ruta-presentaciones.module';


@NgModule({
  declarations: [
    RutaGestionAlbumesComponent,
    RutaCrearEditarAlbumComponent,
  ],
  imports: [
    CommonModule,
    AlbumRoutingModule,
    MenuItemCuadradoModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    AutcompleteGenerosModule,
    AutocompleteArtistaModule,
    FormularioAlbumModule,
    MatStepperModule,
    MatIconModule,
    DragDropCancionesModule,
    SegundosAMinutosHorasPipeModule,
    AutocompleteTipoAlbumModule,
    MigasPanModule,
    SafePipeModule,
    InputTextModule,
    FormsModule,
    CarouselOpcionesModule,
    RutaNominacionesModule,
    RutaPresentacionesModule,
  ]
})
export class AlbumModule {
}
