import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaGestionAlbumesComponent} from './rutas/ruta-gestion-albumes/ruta-gestion-albumes.component';
import {RutaCrearEditarAlbumComponent} from './rutas/ruta-crear-editar-album/ruta-crear-editar-album.component';
import {RutaNominacionesComponent} from '../../componentes/ruta-nominaciones/ruta-nominaciones.component';
import {RutaPresentacionesComponent} from '../../componentes/ruta-presentaciones/ruta-presentaciones.component';


const routes: Routes = [
  {
    path: 'gestion-albumes',
    children: [
      {
        path: '',
        component: RutaGestionAlbumesComponent
      },
      {
        path: 'crear-album',
        component: RutaCrearEditarAlbumComponent
      },
      {
        path: 'editar-album/:idAlbum',
        children: [
          {
            path: '',
            component: RutaCrearEditarAlbumComponent,
          },
          {
            path: 'nominaciones/:tipo',
            component: RutaNominacionesComponent,
          },
          {
            path: 'presentaciones/:tipo',
            component: RutaPresentacionesComponent,
          },
          {
            path: 'chart-modulo/:tipo',
            loadChildren: 'src/app/modulos/chart/chart.module#ChartModule',
          },
        ]
      },

    ]
  },

  {
    path: '',
    redirectTo: 'gestion-albumes',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule { }
