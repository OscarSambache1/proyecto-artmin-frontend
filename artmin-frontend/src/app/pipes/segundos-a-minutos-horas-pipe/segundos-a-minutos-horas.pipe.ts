import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';
import * as momentDurationFormatSetup from 'moment-duration-format';
momentDurationFormatSetup(moment);
@Pipe({name: 'minutosHoras'})
export class SegundosAMinutosHorasPipe implements PipeTransform {
  transform(duracionSegundos: number): string {
    const duration = moment.duration(duracionSegundos, 'seconds') as any;
    const duracionFormato = duration.format('mm:ss');
    if (duracionFormato.length === 2) {
      return  '00:' + duracionFormato;
    } else {
      return  duracionFormato;
    }
  }
}
