import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SegundosAMinutosHorasPipe} from './segundos-a-minutos-horas.pipe';


@NgModule({
  declarations: [
    SegundosAMinutosHorasPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SegundosAMinutosHorasPipe
  ]
})
export class SegundosAMinutosHorasPipeModule {
}
