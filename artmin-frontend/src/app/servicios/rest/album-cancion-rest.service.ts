import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {AlbumCancionInterface} from '../../interfaces/album-cancion.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlbumCancionRestService extends PrincipalRestService<AlbumCancionInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'album-cancion';
  }

  guardarCancionesAlbumCancion(
    idAlbum: number,
    idsCanciones: Array<{ posicion: number, idCancion: number }>
  ): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/guardar-canciones-albums`,
      {
        idAlbum,
        idsCanciones
      }
    );
  }
}
