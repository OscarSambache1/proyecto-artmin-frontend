import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {RecordChartInterface} from '../../interfaces/record-chart.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecordChartRestService extends PrincipalRestService<RecordChartInterface> {
  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'record-chart';
  }

  obtenerRecords(datosConsultaRecords: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/obtener-records?datosConsulta=${
        datosConsultaRecords
      }`,
    );
  }
}
