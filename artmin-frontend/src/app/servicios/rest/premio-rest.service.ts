import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {PremioInterface} from '../../interfaces/premio.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PremioRestService extends PrincipalRestService<PremioInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'premio';
  }

  crearEditarPremio(datos: any): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-editar-premio`,
      datos,
    );
  }
}
