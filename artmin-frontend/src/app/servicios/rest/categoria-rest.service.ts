import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {CategoriaInterface} from '../../interfaces/categoria.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoriaRestService extends PrincipalRestService<CategoriaInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'categoria';
  }
}
