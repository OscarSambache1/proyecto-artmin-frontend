import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../interfaces/enlace-album-cancion-artista-video.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnlaceAlbumCancionArtistaVideoRestService extends PrincipalRestService<EnlaceAlbumCancionArtistaVideoInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'enlace-album-cancion-artista-video';
  }
}
