import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {FestivalInterface} from '../../interfaces/festival.interface';
import {Observable} from 'rxjs';
import {FestivalLugarInterface} from '../../interfaces/festival-lugar.interface';

@Injectable({
  providedIn: 'root'
})
export class FestivalLugarRestService extends PrincipalRestService<FestivalLugarInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'festival-lugar';
  }
}
