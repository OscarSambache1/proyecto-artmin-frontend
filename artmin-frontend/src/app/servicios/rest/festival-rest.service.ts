import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {FestivalInterface} from '../../interfaces/festival.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FestivalRestService extends PrincipalRestService<FestivalInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'festival';
  }

  crearEditarFestival(datos: any): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-editar-festival`,
      datos,
    );
  }
}
