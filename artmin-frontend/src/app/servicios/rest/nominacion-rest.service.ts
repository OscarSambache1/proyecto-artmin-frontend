import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {CategoriaInterface} from '../../interfaces/categoria.interface';
import {NominacionInterface} from '../../interfaces/nominacion.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NominacionRestService extends PrincipalRestService<NominacionInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'nominacion';
  }

  crearEditarNominacion(
    datos: any,
  ): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-editar-nominacion`,
      datos,
    );
  }

  seleccionarGanadores(
    datos: any,
  ): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/seleccionar-ganadores`,
      datos,
    );
  }
}
