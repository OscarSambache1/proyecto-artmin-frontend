import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {TipoVideoInterface} from '../../interfaces/tipo-video.interface';

@Injectable({
  providedIn: 'root'
})
export class TipoVideoRestService extends PrincipalRestService<TipoVideoInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'tipo-video';
  }
}
