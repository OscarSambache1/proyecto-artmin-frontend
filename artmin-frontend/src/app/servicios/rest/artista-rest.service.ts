import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {GeneroInterface} from '../../interfaces/genero.interface';
import {Observable} from 'rxjs';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../interfaces/enlace-album-cancion-artista-video.interface';

@Injectable({
  providedIn: 'root'
})
export class ArtistaRestService extends PrincipalRestService<ArtistaInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'artista';
  }

  crearArtistaImagen(
    artista: ArtistaInterface,
    generos: number[],
    enlaces: EnlaceAlbumCancionArtistaVideoInterface[],
    imagen: File,
  ): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(`imagen`, imagen);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-artista-imagen-generos?datosArtistaGeneros=${JSON.stringify(
        {
          artista,
          generos,
          enlaces
        }
      )}`,
      formData,
      options,
    );
  }

  editarArtistaImagen(
    artista: ArtistaInterface,
    generos: number[],
    imagen: File,
    idArtista: number,
    idImagen: number,
    enlaces: EnlaceAlbumCancionArtistaVideoInterface[]
  ): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(`imagen`, imagen);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/editar-artista-imagen-generos?datosArtistaGeneros=${JSON.stringify(
        {
          artista,
          generos,
          idArtista,
          idImagen,
          enlaces
        }
      )}`,
      formData,
      options,
    );
  }

  obtenerArtistasPorGenero(datosConsultaArtistasPorGenero: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/artistas-por-genero?datosConsulta=${
        datosConsultaArtistasPorGenero
      }`,
    );
  }
}
