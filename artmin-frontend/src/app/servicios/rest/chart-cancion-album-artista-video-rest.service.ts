import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {ChartCancionAlbumArtistaVideoInterface} from '../../interfaces/chart-cancion-album-artista-video.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChartCancionAlbumArtistaVideoRestService extends PrincipalRestService<ChartCancionAlbumArtistaVideoInterface>{

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'chart-album-cancion-artista-video';
  }

  obtenerCharts(datosConsultaCharts: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/obtener-charts-canciones-albums-artistas-videos?datosConsulta=${
        datosConsultaCharts
      }`,
    );
  }
}
