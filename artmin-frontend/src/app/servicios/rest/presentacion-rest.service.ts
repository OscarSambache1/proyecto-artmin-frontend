import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {PresentacionInterface} from '../../interfaces/presentacion.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PresentacionRestService extends PrincipalRestService<PresentacionInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'presentacion';
  }

  crearEditarPresentaacion(
    datos: any,
  ): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-editar-presentacion`,
      datos,
    );
  }

  obtenerPresentaciones(
    datos: string,
  ): Observable<any> {
    return this._http.get(
      `${this.url}:${this.port}/${this.segmento}/obtener-presentaciones?datos=${datos}`,
    );
  }
}
