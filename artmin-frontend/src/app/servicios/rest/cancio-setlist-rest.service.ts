import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {SetlistInterface} from '../../interfaces/setlist.interface';
import {ActoTourInterface} from "../../interfaces/acto-tour.interface";
import {CancionSetlistInterface} from "../../interfaces/cancion-setlist.interface";

@Injectable({
  providedIn: 'root'
})
export class CancioSetlistRestService extends PrincipalRestService<CancionSetlistInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'cancion-setlist';
  }
}
