import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {ChartPosicionInterface} from '../../interfaces/chart-posicion.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChartPosicionRestService extends PrincipalRestService<ChartPosicionInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'chart-posicion';
  }

  obtenerChartsPosiciones(datosConsultaCharts: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/obtener-charts-posiciones?datosConsulta=${
        datosConsultaCharts
      }`,
    );
  }
}
