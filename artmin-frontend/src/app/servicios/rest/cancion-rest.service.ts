import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {AlbumInterface} from '../../interfaces/album.interface';
import {Observable} from 'rxjs';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../interfaces/enlace-album-cancion-artista-video.interface';

@Injectable({
  providedIn: 'root'
})
export class CancionRestService extends PrincipalRestService<CancionInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'cancion';
  }

  crearCancion(
    cancion: CancionInterface,
    generos: number[],
    artistas: Array<{id: number, esPrincipal: 0 | 1}>,
    imagen: File,
    pathAlterno: string,
    enlaces: EnlaceAlbumCancionArtistaVideoInterface[],
    albums?: number[]
  ): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(`imagen`, imagen);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-cancion-artista-album-imagen-generos?datosCrearCancion=${JSON.stringify(
        {
          cancion,
          generos,
          artistas,
          albums,
          pathAlterno,
          enlaces
        }
      )}`,
      formData,
      options,
    );
  }

  editarCancion(
    cancion: CancionInterface,
    idCancion: number,
    generos: number[],
    artistas: Array<{id: number, esPrincipal: 0 | 1}>,
    imagen: File,
    pathAlterno: string,
    albums: number[],
    enlaces: EnlaceAlbumCancionArtistaVideoInterface[],
    idImagen: number,
  ): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(`imagen`, imagen);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/editar-cancion-artista-album-imagen-generos?datosEditarCancion=${JSON.stringify(
        {
          cancion,
          generos,
          idCancion,
          artistas,
          albums,
          pathAlterno,
          idImagen,
          enlaces
        }
      )}`,
      formData,
      options,
    );
  }

  obtenerCancionesPorGeneroArtista(datosConsultaCanciones: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/canciones-por-genero-artista?datosConsulta=${
        datosConsultaCanciones
      }`,
    );
  }
}
