import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {AlbumInterface} from '../../interfaces/album.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {ArtistaInterface} from '../../interfaces/artista.interface';

@Injectable({
  providedIn: 'root'
})
export class AlbumRestService extends PrincipalRestService<AlbumInterface> {

  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'album';
  }

  crearAlbumImagen(
    album: AlbumInterface,
    generos: number[],
    artistas: number[],
    imagen: File,
  ): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(`imagen`, imagen);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-album-imagen-generos?datosAlbumGeneros=${JSON.stringify(
        {
          album,
          generos,
          artistas
        }
      )}`,
      formData,
      options,
    );
  }

  editarAlbumArtistaGenerosImagen(
    album: AlbumInterface,
    generos: number[],
    artistas: number[],
    imagen: File,
    idAlbum: number,
    idImagen: number,
  ): Observable<any> {
    const formData: FormData = new FormData();
    formData.append(`imagen`, imagen);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/editar-album-artista-imagen-generos?datosAlbumGenerosImagenArtistas=${JSON.stringify(
        {
          album,
          generos,
          artistas,
          idAlbum,
          idImagen
        }
      )}`,
      formData,
      options,
    );
  }

  obtenerAlbumesPorGenero(datosConsultaAlbumesPorGenero: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/albumes-por-genero?datosConsulta=${
        datosConsultaAlbumesPorGenero
      }`,
    );
  }
}
