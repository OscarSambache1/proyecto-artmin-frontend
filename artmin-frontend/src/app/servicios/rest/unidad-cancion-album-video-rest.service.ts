import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {UnidadesCancionAlbumVideoInterface} from '../../interfaces/unidades-cancion-album-video.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UnidadCancionAlbumVideoRestService extends PrincipalRestService<UnidadesCancionAlbumVideoInterface> {
  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'unidad-album-cancion-video';
  }

  obtenerUnidades(datosConsultaUnidades: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/obtener-unidades-canciones-albums-videos?datosConsulta=${
        datosConsultaUnidades
      }`,
    );
  }
}
