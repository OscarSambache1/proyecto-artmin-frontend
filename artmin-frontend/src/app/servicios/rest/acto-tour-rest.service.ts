import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {ActoTourInterface} from '../../interfaces/acto-tour.interface';
import {AlbumInterface} from '../../interfaces/album.interface';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActoTourRestService extends PrincipalRestService<ActoTourInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'acto-tour';
  }

  crearActoTpur(
    datos: any,
  ): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-actos-tour`,
      datos,
    );
  }
}
