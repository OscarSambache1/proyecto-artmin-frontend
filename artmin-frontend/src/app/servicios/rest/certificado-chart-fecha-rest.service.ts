import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {CertificadoChartFechaInterface} from '../../interfaces/certificado-chart-fecha.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CertificadoChartFechaRestService extends PrincipalRestService<CertificadoChartFechaInterface> {

  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'certificado-chart-fecha';
  }
}
