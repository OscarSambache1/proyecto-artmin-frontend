import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {PlataformaInterface} from '../../interfaces/plataforma.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlataformaRestService extends PrincipalRestService<PlataformaInterface> {
  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'plataforma';
  }
}
