import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {PrincipalRestService} from './rest-principal.service';
import {GeneroInterface} from '../../interfaces/genero.interface';

@Injectable({
  providedIn: 'root'
})
export class GeneroRestService extends PrincipalRestService<GeneroInterface>{
  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'genero';
  }
}
