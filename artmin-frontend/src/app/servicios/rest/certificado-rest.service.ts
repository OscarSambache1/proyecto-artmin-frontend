import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {CertificadoInterface} from '../../interfaces/certificado.interface';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CertificadoRestService extends PrincipalRestService<CertificadoInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'certificado';
  }
}
