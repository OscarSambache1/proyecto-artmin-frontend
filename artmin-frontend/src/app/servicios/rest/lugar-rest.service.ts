import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {LugarInterface} from '../../interfaces/lugar.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LugarRestService extends PrincipalRestService<LugarInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'lugar';
  }

  crearLugarImagen(
    lugar: LugarInterface,
    imagen?: File,
  ): Observable<any> {
    const formData: FormData = new FormData();
    if (imagen && imagen.type) {
      formData.append(`imagen`, imagen);
    }
    formData.append('lugar', JSON.stringify(lugar));
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-lugar-imagen`,
      formData,
      options,
    );
  }

  obtenerLugares(datosConsulta): Observable<any> {
    return this._http
      .get(
        this.url +
        ':' +
        this.port +
        `/${this.segmento}/buscar-lugares-padre?datosConsulta=${JSON.stringify(datosConsulta)}`,
      );
  }
}
