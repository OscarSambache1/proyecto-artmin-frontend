import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {CertificadoChartInterface} from '../../interfaces/certificado-chart.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {CertificadoChartFechaInterface} from '../../interfaces/certificado-chart-fecha.interface';

@Injectable({
  providedIn: 'root'
})
export class CertificadoChartRestService extends PrincipalRestService<CertificadoChartInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'certificado-chart';
  }

  crearCertificadoChart(
    certificadoChart: CertificadoChartInterface,
    certificadosChartFecha: CertificadoChartFechaInterface[],
  ): Observable<any> {
    const formData: FormData = new FormData();
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-certificado-chart`,
      {
        certificadoChart,
        certificadosChartFecha
      }
    );
  }

  editarCertificadoChart(
    idCertificadoChart: number,
    certificadoChart: CertificadoChartInterface,
    certificadosChartFecha: CertificadoChartFechaInterface[],
  ): Observable<any> {
    const formData: FormData = new FormData();
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/editar-certificado-chart`,
      {
        idCertificadoChart,
        certificadoChart,
        certificadosChartFecha
      }
    );
  }

  obtenerCertificados(datosConsultaCertificados: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/obtener-certificados?datosConsulta=${
        datosConsultaCertificados
      }`,
    );
  }
}
