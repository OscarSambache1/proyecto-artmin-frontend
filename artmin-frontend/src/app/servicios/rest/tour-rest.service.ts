import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {TourInterface} from '../../interfaces/tour.interface';
import {Observable} from 'rxjs';
import {LugarInterface} from '../../interfaces/lugar.interface';

@Injectable({
  providedIn: 'root'
})
export class TourRestService extends PrincipalRestService<TourInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'tour';
  }

  crearEditarTour(
    tour: TourInterface,
    imagen?: File,
  ): Observable<any> {
    const formData: FormData = new FormData();
    if (imagen && imagen.type) {
      formData.append(`imagen`, imagen);
    }
    formData.append('tour', JSON.stringify(tour));
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    const options = {headers};
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-tour-imagen`,
      formData,
      options,
    );
  }

  obtenerTourPorArtistasTipo(datosConsultaTours: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/tours-por-artista-tipo?datosConsulta=${
        datosConsultaTours
      }`,
    );
  }
}
