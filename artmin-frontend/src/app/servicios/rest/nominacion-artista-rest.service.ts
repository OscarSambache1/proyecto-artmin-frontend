import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {CategoriaInterface} from '../../interfaces/categoria.interface';
import {NominacionInterface} from '../../interfaces/nominacion.interface';
import {Observable} from 'rxjs';
import {NominacionArtistaInterface} from '../../interfaces/nominacion-artista.interface';

@Injectable({
  providedIn: 'root'
})
export class NominacionArtistaRestService extends PrincipalRestService<NominacionArtistaInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'nominacion-artista-album-cancion-video';
  }

  obtenerNominaciones(
    datos: string,
  ): Observable<any> {
    return this._http.get(
      `${this.url}:${this.port}/${this.segmento}/obtener-nominaciones?datos=${datos}`,
    );
  }
}
