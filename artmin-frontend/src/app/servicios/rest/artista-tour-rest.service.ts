import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {ArtistaTourInterface} from '../../interfaces/artista-tour.interface';

@Injectable({
  providedIn: 'root'
})
export class ArtistaTourRestService extends PrincipalRestService<ArtistaTourInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'artista-tour';
  }
}
