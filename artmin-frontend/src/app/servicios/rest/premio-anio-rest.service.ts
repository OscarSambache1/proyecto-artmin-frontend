import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {PremioInterface} from '../../interfaces/premio.interface';
import {Observable} from 'rxjs';
import {PremioAnioInterface} from '../../interfaces/premio-anio.interface';

@Injectable({
  providedIn: 'root'
})
export class PremioAnioRestService extends PrincipalRestService<PremioAnioInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'premio-anio';
  }
}
