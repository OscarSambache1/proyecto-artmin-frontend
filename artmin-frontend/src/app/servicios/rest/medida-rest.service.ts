import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {PrincipalRestService} from './rest-principal.service';
import {MedidaInterface} from '../../interfaces/medida.interface';

@Injectable({
  providedIn: 'root'
})
export class MedidaRestService extends PrincipalRestService<MedidaInterface>{
  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'medida';
  }
}
