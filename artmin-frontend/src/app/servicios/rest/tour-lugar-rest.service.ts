import {Injectable} from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {TourLugarInterface} from '../../interfaces/tour-lugar.interface';

@Injectable({
  providedIn: 'root'
})
export class TourLugarRestService extends PrincipalRestService<TourLugarInterface> {
  constructor(
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'tour-lugar';
  }
}
