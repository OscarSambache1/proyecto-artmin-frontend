import { Injectable } from '@angular/core';
import {PrincipalRestService} from './rest-principal.service';
import {VideoInterface} from '../../interfaces/video.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../interfaces/enlace-album-cancion-artista-video.interface';

@Injectable({
  providedIn: 'root'
})
export class VideoRestService extends PrincipalRestService<VideoInterface> {

  constructor(
    // tslint:disable-next-line:variable-name
    public readonly _http: HttpClient,
  ) {
    // @ts-ignore
    super(_http);
    this.url = environment.url;
    this.port = environment.port;
    this.segmento = 'video';
  }
  crearVideo(
    video: VideoInterface,
    enlaces: EnlaceAlbumCancionArtistaVideoInterface[],
  ): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/crear-video?datosCrearVideo=${JSON.stringify(
        {
          video,
          enlaces
        }
      )}`,
      {}
    );
  }

  editarVideo(
    video: VideoInterface,
    enlaces: EnlaceAlbumCancionArtistaVideoInterface[],
    idVideo: number,
  ): Observable<any> {
    return this._http.post(
      `${this.url}:${this.port}/${this.segmento}/editar-video?datosEditarVideo=${JSON.stringify(
        {
          video,
          enlaces,
          idVideo
        }
      )}`,
      {}
    );
  }

  obtenerVideosPorCancionArtistaAlbumTipo(datosConsultaVideos: string): Observable<any> {
    return this._http.get(
      this.url +
      `:${this.port}/${this.segmento}/obtener-videos-por-cancion-artista-album-tipo?datosConsulta=${
        datosConsultaVideos
      }`,
    );
  }
}
