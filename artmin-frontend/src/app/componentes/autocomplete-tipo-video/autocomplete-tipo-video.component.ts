import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TipoVideoInterface} from '../../interfaces/tipo-video.interface';
import {TipoVideoRestService} from '../../servicios/rest/tipo-video-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';

@Component({
  selector: 'app-autocomplete-tipo-video',
  templateUrl: './autocomplete-tipo-video.component.html',
  styleUrls: ['./autocomplete-tipo-video.component.css']
})
export class AutocompleteTipoVideoComponent implements OnInit {


  @Input()
  idTipoVideo: number;

  @Input()
  deshabilitar: boolean;

  @Output()
  emitirTipoVideoSeleccionado: EventEmitter<TipoVideoInterface> = new EventEmitter<TipoVideoInterface>();

  tipoVideoSeleccionado: TipoVideoInterface;
  arregloTiposVideo: TipoVideoInterface[];

  constructor(
    private readonly _tipoVideoService: TipoVideoRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idTipoVideo) {
      const consulta = {
        where: {
          id: this.idTipoVideo
        }
      };
      this._tipoVideoService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaTipoVideo: [TipoVideoInterface[], number]) => {
            this.tipoVideoSeleccionado = respuestaTipoVideo[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarTiposVideo(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
    };
    this._tipoVideoService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaTipoVideo: [TipoVideoInterface[], number]) => {
          this.arregloTiposVideo = respuestaTipoVideo[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarTipoVideoSeleccionado(tipoVideoSeleccionado: TipoVideoInterface) {
    this.emitirTipoVideoSeleccionado.emit(tipoVideoSeleccionado);
  }

  limpiarBusqueda() {
    this.emitirTipoVideoSeleccionado.emit(undefined);

  }

  deseleccionarTipoVideo() {
    this.emitirTipoVideoSeleccionado.emit(this.tipoVideoSeleccionado);
  }

}
