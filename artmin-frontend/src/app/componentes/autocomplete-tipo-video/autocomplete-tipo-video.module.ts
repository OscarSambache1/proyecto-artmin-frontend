import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteTipoVideoComponent } from './autocomplete-tipo-video.component';
import {FormsModule} from '@angular/forms';
import {AutoCompleteModule} from 'primeng';



@NgModule({
  declarations: [AutocompleteTipoVideoComponent],
  imports: [
    CommonModule,
    FormsModule,
    AutoCompleteModule
  ],
  exports: [
    AutocompleteTipoVideoComponent
  ]
})
export class AutocompleteTipoVideoModule { }
