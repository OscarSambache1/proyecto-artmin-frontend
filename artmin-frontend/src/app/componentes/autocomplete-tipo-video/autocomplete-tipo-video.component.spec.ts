import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteTipoVideoComponent } from './autocomplete-tipo-video.component';

describe('AutocompleteTipoVideoComponent', () => {
  let component: AutocompleteTipoVideoComponent;
  let fixture: ComponentFixture<AutocompleteTipoVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteTipoVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteTipoVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
