import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteRecordComponent } from './autocomplete-record.component';

describe('AutocompleteRecordComponent', () => {
  let component: AutocompleteRecordComponent;
  let fixture: ComponentFixture<AutocompleteRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
