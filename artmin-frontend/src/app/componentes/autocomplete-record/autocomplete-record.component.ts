import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RecordInterface} from '../../interfaces/record.interface';
import {RecordRestService} from '../../servicios/rest/record-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {ChartInterface} from '../../interfaces/chart.interface';

@Component({
  selector: 'app-autocomplete-record',
  templateUrl: './autocomplete-record.component.html',
  styleUrls: ['./autocomplete-record.component.css']
})
export class AutocompleteRecordComponent implements OnInit {
  @Input()
  recordSeleccionado: RecordInterface;

  @Input()
  idsRecordsAsignados: number[] = [];

  @Input()
  idChart: number;

  arregloRecords: RecordInterface[];

  @Output()
  emitirRecordSeleccionado: EventEmitter<RecordInterface> = new EventEmitter<RecordInterface>();

  constructor(
    private readonly _recordRestService: RecordRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idsRecordsAsignados && this.idsRecordsAsignados.length) {
      const consulta = {
        camposIn: [
          {
            nombreCampo: 'id',
            valor: this.idsRecordsAsignados
          }
        ],
        relations: [
          'chart'
        ]
      };
      this._recordRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaRecords: [RecordInterface[], number]) => {
            this.recordSeleccionado = respuestaRecords[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarRecords(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'chart'
      ]
    };
    this._recordRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaRecord: [RecordInterface[], number]) => {
          this.arregloRecords = respuestaRecord[0]
            .filter(record => {
              return !(this.idsRecordsAsignados.find(idRecord => idRecord === record.id));
            })
            .filter( recordFiltrado => (recordFiltrado.chart as ChartInterface).id === this.idChart);
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarRecordSeleccionado(recordSeleccionado: RecordInterface) {
    this.emitirRecordSeleccionado.emit(recordSeleccionado);

  }

  limpiarBusqueda() {
    this.emitirRecordSeleccionado.emit(undefined);
  }
}
