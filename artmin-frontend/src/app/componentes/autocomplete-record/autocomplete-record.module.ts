import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteRecordComponent } from './autocomplete-record.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteRecordComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteRecordComponent
  ]
})
export class AutocompleteRecordModule { }
