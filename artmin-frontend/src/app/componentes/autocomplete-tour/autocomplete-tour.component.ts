import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';
import {TourInterface} from '../../interfaces/tour.interface';
import {TourRestService} from '../../servicios/rest/tour-rest.service';
import {ArtistaTourInterface} from '../../interfaces/artista-tour.interface';

@Component({
  selector: 'app-autocomplete-tour',
  templateUrl: './autocomplete-tour.component.html',
  styleUrls: ['./autocomplete-tour.component.css']
})
export class AutocompleteTourComponent implements OnInit {
  tourSeleccionado: TourInterface | TourInterface[];
  arregloTours: TourInterface[] = [];

  @Output()
  emitirTourSeleccionado: EventEmitter<TourInterface | TourInterface[]> = new EventEmitter<TourInterface | TourInterface[]>();

  @Input()
  idTour: number;

  @Input()
  idsArtistas: number[];

  @Input()
  arregloidsTour: number[];

  @Input()
  arregloidsToursSeleccionadas: number[];

  @Input()
  multiple: boolean;

  @Input()
  deshabilitar: boolean;

  constructor(
    private readonly _tourService: TourRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idTour || (this.arregloidsTour && this.arregloidsTour.length)) {
      let consulta;
      if (this.idTour) {
        consulta = {
          where: {
            id: this.idTour
          },
          relations: [
            'imagenesTour',
            'artistasTour',
            'artistasTour.artista'
          ]
        };
      } else {
        if (this.arregloidsTour && this.arregloidsTour.length) {
          consulta = {
            camposIn: [
              {
                nombreCampo: 'id',
                valor: this.arregloidsTour
              }
            ],
            relations: [
              'imagenesTour',
              'artistasTour',
              'artistasTour.artista'
            ]
          };
        }

      }
      this._tourService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((cancion: [TourInterface[], number]) => {
          if (!this.multiple) {
            this.tourSeleccionado = cancion[0][0];
          } else {
            this.tourSeleccionado = cancion[0];
          }
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  enviarTourSeleccionado(tourSeleccionado: TourInterface | TourInterface[]) {
    if (!this.multiple) {
      this.emitirTourSeleccionado.emit(tourSeleccionado);
    } else {
      this.emitirTourSeleccionado.emit(this.tourSeleccionado);
    }
  }

  filtrarTour(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'imagenesTour',
        'artistasTour',
        'artistasTour.artista'
      ]
    };
    this._tourService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaTour: [TourInterface[], number]) => {
          if (this.idsArtistas && this.idsArtistas.length) {
            respuestaTour[0] = respuestaTour[0].filter(
              tour => {
                return (tour.artistasTour as ArtistaTourInterface[]).find(artistaTour => {
                  return this.idsArtistas.find(idArtista => (artistaTour.artista as ArtistaInterface).id === idArtista);
                });
              }
            );
          }
          this.arregloTours = respuestaTour[0];
          if (this.arregloidsToursSeleccionadas && this.arregloidsToursSeleccionadas.length) {
            this.arregloTours = this.arregloTours
              .filter(tourConsulta => {
                return !this.arregloidsToursSeleccionadas.find(idTourSeleccionado => idTourSeleccionado === tourConsulta.id);
              });
          }
      }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  obtenerUrlPrincipal(tour: TourInterface): string {
    return obtenerUrlImagenPrincipal(tour, 'imagenesTour');
  }

  limpiarBusqueda() {
    this.emitirTourSeleccionado.emit(undefined);
  }

  deseleccionarTour() {
    this.emitirTourSeleccionado.emit(this.tourSeleccionado);
  }


}
