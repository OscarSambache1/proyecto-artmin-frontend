import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteTourComponent } from './autocomplete-tour.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteTourComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [AutocompleteTourComponent]
})
export class AutocompleteTourModule { }
