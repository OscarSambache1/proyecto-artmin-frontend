import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {MedidaInterface} from '../../interfaces/medida.interface';
import {MedidaRestService} from '../../servicios/rest/medida-rest.service';

@Component({
  selector: 'app-autocomplete-medida',
  templateUrl: './autocomplete-medida.component.html',
  styleUrls: ['./autocomplete-medida.component.css']
})
export class AutocompleteMedidaComponent implements OnInit {

  @Input()
  medidaSeleccionada: MedidaInterface;

  @Input()
  idsMedidasAsignadas: number[] = [];

  arregloMedidas: MedidaInterface[];

  @Output()
  emitirMedidaSeleccionada: EventEmitter<MedidaInterface> = new EventEmitter<MedidaInterface>();

  constructor(
    private readonly _medidaRestService: MedidaRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idsMedidasAsignadas && this.idsMedidasAsignadas.length) {
      const consulta = {
        camposIn: [
          {
            nombreCampo: 'id',
            valor: this.idsMedidasAsignadas
          }
        ],
      };
      this._medidaRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaMedidas: [MedidaInterface[], number]) => {
            this.medidaSeleccionada = respuestaMedidas[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarMedidas(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
    };
    this._medidaRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaMedida: [MedidaInterface[], number]) => {
          this.arregloMedidas = respuestaMedida[0]
            .filter(medida => {
              return !(this.idsMedidasAsignadas.find(idMedida => idMedida === medida.id));
            });
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarMedidaSeleccionada(medidaSeleccionada: MedidaInterface) {
    this.emitirMedidaSeleccionada.emit(medidaSeleccionada);

  }

  limpiarBusqueda() {
    this.emitirMedidaSeleccionada.emit(undefined);
  }
}
