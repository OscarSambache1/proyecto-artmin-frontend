import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteMedidaComponent } from './autocomplete-medida.component';

describe('AutocompleteMedidaComponent', () => {
  let component: AutocompleteMedidaComponent;
  let fixture: ComponentFixture<AutocompleteMedidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteMedidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteMedidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
