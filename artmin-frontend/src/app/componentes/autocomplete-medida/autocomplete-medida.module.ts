import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AutocompleteMedidaComponent} from './autocomplete-medida.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [AutocompleteMedidaComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteMedidaComponent
  ]
})
export class AutocompleteMedidaModule {
}
