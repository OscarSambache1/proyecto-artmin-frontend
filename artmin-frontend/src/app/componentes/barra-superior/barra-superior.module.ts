import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BarraSuperiorComponent} from './barra-superior.component';
import {ButtonModule, ToolbarModule} from 'primeng';


@NgModule({
  declarations: [BarraSuperiorComponent],
  imports: [
    CommonModule,
    ToolbarModule,
    ButtonModule,
  ],
  exports: [
    BarraSuperiorComponent
  ]
})
export class BarraSuperiorModule {
}
