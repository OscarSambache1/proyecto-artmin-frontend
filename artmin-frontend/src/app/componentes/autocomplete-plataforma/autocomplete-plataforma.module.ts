import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompletePlataformaComponent } from './autocomplete-plataforma.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
    declarations: [AutocompletePlataformaComponent],
    exports: [
        AutocompletePlataformaComponent
    ],
    imports: [
        CommonModule,
        AutoCompleteModule,
        FormsModule
    ]
})
export class AutocompletePlataformaModule { }
