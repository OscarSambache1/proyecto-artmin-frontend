import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PlataformaInterface} from '../../interfaces/plataforma.interface';
import {ToasterService} from 'angular2-toaster';
import {PlataformaRestService} from '../../servicios/rest/plataforma-rest.service';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {AlbumInterface} from '../../interfaces/album.interface';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';
import {LugarInterface} from '../../interfaces/lugar.interface';

@Component({
  selector: 'app-autocomplete-plataforma',
  templateUrl: './autocomplete-plataforma.component.html',
  styleUrls: ['./autocomplete-plataforma.component.css']
})
export class AutocompletePlataformaComponent implements OnInit {
  @Input()
  plataformaSeleccionada: PlataformaInterface;

  @Input()
  idsPlataformasAsignadas: number[] = [];

  arregloPlataformas: PlataformaInterface[];

  @Input()
  deshabilitar: boolean;

  @Input()
  verSoloTipoChart: boolean;

  @Output()
  emitirPlataformaSeleccionada: EventEmitter<PlataformaInterface> = new EventEmitter<PlataformaInterface>();

  constructor(
    private readonly _plataformaRestService: PlataformaRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idsPlataformasAsignadas && this.idsPlataformasAsignadas.length) {
      const consulta = {
        camposIn: [
          {
            nombreCampo: 'id',
            valor: this.idsPlataformasAsignadas
          }
        ],
        relations: [
          'imagenesPlataforma'
        ]
      };
      this._plataformaRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaPlataformas: [PlataformaInterface[], number]) => {
            this.plataformaSeleccionada = respuestaPlataformas[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarPlataformas(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'imagenesPlataforma'
      ]
    };
    this._plataformaRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaPlataforma: [PlataformaInterface[], number]) => {
          this.arregloPlataformas = respuestaPlataforma[0]
            .filter(plataforma => {
              return !(this.idsPlataformasAsignadas.find(idPlataforma => idPlataforma === plataforma.id));
            });
          if (this.verSoloTipoChart) {
            this.arregloPlataformas = this.arregloPlataformas
              .filter( plataforma => plataforma.tipo === 'chart');
          }
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarPlataformaSeleccionada(plataformaSeleccionada: PlataformaInterface) {
    this.emitirPlataformaSeleccionada.emit(plataformaSeleccionada);

  }

  limpiarBusqueda() {
    this.emitirPlataformaSeleccionada.emit(undefined);
  }

  obtenerUrlPrincipal(plataforma: PlataformaInterface): string {
    return obtenerUrlImagenPrincipal(plataforma, 'imagenesPlataforma');
  }
}
