import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {PremioInterface} from '../../interfaces/premio.interface';
import {PremioRestService} from '../../servicios/rest/premio-rest.service';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';

@Component({
  selector: 'app-autocomplete-premio',
  templateUrl: './autocomplete-premio.component.html',
  styleUrls: ['./autocomplete-premio.component.css']
})
export class AutocompletePremioComponent implements OnInit {

  @Input()
  idPremio: number;

  @Input()
  esPremio: number;

  @Input()
  deshabilitar: boolean;

  @Output()
  emitirPremioSeleccionado: EventEmitter<PremioInterface> = new EventEmitter<PremioInterface>();

  premioSeleccionado: PremioInterface;
  arregloPremios: PremioInterface[];

  constructor(
    private readonly _premioService: PremioRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idPremio) {
      const consulta = {
        where: {
          id: this.idPremio
        },
        relations: ['imagenesPremio']
      };
      this._premioService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaPremio: [PremioInterface[], number]) => {
            this.premioSeleccionado = respuestaPremio[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarPremios(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: ['imagenesPremio']

    };
    this._premioService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaPremio: [PremioInterface[], number]) => {
          this.arregloPremios = respuestaPremio[0];
          if (this.esPremio === 1 || this.esPremio === 0) {
            this.arregloPremios = this.arregloPremios.filter(premio => premio.esPremio === this.esPremio);
          }
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarPremioSeleccionado(premioSeleccionado: PremioInterface) {
    this.emitirPremioSeleccionado.emit(premioSeleccionado);
  }

  limpiarBusqueda() {
    this.emitirPremioSeleccionado.emit(undefined);
  }

  deseleccionarPremio() {
    this.emitirPremioSeleccionado.emit(this.premioSeleccionado);
  }

  obtenerUrlPrincipal(premio: PremioInterface): string {
    return obtenerUrlImagenPrincipal(premio, 'imagenesPremio');
  }

}
