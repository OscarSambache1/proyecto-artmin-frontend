import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AutocompletePremioComponent} from './autocomplete-premio.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [AutocompletePremioComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [AutocompletePremioComponent]
})
export class AutocompletePremioModule {
}
