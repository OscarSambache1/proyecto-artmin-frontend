import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {PremioAnioInterface} from '../../interfaces/premio-anio.interface';
import {PremioAnioRestService} from '../../servicios/rest/premio-anio-rest.service';
import {PremioInterface} from '../../interfaces/premio.interface';

@Component({
  selector: 'app-autocomplete-premio-anio',
  templateUrl: './autocomplete-premio-anio.component.html',
  styleUrls: ['./autocomplete-premio-anio.component.css']
})
export class AutocompletePremioAnioComponent implements OnInit {

  premioAnioSeleccionada: PremioAnioInterface | PremioAnioInterface[];
  arregloPremiosAnio: PremioAnioInterface[] = [];

  @Output()
  emitirPremioAnioSeleccionada: EventEmitter<PremioAnioInterface | PremioAnioInterface[]> = new EventEmitter<PremioAnioInterface | PremioAnioInterface[]>();

  @Input()
  idPremioAnio: number;

  @Input()
  idPremio: number;

  @Input()
  arregloidsPremioAnio: number[];

  @Input()
  multiple: boolean;

  @Input()
  deshabilitar: boolean;

  constructor(
    private readonly _premioAnioRestService: PremioAnioRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idPremioAnio || (this.arregloidsPremioAnio && this.arregloidsPremioAnio.length)) {
      let consulta;
      if (this.idPremioAnio) {
        consulta = {
          where: {
            id: this.idPremioAnio
          },
        };
      } else {
        if (this.arregloidsPremioAnio && this.arregloidsPremioAnio.length) {
          consulta = {
            camposIn: [
              {
                nombreCampo: 'id',
                valor: this.arregloidsPremioAnio
              }
            ],
            where: {
              premio: this.idPremio,
            }
          };
        }

      }
      this._premioAnioRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((premioAnio: [PremioAnioInterface[], number]) => {
          if (!this.multiple) {
            this.premioAnioSeleccionada = premioAnio[0][0];
          } else {
            this.premioAnioSeleccionada = premioAnio[0];
          }
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  enviarPremioAnioSeleccionada(premioAnioSeleccionada: PremioAnioInterface | PremioAnioInterface[]) {
    if (!this.multiple) {
      this.emitirPremioAnioSeleccionada.emit(premioAnioSeleccionada);
    } else {
      this.emitirPremioAnioSeleccionada.emit(this.premioAnioSeleccionada);
    }
  }

  filtrarPremio(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'anio',
          like: true,
          valor: busqueda
        }
      ],
      where: {
        premio: this.idPremio,
      },
      relations: ['premio']
    };
    this._premioAnioRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaPremioAnio: [PremioAnioInterface[], number]) => {
          this.arregloPremiosAnio = respuestaPremioAnio[0].filter(categoria => (categoria.premio as PremioInterface)?.id === this.idPremio);
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  limpiarBusqueda() {
    this.emitirPremioAnioSeleccionada.emit(undefined);
  }

  deseleccionarPremioAnio() {
    this.emitirPremioAnioSeleccionada.emit(this.premioAnioSeleccionada);
  }
}
