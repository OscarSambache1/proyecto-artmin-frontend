import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AutocompletePremioAnioComponent} from './autocomplete-premio-anio.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [AutocompletePremioAnioComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [AutocompletePremioAnioComponent]
})
export class AutocompletePremioAnioModule {
}
