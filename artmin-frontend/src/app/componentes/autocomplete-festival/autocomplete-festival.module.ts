import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteFestivalComponent } from './autocomplete-festival.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteFestivalComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [AutocompleteFestivalComponent]
})
export class AutocompleteFestivalModule { }
