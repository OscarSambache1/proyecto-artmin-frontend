import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';
import {FestivalInterface} from '../../interfaces/festival.interface';
import {FestivalRestService} from '../../servicios/rest/festival-rest.service';

@Component({
  selector: 'app-autocomplete-festival',
  templateUrl: './autocomplete-festival.component.html',
  styleUrls: ['./autocomplete-festival.component.css']
})
export class AutocompleteFestivalComponent implements OnInit {

  @Input()
  idFestival: number;

  @Input()
  deshabilitar: boolean;

  @Output()
  emitirFestivalSeleccionado: EventEmitter<FestivalInterface> = new EventEmitter<FestivalInterface>();

  festivalSeleccionado: FestivalInterface;
  arregloFestivales: FestivalInterface[];

  constructor(
    private readonly _festivalService: FestivalRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idFestival) {
      const consulta = {
        where: {
          id: this.idFestival
        },
        relations: ['imagenesFestival']
      };
      this._festivalService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaFestival: [FestivalInterface[], number]) => {
            this.festivalSeleccionado = respuestaFestival[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarFestivales(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: ['imagenesFestival']

    };
    this._festivalService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaFestival: [FestivalInterface[], number]) => {
          this.arregloFestivales = respuestaFestival[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarFestivalSeleccionado(festivalSeleccionado: FestivalInterface) {
    this.emitirFestivalSeleccionado.emit(festivalSeleccionado);
  }

  limpiarBusqueda() {
    this.emitirFestivalSeleccionado.emit(undefined);
  }

  deseleccionarFestival() {
    this.emitirFestivalSeleccionado.emit(this.festivalSeleccionado);
  }

  obtenerUrlPrincipal(festival: FestivalInterface): string {
    return obtenerUrlImagenPrincipal(festival, 'imagenesFestival');
  }
}
