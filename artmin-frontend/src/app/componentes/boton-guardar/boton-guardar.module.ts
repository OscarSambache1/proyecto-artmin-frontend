import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotonGuardarComponent } from './boton-guardar.component';
import {MatDialogModule} from '@angular/material/dialog';



@NgModule({
  declarations: [BotonGuardarComponent],
  imports: [
    CommonModule,
    MatDialogModule,
  ],
  exports: [
    BotonGuardarComponent
  ]
})
export class BotonGuardarModule { }
