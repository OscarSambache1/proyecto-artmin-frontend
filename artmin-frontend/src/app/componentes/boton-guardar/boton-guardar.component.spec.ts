import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotonGuardarComponent } from './boton-guardar.component';

describe('BotonGuardarComponent', () => {
  let component: BotonGuardarComponent;
  let fixture: ComponentFixture<BotonGuardarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotonGuardarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotonGuardarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
