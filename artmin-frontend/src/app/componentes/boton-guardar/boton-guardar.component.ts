import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-boton-guardar',
  templateUrl: './boton-guardar.component.html',
  styleUrls: ['./boton-guardar.component.css']
})
export class BotonGuardarComponent implements OnInit {
  @Output()
  emitirEvento: EventEmitter<null> = new EventEmitter<null>();

  @Input()
  deshabilitarBoton: boolean;

  constructor() {
  }

  ngOnInit(): void {
  }

  enviarEventoClic() {
    this.emitirEvento.emit();
  }
}
