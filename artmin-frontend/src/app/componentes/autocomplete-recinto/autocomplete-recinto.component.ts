import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {RecintoInterface} from '../../interfaces/recinto-interface';
import {RecintoRestService} from '../../servicios/rest/recinto-rest.service';

@Component({
  selector: 'app-autocomplete-recinto',
  templateUrl: './autocomplete-recinto.component.html',
  styleUrls: ['./autocomplete-recinto.component.css']
})
export class AutocompleteRecintoComponent implements OnInit {

  @Input()
  idRecinto: number;

  @Input()
  deshabilitar: boolean;

  @Output()
  emitirRecintoSeleccionado: EventEmitter<RecintoInterface> = new EventEmitter<RecintoInterface>();

  recintoSeleccionado: RecintoInterface;
  arregloRecintos: RecintoInterface[];

  constructor(
    private readonly _recintoService: RecintoRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idRecinto) {
      const consulta = {
        where: {
          id: this.idRecinto
        }
      };
      this._recintoService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaRecinto: [RecintoInterface[], number]) => {
            this.recintoSeleccionado = respuestaRecinto[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarRecintos(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
    };
    this._recintoService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaRecinto: [RecintoInterface[], number]) => {
          this.arregloRecintos = respuestaRecinto[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarRecintoSeleccionado(recintoSeleccionado: RecintoInterface) {
    this.emitirRecintoSeleccionado.emit(recintoSeleccionado);
  }

  limpiarBusqueda() {
    this.emitirRecintoSeleccionado.emit(undefined);

  }

  deseleccionarRecinto() {
    this.emitirRecintoSeleccionado.emit(this.recintoSeleccionado);
  }

}
