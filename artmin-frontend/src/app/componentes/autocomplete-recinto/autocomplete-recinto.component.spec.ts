import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteRecintoComponent } from './autocomplete-recinto.component';

describe('AutocompleteRecintoComponent', () => {
  let component: AutocompleteRecintoComponent;
  let fixture: ComponentFixture<AutocompleteRecintoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteRecintoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteRecintoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
