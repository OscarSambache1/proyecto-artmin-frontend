import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteRecintoComponent } from './autocomplete-recinto.component';
import {FormsModule} from '@angular/forms';
import {AutoCompleteModule} from 'primeng';



@NgModule({
  declarations: [AutocompleteRecintoComponent],
  imports: [
    CommonModule,
    FormsModule,
    AutoCompleteModule
  ],
  exports: [AutocompleteRecintoComponent]
})
export class AutocompleteRecintoModule { }
