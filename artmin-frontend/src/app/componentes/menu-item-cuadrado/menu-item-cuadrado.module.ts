import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuItemCuadradoComponent } from './menu-item-cuadrado.component';



@NgModule({
  declarations: [MenuItemCuadradoComponent],
  imports: [
    CommonModule
  ],
  exports: [
    MenuItemCuadradoComponent
  ]
})
export class MenuItemCuadradoModule { }
