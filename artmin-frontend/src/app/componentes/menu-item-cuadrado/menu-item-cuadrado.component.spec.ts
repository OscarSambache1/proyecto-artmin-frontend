import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemCuadradoComponent } from './menu-item-cuadrado.component';

describe('MenuItemCuadradoComponent', () => {
  let component: MenuItemCuadradoComponent;
  let fixture: ComponentFixture<MenuItemCuadradoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuItemCuadradoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuItemCuadradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
