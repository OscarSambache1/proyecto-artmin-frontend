import {Component, Input, OnInit} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-menu-item-cuadrado',
  templateUrl: './menu-item-cuadrado.component.html',
  styleUrls: ['./menu-item-cuadrado.component.css']
})
export class MenuItemCuadradoComponent implements OnInit {

  @Input()
  texto: string;

  @Input()
  descripcion: string;

  @Input()
  imagen = '';

  @Input()
  url: string[];

  @Input()
  extras: NavigationExtras;

  @Input()
  height: '350px';

  constructor(
    private readonly _router: Router
  ) {
  }

  ngOnInit(): void {
  }

  irARuta() {
    this._router.navigate(this.url, this.extras);
  }
}
