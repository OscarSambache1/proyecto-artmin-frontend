import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteVideoComponent } from './autocomplete-video.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteVideoComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteVideoComponent
  ]
})
export class AutocompleteVideoModule { }
