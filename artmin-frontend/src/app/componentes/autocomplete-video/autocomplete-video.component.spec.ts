import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteVideoComponent } from './autocomplete-video.component';

describe('AutocompleteVideoComponent', () => {
  let component: AutocompleteVideoComponent;
  let fixture: ComponentFixture<AutocompleteVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
