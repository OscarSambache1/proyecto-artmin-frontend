import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {VideoInterface} from '../../interfaces/video.interface';
import {VideoRestService} from '../../servicios/rest/video-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {CancionInterface} from '../../interfaces/cancion.interface';

@Component({
  selector: 'app-autocomplete-video',
  templateUrl: './autocomplete-video.component.html',
  styleUrls: ['./autocomplete-video.component.css']
})
export class AutocompleteVideoComponent implements OnInit {

  videoSeleccionado: VideoInterface | VideoInterface[];
  arregloVideos: VideoInterface[] = [];

  @Output()
  emitirVideoSeleccionado: EventEmitter<VideoInterface | VideoInterface[]> = new EventEmitter<VideoInterface | VideoInterface[]>();

  @Input()
  idVideo: number;

  @Input()
  idsArtistas: number[];

  @Input()
  arregloidsVideo: number[];

  @Input()
  multiple: boolean;

  @Input()
  deshabilitar: boolean;

  @Input()
  arregloIdsVideosSeleccionados = [];

  constructor(
    private readonly _videoService: VideoRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idVideo || (this.arregloidsVideo && this.arregloidsVideo.length)) {
      let consulta;
      if (this.idVideo) {
        consulta = {
          where: {
            id: this.idVideo
          },
          relations: [
            'cancion',
            'cancion.artistasCancion',
            'cancion.artistasCancion.artista',
          ]
        };
      } else {
        if (this.arregloidsVideo && this.arregloidsVideo.length) {
          consulta = {
            camposIn: [
              {
                nombreCampo: 'id',
                valor: this.arregloidsVideo
              }
            ],
            relations: [
              'cancion',
              'cancion.artistasCancion',
              'cancion.artistasCancion.artista',
            ]
          };
        }

      }
      this._videoService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((video: [VideoInterface[], number]) => {
          if (!this.multiple) {
            this.videoSeleccionado = video[0][0];
          } else {
            this.videoSeleccionado = video[0];
          }
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  enviarVideoSeleccionado(videoSeleccionado: VideoInterface | VideoInterface[]) {
    if (!this.multiple) {
      this.emitirVideoSeleccionado.emit(videoSeleccionado);
    } else {
      this.emitirVideoSeleccionado.emit(this.videoSeleccionado);
    }
  }

  filtrarVideos(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'cancion',
        'cancion.artistasCancion',
        'cancion.artistasCancion.artista',
      ]
    };
    this._videoService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaVideo: [VideoInterface[], number]) => {
          if (this.idsArtistas && this.idsArtistas.length) {
            respuestaVideo[0] = respuestaVideo[0].filter(
              video => {
                return (video.cancion as CancionInterface).artistasCancion.find(artistaCancion => {
                  return this.idsArtistas.find(idArtista => (artistaCancion.artista as ArtistaInterface).id === idArtista);
                });
              }
            );
          }
          this.arregloVideos = respuestaVideo[0];
          if (this.arregloIdsVideosSeleccionados && this.arregloIdsVideosSeleccionados.length) {
            this.arregloVideos = this.arregloVideos
              .filter( videoConsulta => {
                return !this.arregloIdsVideosSeleccionados.find( idVideoSeleccionada => idVideoSeleccionada === videoConsulta.id);
              });
          }
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  limpiarBusqueda() {
    this.emitirVideoSeleccionado.emit(undefined);
  }

  deseleccionarVideo() {
    this.emitirVideoSeleccionado.emit(this.videoSeleccionado);
  }
}
