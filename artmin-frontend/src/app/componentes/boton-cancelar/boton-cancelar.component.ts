import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-boton-cancelar',
  templateUrl: './boton-cancelar.component.html',
  styleUrls: ['./boton-cancelar.component.css']
})
export class BotonCancelarComponent implements OnInit {
  @Output()
  emitirEvento: EventEmitter<null> = new EventEmitter<null>();

  constructor() { }

  ngOnInit(): void {
  }

  enviarEventoClic() {
    this.emitirEvento.emit();
  }
}
