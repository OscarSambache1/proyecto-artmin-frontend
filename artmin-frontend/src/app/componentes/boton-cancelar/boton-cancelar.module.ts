import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BotonCancelarComponent} from './boton-cancelar.component';
import {MatDialogModule} from '@angular/material/dialog';



@NgModule({
  declarations: [
    BotonCancelarComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule
  ],
  exports: [
    BotonCancelarComponent
  ]
})
export class BotonCancelarModule { }
