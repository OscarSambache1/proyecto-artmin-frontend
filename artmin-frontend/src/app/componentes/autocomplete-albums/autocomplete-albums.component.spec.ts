import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteAlbumsComponent } from './autocomplete-albums.component';

describe('AutocompleteAlbumsComponent', () => {
  let component: AutocompleteAlbumsComponent;
  let fixture: ComponentFixture<AutocompleteAlbumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteAlbumsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteAlbumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
