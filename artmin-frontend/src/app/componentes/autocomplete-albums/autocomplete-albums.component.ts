import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AlbumInterface} from '../../interfaces/album.interface';
import {AlbumRestService} from '../../servicios/rest/album-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';
import {ArtistaInterface} from '../../interfaces/artista.interface';

@Component({
  selector: 'app-autocomplete-albums',
  templateUrl: './autocomplete-albums.component.html',
  styleUrls: ['./autocomplete-albums.component.css']
})
export class AutocompleteAlbumsComponent implements OnInit {

  albumSeleccionado: AlbumInterface | AlbumInterface[];
  arregloAlbums: AlbumInterface[] = [];

  @Output()
  emitirAlbumSeleccionado: EventEmitter<AlbumInterface | AlbumInterface[]> = new EventEmitter<AlbumInterface | AlbumInterface[]>();

  @Input()
  idAlbum: number;

  @Input()
  idsArtistas: number[];

  @Input()
  arregloidsAlbum: number[];

  @Input()
  multiple: boolean;

  @Input()
  deshabilitar: boolean;

  @Input()
  arregloIdsAlbumsSeleccionados = [];

  constructor(
    private readonly _albumService: AlbumRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idAlbum || (this.arregloidsAlbum && this.arregloidsAlbum.length)) {
      let consulta;
      if (this.idAlbum) {
        consulta = {
          where: {
            id: this.idAlbum
          },
          relations: [
            'imagenesAlbum',
            'artistasAlbum',
            'artistasAlbum.artista'
          ]
        };
      } else {
        if (this.arregloidsAlbum && this.arregloidsAlbum.length) {
          consulta = {
            camposIn: [
              {
                nombreCampo: 'id',
                valor: this.arregloidsAlbum
              }
            ],
            relations: [
              'imagenesAlbum',
              'artistasAlbum',
              'artistasAlbum.artista'
            ]
          };
        }

      }
      this._albumService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((album: [AlbumInterface[], number]) => {
          if (!this.multiple) {
            this.albumSeleccionado = album[0][0];
          } else {
            this.albumSeleccionado = album[0];
          }
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  enviarAlbumSeleccionado(albumSeleccionado: AlbumInterface | AlbumInterface[]) {
    if (!this.multiple) {
      this.emitirAlbumSeleccionado.emit(albumSeleccionado);
    } else {
      this.emitirAlbumSeleccionado.emit(this.albumSeleccionado);
    }
  }

  filtrarAlbums(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'imagenesAlbum',
        'artistasAlbum',
        'artistasAlbum.artista'
      ]
    };
    this._albumService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaAlbum: [AlbumInterface[], number]) => {
          if (this.idsArtistas && this.idsArtistas.length) {
            respuestaAlbum[0] = respuestaAlbum[0].filter(
              album => {
                return album.artistasAlbum.find(artistaAlbum => {
                  return this.idsArtistas.find(idArtista => (artistaAlbum.artista as ArtistaInterface).id === idArtista);
                });
              }
            );
          }
          this.arregloAlbums = respuestaAlbum[0];
          if (this.arregloIdsAlbumsSeleccionados && this.arregloIdsAlbumsSeleccionados.length) {
            this.arregloAlbums = this.arregloAlbums
              .filter( albumConsulta => {
                return !this.arregloIdsAlbumsSeleccionados.find( idAlbumSeleccionado => idAlbumSeleccionado === albumConsulta.id);
              });
          }
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  obtenerUrlPrincipal(album: AlbumInterface): string {
    return obtenerUrlImagenPrincipal(album, 'imagenesAlbum');
  }

  limpiarBusqueda() {
    this.emitirAlbumSeleccionado.emit(undefined);
  }

  deseleccionarAlbum() {
    this.emitirAlbumSeleccionado.emit(this.albumSeleccionado);
  }
}
