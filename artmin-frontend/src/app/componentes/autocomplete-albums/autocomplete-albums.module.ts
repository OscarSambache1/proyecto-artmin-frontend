import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteAlbumsComponent } from './autocomplete-albums.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteAlbumsComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteAlbumsComponent
  ]
})
export class AutocompleteAlbumsModule { }
