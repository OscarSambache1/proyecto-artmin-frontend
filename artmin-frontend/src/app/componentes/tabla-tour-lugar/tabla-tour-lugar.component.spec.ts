import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaTourLugarComponent } from './tabla-tour-lugar.component';

describe('TablaTourLugarComponent', () => {
  let component: TablaTourLugarComponent;
  let fixture: ComponentFixture<TablaTourLugarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaTourLugarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaTourLugarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
