import {Component, Input, OnInit} from '@angular/core';
import {TourLugarInterface} from '../../interfaces/tour-lugar.interface';
import {TourInterface} from '../../interfaces/tour.interface';
import {MatDialog} from '@angular/material/dialog';
import {ModalTourLugarComponent} from '../../modulos/tour/modales/modal-tour-lugar/modal-tour-lugar.component';
import {ToastErrorCargandoDatos, toastExitoEliminar} from '../../constantes/mensajes-toaster';
import {ToasterService} from 'angular2-toaster';
import {CargandoService} from '../../servicios/cargando-service';
import {TourLugarRestService} from '../../servicios/rest/tour-lugar-rest.service';
import {PremioInterface} from '../../interfaces/premio.interface';
import {LugarInterface} from '../../interfaces/lugar.interface';

@Component({
  selector: 'app-tabla-tour-lugar',
  templateUrl: './tabla-tour-lugar.component.html',
  styleUrls: ['./tabla-tour-lugar.component.css']
})
export class TablaTourLugarComponent implements OnInit {
  @Input()
  tour: TourInterface;

  @Input()
  tourLugaresSeleccionadas: TourLugarInterface[] = [];

  columnasTourLugaresSeleccionadas = [
    {
      field: 'numero',
      header: '#',
      width: '5%'
    },
    {
      field: 'anio',
      header: 'Año',
      width: '10%'
    },
    {
      field: 'fecha',
      header: 'Fecha',
      width: '10%'
    },
    {
      field: 'ciudad',
      header: 'Ciudad',
      width: '15%'
    },
    {
      field: 'pais',
      header: 'País',
      width: '15%'
    },
    {
      field: 'lugar',
      header: 'Lugar',
      width: '15%'
    },
    {
      field: 'disponibilidad',
      header: 'Disponiblidad',
      width: '15%'
    },
    {
      field: 'recaudacion',
      header: 'Recaudación',
      width: '10%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '15%'
    },
  ];

  rowGroupMetadataCiudad;
  rowGroupMetadataPais;
  rowGroupMetadataLugar;
  rowGroupMetadataAnio;

  constructor(
    public dialog: MatDialog,
    private readonly _tourLugarRestService: TourLugarRestService,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
  ) {
  }

  ngOnInit(): void {
    this.actualizarTablaCampos();
  }

  actualizarTablaCampos() {
    this.rowGroupMetadataCiudad = this.actualizarTabla('ciudad');
    this.rowGroupMetadataPais = this.actualizarTabla('pais');
    this.rowGroupMetadataLugar = this.actualizarTabla('lugar');
    this.rowGroupMetadataAnio = this.actualizarTabla('anio');
  }

  quitarTourLugar(rowData: any, index: any) {
    this._cargandoService.deshabiltarCargando();
    this._tourLugarRestService
      .deleteOne(
        rowData.id,
      )
      .subscribe(() => {
          this._toasterService.pop(toastExitoEliminar);
          this._cargandoService.deshabiltarCargando();
          this.tourLugaresSeleccionadas.splice(index, 1);
          this.actualizarTablaCampos();
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        });
    this._cargandoService.deshabiltarCargando();
  }


  abrirModalCrearEditarTourLugar(
    tourLugar?: any, index?: any
  ) {
    const dialogRef = this.dialog
      .open(
        ModalTourLugarComponent,
        {
          width: '800px',
          data: {
            tourLugar,
            idTour: this.tour.id,
            tour: this.tour,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (tourLugarCreadoEditado: TourLugarInterface) => {
          if (tourLugarCreadoEditado) {
            if (tourLugar) {
              this.tourLugaresSeleccionadas[index] = tourLugarCreadoEditado;
            } else {
              this.tourLugaresSeleccionadas.push(tourLugarCreadoEditado);
            }
            this.actualizarTablaCampos();
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  actualizarTabla(campo: string) {
    const rowGroupMetadata = {};
    if (this.tourLugaresSeleccionadas) {
      for (let i = 0; i < this.tourLugaresSeleccionadas.length; i++) {
        const rowData = this.tourLugaresSeleccionadas[i];
        let idTourLugar;
        if (campo === 'ciudad') {
          idTourLugar = rowData['lugar']['id'];
        }
        if (campo === 'pais') {
          idTourLugar = rowData['lugar']['lugarPadre']['id'];
        }
        if (campo === 'lugar') {
          idTourLugar = rowData['locacion'];
        }
        if (campo === 'anio') {
          idTourLugar = rowData['anio'];
        }
        if (i === 0) {
          rowGroupMetadata[idTourLugar] = {index: 0, size: 1};
        } else {
          const previousRowData = this.tourLugaresSeleccionadas[i - 1];
          let previousRowGroup;
          if (campo === 'ciudad') {
            previousRowGroup = previousRowData['lugar']['id'];
          }
          if (campo === 'pais') {
            previousRowGroup = previousRowData['lugar']['lugarPadre']['id'];
          }
          if (campo === 'lugar') {
            previousRowGroup = previousRowData['locacion'];
          }
          if (campo === 'anio') {
            previousRowGroup = previousRowData['anio'];
          }
          if (idTourLugar === previousRowGroup) {
            rowGroupMetadata[idTourLugar].size++;
          } else {
            rowGroupMetadata[idTourLugar] = {index: i, size: 1};
          }
        }
      }
    }
    return rowGroupMetadata;
  }
}
