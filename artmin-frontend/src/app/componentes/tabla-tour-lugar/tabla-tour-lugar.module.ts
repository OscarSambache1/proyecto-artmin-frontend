import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaTourLugarComponent } from './tabla-tour-lugar.component';
import {PickListModule} from 'primeng/picklist';
import {SegundosAMinutosHorasPipeModule} from '../../pipes/segundos-a-minutos-horas-pipe/segundos-a-minutos-horas-pipe.module';
import {CrearEditarCancionModule} from '../../modulos/cancion/modales/crear-editar-cancion/crear-editar-cancion.module';
import {CardModule, InputTextModule, TableModule} from 'primeng';
import {FormsModule} from '@angular/forms';
import {ModalTourLugarModule} from '../../modulos/tour/modales/modal-tour-lugar/modal-tour-lugar.module';
import {ModalTourLugarComponent} from '../../modulos/tour/modales/modal-tour-lugar/modal-tour-lugar.component';



@NgModule({
  declarations: [TablaTourLugarComponent],
  imports: [
    CommonModule,
    PickListModule,
    SegundosAMinutosHorasPipeModule,
    CrearEditarCancionModule,
    TableModule,
    InputTextModule,
    CardModule,
    FormsModule,
    ModalTourLugarModule,
  ],
  exports: [
    TablaTourLugarComponent,
  ],
  entryComponents: [
    ModalTourLugarComponent,
  ]
})
export class TablaTourLugarModule { }
