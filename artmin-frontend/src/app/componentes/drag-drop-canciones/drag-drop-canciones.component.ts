import {Component, Input, OnInit} from '@angular/core';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {MatDialog} from '@angular/material/dialog';
import {ToasterService} from 'angular2-toaster';
import {CancionRestService} from '../../servicios/rest/cancion-rest.service';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {CrearEditarCancionComponent} from '../../modulos/cancion/modales/crear-editar-cancion/crear-editar-cancion.component';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';
import {AlbumInterface} from '../../interfaces/album.interface';
import {CancionArtistaInterface} from '../../interfaces/cancion-artista.interface';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {AlbumCancionInterface} from '../../interfaces/album-cancion.interface';
import {CargandoService} from '../../servicios/cargando-service';
import {AlbumCancionRestService} from '../../servicios/rest/album-cancion-rest.service';
import {obtenerObjeto} from '../../funciones/obtener-objeto';

@Component({
  selector: 'app-drag-drop-canciones',
  templateUrl: './drag-drop-canciones.component.html',
  styleUrls: ['./drag-drop-canciones.component.css']
})
export class DragDropCancionesComponent implements OnInit {

  @Input()
  idArtista: number;

  @Input()
  idsArtista: number[] = [];

  @Input()
  album: AlbumInterface;

  @Input()
  cancionesAlbumSeleccionadas: AlbumCancionInterface[] = [];

  @Input()
  cancionesDisponible: CancionInterface[] = [];

  cancionesDisponibleFiltradas: CancionInterface[] = [];

  columnasCancionesSeleccionadas = [
    {
      field: 'posicion',
      header: '#',
      width: '5%'
    },
    {
      field: 'nombre',
      header: 'Nombre',
      width: '35%'
    },
    {
      field: 'artistas',
      header: 'Artista(s)',
      width: '30%'
    },
    {
      field: 'duracionSegundos',
      header: 'Duracion',
      width: '10%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '15%'
    },
  ];
  columnasCancionesDisponibles = [
    {
      field: 'informacion',
      header: 'Información',
      width: '100%'
    },
  ];

  busqueda: string;

  seEmpezoAEditar: boolean;

  cancionesAlbumSeleccionadasOriginal: AlbumCancionInterface[] = [];
  cancionesFiltradasOriginal: CancionInterface[] = [];
  constructor(
    private readonly _cancionRestService: CancionRestService,
    public dialog: MatDialog,
    private readonly _toasterService: ToasterService,
    private readonly _cargandoService: CargandoService,
    private readonly _albumCancionRestService: AlbumCancionRestService,
  ) {
  }

  ngOnInit(): void {
    const consulta = {
      where: {},
      relations: [
        'tipoCancion',
        'imagenesCancion',
        'artistasCancion',
        'artistasCancion.artista',
        'generosCancion',
        'generosCancion.genero',
        'enlacesCancion',
      ]
    };
    this._cancionRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaCancion: [CancionInterface[], number]) => {
          this.cancionesDisponible = respuestaCancion[0].filter(cancion => {
            const cancionYaEstaEnAlbum = this.cancionesAlbumSeleccionadas.find(cancionAlbumSeleccionada => (cancionAlbumSeleccionada.cancion as CancionInterface).id === cancion.id);
            return !cancionYaEstaEnAlbum;
          });
          this.cancionesAlbumSeleccionadasOriginal = [...this.cancionesAlbumSeleccionadas];
          this.cancionesDisponibleFiltradas = [...this.cancionesDisponible]
            .filter( cancion => {
              return cancion.artistasCancion
                .find( artistaCancion => {
                return this.idsArtista
                  .find( idArtista => (artistaCancion.artista as ArtistaInterface).id === idArtista);
              });
            });
          this.cancionesFiltradasOriginal = [...this.cancionesDisponibleFiltradas];
        },
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  obtenerUrlPrincipal(cancion: CancionInterface): string {
    if (cancion.imagenesCancion && cancion.imagenesCancion.length) {
      return obtenerUrlImagenPrincipal(cancion, 'imagenesCancion');
    }
  }

  abrirModalCrearEditarCancion(
    cancion?: CancionInterface,
    indice?: number) {
    let idsArtista = this.idsArtista;
    if (cancion && cancion.artistasCancion) {
      idsArtista = cancion.artistasCancion.map(artistaCancion => (artistaCancion.artista as ArtistaInterface).id);
    }
    const dialogRef = this.dialog
      .open(
        CrearEditarCancionComponent,
        {
          width: '1000px',
          data: {
            idsArtista,
            idsAlbums: [this.album.id],
            album: this.album,
            cancion,
            deshabilitarAlbums: true
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (cancionCreadaEditada: CancionInterface) => {
          if (cancionCreadaEditada) {
            const cancionAlbumCreadaEditada = cancionCreadaEditada.albumesCancion.find(cancionAlbum => (cancionAlbum.album as AlbumInterface).id === this.album.id);
            if (cancionAlbumCreadaEditada) {
              if (!cancion) {
                this.cancionesAlbumSeleccionadas.push(cancionAlbumCreadaEditada);
              } else {
                this.cancionesAlbumSeleccionadas[indice] = cancionAlbumCreadaEditada;
              }
            }
          }
        },
        error => {
          console.error(error);
        },
      );
  }

  agregarCancionSeleccionada(
    cancion: CancionInterface,
    id: number) {
    const indiceArregloFiltradoCancion = this.cancionesDisponibleFiltradas.indexOf(cancion);
    const indiceCancion = this.cancionesDisponible.indexOf(cancion);
    this.cancionesDisponible.splice(indiceCancion, 1);
    this.cancionesDisponibleFiltradas.splice(indiceArregloFiltradoCancion, 1);
    const cancionAlbumACrear: AlbumCancionInterface = {
      posicion: this.cancionesAlbumSeleccionadas.length + 1,
      cancion: cancion,
      album: this.album,
    };
    this.cancionesAlbumSeleccionadas.push(
      cancionAlbumACrear
    );
  }

  quitarCancionSeleccionada(
    cancionAlbum: AlbumCancionInterface,
    indice: number) {
    this.cancionesDisponibleFiltradas.unshift(cancionAlbum.cancion as CancionInterface);
    this.cancionesDisponible.unshift(cancionAlbum.cancion as CancionInterface);
    this.cancionesAlbumSeleccionadas.splice(indice, 1);
  }

  filtrarCancionesDisponibles(busqueda: string) {
    this.busqueda = busqueda.trim();
    this.cancionesDisponibleFiltradas = [...this.cancionesDisponible]
      .filter(cancionDisponible => {
        return cancionDisponible.nombre
          .toString()
          .toLowerCase()
          .includes(
            (this.busqueda)
              .toLowerCase()
              .trim()
          );
      });

  }

  obtenerArtistas(artistasCancion: CancionArtistaInterface[]): string {
    return obtenerObjeto(artistasCancion, 'artista', 'nombre');
  }

  guardarCancionesAlbum() {
    this.seEmpezoAEditar = true;
    const cancionesAlbum = this.cancionesAlbumSeleccionadas.map(
      (cancionSeleccionada, indice) => {
        return {
          posicion: indice + 1,
          idCancion: (cancionSeleccionada.cancion as CancionInterface).id,
        };
      }
    );
    this._cargandoService.habilitarCargando();
    this._albumCancionRestService
      .guardarCancionesAlbumCancion(
        this.album.id,
        cancionesAlbum
      )
      .subscribe(
        (respuestaCancionesAlbum: [AlbumCancionInterface[], number]) => {
          this.cancionesAlbumSeleccionadas = respuestaCancionesAlbum[0];
          this._cargandoService.deshabiltarCargando();
          this.seEmpezoAEditar = false;
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  mostrarComponentesEditar() {
    this.seEmpezoAEditar = true;
  }

  cancelarEdicion() {
    this.seEmpezoAEditar = false;
    this.cancionesAlbumSeleccionadas = this.cancionesAlbumSeleccionadasOriginal;
    this.cancionesDisponibleFiltradas = this.cancionesFiltradasOriginal;
  }
}
