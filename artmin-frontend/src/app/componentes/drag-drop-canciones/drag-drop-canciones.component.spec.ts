import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DragDropCancionesComponent } from './drag-drop-canciones.component';

describe('DragDropCancionesComponent', () => {
  let component: DragDropCancionesComponent;
  let fixture: ComponentFixture<DragDropCancionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DragDropCancionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DragDropCancionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
