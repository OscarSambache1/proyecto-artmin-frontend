import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DragDropCancionesComponent} from './drag-drop-canciones.component';
import {PickListModule} from 'primeng/picklist';
import {SegundosAMinutosHorasPipeModule} from '../../pipes/segundos-a-minutos-horas-pipe/segundos-a-minutos-horas-pipe.module';
import {CrearEditarCancionModule} from '../../modulos/cancion/modales/crear-editar-cancion/crear-editar-cancion.module';
import {CrearEditarCancionComponent} from '../../modulos/cancion/modales/crear-editar-cancion/crear-editar-cancion.component';
import {CardModule, InputTextModule, TableModule} from 'primeng';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [DragDropCancionesComponent],
  imports: [
    CommonModule,
    PickListModule,
    SegundosAMinutosHorasPipeModule,
    CrearEditarCancionModule,
    TableModule,
    InputTextModule,
    CardModule,
    FormsModule
  ],
  exports: [
    DragDropCancionesComponent,
  ],
  entryComponents: [
    CrearEditarCancionComponent
  ]
})
export class DragDropCancionesModule {
}
