import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-alerta-validacion-campo-formulario',
  templateUrl: './alerta-validacion-campo-formulario.component.html',
  styleUrls: ['./alerta-validacion-campo-formulario.component.css']
})
export class AlertaValidacionCampoFormularioComponent implements OnInit {

  @Input()
  mensajesError: [] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
