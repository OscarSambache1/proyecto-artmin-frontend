import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertaValidacionCampoFormularioComponent } from './alerta-validacion-campo-formulario.component';

describe('AlertaValidacionCampoFormularioComponent', () => {
  let component: AlertaValidacionCampoFormularioComponent;
  let fixture: ComponentFixture<AlertaValidacionCampoFormularioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertaValidacionCampoFormularioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertaValidacionCampoFormularioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
