import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertaValidacionCampoFormularioComponent } from './alerta-validacion-campo-formulario.component';



@NgModule({
  declarations: [AlertaValidacionCampoFormularioComponent],
  imports: [
    CommonModule
  ],
  exports: [
    AlertaValidacionCampoFormularioComponent
  ]
})
export class AlertaValidacionCampoFormularioModule { }
