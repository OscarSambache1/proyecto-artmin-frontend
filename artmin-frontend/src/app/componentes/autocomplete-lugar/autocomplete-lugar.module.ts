import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteLugarComponent } from './autocomplete-lugar.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteLugarComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteLugarComponent
  ]
})
export class AutocompleteLugarModule { }
