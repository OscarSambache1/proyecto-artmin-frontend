import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteLugarComponent } from './autocomplete-lugar.component';

describe('AutocompleteLugarComponent', () => {
  let component: AutocompleteLugarComponent;
  let fixture: ComponentFixture<AutocompleteLugarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteLugarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteLugarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
