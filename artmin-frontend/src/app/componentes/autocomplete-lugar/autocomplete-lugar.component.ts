import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LugarInterface} from '../../interfaces/lugar.interface';
import {LugarRestService} from '../../servicios/rest/lugar-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';
import {TipoVideoInterface} from '../../interfaces/tipo-video.interface';

@Component({
  selector: 'app-autocomplete-lugar',
  templateUrl: './autocomplete-lugar.component.html',
  styleUrls: ['./autocomplete-lugar.component.css']
})
export class AutocompleteLugarComponent implements OnInit {

  @Input()
  lugarSeleccionado: LugarInterface;

  @Input()
  tiposAFiltrar: string[] = [];

  @Input()
  idsLugarAsignado: number[] = [];

  arregloLugares: LugarInterface[];

  @Input()
  deshabilitar: boolean;

  @Input()
  idLugarPadre: number;

  @Output()
  emitirLugarSeleccionado: EventEmitter<LugarInterface> = new EventEmitter<LugarInterface>();

  constructor(
    private readonly _lugarRestService: LugarRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    let consulta = {};
    if (this.idsLugarAsignado) {
      consulta = {
        where: {
          id: this.idsLugarAsignado
        }
      };
      this._lugarRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaLugar: [LugarInterface[], number]) => {
            this.lugarSeleccionado = respuestaLugar[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarLugares(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda,
        }
      ],
      relations: [
        'imagenesLugar',
        'lugarPadre',
        'lugarPadre.imagenesLugar'
      ]
    };
    this._lugarRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaLugar: [LugarInterface[], number]) => {
          this.arregloLugares = respuestaLugar[0]
            .filter( lugar => {
              return (this.tiposAFiltrar.find( tipo => tipo === lugar.tipo));
            });
          if (this.idLugarPadre) {
            this.arregloLugares = this.arregloLugares.filter(lugar => {
              return (lugar.lugarPadre as LugarInterface).id === this.idLugarPadre;
            });
          }
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarLugarSeleccionado(lugarSeleccionado: LugarInterface) {
    this.emitirLugarSeleccionado.emit(lugarSeleccionado);

  }

  limpiarBusqueda() {
    this.emitirLugarSeleccionado.emit(undefined);
  }

  obtenerUrlPrincipal(lugar: LugarInterface): string {
    if (lugar.imagenesLugar && lugar.imagenesLugar.length) {
      return obtenerUrlImagenPrincipal(lugar, 'imagenesLugar');
    }
  }
}
