import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GeneroInterface} from '../../interfaces/genero.interface';

@Component({
  selector: 'app-select-tipo-chart',
  templateUrl: './select-tipo-chart.component.html',
  styleUrls: ['./select-tipo-chart.component.css']
})
export class SelectTipoChartComponent implements OnInit {
  tiposChart = [
    {
      label: 'album',
      value: 'album'
    },
    {
      label: 'cancion',
      value: 'cancion'
    },
    {
      label: 'video',
      value: 'video'
    },
  ];

  @Input()
  tipoChartSeleccionado: string;

  @Input()
  tipo: string;

  @Output()
  emitirTipoChartSleccionado: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {

      if (this.tipo === 'C') {
        this.tiposChart =  [this.tiposChart[1]];
      }
      if (this.tipo === 'AL') {
        this.tiposChart =  [this.tiposChart[0]];
      }
      if (this.tipo === 'V') {
        this.tiposChart =  [this.tiposChart[2]];
      }
      if (this.tipo) {
        this.tipoChartSeleccionado = this.tiposChart[0].value;
      }
  }

  enviarTipoChartSeleccionado(tipoSeleccionado) {
    this.emitirTipoChartSleccionado.emit(tipoSeleccionado.value);
  }
}
