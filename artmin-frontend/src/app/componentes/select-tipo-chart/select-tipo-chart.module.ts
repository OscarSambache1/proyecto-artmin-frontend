import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectTipoChartComponent } from './select-tipo-chart.component';
import {FormsModule} from '@angular/forms';
import {DropdownModule} from 'primeng';



@NgModule({
  declarations: [SelectTipoChartComponent],
  imports: [
    CommonModule,
    FormsModule,
    DropdownModule
  ],
  exports: [
    SelectTipoChartComponent
  ]
})
export class SelectTipoChartModule { }
