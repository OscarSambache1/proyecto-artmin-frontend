import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectTipoChartComponent } from './select-tipo-chart.component';

describe('SelectTipoChartComponent', () => {
  let component: SelectTipoChartComponent;
  let fixture: ComponentFixture<SelectTipoChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTipoChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectTipoChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
