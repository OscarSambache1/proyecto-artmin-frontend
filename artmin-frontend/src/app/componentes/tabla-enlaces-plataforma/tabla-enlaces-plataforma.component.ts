import {Component, Input, OnInit} from '@angular/core';
import {EnlaceAlbumCancionArtistaVideoInterface} from '../../interfaces/enlace-album-cancion-artista-video.interface';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';
import {ImagenInterface} from '../../interfaces/imagen.interface';
import {PlataformaInterface} from '../../interfaces/plataforma.interface';

@Component({
  selector: 'app-tabla-enlaces-plataforma',
  templateUrl: './tabla-enlaces-plataforma.component.html',
  styleUrls: ['./tabla-enlaces-plataforma.component.css']
})
export class TablaEnlacesPlataformaComponent implements OnInit {

  cols = [
    {
      field: 'plataforma',
      header: 'Plataforma',
      width: '20%'
    },
    {
      field: 'tipo',
      header: 'Tipo',
      width: '20%'
    },
    {
      field: 'seguidores',
      header: 'Seguidores',
      width: '20%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '20%'
    }
  ];

  @Input()
  enlaces: EnlaceAlbumCancionArtistaVideoInterface[] = [];
  items: any;

  constructor() {
    this.items = [
      {label: 'Angular.io', icon: 'pi pi-info', url: 'http://angular.io'},
      {separator: true},
      {label: 'Setup', icon: 'pi pi-cog', routerLink: ['/setup']}
    ];
  }

  ngOnInit(): void {
  }

  obtenerUrlPrincipal(plataforma: PlataformaInterface) {
    if (plataforma.imagenesPlataforma && plataforma.imagenesPlataforma.length) {
      return obtenerUrlImagenPrincipal(plataforma, 'imagenesPlataforma');
    }
  }
}
