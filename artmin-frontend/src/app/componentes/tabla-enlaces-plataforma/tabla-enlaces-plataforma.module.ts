import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaEnlacesPlataformaComponent } from './tabla-enlaces-plataforma.component';
import {SplitButtonModule, TableModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [TablaEnlacesPlataformaComponent],
  imports: [
    CommonModule,
    TableModule,
    FormsModule,
    SplitButtonModule
  ],
  exports: [
    TablaEnlacesPlataformaComponent
  ]
})
export class TablaEnlacesPlataformaModule { }
