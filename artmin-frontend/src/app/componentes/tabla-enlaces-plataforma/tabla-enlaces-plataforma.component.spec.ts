import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaEnlacesPlataformaComponent } from './tabla-enlaces-plataforma.component';

describe('TablaEnlacesPlataformaComponent', () => {
  let component: TablaEnlacesPlataformaComponent;
  let fixture: ComponentFixture<TablaEnlacesPlataformaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaEnlacesPlataformaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaEnlacesPlataformaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
