import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-carousel-opciones',
  templateUrl: './carousel-opciones.component.html',
  styleUrls: ['./carousel-opciones.component.css']
})
export class CarouselOpcionesComponent implements OnInit {

  @Input()
  opciones: any;

  @Output()
  emitirEvento: EventEmitter<null> = new EventEmitter<null>();

  constructor() {
  }

  ngOnInit(): void {
  }

  irAGestion(texto: any) {
    this.emitirEvento.emit(texto);
  }
}
