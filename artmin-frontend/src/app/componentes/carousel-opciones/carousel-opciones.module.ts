import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselOpcionesComponent } from './carousel-opciones.component';
import {CarouselModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [CarouselOpcionesComponent],
  imports: [
    CommonModule,
    CarouselModule,
    FormsModule
  ],
  exports: [
    CarouselOpcionesComponent
  ]
})
export class CarouselOpcionesModule { }
