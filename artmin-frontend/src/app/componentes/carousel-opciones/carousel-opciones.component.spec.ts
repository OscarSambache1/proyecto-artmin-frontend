import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselOpcionesComponent } from './carousel-opciones.component';

describe('CarouselOpcionesComponent', () => {
  let component: CarouselOpcionesComponent;
  let fixture: ComponentFixture<CarouselOpcionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselOpcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselOpcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
