import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaskedInputDirective} from 'angular2-text-mask';



@NgModule({
  declarations: [MaskedInputDirective],
  imports: [
    CommonModule
  ],
  exports: [
    MaskedInputDirective,
  ]
})
export class MaskinputModuleModule { }
