import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-input-imagen',
  templateUrl: './input-imagen.component.html',
  styleUrls: ['./input-imagen.component.css']
})
export class InputImagenComponent implements OnInit {
  imagen: any;

  @Input()
  pathImagen: string;

  @Input()
  width = 100;

  @Input()
  height = 'auto';

  @Input()
  ocultarInput: boolean;

  @Output()
  emitirImagen: EventEmitter<any> = new EventEmitter<any>();

  ngOnInit(): void {
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
  ) {
  }

  onBasicUploadAuto(eventoImagen: any) {
    const fileList: FileList = eventoImagen.target.files;
    if (eventoImagen.target.files && eventoImagen.target.files[0]) {
      this.imagen = fileList[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.pathImagen = event.target.result;
      };
      reader.readAsDataURL(eventoImagen.target.files[0]);
    } else {
      this.pathImagen = null;
      this.imagen = null;
    }
    this.emitirImagen.emit(this.imagen);
  }
}
