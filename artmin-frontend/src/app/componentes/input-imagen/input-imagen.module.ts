import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InputImagenComponent} from './input-imagen.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ScrollPanelModule} from 'primeng';

@NgModule({
  declarations: [InputImagenComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ScrollPanelModule
  ],
  exports: [
    InputImagenComponent
  ]
})
export class InputImagenModule {
}
