import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteCategoriaComponent } from './autocomplete-categoria.component';

describe('AutocompleteCategoriaComponent', () => {
  let component: AutocompleteCategoriaComponent;
  let fixture: ComponentFixture<AutocompleteCategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteCategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteCategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
