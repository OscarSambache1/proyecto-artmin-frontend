import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteCategoriaComponent } from './autocomplete-categoria.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
    declarations: [AutocompleteCategoriaComponent],
    exports: [
        AutocompleteCategoriaComponent
    ],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ]
})
export class AutocompleteCategoriaModule { }
