import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {CategoriaInterface} from '../../interfaces/categoria.interface';
import {CategoriaRestService} from '../../servicios/rest/categoria-rest.service';

@Component({
  selector: 'app-autocomplete-categoria',
  templateUrl: './autocomplete-categoria.component.html',
  styleUrls: ['./autocomplete-categoria.component.css']
})
export class AutocompleteCategoriaComponent implements OnInit {

  categoriaSeleccionada: CategoriaInterface | CategoriaInterface[];
  arregloCategorias: CategoriaInterface[] = [];

  @Output()
  emitirCategoriaSeleccionada: EventEmitter<CategoriaInterface | CategoriaInterface[]> = new EventEmitter<CategoriaInterface | CategoriaInterface[]>();

  @Input()
  idCategoria: number;

  @Input()
  tipo: string[];

  @Input()
  idPremio: number;

  @Input()
  arregloidsCategoria: number[];

  @Input()
  multiple: boolean;

  @Input()
  deshabilitar: boolean;

  constructor(
    private readonly _categoriaRestService: CategoriaRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idCategoria || (this.arregloidsCategoria && this.arregloidsCategoria.length)) {
      let consulta;
      if (this.idCategoria) {
        consulta = {
          where: {
            id: this.idCategoria
          },
        };
      } else {
        if (this.arregloidsCategoria && this.arregloidsCategoria.length) {
          consulta = {
            camposIn: [
              {
                nombreCampo: 'id',
                valor: this.arregloidsCategoria
              }
            ],
            where: {
              premio: this.idPremio,
            }
          };
        }

      }
      this._categoriaRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((categoria: [CategoriaInterface[], number]) => {
          if (!this.multiple) {
            this.categoriaSeleccionada = categoria[0][0];
          } else {
            this.categoriaSeleccionada = categoria[0];
          }
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  enviarCategoriaSeleccionada(categoriaSeleccionada: CategoriaInterface | CategoriaInterface[]) {
    if (!this.multiple) {
      this.emitirCategoriaSeleccionada.emit(categoriaSeleccionada);
    } else {
      this.emitirCategoriaSeleccionada.emit(this.categoriaSeleccionada);
    }
  }

  filtrarCategoria(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      where: {
        premio: this.idPremio,
      },
      relations: ['premio']
    };
    this._categoriaRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaCaategoria: [CategoriaInterface[], number]) => {
          this.arregloCategorias = respuestaCaategoria[0]
            .filter(categoria => categoria.premio?.id === this.idPremio);
          if (this.tipo?.length) {
            this.arregloCategorias = this.arregloCategorias
              .filter(categoria => this.tipo.includes(categoria.tipo));
          }
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  limpiarBusqueda() {
    this.emitirCategoriaSeleccionada.emit(undefined);
  }

  deseleccionarCategoria() {
    this.emitirCategoriaSeleccionada.emit(this.categoriaSeleccionada);
  }

}
