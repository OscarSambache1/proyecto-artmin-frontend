import {Component, OnInit} from '@angular/core';
import {TourInterface} from '../../interfaces/tour.interface';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {TourRestService} from '../../servicios/rest/tour-rest.service';
import {ArtistaRestService} from '../../servicios/rest/artista-rest.service';
import {CargandoService} from '../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {RUTAS_TOUR} from '../../modulos/tour/rutas/definicion-rutas/rutas-tour';
import {RUTAS_ARTISTA} from '../../modulos/artista/rutas/definicion-rutas/rutas-artista';
import {NominacionArtistaInterface} from '../../interfaces/nominacion-artista.interface';
import {NominacionArtistaRestService} from '../../servicios/rest/nominacion-artista-rest.service';
import {NominacionInterface} from '../../interfaces/nominacion.interface';
import {AlbumInterface} from '../../interfaces/album.interface';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {VideoInterface} from '../../interfaces/video.interface';
import {PremioInterface} from '../../interfaces/premio.interface';
import {PremioAnioInterface} from '../../interfaces/premio-anio.interface';
import {CategoriaInterface} from '../../interfaces/categoria.interface';
import {ModalNominacionComponent} from '../../modulos/premio/modales/modal-nominacion/modal-nominacion.component';
import {ModalSeleccionarGanadoresComponent} from '../../modulos/premio/modales/modal-seleccionar-ganadores/modal-seleccionar-ganadores.component';
import {RUTAS_CANCION} from '../../modulos/cancion/rutas/definicion-rutas/rutas-cancion';
import {CancionRestService} from '../../servicios/rest/cancion-rest.service';
import {AlbumRestService} from '../../servicios/rest/album-rest.service';
import {VideoRestService} from '../../servicios/rest/video-rest.service';
import {RUTAS_ALBUM} from '../../modulos/album/rutas/definicion-rutas/rutas-album';
import {RUTAS_VIDEO} from '../../modulos/video/rutas/definicion-rutas/rutas-videos';

@Component({
  selector: 'app-ruta-nominaciones',
  templateUrl: './ruta-nominaciones.component.html',
  styleUrls: ['./ruta-nominaciones.component.css']
})
export class RutaNominacionesComponent implements OnInit {
  nominaciones: NominacionInterface[] = [];
  columnas = [
    {
      field: 'premio',
      header: 'Premio',
      width: '20%'
    },
    {
      field: 'premioAnio',
      header: 'Año',
      width: '10%'
    },
    {
      field: 'categoria',
      header: 'Categoría',
      width: '30%'
    },
    {
      field: 'nominaciones',
      header: 'Nominaciones',
      width: '50%'
    },
    {
      field: 'ganadores',
      header: 'Ganadores',
      width: '20%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '20%'
    },
  ];

  busqueda = '';
  idArtista: number;
  idCancion: number;
  idAlbum: number;
  idVideo: number;
  idTour: number;
  queryParams: any = {};
  ruta;
  artista: ArtistaInterface;
  cancion: CancionInterface;
  album: AlbumInterface;
  video: VideoInterface;
  tour: TourInterface;
  rutaImagen = 'assets/imagenes/premio.svg';
  tipo: string;
  arregloRutas: any[];
  rowGroupMetadataPremio: any;
  rowGroupMetadataPremioAnio: any;
  idPremio = 0;
  idCategoria = 0;
  idPremioAnio = 0;
  // opcionesTipoTour = TIPOS_TOUR;
  //
  // tipoTourSeleccionado: any;
  constructor(
    private readonly _nominacionArtistaRestService: NominacionArtistaRestService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cancionRestService: CancionRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _videoRestService: VideoRestService,
    private readonly _tourRestService: TourRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtista = params.idArtista ? +params.idArtista : 0;
          this.idCancion = params.idCancion ? +params.idCancion : 0;
          this.idAlbum = params.idAlbum ? +params.idAlbum : 0;
          this.idVideo = params.idVideo ? +params.idVideo : 0;
          this.idTour = params.idTour ? +params.idTour : 0;
          this.tipo = params.tipo;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          if (this.idArtista && this.tipo === 'A') {
            this._artistaRestService.findOne(this.idArtista)
              .subscribe(
                (artista: ArtistaInterface) => {
                  this.artista = artista;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (this.idCancion && this.tipo === 'C') {
            this._cancionRestService.findOne(this.idCancion)
              .subscribe(
                (cancion: CancionInterface) => {
                  this.cancion = cancion;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (this.idAlbum && this.tipo === 'AL') {
            this._albumRestService.findOne(this.idAlbum)
              .subscribe(
                (album: AlbumInterface) => {
                  this.album = album;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (this.idVideo && this.tipo === 'V') {
            this._videoRestService.findOne(this.idVideo)
              .subscribe(
                (video: VideoInterface) => {
                  this.video = video;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (this.idTour && this.tipo === 'T') {
            this._tourRestService.findOne(this.idTour)
              .subscribe(
                (tour: TourInterface) => {
                  this.tour = tour;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          this.queryParams = {
            idPremio: queryParams.idPremio,
            idPremioAnio: queryParams.idPremioAnio,
            idCategoria: queryParams.idCategoria,
          };
          this.idPremio = +this.queryParams.idPremio || 0;
          this.idPremioAnio = +this.queryParams.idPremioAnio || 0;
          this.idCategoria = +this.queryParams.idCategoria || 0;
          const datos = {
            idArtista: this.idArtista,
            idCancion: this.idCancion,
            idAlbum: this.idAlbum,
            idVideo: this.idVideo,
            idTour: this.idTour,
            idPremio: this.idPremio,
            idCategoria: this.idCategoria,
            idPremioAnio: this.idPremioAnio,
            tipo: this.tipo,
          };
          this._cargandoService.habilitarCargando();
          return this._nominacionArtistaRestService.obtenerNominaciones(
            JSON.stringify(datos)
          );
        })
      )
      .subscribe(
        (respuestaNominaciones: [NominacionInterface[], number]) => {
          this.nominaciones = respuestaNominaciones[0];
          this.actualizarTablaPorPremio();
          this.actualizarTablaPorPremioAnio();
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  setearArregloRutasMigasPan() {
    if (this.tipo === 'A') {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
        RUTAS_ARTISTA.rutaNominaciones(false, true, [this.idArtista])
      ];
    }
    if (this.tipo === 'C') {
      if (this.idArtista) {
        this.arregloRutas = [
          RUTAS_ARTISTA.rutaGestionArtistas(false, true),
          RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista, this.idCancion]),
          RUTAS_ARTISTA.rutaNominaciones(false, true, [this.idArtista, this.idCancion])
        ];
      } else {
        this.arregloRutas = [
          RUTAS_CANCION.rutaGestionCanciones(false, true),
          RUTAS_CANCION.rutaEditarCancion(false, true, [0, this.idCancion]),
          RUTAS_ARTISTA.rutaNominaciones(false, true, [0, this.idCancion])
        ];
      }
    }

    if (this.tipo === 'AL') {
      if (this.idArtista) {
        this.arregloRutas = [
          RUTAS_ARTISTA.rutaGestionArtistas(false, true),
          RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idArtista, this.idAlbum]),
          RUTAS_ALBUM.rutaNominaciones(false, true, [this.idArtista, this.idAlbum])
        ];
      } else {
        this.arregloRutas = [
          RUTAS_ALBUM.rutaGestionAlbumes(false, true),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [0, this.idAlbum]),
          RUTAS_ALBUM.rutaNominaciones(false, true, [0, this.idAlbum])
        ];
      }

    }

    if (this.tipo === 'T') {
      if (this.idArtista) {
        this.arregloRutas = [
          RUTAS_ARTISTA.rutaGestionArtistas(false, true),
          RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
          RUTAS_TOUR.rutaGestionTour(false, true, [this.idArtista]),
          RUTAS_TOUR.rutaEditarTour(false, true, [this.idArtista, this.idAlbum]),
          RUTAS_TOUR.rutaNominaciones(false, true, [this.idArtista, this.idAlbum])
        ];
      } else {
        this.arregloRutas = [
          RUTAS_TOUR.rutaGestionTour(false, true),
          RUTAS_TOUR.rutaEditarTour(false, true, [0, this.idAlbum]),
          RUTAS_TOUR.rutaNominaciones(false, true, [0, this.idAlbum])
        ];
      }

    }

    if (this.tipo === 'V') {
      this.arregloRutas = [
        RUTAS_VIDEO.rutaGestionVideos(false, true),
        RUTAS_VIDEO.rutaNominaciones(false, true, [this.idVideo])
      ];

    }
  }

  seteoRutas() {
    if (this.tipo === 'A') {
      this.ruta = RUTAS_ARTISTA
        .rutaNominaciones(
          true,
          false,
          [this.idArtista, this.tipo]
        );
    }
    if (this.tipo === 'C') {
      this.ruta = RUTAS_CANCION
        .rutaNominaciones(
          true,
          false,
          [this.idArtista, this.idCancion, this.tipo]
        );
    }
    if (this.tipo === 'AL') {
      this.ruta = RUTAS_ALBUM
        .rutaNominaciones(
          true,
          false,
          [this.idArtista, this.idAlbum, this.tipo]
        );
    }
    if (this.tipo === 'T') {
      this.ruta = RUTAS_TOUR
        .rutaNominaciones(
          true,
          false,
          [this.idArtista, this.idTour, this.tipo]
        );
    }
    if (this.tipo === 'V') {
      this.ruta = RUTAS_VIDEO
        .rutaNominaciones(
          true,
          false,
          [this.idVideo, this.tipo]
        );
    }
  }

  abrirModalCrearEditarNominacion(nominacion?: any) {
    const dialogRef = this.dialog
      .open(
        ModalNominacionComponent,
        {
          width: '1000px',
          data: {
            nominacion,
            mostrarPremio: true,
            tipos: this.setearTipo(),
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (res: any) => {
          this._router
            .navigate(
              this.ruta,
              {
                queryParams: {aux: 0}
              }
            );
        },
        error => {
          console.error(error);
        },
      );
  }

  abrirModalSeleccionarGanador(rowData: any) {
    const dialogRef = this.dialog
      .open(
        ModalSeleccionarGanadoresComponent,
        {
          width: '1000px',
          data: {
            nominacion: rowData,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (res: any) => {
          this._router
            .navigate(
              this.ruta,
              {
                queryParams: {aux: 0}
              }
            );

        },
        error => {
          console.error(error);
        },
      );
  }

  setearNominacion(nominacion: NominacionArtistaInterface) {
    if (nominacion.artista) {
      return (nominacion.artista as ArtistaInterface)?.nombre;
    }
    if (nominacion.album) {
      return (nominacion.album as AlbumInterface)?.nombre;
    }
    if (nominacion.cancion) {
      return (nominacion.cancion as CancionInterface)?.nombre;
    }
    if (nominacion.video) {
      return (nominacion.video as VideoInterface)?.nombre;
    }
    if (nominacion.tour) {
      return (nominacion.tour as TourInterface)?.nombre;
    }
  }

  actualizarTablaPorPremio() {
    this.rowGroupMetadataPremio = {};
    if (this.nominaciones) {
      for (let i = 0; i < this.nominaciones.length; i++) {
        const rowData = this.nominaciones[i];
        const idPremio = (rowData.premioAnio?.premio as PremioInterface)?.id;
        if (i === 0) {
          this.rowGroupMetadataPremio[idPremio] = {index: 0, size: 1};
        } else {
          const previousRowData = this.nominaciones[i - 1];
          const previousRowGroup = (previousRowData.premioAnio?.premio as PremioInterface)?.id;
          if (idPremio === previousRowGroup) {
            this.rowGroupMetadataPremio[idPremio].size++;
          } else {
            this.rowGroupMetadataPremio[idPremio] = {index: i, size: 1};
          }
        }
      }
    }
  }

  actualizarTablaPorPremioAnio() {
    this.rowGroupMetadataPremioAnio = {};
    if (this.nominaciones) {
      for (let i = 0; i < this.nominaciones.length; i++) {
        const rowData = this.nominaciones[i];
        const idPremioAnio = (rowData.premioAnio)?.id;
        if (i === 0) {
          this.rowGroupMetadataPremioAnio[idPremioAnio] = {index: 0, size: 1};
        } else {
          const previousRowData = this.nominaciones[i - 1];
          const previousRowGroup = (previousRowData.premioAnio)?.id;
          if (idPremioAnio === previousRowGroup) {
            this.rowGroupMetadataPremioAnio[idPremioAnio].size++;
          } else {
            this.rowGroupMetadataPremioAnio[idPremioAnio] = {index: i, size: 1};
          }
        }
      }
    }
  }

  buscarNominacionesPorPremio(premio: PremioInterface) {
    const consulta = {
      ...this.queryParams,
      idPremio: premio?.id || 0,
    };
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: consulta
        }
      );
  }

  buscarNominacionesPorPremioAnio(premioAnio: PremioAnioInterface) {
    const consulta = {
      ...this.queryParams,
      idPremioAnio: premioAnio?.id || 0,
    };
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: consulta
        }
      );

  }

  buscarNominacionesPorCategoria(categoria: CategoriaInterface) {
    const consulta = {
      ...this.queryParams,
      idCategoria: categoria?.id || 0,
    };
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: consulta
        }
      );

  }

  setearObj() {
    if (this.tipo === 'A') {
      return this.artista?.nombre;
    }
    if (this.tipo === 'C') {
      return this.cancion?.nombre;
    }
    if (this.tipo === 'AL') {
      return this.album?.nombre;
    }
    if (this.tipo === 'V') {
      return this.video?.nombre;
    }
    if (this.tipo === 'T') {
      return this.tour?.nombre;
    }
    return '';
  }

  setearTipo() {
    if (this.tipo === 'C') {
      return ['Cancion', 'Video'];
    }
    if (this.tipo === 'AL') {
      return ['Cancion', 'Album'];
    }
    if (this.tipo === 'V') {
      return ['Video'];
    }
    if (this.tipo === 'T') {
      return ['Tour'];
    }
    return null;
  }

  buscarNominaciones() {
    const datos = {
      idArtista: this.idArtista,
      idCancion: this.idCancion,
      idAlbum: this.idAlbum,
      idVideo: this.idVideo,
      idTour: this.idTour,
      idPremio: this.idPremio,
      idCategoria: this.idCategoria,
      idPremioAnio: this.idPremioAnio,
      tipo: this.tipo,
    };
    this._cargandoService.habilitarCargando();
    this._nominacionArtistaRestService.obtenerNominaciones(
      JSON.stringify(datos))
      .subscribe(
        (respuestaNominaciones: [NominacionInterface[], number]) => {
          this.nominaciones = respuestaNominaciones[0];
          this.actualizarTablaPorPremio();
          this.actualizarTablaPorPremioAnio();
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }
}
