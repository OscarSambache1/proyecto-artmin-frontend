import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RutaNominacionesComponent } from './ruta-nominaciones.component';
import {MigasPanModule} from '../migas-pan/migas-pan.module';
import {TableModule} from 'primeng';
import {AutocompletePremioModule} from '../autocomplete-premio/autocomplete-premio.module';
import {AutocompletePremioAnioModule} from '../autocomplete-premio-anio/autocomplete-premio-anio.module';
import {AutocompleteCategoriaModule} from '../autocomplete-categoria/autocomplete-categoria.module';
import {InputBuscarBotonModule} from '../input-buscar-boton/input-buscar-boton.module';
import {BotonNuevoModule} from '../boton-nuevo/boton-nuevo.module';
import {ModalNominacionModule} from '../../modulos/premio/modales/modal-nominacion/modal-nominacion.module';
import {ModalNominacionComponent} from '../../modulos/premio/modales/modal-nominacion/modal-nominacion.component';
import {ModalSeleccionarGanadoresModule} from '../../modulos/premio/modales/modal-seleccionar-ganadores/modal-seleccionar-ganadores.module';
import {ModalSeleccionarGanadoresComponent} from '../../modulos/premio/modales/modal-seleccionar-ganadores/modal-seleccionar-ganadores.component';



@NgModule({
  declarations: [RutaNominacionesComponent],
  imports: [
    CommonModule,
    MigasPanModule,
    TableModule,
    AutocompletePremioModule,
    AutocompletePremioAnioModule,
    AutocompleteCategoriaModule,
    InputBuscarBotonModule,
    BotonNuevoModule,
    ModalNominacionModule,
    ModalSeleccionarGanadoresModule
  ],
  exports: [RutaNominacionesComponent],
  entryComponents: [ModalNominacionComponent, ModalSeleccionarGanadoresComponent]
})
export class RutaNominacionesModule { }
