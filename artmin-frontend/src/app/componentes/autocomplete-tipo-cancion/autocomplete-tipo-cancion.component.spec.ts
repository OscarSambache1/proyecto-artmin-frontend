import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteTipoCancionComponent } from './autocomplete-tipo-cancion.component';

describe('AutocompleteTipoCancionComponent', () => {
  let component: AutocompleteTipoCancionComponent;
  let fixture: ComponentFixture<AutocompleteTipoCancionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteTipoCancionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteTipoCancionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
