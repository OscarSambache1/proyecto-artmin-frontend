import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TipoCancionInterface} from '../../interfaces/tipo-cancion.interface';
import {TipoCancionRestService} from '../../servicios/rest/tipo-cancion-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';

@Component({
  selector: 'app-autocomplete-tipo-cancion',
  templateUrl: './autocomplete-tipo-cancion.component.html',
  styleUrls: ['./autocomplete-tipo-cancion.component.css']
})
export class AutocompleteTipoCancionComponent implements OnInit {

  @Input()
  idTipoCancion: number;

  @Input()
  deshabilitar: boolean;

  @Output()
  emitirTipoCancionSeleccionado: EventEmitter<TipoCancionInterface> = new EventEmitter<TipoCancionInterface>();

  tipoCancionSeleccionado: TipoCancionInterface;
  arregloTiposCancion: TipoCancionInterface[];

  constructor(
    private readonly _tipoCancionService: TipoCancionRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idTipoCancion) {
      const consulta = {
        where: {
          id: this.idTipoCancion
        }
      };
      this._tipoCancionService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaTipoCancion: [TipoCancionInterface[], number]) => {
            this.tipoCancionSeleccionado = respuestaTipoCancion[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarTiposCancion(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
    };
    this._tipoCancionService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaTipoCancion: [TipoCancionInterface[], number]) => {
          this.arregloTiposCancion = respuestaTipoCancion[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarTipoCancionSeleccionado(tipoCancionSeleccionado: TipoCancionInterface) {
    this.emitirTipoCancionSeleccionado.emit(tipoCancionSeleccionado);
  }

  limpiarBusqueda() {
    this.emitirTipoCancionSeleccionado.emit(undefined);

  }

  deseleccionarTipoCancion() {
    this.emitirTipoCancionSeleccionado.emit(this.tipoCancionSeleccionado);
  }
}
