import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteTipoCancionComponent } from './autocomplete-tipo-cancion.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteTipoCancionComponent],
  exports: [
    AutocompleteTipoCancionComponent
  ],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ]
})
export class AutocompleteTipoCancionModule { }
