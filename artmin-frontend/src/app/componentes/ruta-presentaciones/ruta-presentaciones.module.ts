import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RutaPresentacionesComponent } from './ruta-presentaciones.component';
import {MigasPanModule} from '../migas-pan/migas-pan.module';
import {AutocompletePremioModule} from '../autocomplete-premio/autocomplete-premio.module';
import {AutocompletePremioAnioModule} from '../autocomplete-premio-anio/autocomplete-premio-anio.module';
import {BotonNuevoModule} from '../boton-nuevo/boton-nuevo.module';
import {TableModule} from 'primeng';
import {ModalPresentacionModule} from '../../modulos/premio/modales/modal-presentacion/modal-presentacion.module';



@NgModule({
  declarations: [RutaPresentacionesComponent],
  imports: [
    CommonModule,
    MigasPanModule,
    AutocompletePremioModule,
    AutocompletePremioAnioModule,
    BotonNuevoModule,
    TableModule,
    ModalPresentacionModule,
  ]
})
export class RutaPresentacionesModule { }
