import {Component, OnInit} from '@angular/core';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {AlbumInterface} from '../../interfaces/album.interface';
import {ArtistaRestService} from '../../servicios/rest/artista-rest.service';
import {CancionRestService} from '../../servicios/rest/cancion-rest.service';
import {AlbumRestService} from '../../servicios/rest/album-rest.service';
import {CargandoService} from '../../servicios/cargando-service';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {GeneroRestService} from '../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';
import {mergeMap} from 'rxjs/operators';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {RUTAS_ARTISTA} from '../../modulos/artista/rutas/definicion-rutas/rutas-artista';
import {PremioInterface} from '../../interfaces/premio.interface';
import {PremioAnioInterface} from '../../interfaces/premio-anio.interface';
import {CategoriaInterface} from '../../interfaces/categoria.interface';
import {PresentacionInterface} from '../../interfaces/presentacion.interface';
import {PresentacionRestService} from '../../servicios/rest/presentacion-rest.service';
import {ModalPresentacionComponent} from '../../modulos/premio/modales/modal-presentacion/modal-presentacion.component';
import {RUTAS_CANCION} from '../../modulos/cancion/rutas/definicion-rutas/rutas-cancion';
import {RUTAS_ALBUM} from '../../modulos/album/rutas/definicion-rutas/rutas-album';

@Component({
  selector: 'app-ruta-presentaciones',
  templateUrl: './ruta-presentaciones.component.html',
  styleUrls: ['./ruta-presentaciones.component.css']
})
export class RutaPresentacionesComponent implements OnInit {

  presentaciones: PresentacionInterface[] = [];
  columnas = [
    {
      field: 'premio',
      header: 'Premio',
      width: '20%'
    },
    {
      field: 'premioAnio',
      header: 'Año',
      width: '10%'
    },
    {
      field: 'artistas',
      header: 'Artistas',
      width: '40%'
    },
    {
      field: 'canciones',
      header: 'Canciones',
      width: '40%'
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '20%'
    },
  ];

  busqueda = '';
  idArtista: number;
  idCancion: number;
  idAlbum: number;
  queryParams: any = {};
  ruta;
  artista: ArtistaInterface;
  cancion: CancionInterface;
  album: AlbumInterface;
  rutaImagen = 'assets/imagenes/evento.svg';
  tipo: string;
  arregloRutas: any[];
  rowGroupMetadataPremio: any;
  rowGroupMetadataPremioAnio: any;
  idPremio = 0;
  idCategoria = 0;
  idPremioAnio = 0;

  constructor(
    private readonly _presentacionRestService: PresentacionRestService,
    private readonly _artistaRestService: ArtistaRestService,
    private readonly _cancionRestService: CancionRestService,
    private readonly _albumRestService: AlbumRestService,
    private readonly _cargandoService: CargandoService,
    public dialog: MatDialog,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit() {
    this._activatedRoute
      .params
      .pipe(
        mergeMap(params => {
          this.idArtista = params.idArtista ? +params.idArtista : 0;
          this.idCancion = params.idCancion ? +params.idCancion : 0;
          this.idAlbum = params.idAlbum ? +params.idAlbum : 0;
          this.tipo = params.tipo;
          this.setearArregloRutasMigasPan();
          this.seteoRutas();
          return this._activatedRoute.queryParams;
        })
      )
      .pipe(
        mergeMap(queryParams => {
          if (this.idArtista && this.tipo === 'A') {
            this._artistaRestService.findOne(this.idArtista)
              .subscribe(
                (artista: ArtistaInterface) => {
                  this.artista = artista;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (this.idCancion && this.tipo === 'C') {
            this._cancionRestService.findOne(this.idCancion)
              .subscribe(
                (cancion: CancionInterface) => {
                  this.cancion = cancion;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          if (this.idAlbum && this.tipo === 'AL') {
            this._albumRestService.findOne(this.idAlbum)
              .subscribe(
                (album: AlbumInterface) => {
                  this.album = album;
                }, error => {
                  console.error(error);
                  this._toasterService.pop(ToastErrorCargandoDatos);
                }
              );
          }
          this.queryParams = {
            idPremio: queryParams.idPremio,
            idPremioAnio: queryParams.idPremioAnio,
            idCategoria: queryParams.idCategoria,
          };
          this.idPremio = +this.queryParams.idPremio || 0;
          this.idPremioAnio = +this.queryParams.idPremioAnio || 0;
          this.idCategoria = +this.queryParams.idCategoria || 0;
          const datos = {
            idArtista: this.idArtista,
            idCancion: this.idCancion,
            idAlbum: this.idAlbum,
            idPremio: this.idPremio,
            idCategoria: this.idCategoria,
            idPremioAnio: this.idPremioAnio,
            tipo: this.tipo,
          };
          this._cargandoService.habilitarCargando();
          return this._presentacionRestService.obtenerPresentaciones(
            JSON.stringify(datos)
          );
        })
      )
      .subscribe(
        (respuestaPresentacion: [PresentacionInterface[], number]) => {
          this.presentaciones = respuestaPresentacion[0];
          this.actualizarTablaPorPremio();
          this.actualizarTablaPorPremioAnio();
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }

  setearArregloRutasMigasPan() {
    if (this.tipo === 'A') {
      this.arregloRutas = [
        RUTAS_ARTISTA.rutaGestionArtistas(false, true),
        RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
        RUTAS_ARTISTA.rutaPresentaciones(false, true, [this.idArtista])
      ];
    }
    if (this.tipo === 'C') {
      if (this.idArtista) {
        this.arregloRutas = [
          RUTAS_ARTISTA.rutaGestionArtistas(false, true),
          RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaGestionCanciones(false, true, [this.idArtista]),
          RUTAS_CANCION.rutaEditarCancion(false, true, [this.idArtista, this.idCancion]),
          RUTAS_CANCION.rutaPresentaciones(false, true, [this.idArtista, this.idCancion])
        ];
      } else {
        this.arregloRutas = [
          RUTAS_CANCION.rutaGestionCanciones(false, true),
          RUTAS_CANCION.rutaEditarCancion(false, true, [0, this.idCancion]),
          RUTAS_CANCION.rutaPresentaciones(false, true, [0, this.idCancion])
        ];
      }
    }

    if (this.tipo === 'AL') {
      if (this.idArtista) {
        this.arregloRutas = [
          RUTAS_ARTISTA.rutaGestionArtistas(false, true),
          RUTAS_ARTISTA.rutaVerEditarArtistas(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaGestionAlbumes(false, true, [this.idArtista]),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [this.idArtista, this.idAlbum]),
          RUTAS_ALBUM.rutaPresentaciones(false, true, [this.idArtista, this.idAlbum])
        ];
      } else {
        this.arregloRutas = [
          RUTAS_ALBUM.rutaGestionAlbumes(false, true),
          RUTAS_ALBUM.rutaEditarAlbum(false, true, [0, this.idAlbum]),
          RUTAS_ALBUM.rutaPresentaciones(false, true, [0, this.idAlbum])
        ];
      }

    }
  }

  seteoRutas() {
    if (this.tipo === 'A') {
      this.ruta = RUTAS_ARTISTA
        .rutaPresentaciones(
          true,
          false,
          [this.idArtista, this.tipo]
        );
    }
    if (this.tipo === 'C') {
      this.ruta = RUTAS_CANCION
        .rutaPresentaciones(
          true,
          false,
          [this.idArtista, this.idCancion, this.tipo]
        );
    }
    if (this.tipo === 'AL') {
      this.ruta = RUTAS_ALBUM
        .rutaPresentaciones(
          true,
          false,
          [this.idArtista, this.idAlbum, this.tipo]
        );
    }

  }

  abrirModalCrearEditarPresentacion(presentacion?: any) {
    const dialogRef = this.dialog
      .open(
        ModalPresentacionComponent,
        {
          width: '1000px',
          data: {
            presentacion,
            idPremioAnio: this.idPremioAnio,
            mostrarPremio: true,
          },
        }
      );

    const resultadoModal$ = dialogRef.afterClosed();
    resultadoModal$
      .subscribe(
        (res: any) => {
          this.buscarPresentaciones();
        },
        error => {
          console.error(error);
        },
      );
  }

  actualizarTablaPorPremio() {
    this.rowGroupMetadataPremio = {};
    if (this.presentaciones) {
      for (let i = 0; i < this.presentaciones.length; i++) {
        const rowData = this.presentaciones[i];
        const idPremio = (rowData.premioAnio?.premio as PremioInterface)?.id;
        if (i === 0) {
          this.rowGroupMetadataPremio[idPremio] = {index: 0, size: 1};
        } else {
          const previousRowData = this.presentaciones[i - 1];
          const previousRowGroup = (previousRowData.premioAnio?.premio as PremioInterface)?.id;
          if (idPremio === previousRowGroup) {
            this.rowGroupMetadataPremio[idPremio].size++;
          } else {
            this.rowGroupMetadataPremio[idPremio] = {index: i, size: 1};
          }
        }
      }
    }
  }

  actualizarTablaPorPremioAnio() {
    this.rowGroupMetadataPremioAnio = {};
    if (this.presentaciones) {
      for (let i = 0; i < this.presentaciones.length; i++) {
        const rowData = this.presentaciones[i];
        const idPremioAnio = (rowData.premioAnio)?.id;
        if (i === 0) {
          this.rowGroupMetadataPremioAnio[idPremioAnio] = {index: 0, size: 1};
        } else {
          const previousRowData = this.presentaciones[i - 1];
          const previousRowGroup = (previousRowData.premioAnio)?.id;
          if (idPremioAnio === previousRowGroup) {
            this.rowGroupMetadataPremioAnio[idPremioAnio].size++;
          } else {
            this.rowGroupMetadataPremioAnio[idPremioAnio] = {index: i, size: 1};
          }
        }
      }
    }
  }

  buscarPresentacionesPorPremio(premio: PremioInterface) {
    const consulta = {
      ...this.queryParams,
      idPremio: premio?.id || 0,
    };
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: consulta
        }
      );
  }

  buscarPresentacionesPorPremioAnio(premioAnio: PremioAnioInterface) {
    const consulta = {
      ...this.queryParams,
      idPremioAnio: premioAnio?.id || 0,
    };
    this._router
      .navigate(
        this.ruta,
        {
          queryParams: consulta
        }
      );

  }

  setearObj() {
    if (this.tipo === 'A') {
      return this.artista?.nombre;
    }
    if (this.tipo === 'C') {
      return this.cancion?.nombre;
    }
    if (this.tipo === 'AL') {
      return this.album?.nombre;
    }
    return '';
  }

  setearTipo() {
    if (this.tipo === 'C') {
      return ['Cancion', 'Video'];
    }
    if (this.tipo === 'AL') {
      return ['Cancion', 'Album'];
    }
    return null;
  }

  buscarPresentaciones() {
    const datos = {
      idArtista: this.idArtista,
      idCancion: this.idCancion,
      idAlbum: this.idAlbum,
      idPremio: this.idPremio,
      idPremioAnio: this.idPremioAnio,
      tipo: this.tipo,
    };
    this._cargandoService.habilitarCargando();
    this._presentacionRestService.obtenerPresentaciones(
      JSON.stringify(datos)
    )
      .subscribe(
        (respuestaPresentacion: [PresentacionInterface[], number]) => {
          this.presentaciones = respuestaPresentacion[0];
          this.actualizarTablaPorPremio();
          this.actualizarTablaPorPremioAnio();
          this._cargandoService.deshabiltarCargando();
        }
        , error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
          this._cargandoService.deshabiltarCargando();
        }
      );
  }
}
