import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteArtistaComponent } from './autocomplete-artista.component';

describe('AutocompleteArtistaComponent', () => {
  let component: AutocompleteArtistaComponent;
  let fixture: ComponentFixture<AutocompleteArtistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteArtistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteArtistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
