import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {ArtistaRestService} from '../../servicios/rest/artista-rest.service';
import {ToasterService} from 'angular2-toaster';
import {environment} from '../../../environments/environment';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';

@Component({
  selector: 'app-autocomplete-artista',
  templateUrl: './autocomplete-artista.component.html',
  styleUrls: ['./autocomplete-artista.component.css']
})
export class AutocompleteArtistaComponent implements OnInit {
  artistaSeleccionado: ArtistaInterface | ArtistaInterface[];
  arregloArtistas: ArtistaInterface[] = [];

  @Output()
  emitirArtistaSeleccionado: EventEmitter<ArtistaInterface | ArtistaInterface[]> = new EventEmitter<ArtistaInterface | ArtistaInterface[]>();

  @Input()
  idArtista: number;

  @Input()
  arregloidsArtista: number[];

  @Input()
  multiple: boolean;

  @Input()
  deshabilitar: boolean;

  constructor(
    private readonly _artistaService: ArtistaRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idArtista || (this.arregloidsArtista && this.arregloidsArtista.length)) {
      let consulta;
      if (this.idArtista) {
        consulta = {
          where: {
            id: this.idArtista
          },
          relations: [
            'imagenesArtista'
          ]
        };
      } else {
        if (this.arregloidsArtista && this.arregloidsArtista.length) {
          consulta = {
            camposIn: [
              {
                nombreCampo: 'id',
                valor: this.arregloidsArtista
              }
            ],
            relations: [
              'imagenesArtista'
            ]
          };
        }

      }
      this._artistaService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((artista: [ArtistaInterface[], number]) => {
          if (!this.multiple) {
            this.artistaSeleccionado = artista[0][0];
          } else {
            this.artistaSeleccionado = artista[0];
          }
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  enviarArtistaSeleccionado(artistaSeleccionado: ArtistaInterface | ArtistaInterface[]) {
    if (!this.multiple) {
      this.emitirArtistaSeleccionado.emit(artistaSeleccionado);
    } else {
      this.emitirArtistaSeleccionado.emit(this.artistaSeleccionado);
    }
  }

  filtrarArtistas(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'imagenesArtista'
      ]
    };
    this._artistaService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaArtista: [ArtistaInterface[], number]) => {
          this.arregloArtistas = respuestaArtista[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  obtenerUrlPrincipal(artista: ArtistaInterface): string {
    return obtenerUrlImagenPrincipal(artista, 'imagenesArtista');
  }

  limpiarBusqueda() {
    this.emitirArtistaSeleccionado.emit(undefined);
  }

  deseleccionarArtista() {
    this.emitirArtistaSeleccionado.emit(this.artistaSeleccionado);
  }
}
