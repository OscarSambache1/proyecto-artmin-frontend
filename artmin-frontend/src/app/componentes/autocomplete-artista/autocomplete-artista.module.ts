import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteArtistaComponent } from './autocomplete-artista.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteArtistaComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteArtistaComponent
  ]
})
export class AutocompleteArtistaModule { }
