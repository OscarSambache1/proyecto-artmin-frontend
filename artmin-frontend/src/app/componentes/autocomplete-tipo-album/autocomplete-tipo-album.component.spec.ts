import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteTipoAlbumComponent } from './autocomplete-tipo-album.component';

describe('AutocompleteTipoAlbumComponent', () => {
  let component: AutocompleteTipoAlbumComponent;
  let fixture: ComponentFixture<AutocompleteTipoAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteTipoAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteTipoAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
