import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TipoAlbumInterface} from '../../interfaces/tipo-album.interface';
import {AlbumRestService} from '../../servicios/rest/album-rest.service';
import {ToasterService} from 'angular2-toaster';
import {TipoAlbumRestService} from '../../servicios/rest/tipo-album-rest.service';
import {AlbumInterface} from '../../interfaces/album.interface';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';

@Component({
  selector: 'app-autocomplete-tipo-album',
  templateUrl: './autocomplete-tipo-album.component.html',
  styleUrls: ['./autocomplete-tipo-album.component.css']
})
export class AutocompleteTipoAlbumComponent implements OnInit {
  @Input()
  idTipoAlbum: number;

  @Input()
  deshabilitar: boolean;

  @Output()
  emitirTipoAlbumSeleccionado: EventEmitter<TipoAlbumInterface> = new EventEmitter<TipoAlbumInterface>();

  tipoAlbumSeleccionado: TipoAlbumInterface;
  arregloTiposAlbum: TipoAlbumInterface[];

  constructor(
    private readonly _tipoAlbumService: TipoAlbumRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idTipoAlbum) {
      const consulta = {
        where: {
          id: this.idTipoAlbum
        }
      };
      this._tipoAlbumService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaTipoAlbum: [TipoAlbumInterface[], number]) => {
            this.tipoAlbumSeleccionado = respuestaTipoAlbum[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarTiposAlbum(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
    };
    this._tipoAlbumService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaTipoAlbum: [TipoAlbumInterface[], number]) => {
          this.arregloTiposAlbum = respuestaTipoAlbum[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarTipoAlbumSeleccionado(tipoAlbumSeleccionado: TipoAlbumInterface) {
    this.emitirTipoAlbumSeleccionado.emit(tipoAlbumSeleccionado);
  }

  limpiarBusqueda() {
    this.emitirTipoAlbumSeleccionado.emit(undefined);

  }

  deseleccionarTipoAlbum() {
    this.emitirTipoAlbumSeleccionado.emit(this.tipoAlbumSeleccionado);
  }
}
