import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteTipoAlbumComponent } from './autocomplete-tipo-album.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteTipoAlbumComponent],
  exports: [
    AutocompleteTipoAlbumComponent
  ],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ]
})
export class AutocompleteTipoAlbumModule { }
