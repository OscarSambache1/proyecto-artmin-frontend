import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MigasPanComponent } from './migas-pan.component';
import {BreadcrumbModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [MigasPanComponent],
  imports: [
    CommonModule,
    BreadcrumbModule,
    FormsModule
  ],
  exports: [
    MigasPanComponent
  ]
})
export class MigasPanModule { }
