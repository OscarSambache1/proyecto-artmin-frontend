import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MigasPanComponent } from './migas-pan.component';

describe('MigasPanComponent', () => {
  let component: MigasPanComponent;
  let fixture: ComponentFixture<MigasPanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MigasPanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MigasPanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
