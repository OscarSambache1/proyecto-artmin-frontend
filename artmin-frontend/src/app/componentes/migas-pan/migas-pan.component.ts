import {Component, Input, OnInit} from '@angular/core';
import {MenuItem} from 'primeng';

@Component({
  selector: 'app-migas-pan',
  templateUrl: './migas-pan.component.html',
  styleUrls: ['./migas-pan.component.css']
})
export class MigasPanComponent implements OnInit {

  items: any[];

  @Input()
  arregloRutas: any[];
  constructor() {
  }

  ngOnInit(): void {
    this.items = [
      {label: 'Categories'},
    ];
  }

}
