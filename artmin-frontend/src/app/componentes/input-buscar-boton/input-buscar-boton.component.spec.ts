import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputBuscarBotonComponent } from './input-buscar-boton.component';

describe('InputBuscarBotonComponent', () => {
  let component: InputBuscarBotonComponent;
  let fixture: ComponentFixture<InputBuscarBotonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputBuscarBotonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputBuscarBotonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
