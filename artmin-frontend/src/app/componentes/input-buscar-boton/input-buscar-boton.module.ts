import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputBuscarBotonComponent } from './input-buscar-boton.component';
import {ButtonModule, InputTextModule} from 'primeng';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [InputBuscarBotonComponent],
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    InputBuscarBotonComponent
  ]
})
export class InputBuscarBotonModule { }
