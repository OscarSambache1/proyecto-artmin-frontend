import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-input-buscar-boton',
  templateUrl: './input-buscar-boton.component.html',
  styleUrls: ['./input-buscar-boton.component.css']
})
export class InputBuscarBotonComponent implements OnInit {

  @Output()
  emitirBusqueda: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  placeholder: string;

  @Input()
  ayuda: string;

  @Input()
  busqueda = '';

  constructor() {
  }

  ngOnInit(): void {
  }

  enviarBusqueda() {
    this.emitirBusqueda.emit(this.busqueda.trim());
  }
}
