import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotonNuevoComponent } from './boton-nuevo.component';



@NgModule({
  declarations: [BotonNuevoComponent],
  imports: [
    CommonModule
  ],
  exports: [
    BotonNuevoComponent
  ]
})
export class BotonNuevoModule { }
