import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-boton-nuevo',
  templateUrl: './boton-nuevo.component.html',
  styleUrls: ['./boton-nuevo.component.css']
})
export class BotonNuevoComponent implements OnInit {

  @Output()
  emitirEvento: EventEmitter<null> = new EventEmitter<null>();

  constructor() {
  }

  ngOnInit(): void {
  }

  enviarEventoClic() {
    this.emitirEvento.emit();
  }

}
