import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotonNuevoComponent } from './boton-nuevo.component';

describe('BotonNuevoComponent', () => {
  let component: BotonNuevoComponent;
  let fixture: ComponentFixture<BotonNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotonNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotonNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
