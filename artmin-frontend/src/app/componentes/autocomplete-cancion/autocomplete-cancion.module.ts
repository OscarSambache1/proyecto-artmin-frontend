import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteCancionComponent } from './autocomplete-cancion.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteCancionComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteCancionComponent
  ]
})
export class AutocompleteCancionModule { }
