import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteCancionComponent } from './autocomplete-cancion.component';

describe('AutocompleteCancionComponent', () => {
  let component: AutocompleteCancionComponent;
  let fixture: ComponentFixture<AutocompleteCancionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteCancionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteCancionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
