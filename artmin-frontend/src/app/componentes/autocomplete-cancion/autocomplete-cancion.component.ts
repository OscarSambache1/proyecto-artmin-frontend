import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CancionInterface} from '../../interfaces/cancion.interface';
import {CancionRestService} from '../../servicios/rest/cancion-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {ArtistaInterface} from '../../interfaces/artista.interface';
import {obtenerUrlImagenPrincipal} from '../../funciones/obtener-imagen-principal';

@Component({
  selector: 'app-autocomplete-cancion',
  templateUrl: './autocomplete-cancion.component.html',
  styleUrls: ['./autocomplete-cancion.component.css']
})
export class AutocompleteCancionComponent implements OnInit {


  cancionSeleccionado: CancionInterface | CancionInterface[];
  arregloCanciones: CancionInterface[] = [];

  @Output()
  emitirCancionSeleccionado: EventEmitter<CancionInterface | CancionInterface[]> = new EventEmitter<CancionInterface | CancionInterface[]>();

  @Input()
  idCancion: number;

  @Input()
  idsArtistas: number[];

  @Input()
  arregloidsCancion: number[];

  @Input()
  arregloidsCancionesSeleccionadas: number[];

  @Input()
  multiple: boolean;

  @Input()
  deshabilitar: boolean;

  constructor(
    private readonly _cancionService: CancionRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idCancion || (this.arregloidsCancion && this.arregloidsCancion.length)) {
      let consulta;
      if (this.idCancion) {
        consulta = {
          where: {
            id: this.idCancion
          },
          relations: [
            'imagenesCancion',
            'artistasCancion',
            'artistasCancion.artista'
          ]
        };
      } else {
        if (this.arregloidsCancion && this.arregloidsCancion.length) {
          consulta = {
            camposIn: [
              {
                nombreCampo: 'id',
                valor: this.arregloidsCancion
              }
            ],
            relations: [
              'imagenesCancion',
              'artistasCancion',
              'artistasCancion.artista'
            ]
          };
        }

      }
      this._cancionService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((cancion: [CancionInterface[], number]) => {
          if (!this.multiple) {
            this.cancionSeleccionado = cancion[0][0];
          } else {
            this.cancionSeleccionado = cancion[0];
          }
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  enviarCancionSeleccionado(cancionSeleccionado: CancionInterface | CancionInterface[]) {
    if (!this.multiple) {
      this.emitirCancionSeleccionado.emit(cancionSeleccionado);
    } else {
      this.emitirCancionSeleccionado.emit(this.cancionSeleccionado);
    }
  }

  filtrarCanciones(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'imagenesCancion',
        'artistasCancion',
        'artistasCancion.artista'
      ]
    };
    this._cancionService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaCancion: [CancionInterface[], number]) => {
          if (this.idsArtistas && this.idsArtistas.length) {
            respuestaCancion[0] = respuestaCancion[0].filter(
              cancion => {
                return cancion.artistasCancion.find(artistaCancion => {
                  return this.idsArtistas.find(idArtista => (artistaCancion.artista as ArtistaInterface).id === idArtista);
                });
              }
            );
          }
          this.arregloCanciones = respuestaCancion[0];
          if (this.arregloidsCancionesSeleccionadas && this.arregloidsCancionesSeleccionadas.length) {
            this.arregloCanciones = this.arregloCanciones
              .filter( cancionConsulta => {
                return !this.arregloidsCancionesSeleccionadas.find( idCancionSeleccionada => idCancionSeleccionada === cancionConsulta.id);
              });
          }
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  obtenerUrlPrincipal(cancion: CancionInterface): string {
    return obtenerUrlImagenPrincipal(cancion, 'imagenesCancion');
  }

  limpiarBusqueda() {
    this.emitirCancionSeleccionado.emit(undefined);
  }

  deseleccionarCancion() {
    this.emitirCancionSeleccionado.emit(this.cancionSeleccionado);
  }

}
