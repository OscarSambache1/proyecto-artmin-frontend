import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutcompleteGenerosComponent } from './autcomplete-generos.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutcompleteGenerosComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutcompleteGenerosComponent
  ]
})
export class AutcompleteGenerosModule { }
