import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutcompleteGenerosComponent } from './autcomplete-generos.component';

describe('AutcompleteGenerosComponent', () => {
  let component: AutcompleteGenerosComponent;
  let fixture: ComponentFixture<AutcompleteGenerosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutcompleteGenerosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutcompleteGenerosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
