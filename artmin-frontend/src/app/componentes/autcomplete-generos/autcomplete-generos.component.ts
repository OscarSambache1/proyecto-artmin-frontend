import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GeneroInterface} from '../../interfaces/genero.interface';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {GeneroRestService} from '../../servicios/rest/genero-rest.service';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-autcomplete-generos',
  templateUrl: './autcomplete-generos.component.html',
  styleUrls: ['./autcomplete-generos.component.css']
})
export class AutcompleteGenerosComponent implements OnInit {
  generoSeleccionado: GeneroInterface = {};
  arregloGeneros: GeneroInterface[];

  @Output()
  emitirGenroSeleccionado: EventEmitter<GeneroInterface> = new EventEmitter<GeneroInterface>();

  @Input()
  idGenero: number;

  constructor(
    private readonly _generoRestService: GeneroRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idGenero) {
      this._generoRestService
        .findOne(this.idGenero)
        .subscribe((genero: GeneroInterface) => {
          this.generoSeleccionado = genero;
        }, error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
    }
  }

  filtrarGeneros(event: any) {
    const busqueda = event.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
    };
    this._generoRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaGenero: [GeneroInterface[], number]) => {
          this.arregloGeneros = respuestaGenero[0];
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarGeneroSeleccionado(eventoGenero: GeneroInterface) {
    this.emitirGenroSeleccionado.emit(eventoGenero);
  }

  limpiarBusqueda() {
    this.emitirGenroSeleccionado.emit(undefined);
  }
}
