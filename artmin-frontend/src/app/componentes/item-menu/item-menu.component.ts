import {Component, Input, OnInit} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-item-menu',
  templateUrl: './item-menu.component.html',
  styleUrls: ['./item-menu.component.css']
})
export class ItemMenuComponent implements OnInit {

  @Input()
  texto: string;

  @Input()
  descripcion: string;

  @Input()
  imagen = '';

  @Input()
  url: string[];

  @Input()
  extras: NavigationExtras;

  constructor(
    private readonly _router: Router
  ) {
  }

  ngOnInit(): void {
  }

  irARuta() {
    this._router.navigate(this.url, this.extras);
  }

}
