import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemRedondoComponent } from './menu-item-redondo.component';

describe('MenuItemRedondoComponent', () => {
  let component: MenuItemRedondoComponent;
  let fixture: ComponentFixture<MenuItemRedondoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuItemRedondoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuItemRedondoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
