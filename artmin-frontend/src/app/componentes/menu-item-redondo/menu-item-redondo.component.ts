import {Component, Input, OnInit} from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';

@Component({
  selector: 'app-menu-item-redondo',
  templateUrl: './menu-item-redondo.component.html',
  styleUrls: ['./menu-item-redondo.component.css']
})
export class MenuItemRedondoComponent implements OnInit {

  @Input()
  texto: string;

  @Input()
  descripcion: string;

  @Input()
  imagen = '';

  @Input()
  url: string[];

  @Input()
  extras: NavigationExtras;

  constructor(
    private readonly _router: Router
  ) {
  }

  ngOnInit(): void {
  }

  irARuta() {
    this._router.navigate(this.url, this.extras);
  }
}
