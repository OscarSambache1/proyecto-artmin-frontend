import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuItemRedondoComponent } from './menu-item-redondo.component';



@NgModule({
  declarations: [MenuItemRedondoComponent],
  imports: [
    CommonModule
  ],
  exports: [
    MenuItemRedondoComponent
  ]
})
export class MenuItemRedondoModule { }
