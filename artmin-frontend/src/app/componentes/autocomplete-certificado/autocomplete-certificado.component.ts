import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CertificadoInterface} from '../../interfaces/certificado.interface';
import {CertificadoRestService} from '../../servicios/rest/certificado-rest.service';
import {ToasterService} from 'angular2-toaster';
import {ToastErrorCargandoDatos} from '../../constantes/mensajes-toaster';
import {ChartInterface} from '../../interfaces/chart.interface';

@Component({
  selector: 'app-autocomplete-certificado',
  templateUrl: './autocomplete-certificado.component.html',
  styleUrls: ['./autocomplete-certificado.component.css']
})
export class AutocompleteCertificadoComponent implements OnInit {

  @Input()
  certificadoSeleccionado: CertificadoInterface;

  @Input()
  idsCertificadosAsignados: number[] = [];

  @Input()
  idChart: number;

  arregloCertificados: CertificadoInterface[];

  @Output()
  emitirCertificadoSeleccionado: EventEmitter<CertificadoInterface> = new EventEmitter<CertificadoInterface>();

  constructor(
    private readonly _certificadoRestService: CertificadoRestService,
    private readonly _toasterService: ToasterService
  ) {
  }

  ngOnInit(): void {
    if (this.idsCertificadosAsignados && this.idsCertificadosAsignados.length) {
      const consulta = {
        camposIn: [
          {
            nombreCampo: 'id',
            valor: this.idsCertificadosAsignados
          }
        ],
        relations: [
          'chart'
        ]
      };
      this._certificadoRestService
        .findAll(
          JSON.stringify(consulta)
        )
        .subscribe((respuestaCertificados: [CertificadoInterface[], number]) => {
            this.certificadoSeleccionado = respuestaCertificados[0][0];
          }
          ,
          error => {
            console.error(error);
            this._toasterService.pop(ToastErrorCargandoDatos);
          });
    }
  }

  filtrarCertificados(evento: any) {
    const busqueda = evento.query.trim();
    const consulta = {
      camposOr: [
        {
          nombreCampo: 'nombre',
          like: true,
          valor: busqueda
        }
      ],
      relations: [
        'chart'
      ]
    };
    this._certificadoRestService
      .findAll(
        JSON.stringify(consulta)
      )
      .subscribe((respuestaCertificado: [CertificadoInterface[], number]) => {
          this.arregloCertificados = respuestaCertificado[0]
            .filter(certificado => {
              return !(this.idsCertificadosAsignados.find(idCertificado => idCertificado === certificado.id));
            })
            .filter( certificadoFiltrado => (certificadoFiltrado.chart as ChartInterface).id === this.idChart);
        }
        ,
        error => {
          console.error(error);
          this._toasterService.pop(ToastErrorCargandoDatos);
        });
  }

  enviarCertificadoSeleccionado(certificadoSeleccionado: CertificadoInterface) {
    this.emitirCertificadoSeleccionado.emit(certificadoSeleccionado);

  }

  limpiarBusqueda() {
    this.emitirCertificadoSeleccionado.emit(undefined);
  }
}
