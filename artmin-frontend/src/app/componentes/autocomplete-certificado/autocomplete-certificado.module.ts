import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteCertificadoComponent } from './autocomplete-certificado.component';
import {AutoCompleteModule} from 'primeng';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [AutocompleteCertificadoComponent],
  imports: [
    CommonModule,
    AutoCompleteModule,
    FormsModule
  ],
  exports: [
    AutocompleteCertificadoComponent
  ]
})
export class AutocompleteCertificadoModule { }
