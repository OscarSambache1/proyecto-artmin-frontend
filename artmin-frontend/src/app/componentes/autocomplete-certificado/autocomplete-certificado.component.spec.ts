import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteCertificadoComponent } from './autocomplete-certificado.component';

describe('AutocompleteCertificadoComponent', () => {
  let component: AutocompleteCertificadoComponent;
  let fixture: ComponentFixture<AutocompleteCertificadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteCertificadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteCertificadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
