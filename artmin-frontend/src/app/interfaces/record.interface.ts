import {ChartInterface} from './chart.interface';
import {MedidaInterface} from './medida.interface';
import {RecordChartInterface} from './record-chart.interface';

export interface RecordInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  chart?: ChartInterface | number;
  medida?: MedidaInterface | number;
  recordsChartCancionAlbumVideo?: RecordChartInterface[];
}
