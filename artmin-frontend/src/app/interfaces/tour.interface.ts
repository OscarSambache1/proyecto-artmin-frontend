import {ArtistaTourInterface} from './artista-tour.interface';
import {ImagenInterface} from './imagen.interface';
import {TourLugarInterface} from './tour-lugar.interface';
import {FestivalInterface} from './festival.interface';

export interface TourInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  sinopsis?: string;
  observaciones?: string;
  fechaInicio?: string;
  fechaFin?: string;
  lugarInicio?: string;
  lugarFin?: string;
  cantidadShows?: number;
  cantidadTickets?: number;
  recaudacion?: number;
  anio?: number;
  tipo?: 'gira' |  'residencia' | 'festival';
  artistasTour?: ArtistaTourInterface[] | number[];
  artistas?: ArtistaTourInterface[] | number[];
  imagen?: any;
  imagenesTour?: ImagenInterface[];
  idImagenPrincipal?: number;
  lugaresTour?: TourLugarInterface[];
  festival?: FestivalInterface;
}
