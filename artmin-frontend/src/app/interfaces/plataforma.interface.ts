import {EnlaceAlbumCancionArtistaVideoInterface} from './enlace-album-cancion-artista-video.interface';
import {ImagenInterface} from './imagen.interface';

export interface PlataformaInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  tipo?: 'red social' | 'chart';
  enlacesPlataforma: EnlaceAlbumCancionArtistaVideoInterface[];
  imagenesPlataforma: ImagenInterface[];
}
