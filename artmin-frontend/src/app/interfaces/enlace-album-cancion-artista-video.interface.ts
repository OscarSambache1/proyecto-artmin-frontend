import {ArtistaInterface} from './artista.interface';
import {CancionInterface} from './cancion.interface';
import {AlbumInterface} from './album.interface';
import {PlataformaInterface} from './plataforma.interface';
import {VideoInterface} from './video.interface';

export interface EnlaceAlbumCancionArtistaVideoInterface {
  id?: number;
  url?: string;
  seguidores?: number;
  artista?: ArtistaInterface | number;
  cancion?: CancionInterface | number;
  album?: AlbumInterface | number;
  video?: VideoInterface | number;
  plataforma?: PlataformaInterface | number;
}
