import {PremioInterface} from './premio.interface';

export interface PremioAnioInterface {
  id?: number;
  anio: number;
  lugar: string;
  fecha: string;
  premio: PremioInterface | number;
  nominacionesPremioAnio: any[];
  presentacionesPremioAnio: any[];
}
