import {ArtistaInterface} from './artista.interface';
import {CancionInterface} from './cancion.interface';

export interface CancionArtistaInterface {
  id?: number;
  esArtistaPrincipal?: 0 | 1;
  artista?: ArtistaInterface | number;
  cancion?: CancionInterface | number;
}
