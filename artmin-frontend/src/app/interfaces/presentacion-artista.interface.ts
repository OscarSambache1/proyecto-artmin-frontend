import {ArtistaInterface} from './artista.interface';
import {PresentacionInterface} from './presentacion.interface';

export interface PresentacionArtistaInterface {
  id?: number;
  artista: ArtistaInterface | number;
  presentacion: PresentacionInterface | number;
}
