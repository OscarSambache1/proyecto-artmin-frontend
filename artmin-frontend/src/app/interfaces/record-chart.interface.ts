import {MedidaInterface} from './medida.interface';
import {RecordInterface} from './record.interface';
import {CancionInterface} from './cancion.interface';
import {AlbumInterface} from './album.interface';
import {VideoInterface} from './video.interface';
import {ChartInterface} from './chart.interface';

export interface RecordChartInterface {
  id?: number;
  cantidadMedida?: number;
  fechaRecord?: string;
  record?: RecordInterface | number;
  medida?: MedidaInterface | number;
  cancion?: CancionInterface| number;
  album?: AlbumInterface | number;
  video?: VideoInterface | number;
  chart?: ChartInterface | number;
}
