import {ImagenInterface} from './imagen.interface';
import {ChartInterface} from './chart.interface';

export interface LugarInterface {
  id?: number;
  nombre?: string;
  tipo?: 'ciudad' | 'pais' | 'continente' | 'global';
  lugarPadre?: LugarInterface | number;
  lugaresHijos?: LugarInterface[];
  imagenesLugar?: ImagenInterface[];
  chartsLugar?: ChartInterface[];
  imagen?: any;
}
