import {ChartCancionAlbumArtistaVideoInterface} from './chart-cancion-album-artista-video.interface';
import {ChartInterface} from './chart.interface';
import {CancionInterface} from './cancion.interface';
import {AlbumInterface} from './album.interface';
import {VideoInterface} from './video.interface';
import {CertificadoChartFechaInterface} from './certificado-chart-fecha.interface';

export interface CertificadoChartInterface {
  id?: number;
  fechaActualizacion?: string;
  chart?: ChartInterface | number;
  cancion?: CancionInterface | number;
  album?: AlbumInterface | number;
  video?: VideoInterface | number;
  certificadosChartFecha?: CertificadoChartFechaInterface[];
}
