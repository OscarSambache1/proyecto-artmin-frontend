import {PresentacionInterface} from './presentacion.interface';
import {CancionInterface} from './cancion.interface';

export interface PresentacionCancionInterface {
  id?: number;
  cancionNombre?: string;
  cancion?: CancionInterface | number;
  presentacion?: PresentacionInterface | number;
}
