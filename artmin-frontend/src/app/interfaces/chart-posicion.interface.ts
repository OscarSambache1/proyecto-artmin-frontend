import {ArtistaInterface} from './artista.interface';
import {CancionInterface} from './cancion.interface';
import {AlbumInterface} from './album.interface';
import {VideoInterface} from './video.interface';
import {ChartInterface} from './chart.interface';

export interface ChartPosicionInterface {
  id?: number;
  posicion: number;
  numeroSemana: number;
  fecha: string;
  competencia: string;
  observaciones: string;
  artista: ArtistaInterface;
  cancion: CancionInterface;
  album: AlbumInterface;
  video: VideoInterface;
  chart: ChartInterface;
}
