export interface MedidaInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
}
