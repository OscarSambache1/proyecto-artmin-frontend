import {TourInterface} from './tour.interface';

export interface SetlistInterface {
  id?: number;
  nombre?: number;
  fechaInicio?: string;
  fechaFin?: string;
  tour?: TourInterface | number;
  tieneActos?: boolean;
  cancionesSetlistTour?: any[];
}
