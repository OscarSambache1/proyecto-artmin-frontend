import {ImagenInterface} from './imagen.interface';
import {GeneroArtistaAlbumCancionInterface} from './genero-artista-album-cancion.interface';
import {CancionArtistaInterface} from './cancion-artista.interface';
import {GeneroInterface} from './genero.interface';
import {ArtistaInterface} from './artista.interface';
import {AlbumInterface} from './album.interface';
import {AlbumCancionInterface} from './album-cancion.interface';
import {TipoCancionInterface} from './tipo-cancion.interface';
import {EnlaceAlbumCancionArtistaVideoInterface} from './enlace-album-cancion-artista-video.interface';

export interface CancionInterface {
  id?: number;
  nombre?: string;
  duracionSegundos?: number | string;
  fechaLanzamiento?: string;
  descripcion?: string;
  anio?: number;
  tipo?: 'sencillo' | 'promocional' | 'inedito' | 'track';
  imagenesCancion?: ImagenInterface[];
  generosCancion?: GeneroArtistaAlbumCancionInterface[];
  artistasCancion?: CancionArtistaInterface[];
  generos?: GeneroInterface[];
  imagen?: any;
  artistas?: ArtistaInterface[];
  albums?: AlbumInterface[];
  posicion?: number;
  albumesCancion?: AlbumCancionInterface[];
  tipoCancion?: TipoCancionInterface;
  enlacesCancion?: EnlaceAlbumCancionArtistaVideoInterface[];
  enlace?: string;
}
