import {CancionSetlistInterface} from './cancion-setlist.interface';
import {TourInterface} from './tour.interface';

export interface ActoTourInterface {
  id?: number;
  nombre?: string;
  tour?: TourInterface | number;
  cancionesSetlistTour?: CancionSetlistInterface[];
}
