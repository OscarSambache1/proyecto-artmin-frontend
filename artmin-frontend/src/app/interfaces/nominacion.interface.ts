import {AlbumInterface} from './album.interface';
import {ArtistaInterface} from './artista.interface';
import {CancionInterface} from './cancion.interface';
import {VideoInterface} from './video.interface';
import {TourInterface} from './tour.interface';
import {CategoriaInterface} from './categoria.interface';
import {PremioAnioInterface} from './premio-anio.interface';
import {NominacionArtistaInterface} from './nominacion-artista.interface';

export interface NominacionInterface {
  id?: number;
  premioAnio?: PremioAnioInterface;
  categoria?: CategoriaInterface;
  albumes: AlbumInterface[];
  artistas: ArtistaInterface[];
  canciones: CancionInterface[];
  videos: VideoInterface[];
  tours: TourInterface[];
  album: AlbumInterface[];
  artista: ArtistaInterface[];
  cancion: CancionInterface[];
  video: VideoInterface[];
  tour: TourInterface[];
  nominacionesCategoria: NominacionArtistaInterface[];
  nominacionesArtistaAux: any[];
}
