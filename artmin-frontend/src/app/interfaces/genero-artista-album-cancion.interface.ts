import {ArtistaInterface} from './artista.interface';
import {GeneroInterface} from './genero.interface';

export interface GeneroArtistaAlbumCancionInterface {
  id?: number;
  artista?: ArtistaInterface | number;
  genero?: GeneroInterface | number;
}
