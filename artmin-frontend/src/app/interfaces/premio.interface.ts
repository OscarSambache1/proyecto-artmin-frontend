import {ImagenInterface} from './imagen.interface';
import {CategoriaInterface} from './categoria.interface';
import {PremioAnioInterface} from './premio-anio.interface';

export interface PremioInterface {
  id?: number;
  nombre: string;
  descripcion: string;
  esPremio?: 0| 1;
  imagenesPremio: ImagenInterface[];
  imagen?: any;
  categoriasPremio?: CategoriaInterface[];
  premiosAnioPremio?: PremioAnioInterface[];
}
