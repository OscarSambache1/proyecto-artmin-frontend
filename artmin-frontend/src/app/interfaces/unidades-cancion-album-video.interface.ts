import {CancionInterface} from './cancion.interface';
import {AlbumInterface} from './album.interface';
import {VideoInterface} from './video.interface';
import {ChartInterface} from './chart.interface';
import {MedidaInterface} from './medida.interface';

export interface UnidadesCancionAlbumVideoInterface {
  id?: number;
  unidadesDebut?: number;
  unidadesReales?: number;
  cancion?: CancionInterface | number;
  album?: AlbumInterface | number;
  video?: VideoInterface | number;
  chart?: ChartInterface | number;
  medida?: MedidaInterface | number;
}
