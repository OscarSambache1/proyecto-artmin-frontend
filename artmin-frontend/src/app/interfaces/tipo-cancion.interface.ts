export interface TipoCancionInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  cancion?: string;
}
