import {PlataformaInterface} from './plataforma.interface';
import {LugarInterface} from './lugar.interface';

export interface ChartInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  tipo?: 'cancion' | 'album' | 'video';
  plataforma?: PlataformaInterface | number;
  lugar?: LugarInterface | number;
}
