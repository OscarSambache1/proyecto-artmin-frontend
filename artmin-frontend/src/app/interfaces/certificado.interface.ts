import {PlataformaInterface} from './plataforma.interface';
import {MedidaInterface} from './medida.interface';
import {CertificadoChartFechaInterface} from './certificado-chart-fecha.interface';
import {ChartInterface} from './chart.interface';

export interface CertificadoInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  unidades?: number;
  chart?: ChartInterface | number;
  medida?: MedidaInterface | number;
  chartsFechaCertificado?: CertificadoChartFechaInterface[];
}
