import {ImagenInterface} from './imagen.interface';
import {GeneroInterface} from './genero.interface';
import {GeneroArtistaAlbumCancionInterface} from './genero-artista-album-cancion.interface';
import {CancionArtistaInterface} from './cancion-artista.interface';
import {EnlaceAlbumCancionArtistaVideoInterface} from './enlace-album-cancion-artista-video.interface';

export interface ArtistaInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  anioDebut?: string;
  fechaNacimiento?: string;
  imagenesArtista?: ImagenInterface[];
  generos?: GeneroInterface[];
  generosArtista?: GeneroArtistaAlbumCancionInterface[];
  cancionesArtista?: CancionArtistaInterface[];
  imagen?: any;
  enlacesArtista?: EnlaceAlbumCancionArtistaVideoInterface[];
}
