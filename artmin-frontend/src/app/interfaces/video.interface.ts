import {CancionInterface} from './cancion.interface';
import {TipoVideoInterface} from './tipo-video.interface';
import {EnlaceAlbumCancionArtistaVideoInterface} from './enlace-album-cancion-artista-video.interface';

export interface VideoInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  duracionSegundos?: number;
  cancion?: CancionInterface | number;
  tipoVideo?: TipoVideoInterface | number;
  fechaLanzamiento?: string;
  anio?: number;
  enlacesVideo?: EnlaceAlbumCancionArtistaVideoInterface[];
  enlace?: string;
}
