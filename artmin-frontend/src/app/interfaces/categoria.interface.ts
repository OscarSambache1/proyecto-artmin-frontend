import {PremioInterface} from './premio.interface';

export interface CategoriaInterface {
  id?: number;
  nombre: string;
  descripcion: string;
  tipo: string;
  premio: PremioInterface;
}
