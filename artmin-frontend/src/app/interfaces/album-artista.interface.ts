import {AlbumInterface} from './album.interface';
import {ArtistaInterface} from './artista.interface';

export interface AlbumArtistaInterface {
  id?: number;
  orden?: number;
  album?: AlbumInterface;
  artista?: ArtistaInterface;
}
