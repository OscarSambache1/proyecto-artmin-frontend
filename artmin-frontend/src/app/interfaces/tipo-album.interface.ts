export interface TipoAlbumInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  albumes?: string;
}
