import {CancionInterface} from './cancion.interface';
import {AlbumInterface} from './album.interface';

export interface AlbumCancionInterface {
  id?: number;
  posicion?: number;
  cancion?: CancionInterface | number;
  album?: AlbumInterface | number;
}
