import {TourInterface} from './tour.interface';
import {RecintoInterface} from './recinto-interface';
import {LugarInterface} from './lugar.interface';
import {FestivalInterface} from './festival.interface';

export interface TourLugarInterface {
  id?: number;
  locacion?: number;
  ticketsDisponibles?: number;
  porcentaje?: number;
  seCancelo?: 0 | 1;
  razonCancelacion?: string;
  recaudacion?: number;
  fecha?: string;
  hora?: string;
  observacion?: string;
  ticketsVendidos?: number;
  anio?: number;
  tour?: TourInterface | number;
  recinto?: RecintoInterface | number;
  lugar?: LugarInterface | number;
  pais?: any;
  esFestival?: 0 | 1;
  festival?: FestivalInterface;
}
