import {PremioAnioInterface} from './premio-anio.interface';
import {PresentacionArtistaInterface} from './presentacion-artista.interface';
import {PresentacionCancionInterface} from './presentacion-cancion.interface';
import {ArtistaInterface} from './artista.interface';
import {CancionInterface} from './cancion.interface';

export interface PresentacionInterface {
  id?: number;
  descripcion: string;
  premioAnio: PremioAnioInterface;
  eventoAnio: any | number;
  presentacionesArtista: PresentacionArtistaInterface[];
  presentacionesCancion: PresentacionCancionInterface[];
  artista: ArtistaInterface[];
  canciones: CancionInterface[];
}
