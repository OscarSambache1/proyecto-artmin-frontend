import {ArtistaInterface} from './artista.interface';

export interface ImagenInterface {
  'id'?: number;
  'url'?: string;
  'ancho'?: number;
  'largo'?: number;
  'descripcion'?: string;
  'esPrincipal'?: 0 | 1;
  'artista'?: ArtistaInterface | number;
}
