import {ArtistaInterface} from './artista.interface';
import {TourInterface} from './tour.interface';

export interface ArtistaTourInterface {
  id?: number;
  artista?: ArtistaInterface | number;
  tour?: TourInterface | number;
}
