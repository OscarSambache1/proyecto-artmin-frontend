import {CancionInterface} from './cancion.interface';
import {AlbumInterface} from './album.interface';
import {VideoInterface} from './video.interface';
import {ChartInterface} from './chart.interface';

export interface ChartCancionAlbumArtistaVideoInterface {
  id?: number;
  fechaDebut?: string;
  fechaPeak?: string;
  anio?: number;
  peak?: number;
  numeroSemanas?: number;
  numeroDias?: number;
  numeroSemanasPeak?: number;
  numeroDiasPeak?: number;
  numeroSemanasTop5?: number;
  numeroDiasTop5?: number;
  numeroSemanasTop10?: number;
  numeroDiasTop10?: number;
  posicionDebut?: number;
  descripcion?: string;
  cancion?: CancionInterface | number;
  album?: AlbumInterface | number;
  video?: VideoInterface | number;
  chart?: ChartInterface | number;
  unidadesDebut?: number;
  unidadesReales?: number;
  // recordsChartAlbumCAncionArtistaVideo?: RecordChartEntity[];
  // posicionesChartAlbumCAncionArtistaVideo?: ChartPosicionEntity[];
  // certificadosChartAlbumCAncionArtistaVideo?: CertificadoChartEntity[];
  // debutsAlbumCancionVideoChart?: DebutAlbumCancionVideoChartEntity[];
}
