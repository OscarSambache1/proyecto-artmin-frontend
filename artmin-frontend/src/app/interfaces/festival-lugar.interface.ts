import {LugarInterface} from './lugar.interface';
import {FestivalInterface} from './festival.interface';

export interface FestivalLugarInterface {
  id?: number;
  festival: FestivalInterface;
  lugar?: LugarInterface;
}
