import {GeneroArtistaAlbumCancionInterface} from './genero-artista-album-cancion.interface';
import {ImagenInterface} from './imagen.interface';
import {AlbumArtistaInterface} from './album-artista.interface';
import {GeneroInterface} from './genero.interface';
import {ArtistaInterface} from './artista.interface';
import {AlbumCancionInterface} from './album-cancion.interface';
import {TipoAlbumInterface} from './tipo-album.interface';
import {EnlaceAlbumCancionArtistaVideoInterface} from './enlace-album-cancion-artista-video.interface';

export interface AlbumInterface {
  id?: number;
  nombre?: string;
  duracionSegundos?: number;
  fechaLanzamiento?: string;
  descripcion?: string;
  anio?: number;
  calificacion?: number;
  tipo?: 'studio' | 'lp' | 'ep' | 'deluxe' | 'soundtrack' |'re-edition' | 'live';
  artistasAlbum?: AlbumArtistaInterface[];
  generosAlbum?: GeneroArtistaAlbumCancionInterface[];
  imagenesAlbum?: ImagenInterface[];
  generos?: GeneroInterface[];
  artistas?: ArtistaInterface[];
  imagen?: any;
  cancionesAlbum?: AlbumCancionInterface[];
  tipoAlbum?: TipoAlbumInterface;
  enlace?: string;
  enlacesAlbum?: EnlaceAlbumCancionArtistaVideoInterface[];
}
