import {GeneroArtistaAlbumCancionInterface} from './genero-artista-album-cancion.interface';

export interface GeneroInterface {
  id?: number;
  nombre?: string;
  descripcion?: string;
  generosArtistaAlbumCancion?: GeneroArtistaAlbumCancionInterface[];
}
