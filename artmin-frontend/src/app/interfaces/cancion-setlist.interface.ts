import {SetlistInterface} from './setlist.interface';
import {ActoTourInterface} from './acto-tour.interface';
import {CancionInterface} from './cancion.interface';

export interface CancionSetlistInterface {
  id?: number;
  nombre?: string;
  posicion?: number;
  cancion?: CancionInterface | number;
  setlistTour?: SetlistInterface | number;
  actosTour?: ActoTourInterface | number;
  presentacion?: any | number;
}
