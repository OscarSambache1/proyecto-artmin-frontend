import {FestivalLugarInterface} from './festival-lugar.interface';

export interface FestivalInterface {
  id?: number;
  nombre: string;
  descripcion: string;
  imagen?: any;
  festivalLugares: FestivalLugarInterface[];
}
