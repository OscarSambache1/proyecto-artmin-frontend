import {CertificadoChartInterface} from './certificado-chart.interface';
import {CertificadoInterface} from './certificado.interface';

export interface CertificadoChartFechaInterface {
  id?: number;
  cantidad?: number;
  fecha?: string;
  certificadoChart?: CertificadoChartInterface | number;
  certificado?: CertificadoInterface | number;
}
