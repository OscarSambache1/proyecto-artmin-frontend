import {AlbumInterface} from './album.interface';
import {ArtistaInterface} from './artista.interface';
import {CancionInterface} from './cancion.interface';
import {VideoInterface} from './video.interface';
import {TourInterface} from './tour.interface';
import {CategoriaInterface} from './categoria.interface';
import {PremioAnioInterface} from './premio-anio.interface';
import {NominacionInterface} from './nominacion.interface';

export interface NominacionArtistaInterface {
  id?: number;
  siGano: 0 | 1;
  artista: ArtistaInterface | number;
  cancion: CancionInterface | number;
  album: AlbumInterface | number;
  video: VideoInterface | number;
  tour: TourInterface | number;
  nominacion: NominacionInterface | number;
}
