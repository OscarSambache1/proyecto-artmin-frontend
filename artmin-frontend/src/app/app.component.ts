import {Component, OnInit} from '@angular/core';
import {CargandoService} from './servicios/cargando-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'artmin';
  bloqueado = false;
  constructor(
    private readonly _cargandoService: CargandoService
  ) {
  }

  ngOnInit(): void {
    this.escucharCambiosEnCargandoService();
  }

  escucharCambiosEnCargandoService() {
    this._cargandoService
      .cambioCargando
      .subscribe(
        (cambio) => {
          this.bloqueado = cambio;
        }
      );
  }
}
