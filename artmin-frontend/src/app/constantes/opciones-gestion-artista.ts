
export const OPCIONES_ARTISTA = [
  {
    texto: 'CANCIONES',
    descripcion: 'descripcion',
    url: ['cancion-modulo', 'gestion-canciones'],
    imagen: 'assets/imagenes/cancion.svg',
  },
  {
    texto: 'ALBUMS',
    descripcion: 'descripcion',
    url: ['album-modulo', 'gestion-albumes'],
    imagen: 'assets/imagenes/album.svg',
  },
  {
    texto: 'VIDEOS',
    descripcion: 'descripcion',
    url: ['/video-modulo'],
    imagen: 'assets/imagenes/video.svg',
  },
  {
    texto: 'CHARTS',
    descripcion: 'descripcion',
    url: ['/chart-modulo'],
    imagen: 'assets/imagenes/chart.svg',
  },
  {
    texto: 'TOURS',
    descripcion: 'descripcion',
    url: ['/tour-modulo'],
    imagen: 'assets/imagenes/tour.svg',
  },
  {
    texto: 'PRESENTACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/evento.svg',
  },
  {
    texto: 'PREMIOS Y NOMINACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/premio.svg',
  },
];
