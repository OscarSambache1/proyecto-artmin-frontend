
export const OPCIONES_MENU_INICIO = [
  {
    texto: 'ARTISTAS',
    descripcion: 'descripcion',
    url: ['/artista-modulo'],
    imagen: 'assets/imagenes/artista.svg',
  },
  {
    texto: 'CANCIONES',
    descripcion: 'descripcion',
    url: ['/cancion-modulo'],
    imagen: 'assets/imagenes/cancion.svg',
  },
  {
    texto: 'ALBUMS',
    descripcion: 'descripcion',
    url: ['/album-modulo'],
    imagen: 'assets/imagenes/album.svg',
  },
  {
    texto: 'TOURS Y FESTIVALES',
    descripcion: 'descripcion',
    url: ['/tour-modulo'],
    imagen: 'assets/imagenes/tour.svg',
  },
  {
    texto: 'CHARTS',
    descripcion: 'descripcion',
    url: ['/chart-modulo/CH'],
    imagen: 'assets/imagenes/chart.svg',
  },
  {
    texto: 'VIDEOS',
    descripcion: 'descripcion',
    url: ['/video-modulo'],
    imagen: 'assets/imagenes/video.svg',
  },
  {
    texto: 'PLATAFORMA',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/plataforma.svg',
  },
  // {
  //   texto: 'EVENTOS',
  //   descripcion: 'descripcion',
  //   url: ['/empresa-modulo'],
  //   imagen: 'assets/imagenes/evento.svg',
  // },
  {
    texto: 'LUGARES',
    descripcion: 'descripcion',
    url: ['/lugar-modulo'],
    imagen: 'assets/imagenes/lugar.svg',
  },
  {
    texto: 'PREMIOS Y EVENTOS',
    descripcion: 'descripcion',
    url: ['/premio-modulo'],
    imagen: 'assets/imagenes/premio.svg',
  },
  {
    texto: 'FESTIVALES',
    descripcion: 'descripcion',
    url: ['/festival-modulo'],
    imagen: 'assets/imagenes/festival.png',
  },
  {
    texto: 'CONFIGURACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/configuracion.svg',
  },
];
