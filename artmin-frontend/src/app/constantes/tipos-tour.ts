export const TIPOS_TOUR = [
  {
    key: 'Gira',
    value: 'gira'
  },
  {
    key: 'Residencia',
    value: 'residencia'
  },
  {
    key: 'Festival',
    value: 'festival'
  },
];

export enum TIPOS_TOUR_ENUM {
  gira = 'Gira',
  residencia = 'Residencia',
  festival = 'Festival',
}
