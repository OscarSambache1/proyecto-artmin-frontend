import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_ARTISTA = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(50),
];

export const MENSAJES_VALIDACION_NOMBRE_ARTISTA = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 50 caracteres'
};

export const VALIDACION_DESCRIPCION_ARTISTA = [
  Validators.required,
];

export const MENSAJES_VALIDACION_DESCRIPCION_ARTISTA = {
  required: 'El campo descripción es obligatorio',
};

export const VALIDACION_FECHA_NACIMIENTO_ARTISTA = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_NACIMIENTO_ARTISTA = {
  required: 'El campo fecha de nacimiento es obligatorio',
};

export const VALIDACION_ANIO_DEBUT_ARTISTA = [
  Validators.required,
];

export const MENSAJES_VALIDACION_ANIO_DEBUT_ARTISTA = {
  required: 'El campo debut es obligatorio',
};

export const VALIDACION_GENEROS_ARTISTA = [
  Validators.required,
];

export const MENSAJES_VALIDACION_GENEROS_ARTISTA = {
  required: 'El campo generos es obligatorio',
};

export const VALIDACION_IMAGEN_ARTISTA = [
  Validators.required,
];
export const MENSAJES_VALIDACION_OATH_LOGO_ARTISTA = {
  required: 'El logo  es obligatorio',
};
