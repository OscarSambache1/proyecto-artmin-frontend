import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_CHART = [
  Validators.required,
];

export const MENSAJES_VALIDACION_NOMBRE_CHART = {
  required: 'El campo nombre es obligatorio',
};

export const VALIDACION_DESCRIPCION_CHART = [
];

export const MENSAJES_VALIDACION_DESCRIPCION_CHART = {
  required: 'El campo descripción es obligatorio',
};

export const VALIDACION_LUGAR_CHART = [
  Validators.required,
];

export const MENSAJES_VALIDACION_LUGAR_CHART = {
  required: 'El campo lugar es obligatorio',
};

export const VALIDACION_TIPO_CHART = [
  Validators.required,
];

export const MENSAJES_VALIDACION_TIPO_CHART = {
  required: 'El campo tipo es obligatorio',
};

export const VALIDACION_PLATAFORMA_CHART = [
  Validators.required,
];

export const MENSAJES_VALIDACION_PLATAFORMA_CHART = {
  required: 'El campo plataforma es obligatorio',
};
