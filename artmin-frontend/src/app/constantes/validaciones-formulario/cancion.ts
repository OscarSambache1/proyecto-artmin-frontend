import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_CANCION = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(50),
];

export const MENSAJES_VALIDACION_NOMBRE_CANCION = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 50 caracteres'
};

export const VALIDACION_DESCRIPCION_CANCION = [
];

export const MENSAJES_VALIDACION_DESCRIPCION_CANCION = {
  required: 'El campo descripción es obligatorio',
};

export const VALIDACION_FECHA_LANZAMIENTO_CANCION = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_LANZAMIENTO_CANCION = {
  required: 'El campo fecha de lanzamiento es obligatorio',
};

export const VALIDACION_DURACION_SEGUNDOS_CANCION = [
  Validators.required,
];

export const MENSAJES_VALIDACION_DURACION_SEGUNDOS_CANCION = {
  required: 'El campo duración es obligatorio',
};

export const VALIDACION_GENEROS_CANCION = [
  Validators.required,
];

export const MENSAJES_VALIDACION_GENEROS_CANCION = {
  required: 'El campo generos es obligatorio',
};

export const VALIDACION_ARTISTAS_CANCION = [
  Validators.required,
];

export const VALIDACION_ALBUMS_CANCION = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_ARTISTAS_CANCION = {
  required: 'El campo artistas es obligatorio',
};

export const VALIDACION_IMAGEN_CANCION = [
  Validators.required,
];
export const MENSAJES_VALIDACION_PATH_IMAGEN_CANCION = {
  required: 'La portada es obligatoria',
};

export const VALIDACION_TIPO_CANCION = [
  Validators.required,
];
export const MENSAJES_VALIDACION_TIPO_CANCION = {
  required: 'El campo tipo es obligatorio',
};

export const MENSAJES_VALIDACION_ALBUMS_CANCION = {
  required: 'El campo albums es obligatorio',
};

export const VALIDACION_ENLACE_CANCION = [
];

export const MENSAJES_VALIDACION_ENLACE_CANCION = {
  required: 'El campo enlace es obligatorio',
};
