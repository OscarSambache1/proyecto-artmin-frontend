import {Validators} from '@angular/forms';

export const VALIDACION_FESTIVAL_FT = [
  Validators.required,
];

export const MENSAJES_VALIDACION_NOMBRE_FESTIVAL = {
  required: 'El campo festival es obligatorio',
};

export const VALIDACION_LUGAR_FT = [
  Validators.required,
];

export const MENSAJES_VALIDACION_LUGAR_FT = {
  required: 'El campo lugar es obligatorio',
};
