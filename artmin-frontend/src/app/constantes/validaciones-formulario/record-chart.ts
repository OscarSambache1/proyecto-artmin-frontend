export const MENSAJES_VALIDACION_RECORDS_CHART_FECHA = {
  required: 'Se debe agregar al menos un certificado',
};

export const MENSAJES_VALIDACION_RECORDS_CHART_CANTIDAD = {
  required: 'El campo cantidad es obligatorio',
};
export const MENSAJES_VALIDACION_RECORDS_CHART_RECORD = {
  required: 'El campo record es obligatorio',
};
export const MENSAJES_VALIDACION_RECORDS_CHART_MEDIDA = {
  required: 'El campo medida es obligatorio',
};
export const MENSAJES_VALIDACION_CANCION_RECORDS_CHART = {
  required: 'El campo canción es obligatorio',
};

export const MENSAJES_VALIDACION_ALBUM_RECORDS_CHART = {
  required: 'El campo album es obligatorio',
};

export const MENSAJES_VALIDACION_VIDEO_RECORDS_CHART = {
  required: 'El campo video es obligatorio',
};
