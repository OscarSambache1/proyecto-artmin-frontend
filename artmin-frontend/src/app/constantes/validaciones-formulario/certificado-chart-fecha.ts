import {Validators} from '@angular/forms';

export const VALIDACION_FECHA_CERTIFICADO_CHART_FECHA = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_CERTIFICADO_CHART_FECHA = {
  required: 'El campo fecha es obligatorio',
};

export const VALIDACION_CANTIDAD_CERTIFICADO_CHART_FECHA = [
  Validators.required,
];

export const MENSAJES_VALIDACION_CANTIDAD_CERTIFICADO_CHART_FECHA = {
  required: 'El campo cantidad es obligatorio',
};

export const VALIDACION_CERTIFICADO_CHART_FECHA = [
  Validators.required,
];

export const MENSAJES_VALIDACION_CERTIFICADO_CHART_FECHA = {
  required: 'El campo certificado es obligatorio',
};
