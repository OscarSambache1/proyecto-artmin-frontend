import {Validators} from '@angular/forms';

export const VALIDACION_ARTISTAS_PRES = [
  Validators.required,
];

export const MENSAJES_VALIDACION_ARTISTAS_PRES = {
  required: 'El campo artistas es obligatorio',
};

export const MENSAJES_VALIDACION_PREMIO_PRES = {
  required: 'El campo premio es obligatorio',
};

export const MENSAJES_VALIDACION_PMEMIO_ANIO_PRES = {
  required: 'El campo año es obligatorio',
};
