import {Validators} from '@angular/forms';

export const MENSAJES_VALIDACION_PEAK_CHART_CANCION_ALBUM_VIDEO = {
  required: 'El campo peak es obligatorio',
};

export const MENSAJES_VALIDACION_DEBUT_CHART_CANCION_ALBUM_VIDEO = {
  required: 'El campo debut es obligatorio',
};

export const MENSAJES_VALIDACION_FECHA_DEBUT_CHART_CANCION_ALBUM_VIDEO = {
  required: 'El campo fecha debut es obligatorio',
};

export const MENSAJES_VALIDACION_CANCION_CHART_CANCION_ALBUM_VIDEO = {
  required: 'El campo canción es obligatorio',
};

export const MENSAJES_VALIDACION_VIDEO_CHART_CANCION_ALBUM_VIDEO = {
  required: 'El campo video es obligatorio',
};

export const MENSAJES_VALIDACION_ALBUM_CHART_CANCION_ALBUM_VIDEO = {
  required: 'El campo album es obligatorio',
};
