import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_VIDEO = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(50),
];

export const MENSAJES_VALIDACION_NOMBRE_VIDEO = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 50 caracteres'
};

export const VALIDACION_DESCRIPCION_VIDEO = [
];

export const MENSAJES_VALIDACION_DESCRIPCION_VIDEO = {
  required: 'El campo descripción es obligatorio',
};

export const VALIDACION_FECHA_LANZAMIENTO_VIDEO = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_LANZAMIENTO_VIDEO = {
  required: 'El campo fecha de lanzamiento es obligatorio',
};

export const VALIDACION_DURACION_SEGUNDOS_VIDEO = [
  Validators.required,
];

export const MENSAJES_VALIDACION_DURACION_SEGUNDOS_VIDEO = {
  required: 'El campo duración es obligatorio',
};

export const VALIDACION_CANCION_VIDEO = [
  Validators.required,
];

export const MENSAJES_VALIDACION_CANCION_VIDEO = {
  required: 'El campo canción es obligatorio',
};


export const VALIDACION_TIPO_VIDEO = [
  Validators.required,
];
export const MENSAJES_VALIDACION_TIPO_VIDEO = {
  required: 'El campo tipo es obligatorio',
};


export const VALIDACION_ENLACE_VIDEO = [
  Validators.required,
];

export const MENSAJES_VALIDACION_ENLACE_VIDEO = {
  required: 'El campo enlace es obligatorio',
};
