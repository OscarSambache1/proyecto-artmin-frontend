import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_NOMBRE_LUGAR = {
  required: 'El campo nombre es obligatorio',
};

export const VALIDACION_TIPO_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_TIPO_LUGAR = {
  required: 'El campo TIPO es obligatorio',
};

export const VALIDACION_IMAGEN_LUGAR = [
  Validators.required,
];
export const MENSAJES_VALIDACION_IMAGEN_LUGAR = {
  required: 'La imagen es obligatoria',
};
