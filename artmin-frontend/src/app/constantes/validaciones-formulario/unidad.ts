import {Validators} from '@angular/forms';

export const VALIDACION_UNIDADES_DEBUT_UNIDAD = [
  Validators.required,
];

export const MENSAJES_VALIDACION_UNIDADES_DEBUT_UNIDAD = {
  required: 'El campo unidades debut es obligatorio',
};

export const VALIDACION_UNIDADES_REAL_UNIDAD = [
  Validators.required,
];

export const MENSAJES_VALIDACION_UNIDADES_REAL_UNIDAD = {
  required: 'El campo unidades real es obligatorio',
};

export const VALIDACION_MEDIDA_UNIDAD = [
  Validators.required,
];

export const MENSAJES_VALIDACION_MEDIDA_UNIDAD = {
  required: 'El campo medida es obligatorio',
};

export const MENSAJES_VALIDACION_CANCION_UNIDAD = {
  required: 'El campo canción es obligatorio',
};

export const MENSAJES_VALIDACION_ALBUM_UNIDAD = {
  required: 'El campo album es obligatorio',
};

export const MENSAJES_VALIDACION_VIDEO_UNIDAD = {
  required: 'El campo video es obligatorio',
};
