export const MENSAJES_VALIDACION_CERTIFICADOS_CHART_FECHA = {
  required: 'Se debe agregar al menos un certificado',
};

export const MENSAJES_VALIDACION_CANCION_CERTIFICADOS_CHART = {
  required: 'El campo canción es obligatorio',
};

export const MENSAJES_VALIDACION_ALBUM_CERTIFICADOS_CHART = {
  required: 'El campo album es obligatorio',
};

export const MENSAJES_VALIDACION_VIDEO_CERTIFICADOS_CHART = {
  required: 'El campo video es obligatorio',
};
