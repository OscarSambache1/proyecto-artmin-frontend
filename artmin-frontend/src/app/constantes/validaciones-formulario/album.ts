import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_ALBUM = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(50),
];

export const MENSAJES_VALIDACION_NOMBRE_ALBUM = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 50 caracteres'
};

export const VALIDACION_DESCRIPCION_ALBUM = [
  Validators.required,
];

export const MENSAJES_VALIDACION_DESCRIPCION_ALBUM = {
  required: 'El campo descripción es obligatorio',
};

export const VALIDACION_FECHA_LANZAMIENTO_ALBUM = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_LANZAMIENTO_ALBUM = {
  required: 'El campo fecha de lanzamiento es obligatorio',
};

export const VALIDACION_CALIFICACION_ALBUM = [
  Validators.required,
];

export const MENSAJES_VALIDACION_CALIFICACION_ALBUM = {
  required: 'El campo calificación es obligatorio',
};

export const VALIDACION_GENEROS_ALBUM = [
  Validators.required,
];

export const MENSAJES_VALIDACION_GENEROS_ALBUM = {
  required: 'El campo generos es obligatorio',
};

export const VALIDACION_ARTISTAS_ALBUM = [
  Validators.required,
];

export const MENSAJES_VALIDACION_ARTISTAS_ALBUM = {
  required: 'El campo artistas es obligatorio',
};

export const VALIDACION_IMAGEN_ALBUM = [
  Validators.required,
];
export const MENSAJES_VALIDACION_OATH_LOGO_ALBUM = {
  required: 'El logo  es obligatorio',
};

export const VALIDACION_TIPO_ALBUM = [
  Validators.required,
];
export const MENSAJES_VALIDACION_TIPO_ALBUM = {
  required: 'El campo tipo es obligatorio',
};
