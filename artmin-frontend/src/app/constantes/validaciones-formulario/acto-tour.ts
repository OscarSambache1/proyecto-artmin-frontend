import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_ACTO = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(100),
];

export const MENSAJES_VALIDACION_NOMBRE_ACTO = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 100 caracteres'
};
