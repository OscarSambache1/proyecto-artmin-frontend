import {Validators} from '@angular/forms';

export const VALIDACION_FECHA_PREMIO_ANIO = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_PREMIO_ANIO = {
  required: 'El campo fecha es obligatorio',
};

export const VALIDACION_LUGAR_PREMIO_ANIO = [
  Validators.required,
];

export const MENSAJES_VALIDACION_LUGAR_PREMIO_ANIO = {
  required: 'El campo lugar es obligatorio',
};
