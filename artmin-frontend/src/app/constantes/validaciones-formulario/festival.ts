import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_FESTIVAL = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(100),
];

export const MENSAJES_VALIDACION_NOMBRE_FESTIVAL = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 100 caracteres'
};

export const VALIDACION_DESCRIPCION_FESTIVAL  = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_DESCRIPCION_FESTIVAL  = {
  required: 'El campo descripción es obligatorio',
};

export const VALIDACION_IMAGEN_FESTIVAL = [
  Validators.required,
];
export const MENSAJES_VALIDACION_IMAGEN_FESTIVAL = {
  required: 'La imagen  es obligatorio',
};
