import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_SETLIST = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(255),
];

export const MENSAJES_VALIDACION_NOMBRE_SETLIST = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 255 caracteres'
};

export const VALIDACION_FECHA_INICIO_SETLIST = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_INICIO_SETLIST = {
  required: 'El campo fecha inicio es obligatorio',
};

export const VALIDACION_FECHA_FIN_SETLIST = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_FIN_SETLIST = {
  required: 'El campo fecha fin es obligatorio',
};

export const VALIDACION_TIENE_ACTOS_SETLIST = [
  Validators.required,
];

export const MENSAJES_VALIDACION_TIENE_ACTOS_SETLIST = {
  required: 'El campo tiene actos es obligatorio',
};
