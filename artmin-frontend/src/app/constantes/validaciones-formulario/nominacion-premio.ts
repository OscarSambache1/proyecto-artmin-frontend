import {Validators} from '@angular/forms';

export const VALIDACION_CATEGORIA_NOM = [
  Validators.required,
];

export const MENSAJES_VALIDACION_CATEGORIA_NOM = {
  required: 'El campo categoria es obligatorio',
};

export const VALIDACION_ARTISTAS_NOM = [
  Validators.required,
];

export const MENSAJES_VALIDACION_ARTISTAS_NOM = {
  required: 'El campo artistas es obligatorio',
};

export const VALIDACION_CANCIONES_NOM = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_CANCIONES_NOM = {
  required: 'El campo canciones es obligatorio',
};

export const VALIDACION_ALBUMS_NOM = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_ALBUMS_NOM = {
  required: 'El campo albums es obligatorio',
};

export const VALIDACION_VIDEOS_NOM = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_VIDEOS_NOM = {
  required: 'El campo videos es obligatorio',
};

export const VALIDACION_TOURS_NOM = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_TOURS_NOM = {
  required: 'El campo tours es obligatorio',
};

export const MENSAJES_VALIDACION_PREMIO_NOM = {
  required: 'El campo premio es obligatorio',
};

export const MENSAJES_VALIDACION_PMEMIO_ANIO_NOM = {
  required: 'El campo año es obligatorio',
};
