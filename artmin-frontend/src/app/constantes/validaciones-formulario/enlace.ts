import {Validators} from '@angular/forms';

export const VALIDACION_URL_ENLACE = [
  Validators.required,
];

export const MENSAJES_VALIDACION_URL_ENLACE = {
  required: 'El campo url es obligatorio',
};

export const VALIDACION_SEGUIDORES_ENLACE = [
];

export const MENSAJES_VALIDACION_SEGUIDORES_ENLACE = {
  required: 'El campo seguidores es obligatorio',
};

export const VALIDACION_PLATAFORMA_ENLACE = [
  Validators.required,
];

export const MENSAJES_VALIDACION_PLATAFORMA_ENLACE = {
  required: 'El campo plataforma es obligatorio',
};
