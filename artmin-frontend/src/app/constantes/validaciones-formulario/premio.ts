import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_PREMIO = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(100),
];

export const MENSAJES_VALIDACION_NOMBRE_PREMIO = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 100 caracteres'
};

export const VALIDACION_DESCRIPCION_PREMIO  = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_DESCRIPCION_PREMIO  = {
  required: 'El campo descripción es obligatorio',
};

export const VALIDACION_IMAGEN_PREMIO = [
  Validators.required,
];
export const MENSAJES_VALIDACION_IMAGEN_PREMIO = {
  required: 'La imagen  es obligatorio',
};
