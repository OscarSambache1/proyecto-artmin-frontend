import {Validators} from '@angular/forms';

export const MENSAJES_VALIDACION_POSICION_CHART_POSICION = {
  required: 'El campo posición es obligatorio',
};


export const MENSAJES_VALIDACION_FECHA_CHART_POSICION = {
  required: 'El campo fecha es obligatorio',
};

export const MENSAJES_VALIDACION_CANCION_CHART_POSICION = {
  required: 'El campo canción es obligatorio',
};

export const MENSAJES_VALIDACION_VIDEO_CHART_POSICION = {
  required: 'El campo video es obligatorio',
};

export const MENSAJES_VALIDACION_ALBUM_CHART_POSICION = {
  required: 'El campo album es obligatorio',
};
