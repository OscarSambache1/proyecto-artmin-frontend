import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_TOUR = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(100),
];

export const MENSAJES_VALIDACION_NOMBRE_TOUR = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 50 caracteres'
};

export const VALIDACION_DESCRIPCION_TOUR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_DESCRIPCION_TOUR = {
  required: 'El campo sinopsis es obligatorio',
};

export const VALIDACION_SINOPSIS_TOUR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_SINOPSIS_TOUR = {
  required: 'El campo sinopsis es obligatorio',
};


export const VALIDACION_OBSERVACIONES_TOUR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_OBSERVACIONES_TOUR = {
  required: 'El campo observaciones es obligatorio',
};


export const VALIDACION_FECHA_INICIO_TOUR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_INICIO_TOUR = {
  required: 'El campo fecha de inicio es obligatorio',
};

export const VALIDACION_FECHA_FIN_TOUR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_FIN_TOUR = {
  // required: 'El campo fecha de fin es obligatorio',
};

export const VALIDACION_LUGAR_INICIO_TOUR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_LUGAR_INICIO_TOUR = {
  required: 'El campo lugar de inicio es obligatorio',
};

export const VALIDACION_LUGAR_FIN_TOUR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_LUGAR_FIN_TOUR = {
  // required: 'El campo lugar de fin es obligatorio',
};



export const VALIDACION_CANTIDAD_SHOWS_TOUR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_CANTIDAD_SHOWS_TOUR = {
};


export const VALIDACION_CANTIDAD_TICKETS_TOUR = [
];

export const MENSAJES_VALIDACION_CANTIDAD_TICKETS_TOUR = {
};

export const VALIDACION_RECAAUDACION_TOUR = [
];

export const MENSAJES_VALIDACION_RECAUDACION_TOUR = {
};

export const VALIDACION_ARTISTAS_TOUR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_ARTISTAS_TOUR = {
  required: 'El campo artistas es obligatorio',
};

export const VALIDACION_IMAGEN_TOUR = [
  Validators.required,
];
export const MENSAJES_VALIDACION_IMAGEN_TOUR = {
  required: 'La imagen  es obligatorio',
};

export const VALIDACION_TIPO_TOUR = [
  Validators.required,
];
export const MENSAJES_VALIDACION_TIPO_TOUR = {
  required: 'El campo tipo es obligatorio',
};
