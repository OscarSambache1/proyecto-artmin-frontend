import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_CATEGORIA = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(100),
];

export const MENSAJES_VALIDACION_NOMBRE_CATEGORIA = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 100 caracteres'
};

export const VALIDACION_TIPO_CATEGORIA  = [
  Validators.required,
];

export const MENSAJES_VALIDACION_TIPO_CATEGORIA  = {
  required: 'El campo tipo es obligatorio',
};

export const VALIDACION_DESCRIPCION_CATEGORIA  = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_DESCRIPCION_CATEGORIA  = {
  required: 'El campo descripción es obligatorio',
};
