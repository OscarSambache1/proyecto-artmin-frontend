import {Validators} from '@angular/forms';

export const VALIDACION_NOMBRE_CANCION_ACTO = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(100),
];

export const MENSAJES_VALIDACION_CANCION_ACTO = {
  required: 'El campo nombre es obligatorio',
  minlength: 'El campo nombre debe tener mínimo 1 caracter',
  maxlength: 'El campo nombre no debe tener más de 100 caracteres'
};

export const VALIDACION_ARTISTAS_CANCION_ACTO = [];

export const MENSAJES_VALIDACION_ARTISTAS_CANCION_ACTO = {
  required: 'El campo artistas es obligatorio',
};

export const VALIDACION_CANCION_CANCION_ACTO = [];

export const MENSAJES_VALIDACION_CANCION_CANCION_ACTO = {
  required: 'El campo canción es obligatorio',
};
