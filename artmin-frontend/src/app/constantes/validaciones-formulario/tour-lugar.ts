import {Validators} from '@angular/forms';

export const VALIDACION_LOCACION_TOUR_LUGAR = [
  Validators.required,
  Validators.minLength(1),
  Validators.maxLength(100),
];

export const MENSAJES_VALIDACION_LOCACION_TOUR_LUGAR = {
  required: 'El campo locacion es obligatorio',
  minlength: 'El campo locacion debe tener mínimo 1 caracter',
  maxlength: 'El campo locacion no debe tener más de 50 caracteres'
};

export const VALIDACION_TICKETS_DISPONIBLES_TOUR_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_TICKETS_DISPONIBLES_TOUR_LUGAR = {
  required: 'El campo tickets disponibles es obligatorio',
};

export const VALIDACION_SE_CANCELO_TOUR_LUGAR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_SE_CANCELO_TOUR_LUGAR = {
  required: 'El campo se cancelo es obligatorio',
};


export const VALIDACION_RAZON_CANCELACION_TOUR_LUGAR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_RAZON_CANCELACION_TOUR_LUGAR = {
  required: 'El campo razón cancelación es obligatorio',
};


export const VALIDACION_RECAUDACION_TOUR_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_RECAUDACION_TOUR_LUGAR = {
  required: 'El campo recaudación es obligatorio',
};

export const VALIDACION_FECHA_TOUR_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_FECHA_TOUR_LUGAR = {
  required: 'El campo fecha es obligatorio',
};

export const VALIDACION_HORA_TOUR_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_HORA_TOUR_LUGAR = {
  required: 'El campo hora es obligatorio',
};

export const VALIDACION_OBSERVACION_TOUR_LUGAR = [
  // Validators.required,
];

export const MENSAJES_VALIDACION_OBSERVACION_TOUR_LUGAR = {
  required: 'El campo observación es obligatorio',
};

export const VALIDACION_TICKETS_VENDIDOS_TOUR_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_TICKETS_VENDIDOS_TOUR_LUGAR = {
  required: 'El campo tickets vendidos es obligatorio',
};

export const VALIDACION_RECINTO_TOUR_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_RECINTO_TOUR_LUGAR = {
  required: 'El campo recinto es obligatorio',
};

export const VALIDACION_LUGAR_TOUR_LUGAR = [
  Validators.required,
];

export const MENSAJES_VALIDACION_LUGAR_TOUR_LUGAR = {
  required: 'El campo lugar es obligatorio',
};

export const MENSAJES_VALIDACION_PAIS_TOUR_LUGAR = {
  required: 'El campo pais es obligatorio',
};

export const VALIDACION_PAIS_TOUR_LUGAR = [
  Validators.required,
];

