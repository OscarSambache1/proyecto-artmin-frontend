export const TIPOS_ALBUMES = [
  {label: 'studio', value: 'studio'},
  {label: 'lp', value: 'lp'},
  {label: 'ep', value: 'ep'},
  {label: 'deluxe', value: 'deluxe'},
  {label: 'soundtrack', value: 'soundtrack'},
  {label: 're-edition', value: 're-edition'},
  {label: 'live', value: 'live'},
];
