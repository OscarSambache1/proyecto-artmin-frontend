export const OPCIONES_CANCION = [
  {
    texto: 'CHARTS',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/chart.svg',
  },
  {
    texto: 'PRESENTACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/evento.svg',
  },
  {
    texto: 'PREMIOS Y NOMINACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/premio.svg',
  },
];

export const OPCIONES_ALBUM = [
  {
    texto: 'CHARTS',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/chart.svg',
  },
  {
    texto: 'PRESENTACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/evento.svg',
  },
  {
    texto: 'PREMIOS Y NOMINACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/premio.svg',
  },
];

export const OPCIONES_TOUR = [
  {
    texto: 'PRESENTACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/evento.svg',
  },
  {
    texto: 'PREMIOS Y NOMINACIONES',
    descripcion: 'descripcion',
    url: ['/empresa-modulo'],
    imagen: 'assets/imagenes/premio.svg',
  },
];
