import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RutaInicioComponent} from './rutas/ruta-inicio/ruta-inicio.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: RutaInicioComponent,
  },
  {
    path: 'artista-modulo',
    loadChildren: 'src/app/modulos/artista/artista.module#ArtistaModule',
  },
  {
    path: 'album-modulo',
    loadChildren: 'src/app/modulos/album/album.module#AlbumModule',
  },
  {
    path: 'cancion-modulo',
    loadChildren: 'src/app/modulos/cancion/cancion.module#CancionModule',
  },
  {
    path: 'video-modulo',
    loadChildren: 'src/app/modulos/video/video.module#VideoModule',
  },
  {
    path: 'chart-modulo/:tipo',
    loadChildren: 'src/app/modulos/chart/chart.module#ChartModule',
  },
  {
    path: 'lugar-modulo',
    loadChildren: 'src/app/modulos/lugar/lugar.module#LugarModule',
  },
  {
    path: 'tour-modulo',
    loadChildren: 'src/app/modulos/tour/tour.module#TourModule',
  },
  {
    path: 'premio-modulo',
    loadChildren: 'src/app/modulos/premio/premio.module#PremioModule',
  },
  {
    path: 'festival-modulo',
    loadChildren: 'src/app/modulos/festival/festival.module#FestivalModule',
  },
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
