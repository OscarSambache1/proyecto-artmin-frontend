import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RutaInicioComponent} from './rutas/ruta-inicio/ruta-inicio.component';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {BlockUIModule, CardModule} from 'primeng';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {CargandoService} from './servicios/cargando-service';
import {HttpClientModule} from '@angular/common/http';
import {MomentModule} from 'angular2-moment';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ItemMenuModule} from './componentes/item-menu/item-menu.module';
import {ArtistaModule} from './modulos/artista/artista.module';
import {BarraSuperiorModule} from './componentes/barra-superior/barra-superior.module';

@NgModule({
  declarations: [
    AppComponent,
    RutaInicioComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    BlockUIModule,
    ToasterModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MomentModule,
    BlockUIModule,
    CommonModule,
    FormsModule,
    CardModule,
    ItemMenuModule,
    ArtistaModule,
    BarraSuperiorModule
  ],
  providers: [
    CargandoService,
    ToasterService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
