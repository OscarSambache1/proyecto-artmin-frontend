import { Component, OnInit } from '@angular/core';
import {CargandoService} from '../../servicios/cargando-service';
import {OPCIONES_MENU_INICIO} from '../../constantes/items-menu-inicio';

@Component({
  selector: 'app-ruta-inicio',
  templateUrl: './ruta-inicio.component.html',
  styleUrls: ['./ruta-inicio.component.css']
})
export class RutaInicioComponent implements OnInit {

  arregloItemsMenuInicio = OPCIONES_MENU_INICIO;
  constructor(
    private readonly _cargandoService: CargandoService
  ) { }

  ngOnInit() {
  }
}
