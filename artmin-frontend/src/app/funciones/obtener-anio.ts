import * as moment from 'moment';

export function obtenerAnio(fecha: string): number {
  const fechaNueva = moment(fecha, 'YYYY-MM-DD');
  return  +(fechaNueva.format('YYYY'));
}
