import {TreeNode} from 'primeng/api';

export  function obtenerIdsEmpresasPadre(nodoActual: TreeNode, idsEmpresasPadre): number[] {
  idsEmpresasPadre.push(nodoActual.data.empresaActual.id);
  if (nodoActual.parent) {
    return obtenerIdsEmpresasPadre(nodoActual.parent, idsEmpresasPadre);
  } else {
    return [...idsEmpresasPadre];
  }
}
