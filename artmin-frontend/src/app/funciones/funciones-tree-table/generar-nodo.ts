import {TreeNode} from 'primeng/api';

export function generarNodo(
  registro: any,
  leaf = false
): TreeNode {
  // const tieneHijos = registro[nombreCampoHijos];
  // if (nombreCampoHijos) {
  //   leaf = !(tieneHijos && tieneHijos.length > 0);
  // } else {
  //   leaf = false;
  // }
  return {
    data: registro,
    leaf,
    children: [],
  };
}
