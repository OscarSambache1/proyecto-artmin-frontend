import { cambiarEstadoTreeTable } from './cambiar-estado-tree-table';
import { buscarNodo } from './buscar-nodo';
import {TreeNode} from 'primeng';

export function escucharBusquedaArbol(
  treeTable: TreeNode[],
  busqueda: string,
  camposABuscar: string[],
  objetoABuscar?: string,
) {
  let arbol = cambiarEstadoTreeTable(treeTable, 'expanded', false);
  arbol = cambiarEstadoTreeTable(arbol, 'partialSelected', false);
  buscarNodo(arbol, busqueda, camposABuscar, objetoABuscar);
}
