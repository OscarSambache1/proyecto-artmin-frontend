import { generarNodo } from './generar-nodo';
import {TreeNode} from 'primeng/api';

export function generarArrayNodos(
  values: any[],
  leaf?: boolean,
): TreeNode[] {
  return values.map(valor => {
    return generarNodo(valor, leaf);
  });
}
