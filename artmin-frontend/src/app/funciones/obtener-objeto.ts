export function obtenerObjeto(
  arreglo: any[],
  propiedadObjeto: string,
  propiedad: string
): string {
  return arreglo.reduce((cadena, objetoCancion, indice) => {
    return cadena + objetoCancion[propiedadObjeto][propiedad] +
      ((indice + 1) === arreglo.length ? '' : ', ');
  }, '');
}
