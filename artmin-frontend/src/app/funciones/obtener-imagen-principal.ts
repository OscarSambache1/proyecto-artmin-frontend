import {environment} from '../../environments/environment';

export  function  obtenerUrlImagenPrincipal(registro: any, campoImagenes: string): string {
  const imagenPrincipal = registro[campoImagenes].filter(
    imagen => imagen.esPrincipal
  );
  return environment.url + ':' + environment.port + environment.pathImagenes + '/' + imagenPrincipal[0].url;
}
