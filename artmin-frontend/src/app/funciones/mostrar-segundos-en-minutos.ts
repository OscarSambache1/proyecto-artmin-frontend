import * as moment from 'moment';

export function mostrarSegundosEnMinutos(duracionSegundos): string {
  const duration = moment.duration(duracionSegundos, 'seconds') as any;
  const duracionFormato = duration.format('hh:mm:ss');
  if (duracionFormato.length === 2) {
    return  '00:' + duracionFormato;
  } else {
    return  duracionFormato;
  }
}
