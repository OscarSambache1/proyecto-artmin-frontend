import {separarPorCaracter} from './separar-por-caracter';
import {generarMigaDePan} from './generar-migas-pan';

export function generarRespuestaRuta(arreglo, migasDePan, rutaArreglo, argumentos) {
  if (arreglo === void 0) { arreglo = false; }
  if (migasDePan === void 0) { migasDePan = false; }
  const rutaSegmentos = rutaArreglo.map(function (r) { return r.generarRuta.apply(r, argumentos); });
  rutaSegmentos[0] = '/' + rutaSegmentos[0];
  const ruta = [];
  rutaSegmentos.slice().forEach(function (segmento) {
    separarPorCaracter(ruta, segmento, '/');
  });
  ruta[0] = '/' + ruta[0];
  if (migasDePan) {
    return generarMigaDePan(rutaArreglo, ruta);
  } else {
    return arreglo ? ruta : rutaArreglo[rutaArreglo.length - 1].ruta;
  }
}
