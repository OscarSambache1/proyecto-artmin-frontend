import * as moment from 'moment';

export function convertirMinutosMascaraASegundos(duracionMascaraMinutos: string) {
  return moment.duration('00:' + duracionMascaraMinutos).asSeconds() as any;
}
