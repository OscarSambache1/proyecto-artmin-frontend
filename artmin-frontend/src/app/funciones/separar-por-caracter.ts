export function separarPorCaracter(arreglo, palabra, caracter) {
  const indice = palabra.indexOf(caracter);
  const existeOtraOcurrenciaDelCaracter = indice !== -1;
  if (existeOtraOcurrenciaDelCaracter) {
    const elCaracterEstaEnLaPosicionInicial = indice === 0;
    if (elCaracterEstaEnLaPosicionInicial) {
      const palabraSinElCaracterEnLaPosicionInicial = palabra.substring(1);
      return separarPorCaracter(arreglo, palabraSinElCaracterEnLaPosicionInicial, caracter);
    } else {
      const palabraInicial = palabra.substring(0, indice);
      arreglo.push(palabraInicial);
      const restoDeLaPalabra = palabra.substring(indice + 1);
      return separarPorCaracter(arreglo, restoDeLaPalabra, caracter);
    }
  } else {
    arreglo.push(palabra);
    return arreglo;
  }
}
